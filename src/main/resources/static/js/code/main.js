var pageObj = {
    addPage: function () {
        location.href = url + '/add';
    },
    delete: function () {
        let checked = $(".chk:checked");
        if (checked.length > 0) {
            let clCode = checked.prop('id');
            $ajax.post({
                url: url + "/delete",
                data: {clCode: clCode},
                success: function (res) {
                    alert("미사용 처리되었습니다.");
                    location.reload();
                }
            })
        } else {
            alert("미사용 처리할 항목을 선택해 주세요.");
            return false;
        }
    },
    list: function () {
        location.href = url;
    },
    checkbox: function (target) {
        if ($(target).is(':checked')) {
            $.each($(".chk"), function () {
                $(this).prop('checked', false);
            })
            $(target).prop('checked', true);
        }
    },
    getData: function () {
        let list = [];
        $.each($("ul.list-body").find('li'), function () {
            let _this = this;
            let obj = {};

            $.each($(_this).find('input:text, input[type=number], input:hidden, select'), function () {
                obj[this.id] = this.value == "" ? null : this.value;
            })
            list.push(obj);
        })

        return list;
    },
    add: function () {
        let clCode = '';
        let clNm = '';
        $.each($('ul.list-body').find('li:first-child'), function () {
            clCode = $(this).find('#clCode').val();
            clNm = $(this).find('#clNm').val();
        })

        $('ul.list-body').append(`
            <li>
                <div class="data-group">
                    <div class="col">
                        <label class="chk-item"><input type="checkbox" class="chk" onclick="pageObj.checkbox(this)"></label>
                    </div>
                    <div class="col">
                        <span class="mo">분류</span>
                        <input type="text" id="clCode" name="clCode" value="${clCode}" title="분류" readonly>
                    </div>
                    <div class="col">
                        <span class="mo">분류명</span>
                        <input type="text" id="clNm" name="clNm" value="${clNm}" title="분류명" readonly>
                    </div>
                    <div class="col">
                        <span class="mo">코드</span>
                        <input type="hidden" id="orgCode" name="orgCode">
                        <input type="text" id="code" name="code" title="코드" essential="true">
                    </div>
                    <div class="col">
                        <span class="mo">코드이름</span>
                        <input type="text" id="codeNm" name="codeNm" title="코드이름" essential="true">
                    </div>
                    <div class="col">
                        <span class="mo">코드설명</span>
                        <input type="text" id="codeDc" name="codeDc" title="코드설명">
                    </div>
                    <div class="col">
                        <span class="mo">부모코드</span>
                        <input type="text" id="parntsCode" name="parntsCode" title="부모코드" essential="true" readonly>
                    </div>
                    <div class="col">
                        <span class="mo">코드깊이</span>
                        <input type="number" id="codeDp" name="codeDp" title="코드깊이" essential="true">
                    </div>
                    <div class="col">
                        <span class="mo">코드순번</span>
                        <input type="number" id="codeSn" name="codeSn" title="코드순번" essential="true">
                    </div>
                    <div class="col">
                        <span class="mo">닉네임</span>
                        <input type="text" id="ncnm" name="ncnm" title="닉네임">
                    </div>
                    <div class="col">
                        <span class="mo">사용여부</span>
                        <select id="delAt" name="delAt">
                            <option value="N">사용</option>
                            <option value="Y">미사용</option>
                        </select>
                    </div>
                </div>
            </li>
        `);
    },
    copy: function () {
        let checked = $(".chk:checked");
        if (checked.length > 0) {
            let targetTr = checked.parent().parent().parent();
            let clone = targetTr.clone();
            clone.find('input:checkbox').prop('checked', false);
            clone.find('#code').prop('readonly', false);
            $(targetTr).after(clone);
        } else {
            alert("복사사할 항목을 선택해 주요.");
            return false;
        }
    },
    validation: function () {
        let result = true;
        $.each($("input[essential='true']"), function () {
            if (this.id == 'parntsCode') {
                let dp = $(this).parent().parent().find('input[id=codeDp]').val();
                if (dp > 1) {
                    if (isNull(this.value)) {
                        alert(this.title + "을(를) 입력해 주세요.");
                        $(this).focus();
                        return result = false;
                    }
                }
            } else {
                if (isNull(this.value)) {
                    alert(this.title + "을(를) 입력해 주세요.");
                    $(this).focus();
                    return result = false;
                }
            }
        })

        let arr = [];
        $.each($("input[id=code]"), function () {
            arr.push(this.value);
        })
        let set = new Set(arr);
        if (arr.length != set.size) {
            alert("동일한 코드가 존재합니다.");
            return false;
        }

        return result;
    }
}

var url;
pageObj.pageStart = function () {
    if ($url.getPath().includes("sys")) {
        url = '/adm/sys/code';
    } else if ($url.getPath().includes("edu")) {
        url = '/adm/edu/code';
    }
}