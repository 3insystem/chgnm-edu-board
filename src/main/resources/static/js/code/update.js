pageObj.setChild = function () {
    let checked = $(".chk:checked");
    if (checked.length > 0) {
        let targetTr = checked.parent().parent().parent();
        let clone = targetTr.clone();
        let code = clone.find('#code').val();
        let codeDp = clone.find('#codeDp').val();
        let parntsCode = clone.find('#code').val();
        clone.find('input:checkbox').prop('checked', false);
        if (isNull(code)) {
            alert("코드를 입력해 주세요.");
            targetTr.find('#code').focus();
            return false;
        }

        $.each($(clone).find('input:text, input[type=number], input:hidden'), function () {
            if ($(this).prop('readonly') == false) {
                if (this.type == 'text') {
                    this.value = "";
                } else {
                    if (this.id == 'codeDp') {
                        this.value = Number(codeDp) + 1;
                    } else {
                        this.value = 0;
                    }
                }
            }

            if (this.id == 'parntsCode') {
                this.value = code;
            }

            if (this.id == 'code') {
                $(this).prop('readonly', false);
                $(this).val("");
            }

            if (this.id == 'orgCode') {
                $(this).val("");
            }
        })

        let list = [];
        let target;
        $.each($("input[id=parntsCode]"), function (index) {
            if (this.value == parntsCode) {
                list.push($(this))
            }
        })
        if (list.length == 0) {
            target = targetTr;
        } else {
            target = list[list.length-1].parent().parent();
        }

        $(target).after(clone);
    } else {
        alert("자식을 추가할 항목을 선택해 주세요.");
        return false;
    }
}

pageObj.update = function () {
    console.log(this.getData())
    if (pageObj.validation()) {
        $ajax.post({
            data: this.getData(),
            success: function (res) {
                alert("저장되었습니다.");
                location.reload();
            }
        })
    }
}