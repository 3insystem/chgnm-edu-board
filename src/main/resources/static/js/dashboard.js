pageObj = {
    view: {
        maintenanceList: () => {
            let url = $url.getPath();
            let split = url.split('/');
            url = getCombineStrings(split);
            location.href = url + 'mntmgt';
        },
        maintenanceDetail: (rceptNo) => {
            let url = $url.getPath();
            url = url.endsWith("dashboard") ? url.substring(0, url.indexOf('dashboard')) + 'mntmgt/' : url;
            url = url + "detail/";
            location.href = url + rceptNo;
        },
        expendablesList: () => {
            let url = $url.getPath();
            let split = url.split('/');
            url = getCombineStrings(split);
            location.href = url + 'cmpds';
        },
        expendablesDetail: (rceptNo) => {
            let url = $url.getPath();
            url = url.endsWith("dashboard") ? url.substring(0, url.indexOf('dashboard')) + 'cmpds/' : url;
            url = url + "detail/";
            location.href = url + rceptNo;
        }
    }
}

const getCombineStrings = (split) => {
    let url = '';
    for(let i = 0; i < split.length - 1; i++) {
        url = url + split[i] + '/';
    }
    return url;
}
