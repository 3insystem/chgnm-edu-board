var pageObj = {
    view: {
        join: function () {
            location.href = "/join/step1";
        },
        findIdModal: function () {
            modal.open({
                title: "아이디 찾기",
                path: '/find/id'
            });
        },
        findPwdModal: function () {
            modal.open({
                title: "비밀번호 찾기",
                path: '/find/pwd'
            });
        },
        close: function () {
            mailCheck = "N";
            certifyNum = "";
            modal.close();
        }
    },
    getCertifyNum: function (type) {
        if (type == 'pwd' && isNull($("#userId").val())) {
            alert("아이디를 입력해 주세요.");
            $("#userId").focus();
            return false;
        }

        if (isNull($("#userNm").val())) {
            alert("이름을 입력해 주세요.");
            $("#userNm").focus();
            return false;
        }

/*
        if (isNull($("#email").val())) {
            alert("이메일을 입력해 주세요.");
            $("#email").focus();
            return false;
        }
*/

        let obj = {
            userNm: $("#userNm").val(),
            mbtlnum: $("#mbtlnum").val()
            // email: $("#email").val()
        };

        if (type == 'pwd') {
            obj["userId"] = $("#userId").val();
        }

        $ajax.post({
            url: '/find/mbtlnumCheck',
            data: obj,
            success: function (res) {
                if (res == 'Y') {
                    $api.certifyMsg($("#userNm").val(), $("#mbtlnum").val());
                } else {
                    if (confirm("일치하는 아이디가 없습니다.\n회원가입 하시겠습니까?")) {
                        location.href = '/join/step1';
                    } else {
                        return false;
                    }
                }
            }
        })
    },
    certify: function (type) {
        let num = $("#certify").val();
        if (isNull(num)) {
            alert("인증번호를 입력해 주세요.");
            return false;
        }

        if (num == certifyNum) {
            let obj = {
                userNm: $("#userNm").val(),
                mbtlnum: $("#mbtlnum").val()
            };

            if (type == 'pwd') {
                obj["userId"] = $("#userId").val();
            }

            $ajax.post({
                url: '/find/findId',
                data: obj,
                success: function (res) {
                    if (type == 'id') {
                        $("#checkId").text(res);
                        $("#checkIdHidden").val(res);
                    } else {
                        $.each($("#form").find('input, button'), function () {
                            let tagName = this.tagName.toLowerCase();
                            if (tagName == 'input' || tagName == 'INPUT') {
                                if (this.type == 'text') {
                                    $(this).prop('readonly', true);
                                }
                            } else {
                                if (this.className.includes('reset-check')) {
                                    $(this).attr('disabled', true);
                                }
                            }
                        })
                        $(".pwd-reset").css('display', 'block');
                    }
                }
            })
        } else {
            alert("인증번호가 일치하지 않습니다.");
            $("#certify").focus();
            return false;
        }
    },
    findId: function () {
        let id = $("#checkIdHidden").val();
        $("input[name=username]").val(id);

        mailCheck = "N";
        certifyNum = "";
        modal.close();
    },
    savePwd: function () {
        if (isNull($("#password").val())) {
            alert("비밀번호를 입력해 주세요.");
            $("#password").focus();
            return false;
        }

        if (isNull($("#rePassword").val())) {
            alert("비밀번호를 입력해 주세요.");
            $("#rePassword").focus();
            return false;
        }

        var regExp = /^(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#$]).{8,}$/
        if (!regExp.test($("#password").val())) {
            alert("비밀번호를 확인해 주세요.");
            $("#password").focus();
            return false;
        }

        if (!regExp.test($("#rePassword").val())) {
            alert("비밀번호를 확인해 주세요.");
            $("#rePassword").focus();
            return false;
        }

        if ($("#password").val() != $("#rePassword").val()) {
            alert("비밀번호가 일치하지 않습니다.");
            $("#password").focus();
            return false;
        }

        let obj = {
            userId: $("#userId").val(),
            password: $("#password").val()
        };

        $ajax.post({
            url: '/find/reset',
            data: obj,
            success: function (res) {
                if (res == 'Y') {
                    alert("비밀번호 변경이 완료되었습니다.");
                    let id = $("#userId").val();
                    $("input[name=username]").val(id);
                    pageObj.view.close();
                } else {
                    alert("비밀번호 변경하는 중 오류가 발생했습니다.");
                    return false;
                }
            }
        })
    }
}

var mailCheck = "N";
var certifyNum = "";
var modal = $modal();
pageObj.pageStart = function () {
    modal.init($("#modal"), {
        width: 1000,
        height: 3010
    });
};