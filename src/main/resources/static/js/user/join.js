var pageObj = {
    view : {
        join: function () {
            location.href = "/join/step1";
        },
        agree: function (type) {
            location.href = `/join/step2?type=${type}`;
        },
        input: function (type) {
            if($("input[name=chkAgree]:checked").length == $("input[name=chkAgree]").length){
                location.href = `/join/step3?type=${type}`;
            } else {
                alert("모든 약관에 동의하여 주십시오");
            }
        },
        success: function (type) {
            location.href = `/join/step4?type=${type}`;
        }
    },
    idCheck: function () {
        const id = $("#userId").val();
        const regExp = /^[a-zA-Z0-9]{4,19}$/g;

        if (!regExp.test(id)){
            alert("아이디는 영문 또는 영문 + 숫자를 포함하여 4 ~ 19자로 생성해 주세요.");
            $("#id").focus();
            idCheckFlag = 'N';
            return false;
        }

        if (isNull(id)) {
            alert("아이디를 입력해주세요.");
            $("#userId").focus();
            idCheckFlag = 'N';
            return false;
        } else {
            $ajax.post({
                url: "/join/idCheck",
                data: {userId: $("#userId").val()},
                success: function (res) {
                    if (res == 'N') {
                        alert("사용 가능한 아이디입니다.");
                        idCheckFlag = 'Y';
                    } else if (res == 'B') {
                        alert("아이디를 입력해 주세요.");
                        $("#id").focus();
                        idCheckFlag = 'N';
                    } else if (res == 'Y') {
                        alert("이미 사용 중인 아이디입니다.");
                        $("#id").focus();
                        idCheckFlag = 'N';
                    } else {
                        alert("잘못된 접근입니다.");
                        $("#id").focus();
                        idCheckFlag = 'N';
                    }
                }
            })
        }
    },
    bizrnoCheck: function () {

        const bizrno = $("#bizrno").val().replace(/-/gi, '');
        var r = true;
        var numberMap = bizrno.replace(/-/gi, '').split('').map(function (d){
            return parseInt(d, 10);
        });

        if(numberMap.length == 10) {
            var keyArr = [1, 3, 7, 1, 3, 7, 1, 3, 5];
            var chk = 0;

            keyArr.forEach(function (d, i) {
                chk += d * numberMap[i];
            });

            chk += parseInt((keyArr[8] * numberMap[8]) / 10, 10);

            r = Math.floor(numberMap[9]) === ((10 - (chk % 10)) % 10);

            if (!r) {
                alert(`사업자 번호가 유효한 패턴이 아닙니다.`);
                bizrnoCheckFlag = "N";
                return false;
            } else {
                $ajax.post({
                    url: "/join/bizrnoCheck",
                    data: {bizrno: bizrno},
                    success: function (res) {
                        let URLSearch = new URLSearchParams(location.search);
                        let type = URLSearch.get("type");
                        if(type == 'cmpny'){
                            if(res.result == 'B'){
                                alert('사용 가능한 사업자 번호입니다.');
                                bizrnoCheckFlag = 'Y';
                            } else {
                                alert('사업자 번호가 중복되어 있습니다');
                                bizrnoCheckFlag = 'N';
                            }
                        } else if (type == 'employee'){
                            if(res.result == 'B'){
                                alert('등록되지않은 업체입니다.');
                                bizrnoCheckFlag = 'N';
                            } else if (res.result == 'S'){
                                alert("조회되었습니다.");
                                $('#cmpnyNm').val(res.dto.cmpnyNm);
                                bizrnoCheckFlag = 'Y';
                            } else {
                                alert('사업자 번호가 중복되어 있습니다');
                                bizrnoCheckFlag = 'N';
                            }
                        } else {
                            alert(`잘못된 접근입니다.`);
                            bizrnoCheckFlag = 'N';
                        }
                        $("#bizrno").focus();
                    }
                })
            }
        } else {
            alert(`사업자 번호의 자릿수를 확인해주십시오.`);
            bizrnoCheckFlag = "N";
            return false;
        }
    },
    pwdCheck: function () {
        if ($("#password").val() == $("#rePassword").val()) {
            return true;
        }

        return false;
    },
    save: function (type) {
        if(!$valid.dataValidation()){
            return false;
        }

        if (idCheckFlag != 'Y') {
            alert("아이디를 확인해 주세요.");
            return false;
        }

        if (!this.pwdCheck()) {
            alert("비밀번호를 확인해 주세요.");
            return false;
        }

        if (bizrnoCheckFlag != 'Y') {
            alert("사업자 번호를 확인해 주세요.");
            return false;
        }
        if (idCheckFlag == 'Y' && this.pwdCheck) {
            if (type == 'cmpny') {
                saveCmpny();
            } else {
                saveEmply();
            }
        }

        function saveCmpny() {
            // 파일 용량 체크
            if ($files.getFiles().length > 0 && $files.getFiles()[0].maxSize > $files.maxSize) {
                alert("파일은 10MB까지 업로드할 수 있습니다.");
                return false;
            }

            $ajax.postMultiPart({
                url: $url.getPath('/saveFile'),
                data: $form.getData(),
                success: function (res) {
                    success(res, type);
                }
            })
        }

        function saveEmply() {
            $ajax.post({
                url: $url.getPath('/save'),
                data: $form.getData(),
                success: function (res) {
                    success(res, type);
                }
            })
        }

        function success(res, type) {
            if (!isNull(res)) {
                alert("회원가입이 완료되었습니다.");
                pageObj.view.success(type);
            } else {
                alert("회원가입 중 오류가 발생했습니다.");
                return false;
            }
        }
    },
}

var idCheckFlag = 'N';
var bizrnoCheckFlag = 'N';
$(function () {
    //전부 동의, 해제
    $("#chk1").on("click", function (){
        if($(this).prop("checked")==true){
            $("input[name=chkAgree]").prop("checked", true);
            $("#submit").focus();
        } else {
            $("input[name=chkAgree]").prop("checked", false);
        }
    })

    //
    $("input[name=chkAgree]").on("click", function (){
        if($(this).prop("checked")==true){
            if($("input[name=chkAgree]:checked").length == $("input[name=chkAgree]").length){
                $('#chk1').prop("checked", true);
            }
        } else {
            $('#chk1').prop("checked", false);
        }
    })
})

pageObj.pageStart = function () {
    eventInit();
}

var eventInit = function () {
    $("#userId").keypress(function () {
        idCheckFlag = 'N';
    })

    $("#bizrno").keypress(function () {
        bizrnoCheckFlag = 'N';
    })
}