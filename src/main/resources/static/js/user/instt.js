
$(function() {
    $("#date-package").hide(); //파견 기간 숨김
    $("#dsptcAgre").hide();

    $("#dsptcAt").change( function() {
        if ($("select[name=dsptcAt]").val() == 'Y') { //파견근무
            $("#date-package").show(); //파견 기간 표시
            $("#dsptcAgre").show();

        } else {
            $("#date-package").hide(); //파견 기간 숨김
            $("#dsptcAgre").hide();
        }
    });

});

var pageObj = {
    save: function () {
        if (dataValidation()) {
            let obj = '';
            if ($("#chkDsptcAt").val() == 'N') { //파견근무중 체크추가
                obj = {
                    password: $("#password").val(),
                    email: $("#email").val(),
                    dsptcAt: $("#dsptcAt").val(),
                    orgCd: $("#viewCd").val(),
                    dsptcBgnde: $("#startDate").val(),
                    dsptcEndde: $("#endDate").val(),
                    chkDsptcAt: $("#chkDsptcAt").val(),
                    agreAt: "Y"
                }
            } else {
                obj = {
                    password: $("#password").val(),
                    email: $("#email").val(),
                    chkDsptcAt: $("#chkDsptcAt").val()
                }
            }
            console.log(obj);

            $ajax.post({
                url: $url.getPath(),
                data: obj,
                success: function (id) {
                    if (isNull(id)) {
                        alert("저장하는데 오류가 발생했습니다.");
                        return false;
                    } else {
                        alert("저장이 완료되었습니다");
                        location.href = '/';
                    }
                }
            })
        }

        function dataValidation() {
            var pwdRegExp = /^(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#$]).{8,}$/
            var emailRegExp = /(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/

            /*
             if (isNull($("#password").val())) {
                 alert("비밀번호를 입력해 주세요.");
                 $("#password").focus();
                 return false;
             }
             if (isNull($("#rePassword").val())) {
                 alert("비밀번호를 입력해 주세요.");
                 $("#rePassword").focus();
                 return false;
             }
             */

            if (isNull($("#password").val())) {

                if ($("#password").val() != $("#rePassword").val()) {
                    alert("비밀번호를 확인해 주세요.");
                    $("#password").focus();
                    return false;
                }

            } else {
                if (isNull($("#rePassword").val())) {
                    alert("비밀번호를 입력해 주세요.");
                    $("#rePassword").focus();
                    return false;
                }
            }

            if (!isNull($("#password").val()) && !pwdRegExp.test($("#password").val())) {
                alert("비밀번호 규칙이 맞지 않습니다.");
                $("#password").focus();
                return false;
            }
            if (!isNull($("#rePassword").val()) && !pwdRegExp.test($("#rePassword").val())) {
                alert("비밀번호 규칙이 맞지 않습니다.");
                $("#rePassword").focus();
                return false;
            }

            /*
            if (isNull($("#email").val())) {
                alert("이메일을 입력해 주세요.");
                $("#email").focus();
                return false;
            }
            */

            if (!isNull($("#email").val()) &&  !emailRegExp.test($("#email").val())) {
                alert("이메일 규칙이 맞지 않습니다.");
                $("#email").focus();
                return false;
            }


            if ($("#chkDsptcAt").val() == 'N') { //파견근무중 체크추가

                if ($("#dsptcAt").val() == 'Y') {
                    if (isNull($("#viewCd").val())) {
                        alert("파견지 상세를 선택해 주세요.");
                        return false;
                    }

                    if (isNull($("#startDate").val())) {
                        alert("파견기간 시작일을 선택해 주세요.");
                        return false;
                    }

                    if (isNull($("#endDate").val())) {
                        alert("파견기간 종료일을 선택해 주세요.");
                        return false;
                    }

                    if ($("#chk4").is(':checked') == false) {
                        alert("파견 근무지 권한 변경에 동의해주시기 바랍니다.");
                        return false;
                    }
                }
            }

            return true;
        }
    }
}

pageObj.pageStart = function () {
    eventInit();
}

var eventInit = function () {
    onchangeDsptcAt();
}

var onchangeDsptcAt = function () {
    $("#dsptcAt").on('change', function () {
        if ($(this).val() == 'Y') {
            let template = `
                <div class="input-list" id="detailDsptc">
                    <div class="label essential"><span>파견지 상세</span></div>
                    <div class="input-box">
                        <div class="select-list col" id="col">
                            <select id="sel0" name="supCode">
                                 <option value="">선택</option>
                                 <option value="N100000001">충청남도교육청</option>
                            </select>
                            <select id="sel1" name="supCode"></select>
                            <select id="sel2" name="schAt"></select>
                            <input type="text" id="viewCdNm" readonly>
                            <input type="hidden" id="viewCd" readonly>
                        </div>
                    </div>
                </div>
            `;
            $("#dsptcDiv").append(template);

            onchangeSelDsptc();
        } else {
            $("#selDsptc").remove();
            $("#detailDsptc").remove();
            // $("#dsptcAgre").remove();
        }
    })
}

var onchangeSelDsptc = function () {
    $("#sel1").append(`<option value="">선택</option>`)
    $("#sel2").append(`<option value="">선택</option>`)
    $("#sel3").append(`<option value="">선택</option>`)

    $("#sel0").on('change', function () {
        let type = this.value;
        if (!isNull(type)) {
            $ajax.post({
                url: "/common/selAduOrgCd",
                data: {orgCd: type},
                success: function (list) {
                    $("#sel1").empty();
                    $("#sel2").empty();
                    $("#sel3").empty();
                    $("#sel1").append(`<option value="">선택</option>`);
                    $("#sel2").append(`<option value="">선택</option>`);
                    $("#sel3").append(`<option value="">선택</option>`);

                    $.each(list, function () {
                        $("#sel1").append(`<option value="${this.orgCd}">${this.orgNm}</option>`)
                    })

                    $("#sel1").on('change', function () {
                        $ajax.post({
                            url: "/common/selNomalOrgCd",
                            data: {orgCd: $("#sel1").val()},
                            success: function (list) {
                                $("#sel2").empty();
                                $("#sel3").empty();

                                $("#sel2").append(`<option value="">선택</option>`)
                                $("#sel3").append(`<option value="">선택</option>`)
                                $.each(list, function () {
                                    $("#sel2").append(`<option value="${this.orgCd}">${this.orgNm}</option>`)
                                })

                                let value = $("#sel1 option:checked").val();
                                if (isNull(value)) {
                                    $("#viewCdNm").val("");
                                    $("#viewCd").val("");
                                } else {
                                    $("#viewCdNm").val($("#sel1 option:checked").text());
                                    $("#viewCd").val($("#sel1 option:checked").val());
                                }
                            }
                        })
                    })

                    $("#sel2").on('change', function () {

                        let value = $("#sel2 option:checked").val();
                        if (isNull(value)) {
                            $("#viewCdNm").val($("#sel1 option:checked").text());
                            $("#viewCd").val($("#sel1 option:checked").val());
                        } else {
                            $("#viewCdNm").val($("#sel2 option:checked").text());
                            $("#viewCd").val($("#sel2 option:checked").val());
                        }
                    })
                }
            })
        } else {
            $("#sel1").empty();
            $("#sel2").empty();
            $("#viewCdNm").val("");
            $("#viewCd").val("");

            $("#sel1").append('<option>선택</option>');
            $("#sel2").append('<option>선택</option>');
        }
    })
}