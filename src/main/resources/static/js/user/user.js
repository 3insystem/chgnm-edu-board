var pageObj = {
    update: function () {
        if (dataValidation()) {
            let form = $("[name=useSe]:checked").val() == 'USESE00002' ? $("form[id=forRep]") : $("form[id=forEmp]");
            let formData = new FormData(form[0]);

            let obj = {
                userId: $("#userId").val(),
                password: $("#password").val(),
                userNm: $("#userNm").val(),
                mbtlnum: $("#mbtlnum").val(),
                email: $("#email").val(),
                useSe: $("[name=useSe]:checked").val()
            };

            if (obj.useSe == 'USESE00002') {
                obj['cmpnyNm'] = $("#repcmpnyNm").val();
                obj['bizrno'] = $("#repBizrno").val();
                obj['zip'] = $("#zip").val();
                obj['adres'] = $("#adres").val();
                obj['detailAdres'] = $("#detailAdres").val();
                obj['ctprvn'] = $("#ctprvn").val();
                obj['signgu'] = $("#signgu").val();
                obj['reprsntTelno'] = $("#reprsntTelno").val();

                if ($files.getFiles().length > 0) {
                    formData.append("file", $files.getFiles()[0]);
                }

                if (!isNull($("[name=savedFileIds]").val())) {
                    obj['savedFileIds'] = $("[name=savedFileIds]").val();
                }
            } else {
                obj['bizrno'] = $("#empBizrno").val();
                obj['position'] = $("#position").val();
            }

            formData.append("userUpdate", new Blob([JSON.stringify(obj)], {
                type: "application/json"
            }));

            $ajax.postMultiPart({
                data: formData,
                success: function (id) {
                    if (isNull(id)) {
                        alert("저장하는데 오류가 발생했습니다.");
                        return false;
                    } else {
                        alert("저장이 완료되었습니다.");
                        location.href = '/';
                    }
                },
            });
        }

        function dataValidation() {
            var pwdRegExp = /^(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#$]).{8,}$/
            var emailRegExp = /(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/

            if ($("#password").val() != $("#rePassword").val()) {
                alert("비밀번호를 확인해 주세요.");
                $("#password").focus();
                return false;
            }
            if (!isNull($("#password").val()) && !pwdRegExp.test($("#password").val())) {
                alert("비밀번호 규칙이 맞지 않습니다.");
                $("#password").focus();
                return false;
            }
            if (!isNull($("#rePassword").val()) && !pwdRegExp.test($("#rePassword").val())) {
                alert("비밀번호 규칙이 맞지 않습니다.");
                $("#rePassword").focus();
                return false;
            }
            if (isNull($("#mbtlnum").val())) {
                alert("휴대폰을 입력해 주세요.");
                $("#mbtlnum").focus();
                return false;
            }
            if (isNull($("#email").val())) {
                alert("이메일을 입력해 주세요.");
                $("#email").focus();
                return false;
            }
            if (!emailRegExp.test($("#email").val())) {
                alert("이메일 규칙이 맞지 않습니다.");
                $("#email").focus();
                return false;
            }
            if ($("[name=useSe]:checked").length == 0) {
                alert("사용자 구분을 선택해 주세요.");
                return false;
            }

            if (bizrnoCheckFlag == 'N') {
                alert("사업자번호가 조회되지 않습니다. 사업자번호를 조회해 주세요.");
                return false;
            }

            let useSe = $("[name=useSe]:checked").val();
            if (useSe == 'USESE00002') {
                if (isNull($("#repcmpnyNm").val())) {
                    alert("업체명을 입력해 주세요.");
                    $("#repcmpnyNm").focus();
                    return false;
                }
                if (isNull($("#repBizrno").val())) {
                    alert("사업자번호를 입력해 주세요.");
                    $("#repBizrno").focus();
                    return false;
                }
                if (isNull($("#zip").val())) {
                    alert("주소를 입력해 주세요.");
                    $("#zip").focus();
                    return false;
                }
                if (isNull($("#detailAdres").val())) {
                    alert("상세주소를 입력해 주세요.");
                    $("#detailAdres").focus();
                    return false;
                }
            } else if (useSe == 'USESE00003') {
                if (isNull($("#empBizrno"))) {
                    alert("사업자번호를 입력해 주세요.");
                    $("#empBizrno").focus();
                    return false;
                }
            } else {
                return false;
            }

            return true;
        }
    },
    bizrnoCheck: function (target) {
        const sibling = $(target).siblings('[name=bizrno]');

        const bizrno = $(sibling).val().replace(/-/gi, '');
        var r = true;
        var numberMap = bizrno.replace(/-/gi, '').split('').map(function (d){
            return parseInt(d, 10);
        });

        if(numberMap.length == 10) {
            var keyArr = [1, 3, 7, 1, 3, 7, 1, 3, 5];
            var chk = 0;

            keyArr.forEach(function (d, i) {
                chk += d * numberMap[i];
            });

            chk += parseInt((keyArr[8] * numberMap[8]) / 10, 10);

            r = Math.floor(numberMap[9]) === ((10 - (chk % 10)) % 10);

            if (!r) {
                alert(`사업자 번호가 유효한 패턴이 아닙니다.`);
                bizrnoCheckFlag = "N";
                return false;
            } else {
                $ajax.post({
                    url: "/user/findBizrno",
                    data: {bizrno: bizrno},
                    success: function (res) {
                        let type = $("[name=useSe]:checked").val() == 'USESE00002' ? 'cmpny' : 'employee';
                        if(type == 'cmpny'){
                            if(res.result == 'B'){
                                alert('사용 가능한 사업자 번호입니다.');
                                bizrnoCheckFlag = 'Y';
                            } else {
                                if (res.dto.usid == null) {
                                    if (confirm("이미 존재하는 업체입니다. 사용하시겠습니까?")) {
                                        setCmpnyData(res.dto, type);
                                        bizrnoCheckFlag = 'Y';
                                    } else {
                                        bizrnoCheckFlag = 'N';
                                    }
                                } else {
                                    alert("이미 대표가 존재하는 업체입니다.");
                                    bizrnoCheckFlag = 'N';
                                }
                            }
                        } else if (type == 'employee'){
                            if(res.result == 'B'){
                                alert('등록되지않은 업체입니다.');
                                bizrnoCheckFlag = 'N';
                            } else if (res.result == 'S'){
                                alert("조회되었습니다.");
                                $('#empCmpnyNm').val(res.dto.cmpnyNm);
                                bizrnoCheckFlag = 'Y';
                            } else {
                                alert('사업자 번호가 중복되어 있습니다');
                                bizrnoCheckFlag = 'N';
                            }
                        } else {
                            alert(`잘못된 접근입니다.`);
                            bizrnoCheckFlag = 'N';
                        }
                        $(sibling).focus();
                    }
                })
            }
        } else {
            alert(`사업자 번호의 자릿수를 확인해주십시오.`);
            bizrnoCheckFlag = "N";
            return false;
        }

        function setCmpnyData(dto) {
            for (let key in dto) {
                if (key == 'cmpnyNm') {
                    $("#rep"+key).val(dto['cmpnyNm']);
                } else {
                    $("#"+key).val(dto[key]);
                }
            }

            if (!isNull(dto.fileNo)) {
                let template = `<div class="file_wraper">
                                    <input type="hidden" name="savedFileIds" value="${dto.fileNo}"/>
                                    <span>${dto.orignlFileNm}</span>
                                    <button class="btn-remove" type="button" onclick="$files.removeFile(this)"><img
                                        src="/images/common/file_delete_ico.svg"></button>
                                </div>`;
                $(".filenames").empty();
                $(".filenames").append(template);
            } else {
                $(".filenames").empty();
            }
        }
    }
}

var bizrnoCheckFlag = 'N';
pageObj.pageStart = function () {
    selectUseSe();

    $("[name=bizrno]").keypress(function () {
        bizrnoCheckFlag = 'N';
    })
}

function selectUseSe() {
    $("[name=useSe]").on('click', function () {
        if (this.value == 'USESE00002') {
            $("form[id=forEmp]").css('display', 'none');
            $("form[id=forRep]").css('display', 'block');
            bizrnoCheckFlag = 'N'
        } else if (this.value == 'USESE00003') {
            $("form[id=forEmp]").css('display', 'block');
            $("form[id=forRep]").css('display', 'none');
            bizrnoCheckFlag = 'N'
        } else {
            alert("구분 선택 오류 발생");
            return false;
        }
    })
}