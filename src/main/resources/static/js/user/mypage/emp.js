var pageObj = {
    update: function () {
        if (dataValidation()) {
            let formData = new FormData();

            let obj = {
                userId: $("#usid").val(),
                password: $("#password").val(),
                userNm: $("#userNm").val(),
                mbtlnum: $("#mbtlnum").val(),
                email: $("#email").val(),
                bizrno:$("#empBizrno").val(),
                position:$("#position").val(),
                useSe: $("#useSe").val()
            };

            formData.append("userUpdate", new Blob([JSON.stringify(obj)], {
                type: "application/json"
            }));

            $ajax.postMultiPart({
                url: '/user/mypage/emp',
                data: formData,
                success: function (id) {
                    if (isNull(id)) {
                        alert("저장하는데 오류가 발생했습니다.");
                        return false;
                    } else {
                        alert("저장이 완료되었습니다.");
                        location.reload();
                    }
                },
            });
        }

        function dataValidation() {
            var pwdRegExp = /^(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#$]).{9,12}$/
            var emailRegExp = /(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/
            if(!$valid.dataValidation()){
                return false;
            }
            if ($("#password").val() != $("#rePassword").val()) {
                alert("비밀번호를 확인해 주세요.");
                $("#password").focus();
                return false;
            }
            if (!isNull($("#password").val()) && !pwdRegExp.test($("#password").val())) {
                alert("비밀번호 규칙이 맞지 않습니다.");
                $("#password").focus();
                return false;
            }
            if (!isNull($("#rePassword").val()) && !pwdRegExp.test($("#rePassword").val())) {
                alert("비밀번호 규칙이 맞지 않습니다.");
                $("#rePassword").focus();
                return false;
            }
            if (isNull($("#mbtlnum").val())) {
                alert("휴대폰을 입력해 주세요.");
                $("#mbtlnum").focus();
                return false;
            }
            if (isNull($("#email").val())) {
                alert("이메일을 입력해 주세요.");
                $("#email").focus();
                return false;
            }
            if (!emailRegExp.test($("#email").val())) {
                alert("이메일 규칙이 맞지 않습니다.");
                $("#email").focus();
                return false;
            }
            if (isNull($("#empBizrno").val())) {
                alert("사업자번호를 입력해 주세요.");
                $("#empBizrno").focus();
                return false;
            }

            if (bizrnoCheckFlag == 'N') {
                alert("사업자번호가 조회되지 않습니다. 사업자번호를 조회해 주세요.");
                return false;
            }

            return true;
        }
    },
    bizrnoCheck: function (target) {
        const sibling = $(target).siblings('[name=bizrno]');

        const bizrno = $(sibling).val().replace(/-/gi, '');
        var r = true;
        var numberMap = bizrno.replace(/-/gi, '').split('').map(function (d){
            return parseInt(d, 10);
        });

        if ($('#mntmgtChk').val() != "0") {
            alert("진행중인 유지관리가 [" + $('#mntmgtChk').val() + "]건 존재 합니다.");
            return false;
        }

        if(numberMap.length == 10) {
            var keyArr = [1, 3, 7, 1, 3, 7, 1, 3, 5];
            var chk = 0;

            keyArr.forEach(function (d, i) {
                chk += d * numberMap[i];
            });

            chk += parseInt((keyArr[8] * numberMap[8]) / 10, 10);

            r = Math.floor(numberMap[9]) === ((10 - (chk % 10)) % 10);

            if (!r) {
                alert(`사업자 번호가 유효한 패턴이 아닙니다.`);
                bizrnoCheckFlag = "N";
                return false;
            } else {
                $ajax.post({
                    url: "/user/findBizrno",
                    data: {bizrno: bizrno},
                    success: function (res) {
                        let type = $("[name=useSe]:checked").val() == 'USESE00002' ? 'cmpny' : 'employee';
                        if(type == 'cmpny'){
                            if(res.result == 'B'){
                                alert('사용 가능한 사업자 번호입니다.');
                                bizrnoCheckFlag = 'Y';
                            } else {
                                if (res.dto.usid == null) {
                                    if (confirm("이미 존재하는 업체입니다. 사용하시겠습니까?")) {
                                        setCmpnyData(res.dto, type);
                                        bizrnoCheckFlag = 'Y';
                                    } else {
                                        bizrnoCheckFlag = 'N';
                                    }
                                } else {
                                    alert("이미 대표가 존재하는 업체입니다.");
                                    bizrnoCheckFlag = 'N';
                                }
                            }
                        } else if (type == 'employee'){
                            if(res.result == 'B'){
                                alert('등록되지않은 업체입니다.');
                                bizrnoCheckFlag = 'N';
                            } else if (res.result == 'S'){
                                alert("조회되었습니다.");
                                $('#cmpnyNm').val(res.dto.cmpnyNm);
                                $("#position").children('option:first-child').prop('selected', true);
                                bizrnoCheckFlag = 'Y';
                            } else {
                                alert('사업자 번호가 중복되어 있습니다');
                                bizrnoCheckFlag = 'N';
                            }
                        } else {
                            alert(`잘못된 접근입니다.`);
                            bizrnoCheckFlag = 'N';
                        }
                        $(sibling).focus();
                    }
                })
            }
        } else {
            alert(`사업자 번호의 자릿수를 확인해주십시오.`);
            bizrnoCheckFlag = "N";
            return false;
        }

        function setCmpnyData(dto) {
            for (let key in dto) {
                if (key == 'cmpnyNm') {
                    $("#rep"+key).val(dto['cmpnyNm']);
                } else {
                    $("#"+key).val(dto[key]);
                }
            }

            if (!isNull(dto.fileNo)) {
                let template = `<div class="file_wraper">
                                    <input type="hidden" name="savedFileIds" value="${dto.fileNo}"/>
                                    <span>${dto.orignlFileNm}</span>
                                    <button class="btn-remove" type="button" onclick="$files.removeFile(this)"><img
                                        src="/images/common/file_delete_ico.svg"></button>
                                </div>`;
                $(".filenames").empty();
                $(".filenames").append(template);
            } else {
                $(".filenames").empty();
            }
        }
    }
}

var bizrnoCheckFlag = 'Y';
pageObj.pageStart = function () {

    $("[name=bizrno]").keypress(function () {
        bizrnoCheckFlag = 'N';
    })
}