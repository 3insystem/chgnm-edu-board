let domain = $url.getPath();

pageObj = {
    view: {
        detail: (reprtNo) => {
            let url = $url.getPath();
            url = isLastUrlSlushExistence(url) ? url + 'detail/' : url + '/detail/';
            $view.setSearchParam();
            location.href = url + reprtNo;
        },
        list: () => {
            if (localStorage.getItem("queryString")) {
                domain = `${urlSubString(domain, 'detail')}?${localStorage.getItem("queryString")}`;
            } else {
                domain = urlSubString(domain, 'detail');
            }
            location.href = domain;
        }
    }
}

const isLastUrlSlushExistence = (url) => {
    return url.endsWith('/');
}

const urlSubString = (url, str) => {
    let index = url.indexOf(str);
    return url.substring(0, index - 1);
}

$(document).ready(() => {
    $(".content_tr > td").on('click', function () {
        let reprtNo = $(this).parent().attr("id");
        let chk = $(this).attr("class") /* 인쇄 버튼 확인 */

        if (chk != "button") {
            pageObj.view.detail(reprtNo);
        }
    })
})

