let modal = $modal();
let initBody;
let printContent;
const getReportModal = (reprtNo) => {
    let url = '/print/report/' + reprtNo;

    fetch(url)
        .then(res => res.text())
        .then(data => {
            let element = document.getElementById('print');
            let wrapElement = document.getElementById('printModal');
            element.innerHTML = data
            wrapElement.classList.remove('fade');
        })
}

const closeModal = () => {
    let wrapElement = document.getElementById('printModal');
    wrapElement.classList.add('fade');
}

const reportPrint = (reprtNo) => {
    let url = '/print/report/' + reprtNo;

    const width = 1100;
    const height = 750;
    let windowPositionX = (document.body.offsetWidth / 2) - (width / 2); // 가운데 정렬
    windowPositionX += window.screenLeft; // 듀얼 모니터일 때
    let windowPositionY = (document.body.offsetHeight / 2) - (height / 2);

    printContent = document.querySelector('.modal-body');
    let printWindow = window.open(url, '점검보고서', "width="+ width+", height=" + height +", left="+windowPositionX+", top="+windowPositionY)

    printWindow.print();
    printWindow.setTimeout(window.close, 500);
};