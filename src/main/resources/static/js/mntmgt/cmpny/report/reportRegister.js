let submitFlag = false;
const doubleSubmit = () => {
    if (!submitFlag) {
        submitFlag = true;
        return false;
    } else {
        return true;
    }
}


var pageObj = {
    register : function () {
        let url = $url.getPath();
        let register = url.indexOf("register");
        url = url.substring(0,register);
        url = url + "register";

        if (!doubleSubmit()) {
            let nullChk = false; /* 유지관리 및 소모품 조회 체크 */
            let list = [];
            $.each($("[id^=tr_]"), function () {
                let obj = {};
                $.each($(this).find('input'), function () {
                    obj[this.id.split('_')[0]] = this.value;
                    nullChk = true;
                })
                list.push(obj);
            })

            if (nullChk) {
                let data = $form.getData();
                data.detailList = list;
                data.sj = $('#year').val() + '년 ' + $('#month').val() + '월 유지관리 점검 보고서'

                if ($('#reprtNo').val() != null) {
                    url = url + "/" + $('#reprtNo').val();
                }

                $ajax.post( {
                    url: url,
                    data: data,
                    success: function () {
                        alert("저장되었습니다.");
                        let register = url.indexOf("register");
                        url = url.substring(0,register);
                        window.location.href = url; // 최신 목록 페이지 이동
                        localStorage.setItem("queryString",""); // queryString 비워줌
                    }
                })
            } else {
                alert("유지관리 목록을 조회 후 등록해 주세요.")
            }
        }

    },
    close : function () {
        let url = $url.getPath();
        let register = url.indexOf("register");
        url = url.substring(0,register);
        window.location.href = url + "?" + localStorage.getItem("queryString"); // 이전 페이지로 이동
        localStorage.setItem("queryString","");
    }
}