var pageObj = {
    view : {
        detail : function (reprtNo) {
            let url = $url.getPath();
            url = url.charAt(url.length - 1); //  url 뒤에  '/' 가 안붙을 떄를 대비
            if (url == '/') {
                url = $url.getPath() + "register/" + reprtNo;
            } else {
                url = $url.getPath() + "/register/" + reprtNo;
            }
            localStorage.setItem("queryString", new URLSearchParams(location.search).toString()); // 스토리지에 페이지 정보 저장
            location.href=url;
        },
        register : function () {
            let url = $url.getPath(); // 현재 url 가져옴
            url = url.charAt(url.length - 1); //  url 뒤에  '/' 가 안붙을 떄를 대비
            if (url == '/') {
                url = $url.getPath() + "register"
            } else {
                url = $url.getPath() + "/register"
            }
            location.href=url; // 유지보수 작성 화면으로 이동
        }
    }
}

pageObj.pageStart = function () {
    trClickEvent();
}

function trClickEvent() {
    $(".content_tr > td").on('click', function () {
        let reprtNo = $(this).parent().attr("id");
        let chk = $(this).attr("class") /* 인쇄 버튼 확인 */

        if (chk != "button") {
            pageObj.view.detail(reprtNo);
        }
    })
}