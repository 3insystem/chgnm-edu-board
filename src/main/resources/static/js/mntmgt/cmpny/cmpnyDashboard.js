var pageObj = {
    view : {
        mntmgtList: function () {
            let url = $url.getPath();
            url = url.replace('dashboard','mntmgt');
            localStorage.setItem("queryString","");
            location.href=url;
        },
        mntmgtDetail: function (rceptNo) {
            let url = $url.getPath();
            let urlchk = url.charAt(url.length - 1); //  url 뒤에  '/' 가 안붙을 떄를 대비
            if (urlchk == '/') {
                url = url.replace('dashboard','mntmgt');
                url = url + "detail/" + rceptNo;
            } else {
                url = url.replace('dashboard','mntmgt');
                url = url + "/detail/" + rceptNo;
            }
            localStorage.setItem("queryString","");
            location.href=url;
        },
        cmpnyList: function () {
            let url = $url.getPath();
            url = url.replace('dashboard','cmpds');
            localStorage.setItem("queryString","");
            location.href=url;
        },
        cmpnyDetail: function (rceptNo) {
            let url = $url.getPath();
            let urlchk = url.charAt(url.length - 1); //  url 뒤에  '/' 가 안붙을 떄를 대비
            if (urlchk == '/') {
                url = url.replace('dashboard','cmpds');
                url = url + "detail/" + rceptNo;
            } else {
                url = url.replace('dashboard','cmpds');
                url = url + "/detail/" + rceptNo;
            }
            localStorage.setItem("queryString","");
            location.href=url;
        }
    }
}

$(document).ready(function () {
    MntmgtlistClickEvent();
    CmpnylistClickEvent();
})

function MntmgtlistClickEvent() {
    $("#mntList > a").on('click', function () {
        let rceptNo = $(this).attr('id'); // 상세페이지 접수번호
        pageObj.view.mntmgtDetail(rceptNo);
    })
}

function CmpnylistClickEvent() {
    $("#cmpList > a").on('click', function () {
        let rceptNo = $(this).attr('id'); // 상세페이지 접수번호
        pageObj.view.cmpnyDetail(rceptNo);
    })
}