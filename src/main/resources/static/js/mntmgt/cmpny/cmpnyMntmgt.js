var pageObj = {
    view : {
        register : function () {
            let url = $url.getPath(); // 현재 url 가져옴
            url = url.charAt(url.length - 1); //  url 뒤에  '/' 가 안붙을 떄를 대비
            if (url == '/') {
                url = $url.getPath() + "register"
            } else {
                url = $url.getPath() + "/register"
            }
            location.href=url; // 유지보수 작성 화면으로 이동
        },
        detail : function (rceptNo) {
            let url = $url.getPath();
            url = url.charAt(url.length - 1); //  url 뒤에  '/' 가 안붙을 떄를 대비
            if (url == '/') {
                url = $url.getPath() + "detail/" + rceptNo;
            } else {
                url = $url.getPath() + "/detail/" + rceptNo;
            }
            localStorage.setItem("queryString", new URLSearchParams(location.search).toString()); // 스토리지에 페이지 정보 저장
            location.href=url;
        }
    }
}


// $(document).ready(function () {
//     trClickEvent();
// })

// function trClickEvent() {
//     $(".content_tr > td").on('click', function () {
//         let rceptNo = $(this).siblings().first().attr('id'); // 상세페이지 접수번호
//         pageObj.view.detail(rceptNo);
//     })
// }