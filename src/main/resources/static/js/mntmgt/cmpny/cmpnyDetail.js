let submitFlag = false;
const doubleSubmit = () => {
    if (!submitFlag) {
        submitFlag = true;
        return false;
    } else {
        return true;
    }
}


var pageObj = {
    update : function () {
        let url = $url.getPath();

        if ($('[name=statusType]:checked').val() != null) {
            if (!doubleSubmit()) {
                $ajax.postMultiPart( {
                    url: url,
                    data: $form.getData(),
                    success: function (updateChk) {
                        if (updateChk == "true") {
                            alert("저장되었습니다.");
                            let urldetail = url.indexOf("detail");
                            url = url.substring(0,urldetail);
                            window.location.href = url + "?" + localStorage.getItem("queryString"); // 이전 페이지로 이동
                            localStorage.setItem("queryString","");
                        } else if (updateChk == "false"){
                            alert("잘못된 요청 입니다.");
                            location.reload();
                        } else {
                            alert("개인정보가 포함되어 있습니다.");
                            submitFlag = false;
                        }
                    }
                })
            }
        } else {
            alert("접수 체크 후 진행할수 있습니다.");
        }
    },
    close : function () {
        let url = $url.getPath();
        let urldetail = url.indexOf("detail");
        url = url.substring(0,urldetail);
        window.location.href = url + "?" + localStorage.getItem("queryString"); // 이전 페이지로 이동
        localStorage.setItem("queryString","");
    }
}

pageObj.pageStart = function () {
    $editor.init('processCn');
    pagecheck();
    talbeBorderChk();
}

function pagecheck () {
    if($('#rceptCheck').attr("value") == null || $('#rceptCheck').attr("value") == "") {
        $('#filecheck').css("display", "none");
    }
}

function talbeBorderChk () {
    $('.table > tbody > tr').css('border', '1px solid #EFEFEF');
    $('.table > tbody > tr > td').css('border', '1px solid #EFEFEF');
    $('.table > tbody > tr > td').css('text-align', 'center');
    $('.table > tbody > tr > td').css('padding', '10px 0');
}