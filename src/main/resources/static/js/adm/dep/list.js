var pageObj = {
    matchModal: function (upperOrgCd, orgCd, orgNm, depNm){
        modal.open({
            title: "지원청 지정",
            path: $url.getPath() + '/match?upperOrgCd=' + upperOrgCd + '&orgCd=' + orgCd + '&orgNm=' + orgNm + '&depNm=' + depNm
        });

        $ajax.post({
            url: "/common/selAduOrgCd",
            data: {},
            async: false,
            success: function (list) {
                $("#supBox").empty();
                $("#supBox").append(`<option value="">선택</option>`)
                $.each(list, function () {
                    $("#supBox").append(`<option value="${this.orgCd}">${this.orgNm}</option>`)
                })
            }
        });
    },
    goSlc: function (orgCd){
        domain = $url.getPath().substring(0, $url.getPath().lastIndexOf('/')) + "/auth"
        localStorage.setItem("queryString", "viewCd=" + orgCd);
        $view.main()
    },
    goAuth: function (orgCd){
        domain = $url.getPath().substring(0, $url.getPath().lastIndexOf('/')) + "/eduSlcList"
        localStorage.setItem("queryString", "viewCd=" + orgCd);
        $view.main()
    },
    update: function (orgCd, depNm){

        let upperOrgCd = $("#supBox").val();
        if($.trim(upperOrgCd) == "" ){
            alert("교육청을 선택해주십시오.");
            return false;
        } else {
            if($.trim(depNm) == ""){
                let orgNm = $("#orgNm").text();
                if($.trim(orgNm).includes('초등학교')){
                    depNm = '초등학교';
                } else if($.trim(orgNm).includes('중학교')) {
                    depNm = '중학교';
                } else if($.trim(orgNm).includes('고등학교')){
                    depNm = '고등학교';
                } else if($.trim(orgNm).includes('유치원')){
                    depNm = '유치원';
                } else {
                    alert("해당 항목은 시스템 관리자에게 문의 해주십시오");
                    return false;
                }
            }

            data = {
                upperOrgCd: $("#supBox").val(),
                orgCd     : orgCd,
                depNm     : depNm
            }
            $ajax.post({
                url: domain + "/depUpdate",
                'data': data,
                success: function (res){
                    if(res.result == 'S'){
                        $view.setSearchParam();
                        $view.main();
                    }
                },
                error: function (error){
                    alert(error)
                }
            });
        }
    }
}

var domain = $url.getPath();
var modal = $modal();
pageObj.pageStart = function () {
    modal.init($("#modal"), {
        width: 1000,
        height: 3010
    });
};