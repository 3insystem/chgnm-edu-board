var pageObj = {
    save: function (){
        let rowList = [];
        var blankCheck = true;
        $.each($("[name=row]"),function (idx, item){
            var $dsptcSe = $(item).find("#dsptcSe");
            var $useSe = $(item).find("#useSe");
            if(($dsptcSe.val() != $dsptcSe.data("default") || $useSe.val() != $useSe.data("default")) && $dsptcSe.val() != "DSPTC00001"){
                let row = {};
                row["usid"] = $(item).find("#usid").text();
                row["dsptcSe"] = $dsptcSe.val();
                row["useSe"] = $useSe.val();
                if ($.trim(useSe) == "") {
                    blankCheck = false
                }
                rowList.push(row);
            }
        });
        if(!blankCheck){
            alert("권한지정이 되어있지 않은 유저가 있습니다");
            return false;
        } else if(rowList.length == 0) {
            alert("변경 된 사항이 없습니다.");
            return false;
        } else {
            $ajax.post({
                url: $url.getPath() + "/authSave",
                data: {data: rowList},
                success: function (res){
                    if(res.result == 'S'){
                        alert("저장에 성공하였습니다");
                        $view.setSearchParam();
                        $view.main();
                    } else {
                        alert(res.msg);
                    }
                }
            })
        }
    }
}

var domain = $url.getPath();
pageObj.pageStart = function () {
    $("#serOrgNm").text($("#viewCdNm").val());

    $("[name=dsptcSe]").on('change', function () {
        if($(this).val() == "DSPTC00002" || $(this).val() == "DSPTC00003"){
            $(this).parent().parent().find("#useSe").attr("disabled", false);
        } else {
            $(this).parent().parent().find("#useSe").attr("disabled", true);
        }
    })
}