var pageObj = {
    slcDeleteBtn: function (target) {
        if (!isNull(target)) {
            $ajax.post({
                url: '/instt/slc/delete',
                data: {slctnNo: target},
                success: function (response) {
                    if (!isNull(response)) {
                        alert("삭제되었습니다.");
                        location.reload();
                    } else {
                        alert("오류가 발생했습니다.");
                    }
                }
            })
        } else {
            alert("삭제 실패");
            return false;
        }
    },
    slcConfmBtn: function (target) {
        if (!isNull(target)) {
            $ajax.post({
                url: "/instt/slc/slcConfm",
                data: {slctnNo: target},
                success: function (response) {
                    console.log(response);
                    if (!isNull(response)) {
                        alert("승인이 완료되었습니다.");
                        location.reload();
                        obj.view.detail(response);
                    } else {
                        alert("오류가 발생했습니다.");
                    }
                }
            })
        } else {
            alert("승인 실패");
            return false;
        }
    },
    slcStopBtn: function (target) {
        if (!isNull(target)) {
            $ajax.post({
                url: "/instt/slc/slcStop",
                data: {slctnNo: target},
                success: function (response) {
                    console.log(response);
                    if (!isNull(response)) {
                        alert("반려되었습니다.");
                        location.reload();
                        obj.view.detail(response);
                    } else {
                        alert("오류가 발생했습니다.");
                    }
                }
            })
        } else {
            alert("반려 실패");
            return false;
        }
    },
    searchBtn: function (w1) {
        $("form").submit();
    }
}
const domain = $url.getPath();
pageObj.pageStart = function () {
    // selCmmnCode('CONFM', 'confmSe');
}