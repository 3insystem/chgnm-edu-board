var timeSttus = "d";
var catSttus = "rec";

var pageObj = {
    chgSelYear: function (y){
        let now = new Date();
        let month = 0;
        let monthList = [];
        let year = now.getFullYear();
        if(y != year){
            month = 12;
        } else {
            month = now.getMonth() + 1;
        }

        for(let i = 0; i < month; i++){
            monthList.push(String(i + 1).padStart(2, "0"));
        }

        $("#month").empty();
        $.each(monthList, function (idx, item){
            $("#month").append('<option value="' + item + '">' + item + '월' + '</option>')
        });

        let minDate = y + "-01-01";
        let maxDate = y + "-01-31";

        $("#startDate").datepicker('destroy');
        $("#endDate").datepicker('destroy');

        $("#startDate").datepicker({
            minDate: minDate,
            maxDate: maxDate
        });
        $("#endDate").datepicker({
            minDate: minDate,
            maxDate: maxDate
        });

        $("#startDate").datepicker('setDate', minDate);
        $("#endDate").datepicker('setDate', maxDate);

        $("#ui-datepicker-div").css('display', 'none');
    },
    chgSelMonth: function (m){
        let y = $("#year option:selected").val();
        let now = new Date();
        let lastDay = new Date(y, Number(m), 0);
        let month = String(now.getMonth() + 1).padStart(2, "0");
        let year = now.getFullYear();
        let minDate = y + "-" + m + "-01";
        let maxDate;
        if(y == year && m == month){
            maxDate = y + "-" + m + "-"+ String(now.getDate()).padStart(2, "0");
        } else {
            maxDate = y + "-" + m + "-"+ String(lastDay.getDate()).padStart(2, "0");
        }

        $("#startDate").datepicker('destroy');
        $("#endDate").datepicker('destroy');

        $("#startDate").datepicker({
            minDate: minDate,
            maxDate: maxDate
        });
        $("#endDate").datepicker({
            minDate: minDate,
            maxDate: maxDate
        });

        $("#startDate").datepicker('setDate', minDate);
        $("#endDate").datepicker('setDate', maxDate);

        $("#ui-datepicker-div").css('display', 'none');
    },
    changeCategory: function (key = 'rec') {
        catSttus = key;
        this.search(key)
    },
    changeTime: function (t) {
        this.search(catSttus, t)
    },
    search: function (cat, t = ""){
        //기본은 일별
        if(!$.trim(t) == ""){
            timeSttus = t;
        }

        searchParam = {
            "viewCd" : $("#viewCd").val(),
            "allYn" : $("input:checkbox[id='allYn1']:checked").val(),
            "startDate" : $("#startDate").val(),
            "endDate" : $("#endDate").val(),
            "sel0" : $("#year option:selected").val(),
            "sel1" : $("#month option:selected").val(),
            "selectKey1" : timeSttus,
            "selectKey2" : catSttus
        }

        this.getData(searchParam);

    },
    getData: function (param) {
        $chart.dataInit();

        $ajax.post({
            // url: $url.getPath() + "/mntmgt/month/" + key,
            url: $url.getPath(catSttus),
            data: param,
            dataType: 'json',
            success: function (res){
                if(res.categories.length > 0){
                    chart.setData(res)
                } else {
                    alert("조회된 데이터가 없습니다.");
                }
            },
            fail: function (){
                alert("데이터 조회에 실패했습니다");
            }
        });
    }
}

pageObj.pageStart = function () {
    $("#startDate").datepicker({
        minDate: $("#startDate").val(),
        maxDate: $("#endDate").val()
    });
    $("#endDate").datepicker({
        minDate: $("#startDate").val(),
        maxDate: $("#endDate").val()
    });
    this.changeCategory();
}