let domain = $url.getPath();

pageObj = {
    view: {
        register: () => {
            let url = $url.getPath();
            url = isLastUrlSlushExistence(url) ? url + 'register' : url + '/register';
            $view.setSearchParam();
            location.href = url // 소모품 작성 화면으로 이동
        },
        detail: (rceptNo) => {
            let url = $url.getPath();
            url = isLastUrlSlushExistence(url) ? url + 'detail/' : url + '/detail/';
            $view.setSearchParam();
            location.href = url + rceptNo;
        }
    }
}

const isLastUrlSlushExistence = (url) => {
    return url.endsWith('/');
}

$(document).ready(function() {
    $('.content_tr').keypress(function(e){
        if(e.keyCode == 13) {
            let url = $url.getPath() + '/detail/';
            let rceptNo = $(this).attr('rceptNo');
            location.href = url + rceptNo;
        }
    })
})


