let url = '';
let domain = $url.getPath();

pageObj = {
    view: {
        update: () => {
            url = $url.getPath().replace("detail", "update");
            location.href = url;
        }
    },
    cancel: (rceptNo) => {
        let replaceUrl = $url.getPath().replace("detail", "cancel");
        url = urlSubString(replaceUrl, rceptNo);

        if (confirm("신청내역을 취소하시겠습니까?")) {

            const data = {
                "rceptNo": rceptNo
            }

            $ajax.post({
                url: url,
                data: data,
                success: (response) => {
                    alert(response);
                    location.reload();
                },
                error : (error) => {
                    console.log(error)
                }
            })
        }
    },
    checkComplete: (rceptNo) => {
        let replaceUrl = $url.getPath().replace("detail", "complete");
        url = urlSubString(replaceUrl, rceptNo);

        if (confirm("완료 처리 하시겠습니까?")) {

            const data = {
                "rceptNo": rceptNo
            }

            $ajax.post({
                url: url,
                data: data,
                success: (response) => {
                    alert(response);
                    pageObj.list()
                }
            })
        }
    },
    list: () =>  {
        if (localStorage.getItem("queryString")) {
            domain = `${urlSubString(domain, 'detail')}?${localStorage.getItem("queryString")}`;
        } else {
            domain = urlSubString(domain, 'detail');
        }
        location.href = domain;
    },
}

const urlSubString = (url, str) => {
    let index = url.indexOf(str);
    return url.substring(0, index - 1);
}

$(document).ready(() => {
    $('.table > tbody > tr').css('border', '1px solid #EBEBEB');
    $('.table > tbody > tr > td').css('border', '1px solid #EBEBEB');
    $('.table > tbody > tr > td').css('text-align', 'center');
    $('.table > tbody > tr > td').css('padding', '10px 0');
})