let submitFlag = false;
let url = '';
let smsInfo = {};
const doubleSubmit = () => {
    if (!submitFlag) {
        submitFlag = true;
        return false;
    } else {
        return true;
    }
}

pageObj = {
    view: {
        list: () => {
            if (url != null) {
                url = '';
            }
            url = urlSubString($url.getPath(), 'register');

            if (localStorage.getItem("queryString")) {
                url = `${url}?${localStorage.getItem("queryString")}`;
            }

            location.href = url;
        },
        detail: () => {
            url = $url.getPath().replace("update", "detail");
            location.href = url;
        }
    },
    save: (rceptNo) => {
        url = rceptNo == null ? $url.getPath() : urlSubString($url.getPath(), rceptNo);
        if (!doubleSubmit()) {

            // 장애관리 분류
            if ($(".radio-list").children().length > 0) {
                if (isNull($("input[name='maintenanceType']:checked").val())) {
                    alert("분류를 선택해주세요.");
                    submitFlag = false;
                    return;
                }
            }

            // 장애관리 분류 상세
            if ($("#clSe").val() == 0) {
                alert("분류 상세를 선택해주세요.");
                submitFlag = false;
                return;
            }

            if ($("#first_selectbox").val() == 0) {
                alert("분류를 선택해주세요.");
                submitFlag = false;
                return;
            }

            if ($("#first_selectbox option:checked").text() == '인쇄용품') {
                if ($("#second_selectbox").val() == 0) {
                    alert("2차 분류를 선택해주세요.");
                    submitFlag = false;
                    return;
                }
            }

            if ($("#bizrno").val() == 0) {
                alert("업체를 선택해주세요.");
                submitFlag = false;
                return;
            }

            if (isNull($("#cttpcl").val())) {
                alert("연락처를 입력해주세요.");
                $("#cttpcl").focus();
                submitFlag = false;
                return;
            }

            // 휴대폰, 전화번호 유효성 검사
            const phoneRegexp =/^(?:(010)|(01[1|6|7|8|9]))-\d{3,4}-(\d{4})$/;
            const telRegexp = /^(0(2|3[1-3]|4[1-4]|5[1-5]|6[1-4])-)(\d{3,4}-)(\d{4})$/;
            if(!phoneRegexp.test($("#cttpcl").val()) && !telRegexp.test($("#cttpcl").val())) {
                alert("잘못된 번호입니다. 다시 입력해주세요.");
                $("#cttpcl").focus();
                submitFlag = false;
                return;
            }


            if (isNull($("#subject").val())) {
                alert("제목을 입력해주세요.");
                $("#subject").focus();
                submitFlag = false;
                return;
            }

            if (isNull($("#content").val())) {
                alert("내용을 입력해주세요.");
                $("#content").focus();
                submitFlag = false;
                return;
            }



            $ajax.postMultiPart({
                url: url,
                data: $form.getData(),
                success: (response) => {
                    if (response.message.indexOf("등록") == 0) {
                        smsInfo = response.data;
                        submitFlag = false;
                        $('#smsModal').removeClass('fade');
                    } else if (response.message.indexOf("수정") == 0) {
                        alert("수정되었습니다.")
                        pageObj.view.detail(rceptNo);
                    } else {
                        alert(response.message);
                        submitFlag = false;
                    }
                }
            })
        }
    },
    cancel: (rceptNo) => {
        rceptNo == null ? pageObj.view.list() : pageObj.view.detail();
    },
    cmpdsCategoryChange: (target) => {
        let secondSelectBox = document.getElementById("second_selectbox");
        let selectedText = target.options[target.selectedIndex].text;

        selectedText == '인쇄용품' ? secondSelectBox.disabled = false : secondSelectBox.disabled = true;

        if (secondSelectBox.disabled == true) {
            secondSelectBox.selectedIndex = 0;
        }

    }

}

pageObj.pageStart = function () {
    $editor.init('content');
    $(".note-editable").attr('tabindex', 214)
}

window.onload = () => {
    let url = $url.getPath();

    if (url.indexOf("cmpds") > 0) {
        let secondSelectBox = document.getElementById("second_selectbox");
        let value = secondSelectBox.options[secondSelectBox.selectedIndex].value;

        if (value != 0) {
            secondSelectBox.disabled = false;
        }
    }
}

const urlSubString = (url, str) => {
    let index = url.indexOf(str);
    return url.substring(0, index - 1);
}

const smsOpenModal = () => {
    $('.sms').css('transform', 'translate(0)');
    $('.sms-wrap').css('visibility', 'visible');
    $('.sms-btn').css('display', 'none');
}

const smsCloseModal = (type) => {
    if (type == null) {
        alert("등록되었습니다.");
    }
    $('.sms').css('transform', 'translate(0, calc(100% + 20px))');
    setTimeout(function () {
        $('.sms-wrap').css('visibility', 'hidden');
        $('.sms-btn').css('display', 'block');
    }, 500)
    location.href = urlSubString(url, 'register')
}

const smsSendConfirm = (isSend) => {
    if (isSend) {
        $('#smsModal').addClass('fade');
        smsOpenModal()

        $('#smsContent').val(smsInfo.content);
        let callerNumber = smsInfo.callback.split('-');
        let receivingNumber = smsInfo.recverList[0].phone.split('-');

        for (let i = 0; i < callerNumber.length; i++) {
            $('#callerNumber' + (i + 1)).val(callerNumber[i]);
            $('#receivingNumber' + (i + 1)).val(receivingNumber[i]);
        }
    } else {
        $('#smsModal').addClass('fade');
        alert("등록되었습니다.");
        location.href = urlSubString(url, 'register');
    }
}

const sendSms = () => {
    if (!doubleSubmit()) {
        url = urlSubString($url.getPath(), "mntmgt") + '/sms/send';
        $ajax.post({
            url: url,
            data: smsInfo,
            contentType: 'application/json; charset=utf-8',
            success: (response) => {
                alert(response);
                smsCloseModal('sms');
                location.href = urlSubString(url, 'sms') + '/mntmgt';
            }
        })
    }
}

const autoHyphen = (target) => {
    target.maxLength = 13;
    target.value = target.value.replace(/[^0-9]/g, '')
        .replace(/^(\d{2,3})(\d{3,4})(\d{4})$/, `$1-$2-$3`);
}