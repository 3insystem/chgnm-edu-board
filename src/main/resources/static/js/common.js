$errors = {
    valid: function (errors) {
        const globalErrorsTarget = $("[globalErrors]");
        $(".modal_error").each(function () {
            if ($(this).attr("override")) {
                $(this).hide();
                $(this).text("");
            } else {
                $(this).remove();
            }
        });

        $("[errorclass]").each(function () {
            $(this).removeClass($(this).attr("errorclass"));
        });

        const fieldErrors = errors["fieldErrors"];
        if (fieldErrors) {
            fieldErrors.forEach(error => {
                const errorField = $("#" + error.field);
                errorField.addClass(errorField.attr("errorclass"));

                let errorMsgField
                if (errorField.attr("editor")) {
                    errorMsgField = $('[placeholder="' + error.field + '"]')
                    errorMsgField.text(error.message);
                } else {
                    if ($(`span[errors='${error.field}']`).length) {
                        $(`span[errors='${error.field}']`).text(error.message);
                        $(`span[errors='${error.field}']`).css("display", "inline-block");
                    } else {
                        errorMsgField = `<span errors="${error.field}" class="modal_error">${error.message}</span>`
                        errorField.after(errorMsgField);
                    }
                }
            });
        }

        const globalErrors = errors["globalErrors"];
        if (globalErrors) {
            if (globalErrorsTarget.length) {
                globalErrors.forEach(error => {
                    globalErrorsTarget.append(`<span class="modal_error">${error.message}</span>`)
                });
            } else {
                globalErrors.forEach(error => {
                    alert(error.message);
                });
            }
        }
    }
}

$url = {
    getPath: function (extPath) {
        if (extPath && extPath[0] != '/') extPath = '/' + extPath;
        return location.pathname + (extPath ? extPath : '');
    },
    getHostUrl: function () {
        return location.protocol + "//" + this.getHost();
    },
    getHost: function () {
        return location.host;
    },
    redirect: function (path) {
        location.href = path ? path : this.getPath();
    },
    gotoUrl: function (id, url) {
        localStorage["clickMenus"] = id;
        location.href = url;
    }
}

$ajax = {
    defaultOption: {
        url: $url.getPath(),
        contentType: 'application/json',
        async: true
    },

    post: function (options) {
        options = $.extend({}, this.defaultOption, options);
        $.ajax({
            url: options.url,
            type: 'POST',
            data: JSON.stringify(options.data),
            contentType: options.contentType,
            async: options.async
        }).done(function (data) {
            if (options.success) {
                options.success(data);
            } else {
                $url.redirect();
            }
        }).fail(function (error) {
            if (options.error) {
                options.error(error.responseJSON);
            } else {
                $errors.valid(error.responseJSON);
            }
        });
    },
    postMultiPart: function (options) {
        options = $.extend({}, this.defaultOption, options);

        $.ajax({
            url: options.url,
            type: 'POST',
            data: options.data,
            processData: false,
            contentType: false,
            enctype: 'multipart/form-data',
            async: options.async
        }).done(function (data) {
            if (options.success) {
                options.success(data);
            } else {
                $url.redirect();
            }
        }).fail(function (error) {
            if (options.error) {
                options.error(error.responseJSON);
            } else {
                $errors.valid(error.responseJSON);
            }
        });
    },

    put: function (options) {
        options = $.extend({}, this.defaultOption, options);

        $.ajax({
            url: options.url,
            type: 'PUT',
            data: JSON.stringify(options.data),
            contentType: options.contentType,
            async: options.async
        }).done(function (data) {
            if (options.success) {
                options.success(data);
            } else {
                $url.redirect();
            }
        }).fail(function (error) {
            if (options.error) {
                options.error(error.responseJSON);
            } else {
                $errors.valid(error.responseJSON);
            }
        });
    },
    putMultiPart: function (options) {
        options = $.extend({}, this.defaultOption, options);

        $.ajax({
            url: options.url,
            type: 'PUT',
            data: options.data,
            processData: false,
            contentType: false,
            async: options.async
        }).done(function (data) {
            if (options.success) {
                options.success(data);
            } else {
                $url.redirect();
            }
        }).fail(function (error) {
            if (options.error) {
                options.error(error.responseJSON);
            } else {
                $errors.valid(error.responseJSON);
            }
        });
    },

    get: function (options) {
        options = $.extend({}, this.defaultOption, options);

        $.ajax({
            url: options.url,
            type: 'GET',
            contentType: options.contentType,
            async: options.async
        }).done(function (data) {
            if (options.success) {
                options.success(data);
            } else {
                $url.redirect();
            }
        }).fail(function (error) {
            if (options.error) {
                options.error(error.responseJSON);
            } else {
                $errors.valid(error.responseJSON);
            }
        });
    },

    delete: function (options) {
        options = $.extend({}, this.defaultOption, options);

        $.ajax({
            url: options.url,
            type: 'DELETE',
            data: JSON.stringify(options.data),
            contentType: options.contentType,
            async: options.async
        }).done(function (data) {
            alert("삭제 되었습니다.");

            if (options.success) {
                options.success(data);
            } else {
                $url.redirect();
            }
        }).fail(function (error) {
            if (options.error) {
                options.error(error.responseJSON);
            } else {
                console.log(error);
                $errors.valid(error.responseJSON);
            }
        });
    }
}

$editor = {
    init: function (targetId) {
        const target = $("#" + targetId);
        target.summernote({
            lang: "ko-KR",
            height: 500,
            placeholder: `<span placeholder="${targetId}" className="modal_error"></span>`,
            fontNames: ["맑은 고딕", "굴림", "돋움", "바탕", "나눔고딕", "궁서", "나눔명조", "HY헤드라인M", 'NotoSansKR','Arial','Courier New','Verdana','Tahoma','Times New Roamn'],
            fontSizes: ['8','9','10','11','12','14','18','24','36','48'],
            toolbar: [
                ['style', ['style']],
                ['fontname', ['fontname']],
                ['fontsize', ['fontsize']],
                // ['style', ['hr', 'bold', 'italic', 'underline', 'strikethrough', 'clear']],
                ['color', ['forecolor', 'color']],
                ['table', ['table']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['insert', ['picture']],
                ['view', ['codeview']]
            ],
            callbacks: {
                onImageUpload: function (file) {
                    const img = new FormData();
                    img.append("file", file[0]);

                    $ajax.postMultiPart({
                        url: "/api/v1/files/edit/upload",
                        data: img,
                        success: function (imgUrl) {
                            target.summernote('insertImage', imgUrl);
                        }
                    });
                },
            }
        });

        target.summernote('fontName', "맑은 고딕");
        target.summernote('fontSize', "16");
    }
}

var chart;
$chart = {
    init: function () {
        // if (!isNull($("#chart"))) {
        //     $("#chart").remove();
        //     $("#chart_cont").append('<div class="tab-item" id="chart"></div>');
        // }

        let el = document.getElementById('chart');
        if ($.trim(el) != "") {
            let data = {
                categories: [],
                series: [
                    {
                        name: '',
                        data: [],
                    },
                ],
            };
            let options = {
                chart: {width: 'auto', height: 'auto'},
                legend: {
                    align: 'bottom',
                },
                exportMenu: {
                    visible: false
                },
            };

            chart = toastui.Chart.lineChart({el, data, options});
        }
    },
    dataInit: function () {
        let data = {
            categories: [],
            series: [
                {
                    name: '',
                    data: [],
                },
            ],
        };
        chart.setData(data);
    }
}

$files = {
    types: ['gif', 'jpg', 'jpeg', 'png', 'bmp', 'xlsx', 'xls', 'doc', 'docx', 'pdf', 'hwp', 'hwpx', 'ppt', 'pptx', 'txt', 'zip', 'mp4'],
    maxSize: 10485760,
    filesMap: new Map(),
    init: function () {
        var _this = this;

        $(".file-input").prop('multiple', true);
        $(".only-input").prop('multiple', false);

        $(".file-input").on("change", function () {
            $.each(this.files, function (idx, file) {
                _this.addFile(file, "multy");
            });
        })

        $(".only-input").on("change", function () {
            $.each(this.files, function (idx, file) {
                _this.addFile(file, "single");
            });
        })
    },
    removeFile: function (file) {
        const filename = $(file).siblings('span').text();
        this.filesMap.delete(filename);
        $(file).closest('.file_wraper').remove();

        if ($("input[id^='orgFile_']").length > 0) {
            $("input[id^='orgFile_']").each(function () {
                $(this).remove();
            })
        }
    },
    addFile: function (file, type) {
        let ext = file.name.substring(file.name.lastIndexOf('.') + 1, file.name.length).toLowerCase();

        if (!$files.types.includes(ext) || isNull(file.type)) {
            alert("해당 파일은 업로드 할 수 없습니다.");
            return false;
        } else {
            if (file.size > $files.maxSize) {
                alert("파일은 "+$files.maxSize+"KB까지 업로드할 수 있습니다.");
                return false;
            }
            let filename = file.name;

            if (this.filesMap[filename]) return;

            if (type == 'single') {
                if (this.filesMap.size > 0) {
                    if (!confirm("이미 등록된 첨부파일이 존재합니다. 변경하시겠습니까?")) {
                        $(".filenames").children().remove();
                        return false;
                    }
                }

                this.filesMap = new Map();
                this.filesMap.set(filename, file);
            } else {
                this.filesMap.set(filename, file);
            }

            const template = `
                    <li class="file_wraper">
                        <span>${filename}</span>
                        <button class="btn-remove" type="button" onclick="$files.removeFile(this)"><img src="${$url.getHostUrl()}/images/common/file_delete_ico.svg"></button>
                    </li>`;

            $(".filenames").append(template);
        }
    },

    /* 드래그앤드랍 전용 파일 input */
    onlyAdd: function (file) {
        let filename = file.name;
        $(".filenames").children().remove();

        if (this.filesMap[filename]) return;

        this.filesMap = new Map();
        this.filesMap.set(filename, file);
        const template = `
                    <li class="file_wraper">
                        <span>${filename}</span>
                        <button class="btn-remove" type="button" onclick="$files.removeFile(this)"><img src="${$url.getHostUrl()}/images/common/file_delete_ico.svg"></button>
                    </li>`;

        $(".filenames").append(template);
    },

    getFiles: function () {
        return Array.from(this.filesMap.values());
    },

    validFileCount(text = null) {
        if (text == null) text = '첨부 파일은 필수 항목입니다.';

        if ($files.filesMap.size < 1) {
            alert(text);
            return false;
        }

        return true;
    }
}

$dragAndDrop = {
    defaultType: 'single',
    init: function () {
        if ($("input[type=file]").length == 1) {
            const section = document.querySelector('#section');
            const uploadBox = section.querySelector('.input-file');

            if (uploadBox) {
                const inputFile = uploadBox.querySelector('input[type=file]');
                this.defaultType = $(inputFile).prop('multiple') ? 'multy' : 'single';

                /* 박스 안에서 Drag를 Drop했을 때 */
                section.addEventListener('drop', this.drop);
                section.addEventListener('drag', this.dragover)

                /* 박스 안에 Drag를 하고 있을 때 */
                section.addEventListener('dragover', this.dragover);

                section.addEventListener("dragenter", this.dragenter);
                section.addEventListener("dragleave", this.dragleave);
            }
        }
    },
    drop: function (e) {
        e.preventDefault();
        $(".input-file").css('background', '#F9FBFC');

        let type = $dragAndDrop.defaultType;

        if (type == 'single') {
            if (e.dataTransfer.files.length > 1) {
                alert("2개 이상의 파일은 업로드할 수 없습니다.");
                return false;
            }

            let data = e.dataTransfer.files[0];
            let ext = data.name.substring(data.name.lastIndexOf('.') + 1, data.name.length).toLowerCase();

            if (!$files.types.includes(ext) || isNull(data.type)) {
                alert("해당 파일은 업로드 할 수 없습니다.");
                return false;
            } else {
                $files.onlyAdd(e.dataTransfer.files[0], $(".filenames"));
            }
        } else {
            let result = 'Y';
            for (let i = 0; i < e.dataTransfer.files.length; i++) {
                let name = e.dataTransfer.files[i]['name'];
                let type = e.dataTransfer.files[i]['type']
                let ext = name.substring(name.lastIndexOf('.') + 1, name.length).toLowerCase();
                if (!$files.types.includes(ext) || isNull(type)) {
                    alert("업로드할 수 없는 파일이 존재합니다.");
                    result = 'N';
                    break;
                }
            }

            if (result == 'Y') {
                $.each(e.dataTransfer.files, function (idx, file) {
                    $files.addFile(file, $("#filenames"));
                });
            }
        }
    },
    dragenter: function (e) {
        e.stopPropagation();
        e.preventDefault();
        $(".input-file").css('background', '#E3F2FC');
    },
    dragover: function (e) {
        e.stopPropagation();
        e.preventDefault();
        $(".input-file").css('background', '#E3F2FC');
    },
    dragleave: function (e) {
        e.stopPropagation();
        e.preventDefault();
        $(".input-file").css('background', '#F9FBFC');
    }
}

$api = {
    addressApi: function (zipCodeId = 'zip',
                          addressId = 'adres',
                          detailId = 'detailAdres',
                          sido = 'ctprvn',
                          sigungu = 'signgu') {
        new daum.Postcode({
            oncomplete: function (data) { //선택시 입력값 세팅
                document.getElementById(zipCodeId).value = data.zonecode; // 주소 넣기
                document.getElementById(addressId).value = data.address; // 주소 넣기
                document.getElementById(sido).value = data.sido;
                document.getElementById(sigungu).value = data.sigungu;
                document.getElementById(detailId).focus();
            }
        }).open();
    },
    certifyMsg: function (name, mbtlnum) {
        if (!isNull(mbtlnum)) {
            $ajax.post({
                url: "/common/mbtlnum/certify",
                data: {name: name,
                    mbtlnum: mbtlnum},
                success: function (num) {
                    if (isNull(num)) {
                        alert("인증번호를 보내는 과정에 오류가 발생했습니다.");
                        return false;
                    } else {
                        console.log(num)
                        certifyNum = num;
                        phoneCheck = 'Y';
                    }
                }
            })
        } else {
            alert("전화번호를 입력해 주세요.");
            return false;
        }
    }
    /*
    certifyMail: function (email) {
        if (!isNull(email)) {
            $ajax.post({
                url: "/common/mail/certify",
                data: {email: email},
                success: function (num) {
                    if (isNull(num)) {
                        alert("메일을 보내는 과정에 오류가 발생했습니다.");
                        return false;
                    } else {
                        console.log(num)
                        certifyMailNum = num;
                        mailCheck = 'Y';
                    }
                }
            })
        } else {
            alert("이메일을 입력해 주세요.");
            return false;
        }
    }
    */
}

$valid = {
    delete: function () {
        return confirm("삭제 하시겠습니까?")
    },
    deletes: function (selectCondition) {
        if (!selectCondition) {
            alert("삭제할 항목을 선택해 주세요.");
        } else {
            return confirm("선택된 항목을 삭제 하시겠습니까?")
        }
    },
    duplicate: function (option) {
        var result = true;
        $.ajax({
            url: option.url,
            type: 'POST',
            data: JSON.stringify(option.data),
            contentType: 'application/json',
            async: false,
        }).done(function (data) {
            result = data;
        });

        return result
    },
    modalInputValid: function () {
        let result = true;
        $.each($("input[valid='modal']"), function () {
            let value = this.value;
            let title = this.title;
            if (value == null || value == '') {
                alert(`${title}을(를) 입력해주세요.`);
                $(this).focus();
                return result = false;
            }
        })

        return result;
    },
    periodValid: function (start = moment().format('YYYY-MM-DD'), end = moment().format('YYYY-MM-DD')) {
        let result = true;
        if (new Date(start) >= new Date(end)) {
            alert("기간을 다시 입력해주세요.");
            result = false;
        }

        return result;
    },
    dataValidation: function () {
        let result = true;

        $.each($("[valid*='id']"), function () {
            var regExp = /^[a-zA-Z0-9]{4,19}$/g;
            let r = regExp.test(this.value);

            if (!r) {
                alert(`${this.title}을(를) 확인해 주세요.`);
                $(this).focus();
            }

            return result = r;
        })
        if (!result) return false;

        $.each($("[valid*='pwd']"), function () {
            var regExp = /^(?=.*?[A-Za-z])(?=.*?[0-9])(?=.*?[!@#$]).{9,12}$/
            let r = regExp.test(this.value);

            if (!r) {
                alert(`${this.title}을(를) 확인해 주세요.`);
                $(this).focus();
            }

            return result = r;
        })
        if (!result) return false;

        $.each($("[valid*='email']"), function () {
            var regExp = /(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/
            let r = regExp.test(this.value);

            if (!r) {
                alert(`${this.title}을(를) 확인해 주세요.`);
                $(this).focus();
            }

            return result = r;
        })
        if (!result) return false;

        $.each($("[valid*='bizrno']"), function () {
            var r = true;
            var bizrno = this.value;
            var numberMap = bizrno.replace(/-/gi, '').split('').map(function (d) {
                return parseInt(d, 10);
            });

            if (numberMap.length == 10) {
                var keyArr = [1, 3, 7, 1, 3, 7, 1, 3, 5];
                var chk = 0;

                keyArr.forEach(function (d, i) {
                    chk += d * numberMap[i];
                });

                chk += parseInt((keyArr[8] * numberMap[8]) / 10, 10);

                r = Math.floor(numberMap[9]) === ((10 - (chk % 10)) % 10);

                if (!r) {
                    alert(`${this.title}가 유효한 패턴이 아닙니다.`);
                    $(this).focus();
                }
            } else {
                alert(`${this.title}의 자릿수를 확인해주십시오.`);
                $(this).focus();
                r = false;
            }
            return r;
        })
        if (!result) return false;

        $.each($("[valid*='num']"), function () {
            var regExp = /^[0-9]$/g;
            let r = regExp.test(this.value);

            if (!r) {
                alert(`${this.title}는 숫자만 입력 가능합니다.`);
                $(this).focus();
            }

            return result = r;
        })
        if (!result) return false;

        $.each($("[valid*='length']"), function () {
            let length = 0;
            if ($(this).attr('length')) {
                length = $(this).attr('length');
            }

            if (this.value.length < length) {
                alert(`${this.title}는 ${length}자리 까지 입력 가능합니다.`);
                $(this).focus();
                return result = false
            }

            return result = true;
        })
        if (!result) return false;

        $.each($("[valid*='notNull']"), function () {
            if (this.value == null || this.value == '' || this.value.length == 0) {
                alert(`${this.title}는 필수 항목입니다.`);
                $(this).focus();
                return result = false;
            }

            return result = true;
        })
        if (!result) return false;

        $.each($("[valid*='phone']"), function () {
            var regExp = /^\d{3}-\d{4}-\d{4}$/;
            let r = regExp.test(this.value);

            if (!r) {
                alert(`${this.title}의 패턴이 유효하지 않습니다.`);
                $(this).focus();
                return result = false;
            }
        })
        if (!result) return false;

        return true;
    }
}

$checkBox = {
    init: function () {
        $('input:checkbox[check="all"]').on('click', function () {
            const table = $(this).closest('table');
            table.find('input:checkbox').prop("checked", $(this).is(":checked"));
        });

        $('input:checkbox[checkType="radio"]').click(function () {
            $('input:checkbox[checkType="radio"]').prop("checked", false);
            $(this).prop("checked", true);
        });
    },
    getAllChecked: function (target = $("table")) {
        var result = [];
        target.find('input:checkbox[check!="all"]:checked').each(function () {
            result.push($(this).val());
        });

        return result;
    }
}

$form = {
    getData: function (target = $("form")) {
        if ($valid.dataValidation()) {
            const formData = new FormData(target[0]);

            if ($("input[type='file']").length) {
                formData.delete("files");

                for (const file of $files.getFiles()) {
                    formData.append("files", file);
                }

                return formData;
            } else {
                return Object.fromEntries(formData);
            }
        }
    }
}

$formatter = {
    init: function () {
        this.userIdFormatter();
        this.yearFormatter();
        this.emailFormatter();
        this.phoneNumFormatter();
        this.numberFormatter();
        this.moneyFormatter();
        this.bizrnoFormatter();
        this.engFormatter();
    },

    userIdFormatter: function () {
        $("input[formatter='userId']").keyup(function () {
            this.value = parse(this.value);
        });

        const parse = function (value) {
            if (!value) {
                return "";
            }

            return value.replace(/[^a-z0-9@._-]/g, "");
        }
    },

    emailFormatter: function () {
        $("input[formatter='email']").keyup(function () {
            this.value = parse(this.value);
        });

        const parse = function (value) {
            if (!value) {
                return "";
            }

            return value.replace(/[^a-z0-9@._-]/g, "");
        }
    },

    yearFormatter: function () {
        $("input[formatter='year']").keyup(function () {
            $(this).attr("maxlength", 4);
            this.value = parse(this.value);
        });

        const parse = function (value) {
            if (!value) {
                return "";
            }
            return value.replace(/[^0-9]/g, "");
        }
    },

    phoneNumFormatter: function () {
        $("input[formatter='phoneNum']").keyup(function () {
            $(this).attr("maxlength", 13);
            this.value = parse(this.value);
        });

        const parse = function (value) {
            if (!value) {
                return "";
            }

            value = value.replace(/[^0-9]/g, "");

            let result = [];
            let restNumber = "";

            if (value.startsWith("02")) {
                if (value.length == "10") {
                    $(this).val(value.substring(0, 2) + "-" + value.substring(2, 6) + "-" + value.substring(6));
                } else {
                    $(this).val(value.substring(0, 2) + "-" + value.substring(2, 5) + "-" + value.substring(5));
                }

                result.push(value.substring(0, 2));
                restNumber = value.substring(2);

            } else if (value.startsWith("1")) {
                restNumber = value;
            } else {
                result.push(value.substring(0, 3));
                restNumber = value.substring(3);
            }

            if (restNumber.length === 7) {
                result.push(restNumber.substring(0, 3));
                result.push(restNumber.substring(3));
            } else {
                result.push(restNumber.substring(0, 4));
                result.push(restNumber.substring(4));
            }

            return result.filter((val) => val).join("-");
        };
    },

    numberFormatter: function () {
        $("input[formatter='number']").keyup(function () {
            this.value = parse(this.value);
        });

        const parse = function (value) {
            if (!value) {
                return "";
            }

            value = value.replace(/[^0-9]/g, "");
            return new Intl.NumberFormat().format(value)
        };
    },

    moneyFormatter: function () {
        const parse = function (value) {
            if (!value || value == 0) return 0;

            value = value.replace(/[^0-9]/g, "");
            return new Intl.NumberFormat().format(value)
        }

        $("input[formatter='money']").each(function () {
            $(this).attr("maxlength", 15);
            this.style.textAlign = "right";
            this.value = parse(this.value);
        });

        $("input[formatter='money']").keyup(function () {
            this.value = parse(this.value);
        });
    },

    bizrnoFormatter: function () {
        $("input[formatter='bizrno']").keyup(function () {
            $(this).attr("maxlength", 12);
            $(this).val(parse([3, 2, 5], $(this).val()));
        });

        const parse = function (arr, str) {
            str = str.replace(/[^0-9]/g, '');
            var delimiter = '-';
            var result = '';
            var fromIndex = 0;
            var strlen = str.length;
            for (var i = 0; i < arr.length; i++) {
                var currentStrCnt = arr[i];
                result += str.substr(fromIndex, currentStrCnt);
                fromIndex += currentStrCnt;
                if (strlen <= fromIndex) {
                    result += str.substr(fromIndex, strlen);
                    break;
                }
                if (i < arr.length - 1) {
                    result += delimiter;
                }
            }
            return result;
        };
    },

    engFormatter: function () {
        $("input[formatter='eng']").keyup(function () {
            this.value = parse(this.value);
        });

        const parse = function (value) {
            if (!value) {
                return "";
            }

            return value.replace(/[ㄱ-힣~!@#$%^&*()_+|<>?:{}= ]/g, "");
        }
    },
};

$view = {
    domain: function () {
        if (isNull(domain)) {
            return $url.getPath();
        }  else {
            return domain;
        }
    },
    main: function () {
        if (localStorage.getItem("queryString")) {
            location.href = `${$view.domain()}?${localStorage.getItem("queryString")}`;
        } else {
            location.href = $view.domain();
        }
    },
    add: function () {
        this.setSearchParam();
        location.href = $view.domain() + '/add';
    },
    detail: function (id, setParam = true) {
        if (setParam) this.setSearchParam();
        location.href = $view.domain() + `/${id}`;
    },
    edit: function (id) {
        location.href = $view.domain() + `/${id}/edit`;
    },

    setSearchParam: function () {
        localStorage.setItem("queryString", new URLSearchParams(location.search).toString());
    },
    allView: function () {
        let sessionUser = $user.getSessionUser();
        localStorage.setItem("queryString", "viewCd=" + sessionUser.upperOrgCd + "&allYn=Y");
        this.main();
    }
}

$event = {
    init: function () {
        this.inputAutocompleteOff();
        this.dateInputChangeEvent();
        $checkBox.init();
        $files.init();
        $dragAndDrop.init();
        $chart.init();
        $formatter.init();
        this.tableTextNull();

        if ($("#sel0").length > 0) {
            this.orgCdSelectSetting()
            this.orgCdDefaultCodeSetting();
        }
    },
    inputAutocompleteOff: function () {
        $('input').prop("autocomplete", "off");
    },
    dateInputChangeEvent: function () {
        $('input[type="date"]').change(function () {
            if ($(this).attr("to")) {
                let target = $("#" + $(this).attr("to"));

                if (target.val() && $(this).val() > target.val()) {
                    target.val($(this).val())
                }
            } else if ($(this).attr("from")) {
                let target = $("#" + $(this).attr("from"));

                if (target.val() && $(this).val() < target.val()) {
                    target.val($(this).val())
                }
            }
        });
    },
    tableTextNull: function () {
        $.each($("table").find("td"), function () {
            if ($(this).children().length == 0 && isNull($(this).text())) {
                $(this).text('-');
            }
        })
    },
    orgCdSelectSetting: function () {
        let sessionUser = $user.getSessionUser();
        $("#sel0").on('change', function () {
            let orgCd = this.value;
            if (!isNull(orgCd)) {
                $ajax.post({
                    url: "/common/selAduOrgCd",
                    data: {},
                    success: function (list) {
                        $("#sel1").empty();
                        $("#sel2").empty();
                        $("#sel1").append(`<option value="">선택</option>`);
                        $("#sel2").append(`<option value="">선택</option>`);

                        let value = $("#sel0 option:checked").val();

                        $.each(list, function () {
                            $("#sel1").append(`<option value="${this.orgCd}">${this.orgNm}</option>`)
                        })

                        if (isNull(value)) {
                            $("#viewCdNm").val("");
                            $("#viewCd").val("");
                        } else {
                            $("#viewCdNm").val($("#sel0 option:checked").text());
                            $("#viewCd").val($("#sel0 option:checked").val());
                        }

                        if (sessionUser.upperOrgCd == $("#viewCd").val()) {
                            $("#allYn").attr("disabled", false);
                        } else {
                            $("#allYn").attr("disabled", true);
                        }
                    }
                })
            } else {
                $("#sel1").empty();
                $("#sel2").empty();
                $("#viewCdNm").val("");
                $("#viewCd").val("");

                $("#sel1").append('<option>선택</option>');
                $("#sel2").append('<option>선택</option>');
            }
        })

        $("#sel1").on('change', function () {
            $ajax.post({
                url: "/common/selNomalOrgCd",
                data: {orgCd: $("#sel1").val()},
                success: function (list) {
                    $("#sel2").empty();

                    $("#sel2").append(`<option value="">선택</option>`);
                    if(list.length > 0){
                        $("#sel2").attr("disabled", false);
                        $.each(list, function () {
                            $("#sel2").append(`<option value="${this.orgCd}">${this.orgNm}</option>`)
                        })
                    } else {
                        $("#sel2").attr("disabled", true);
                    }

                    let value = $("#sel1 option:checked").val();
                    if (isNull(value)) {
                        if (sessionUser.orgType == 'sup' || sessionUser.orgType == 'sch') {
                            $("#viewCdNm").val((""));
                            $("#viewCd").val((""));
                        } else {
                            $("#viewCdNm").val($("#sel0 option:checked").text());
                            $("#viewCd").val($("#sel0 option:checked").val());
                        }
                    } else {
                        $("#viewCdNm").val($("#sel1 option:checked").text());
                        $("#viewCd").val($("#sel1 option:checked").val());
                    }

                    if (sessionUser.upperOrgCd == $("#viewCd").val()) {
                        $("#allYn").attr("disabled", false);
                    } else {
                        $("#allYn").attr("disabled", true);
                    }
                }
            })
        })

        $("#sel2").on('change', function () {
            let value = $("#sel2 option:checked").val();
            if (isNull(value)) {
                if (sessionUser.orgType == 'sch') {
                    $("#viewCdNm").val((""));
                    $("#viewCd").val((""));
                } else {
                    $("#viewCdNm").val($("#sel1 option:checked").text());
                    $("#viewCd").val($("#sel1 option:checked").val());
                }
            } else {
                $("#viewCdNm").val($("#sel2 option:checked").text());
                $("#viewCd").val($("#sel2 option:checked").val());
            }

            if (sessionUser.upperOrgCd == $("#viewCd").val()) {
                $("#allYn").attr("disabled", false);
            } else {
                $("#allYn").attr("disabled", true);
            }
        })
    },
    orgCdDefaultCodeSetting: function () {
        let sessionUser = $user.getSessionUser();
        const maxDepth = 3;
        var orgnztList = []
        $("#sel0").val("");
        $("#sel1").empty();
        $("#sel2").empty();
        $("#viewCdNm").val("");
        $("#viewCd").val("");
        $("#sel1").append('<option>선택</option>');
        $("#sel2").append('<option>선택</option>');
        if (!isNull(sessionUser)) {
            let orgCd = sessionUser.upperOrgCd;
            if ($.trim(searchParam.viewCd) != '') {
                orgCd = searchParam.viewCd;
            }
            $ajax.post({
                url: "/common/selUpperOrgByOrgCd",
                data: {orgCd: orgCd},
                async: false,
                success: function (res) {
                    if (res.result == "S") {
                        if (!isNull(res.data)) {
                            orgnztList = res.data;
                            let sel0Cd = orgnztList[0].orgCd;
                            $("#sel0").val(sel0Cd);
                            let s = orgnztList.length;
                            for (var i = 0; i < s; i++) {
                                $("#sel" + i).val(orgnztList[i].orgCd);
                                if (i == 0) {
                                    url = "/common/selAduOrgCd";
                                } else if (i == 1) {
                                    url = "/common/selNomalOrgCd";
                                }
                                $ajax.post({
                                    url: url,
                                    data: {orgCd: orgnztList[i].orgCd},
                                    async: false,
                                    success: function (list) {
                                        $("#sel" + (i + 1)).empty();
                                        $("#sel" + (i + 1)).append(`<option value="">선택</option>`);
                                        if (i == 0) {
                                            $.each(list, function () {
                                                $("#sel" + (i + 1)).append(`<option value="${this.orgCd}">${this.orgNm}</option>`);
                                            })
                                        } else if (i == 1) {
                                            if (list.length > 0) {
                                                $("#sel" + (i + 1)).attr("disabled", false);
                                                $.each(list, function () {
                                                    $("#sel" + (i + 1)).append(`<option value="${this.orgCd}">${this.orgNm}</option>`);
                                                })
                                            } else {
                                                $("#sel" + (i + 1)).attr("disabled", true);
                                            }
                                        }
                                        $("#sel" + (i + 1)).val(orgnztList[i].orgCd);
                                        if (maxDepth <= i || i == s - 1) {
                                            let value = $("#sel" + i + " option:checked").val();
                                            if (isNull(value)) {
                                                $("#viewCdNm").val("");
                                                $("#viewCd").val("");
                                            } else {
                                                $("#viewCdNm").val($("#sel" + i + " option:checked").text());
                                                $("#viewCd").val($("#sel" + i + " option:checked").val());
                                            }
                                            if ($("#sel" + (i + 1)).length > 0) {
                                                $("#sel" + (i + 1)).val("");
                                            }
                                            //break;
                                        }
                                    }
                                });
                            }
                        }
                    } else {
                        alert(res.msg);
                    }
                }
            });
        }
    }
}

$user = {
    getSessionUser: function () {
        let user = {};
        $ajax.post({
            url: '/user/getSessionUser',
            async: false,
            success: function (res) {
                if (res != null) {
                    user = res;
                }
            }
        })

        return user;
    },
    sessionUserCancel: function (button) {
        $(button).parent().parent().parent().addClass("fade");
    },
    sessionUserChg: function (button) {
        $ajax.post({
            url: '/common/sessionUserChg',
            async: false,
            success: function (res) {
                alert("권한을 변경 하였습니다.");
                $(button).parent().parent().parent().addClass("fade");
                location.href = '/';
            }
        })
    },
    sessionUserSysChg: function () {
        $ajax.post({
            url: '/common/sessionUserChg',
            async: false,
            success: function (res) {
                alert("권한을 변경 하였습니다.");
                location.href = '/';
            }
        })
    },
}

$modal = function () {
    this.defaultOption = {
        height: 400,
        width: 400,
        title: 'title'
    }

    var init = function (element, options) {
        this.defaultOption = $.extend({}, this.defaultOption, options);

        this.target = $(element);
        this.$content = this.target.find('.modal-content');
        this.$title = this.target.find('.modal-top h3');
        this.$body = this.target.find('.modal-container');
        this.$content.css({'height': this.defaultOption.height, 'width': this.defaultOption.width});
        this.setTitle(this.defaultOption.title);

        if (this.defaultOption.path) {
            this.setContent(this.defaultOption.path);
        }

        if (this.defaultOption.contents) {
            this.setHtmlContent(this.defaultOption.contents)
        }
    };

    var setCss = function (width, height) {
        this.$content.css({'height': height, 'width': width});
    };

    var open = function (options) {
        this.defaultOption = $.extend({}, this.defaultOption, options);

        if (this.defaultOption.width && this.defaultOption.height) {
            this.setCss(this.defaultOption.width, this.defaultOption.height);
        }

        if (this.defaultOption.path) {
            this.setContent(this.defaultOption.path);
        }

        if (this.defaultOption.contents) {
            this.setHtmlContent(this.defaultOption.contents)
        }

        $(this.target).removeClass('fade');
    };

    var close = function (id = 'modal') {
        $("#" + id).find('input[type=text], input[type=date], input[type=time], textarea').val("");
        $("#" + id).addClass('fade');
    };

    var getData = function () {
        var result = {};
        var query = this.target.find('input, textarea').serializeArray();
        query.forEach(function (data) {
            result[data.name] = data.value;
        });

        return result;
    };

    const setHtmlContent = function (html) {
        $(this.$body).html(html);
    };

    var setContent = function (path) {
        if (path) {
            var _this = this;
            $.get(path, function (data) {
                $(_this.$body).html(data);
                let h3 = $(_this.$body).find('h3')[0];
                $(h3).text(_this.defaultOption.title)
                $event.init();
            }).fail(function (error) {
                console.log(error)
                // $url.redirect("/login");
            });
        } else {
            $(this.$body).html('<div>Modal Body</div>');
        }
    };

    var setTitle = function (title) {
        this.defaultOption.title = title;
        $(this.$title).text(title);
    };

    var setData = function (obj) {
        for (key in obj) {
            $("#" + key).val(obj[key]);
        }
    };

    return {
        "init": init,
        "open": open,
        "close": close,
        "getData": getData,
        "setCss": setCss,
        "setData": setData,
        "setHtmlContent": setHtmlContent,
        "setContent": setContent,
        "setTitle": setTitle,
    }
}

const moneyToNumber = function (value) {
    return Number(value.replaceAll(",", ""));
}

const numberToMoney = function (value) {
    return new Intl.NumberFormat().format(value)
}

function isNull(val) {
    if (val == null || val == "null" || val == "" || val == undefined || val == "undefined" || (val != null && typeof val == "object" && !Object.keys(val).length))
        return true;
    else if (val.toString().replace(/\s/gi, "") == "")
        return true;
    else
        return false;
}

var searchParam = {};
$(document).ready(function () {
    $event.init();

    var pageFunctionName = "pageObj";
    if (window[pageFunctionName] && window[pageFunctionName].pageStart) {
        window[pageFunctionName].pageStart();
    }
});


$(function () {
    header();
    nav();

    $(window).resize(function () {
        header();
    });

    // searchBar 버튼
    $('#searchBtn').click(() => {
        if ($('#searchBtnText').text().endsWith('접기')) {
            $('.search-filter').removeClass('show').addClass('hide');
            $('#searchBtnText').text("상세 검색 보기");
        } else {
            $('.search-filter').removeClass('hide').addClass('show');
            $('#searchBtnText').text("상세 검색 접기");
        }
    })


    // lnb
    $('.lnb .btn-hide').on('click', function () {
        $('.lnb').toggleClass('hide');
        $('.wrap').toggleClass('wide');
        $('.hd-logo').toggleClass('on');
    });

    $('.menu-btn__mo').on('click', function () {
        $('.lnb').addClass('on');
    });
    $('.lnb .btn-close').on('click', function () {
        $('.lnb').removeClass('on');
    });

    $('.lnb-list > li > a').on('click', function () {
        $(this).siblings('.depth2').show();
        $('#community').css('display', 'none')
    });

    $('.arrow > a').on('click', function() {
        if($(this).hasClass('on')){
            $(this).removeClass('on');
            $(this).parent()[0].children.nav_board.style.display = 'none';
        }else{
            $(this).addClass('on');
            $(this).parent()[0].children.nav_board.style.display = 'block';
        }

    });


    // sms
    $('.sms-btn').on('click', function () {
        $('.sms').css('transform', 'translate(0)');
        $('.sms-wrap').css('visibility', 'visible');
        $(this).css('display', 'none');
    });
    $('.sms .modal-close').on('click', function () {
        $('.sms').css('transform', 'translate(0, calc(100% + 20px))');
        setTimeout(function () {
            $('.sms-wrap').css('visibility', 'hidden');
            $('.sms-btn').css('display', 'block');
        }, 500)
    });

    // font size
    let baseSize = 16;
    $('#sizeDown').click(function () {
        baseSize--;
        if (baseSize < 12) {
            baseSize = 12;
            return false;
        }
        $('html').css('font-size', baseSize);
    });
    $('#sizeUp').click(function () {
        baseSize++;
        if (baseSize > 20) {
            baseSize = 20;
            return false;
        }
        $('html').css('font-size', baseSize);
    });


    // datepicker
    $(".datepicker").datepicker();
    $.datepicker.setDefaults({
        dateFormat: 'yy-mm-dd',
        closeText: "닫기",
        currentText: "오늘",
        prevText: '이전 달',
        nextText: '다음 달',
        monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
        monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
        dayNames: ['일', '월', '화', '수', '목', '금', '토'],
        dayNamesShort: ['일', '월', '화', '수', '목', '금', '토'],
        dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
        weekHeader: "주",
        yearSuffix: '년',
        showMonthAfterYear: true,
        onClose: function (selectedDate) {
            if (!isNull(selectedDate)) {
                $('#endDate').attr('disabled', false);
                $('#endDate').datepicker('option', 'minDate', selectedDate);
            }
        }
    });


    // tab
    $('.tab-btn li').first().addClass('on');
    $('.tab-btn li').on('click', function () {
        var currentIdx = $(this).index();
        $(this).parent('.tab-btn').find('li').removeClass('on').eq(currentIdx).addClass('on');
        // $(this).parents('.tab-container').find('.tab-item').hide().eq(currentIdx).show();
    });


    // accordion
    var accordionTab = $('.accordion-tit'),
        accordionCont = $('.accordion-cont');

    accordionTab.on('click', function (e) {
        e.preventDefault();
        $(this).parents('.accordion').find('li').removeClass('on');
        $(this).parents('.accordion').find(accordionCont).slideUp(400);
        if ($(this).next().is(':hidden') == true) {
            $(this).parent('li').addClass('on');
            $(this).next().slideDown(400);
        }
    });


    // btn list
    const viewWidth = $(window).width();

    if (viewWidth > 1024) {
        $('.more-wrap .btn-more').on('click', function () {
            $(this).siblings('.btn-list').css('display', 'block');
        });
        $(document).mouseup(function (e) {
            if ($('.more-wrap .btn-list').has(e.target).length === 0) {
                $('.more-wrap .btn-list').hide();
            }
        });
    }

    isNull($('#startDate').val()) ? $('#endDate').attr('disabled', true) : $('#endDate').attr('disabled', false);
});

function header() {
    const viewWidth = $(window).width();

    if (viewWidth <= 1024) {
        $('.lnb').removeClass('hide');
        $('.wrap').removeClass('wide');
    } else {
        $('.lnb').removeClass('on')
    }
}

function nav() {
    if (!isNull($("#nav_board"))) {
        $ajax.post({
            url: "/api/v1/board/nav",
            success: function (list) {
                let user = $user.getSessionUser();

                list.forEach(l => {
                    let id = l.boardMngId;
                    let nm = l.boardTitle;
                    let nc = l.ncnm;
                    let url = `/${user.auth}/${user.useSeNm}/community/${nc}/${id}/all`;
                    let _class = "";
                    let _offSet = $(".lnb-list").offset();

                    if ($url.getPath().includes(`${nc}/${id}`)) {//$url.getPath() == url ->  url만 비교하면, 카테고리 or 게시판 상세 들어갔을때 비교가 되지않음.
                        $('.lnb-list .on').removeClass('on');

                        _class = 'on';
                        $("#nav_board").siblings('a').prop('class', _class);
                        $("#nav_board").css('display', 'block'); //
                    }

                    let path = `<li><a href="${url}" class="board-link ${_class}" + title="${nm}"><span>${nm}</span></a></li>`;

                    if($(".lnb-list .board-link.on").length>0){
                        _offSet = $(".lnb-list .board-link.on").offset();
                    }else if($(".lnb-list .on").length>0){
                        _offSet = $(".lnb-list .on").offset();
                    }

                    $('.lnb-top').animate({scrollTop: ((_offSet.top) - $('.lnb-top').height() / 2)+300}, 0);

                    $("#nav_board").append(path);

                })
            }
        })
    }
}
/*
* 분류코드 selectbox에 설정
* cmmnCode 분류코드
* obj 객체
*/
function selCmmnCode(cmmnCode, obj) {
    if ($("#" + obj).prop("tagName") != 'SELECT') {
        alert('selectBox를 지정해주십시오');
        return false;
    } else {
        if ($.trim(cmmnCode) != '') {
            $ajax.post({
                url: "/common/selCmmnCode",
                data: {clCode: cmmnCode},
                success: function (res) {
                    if (res == "fail") {
                        alert("분류 코드 값을 넣어주세요.");
                    } else {
                        $('#' + obj).empty();
                        $('#' + obj).append("<option value=''>선택</option>");
                        $.each(res, function (idx, item) {
                            var option = $("<option value='" + idx + "'>" + item + "</option>");
                            $('#' + obj).append(option);
                        })
                    }
                }
            })
        } else {
            alert('클래스 코드명을 적어주십시오');
            return false;
        }
    }
}
// SMS 글자수 제한
function byteSizeCheck(obj, maxByte) {
    let totalByte = 0;
    let maxLength = 0;
    let message = obj.value;
    for (let i = 0; i < message.length; i++) {
        let currentByte = message.charCodeAt(i);
        if (currentByte > 128) {
            totalByte += 2;
        } else {
            totalByte++;
        }
        if (totalByte <= maxByte) {
            maxLength = i + 1;
        }
    }

    $(".bytes").text(totalByte + ' / 80bytes');

    if (totalByte > maxByte) {
        alert("SMS 발송은" + maxByte + "Byte를 초과 입력할 수 없습니다.")
        obj.value = message.substring(0, maxLength);
        $(".bytes").text(maxByte + ' / 80bytes')
    }
}