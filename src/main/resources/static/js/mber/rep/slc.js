var pageObj = {
    insttAddBtn: function () {
        let orgCd = $("#viewCd").val();
        if (orgCd == null || orgCd == '' ) {
                alert("기관(학교)를 선택해 주세요.");
        } else {
            $ajax.post({
                url: "/mber/slc/insertSlc",
                data: {orgCd: orgCd},
                success: function (response) {
                    console.log(response)
                    if (!isNull(response)) {
                        if(!isNull(response.msg)) {
                            alert(response.msg);
                        } else {
                            alert("요청이 완료되었습니다.");
                            location.reload();
                            obj.view.detail(response);
                        }
                    } else {
                        alert("오류가 발생했습니다.");
                    }
                }
            })
        }
    },
    slcDeleteBtn: function (target) {
        if (!isNull(target)) {
            $ajax.post({
                url: '/mber/slc/delete',
                data: {slctnNo: target},
                success: function (response) {
                    if (!isNull(response)) {
                        alert("삭제되었습니다.");
                        location.reload();
                    } else {
                        alert("오류가 발생했습니다.");
                    }
                }
            })
        } else {
            alert("삭제 실패");
            return false;
        }
    },
    slcStopBtn: function (target) {
        if (!isNull(target)) {
            $ajax.post({
                url: "/mber/slc/slcStop",
                data: {slctnNo: target},
                success: function (response) {
                    console.log(response);
                    if (!isNull(response)) {
                        alert("반려되었습니다.");
                        location.reload();
                        obj.view.detail(response);
                    } else {
                        alert("오류가 발생했습니다.");
                    }
                }
            })
        } else {
            alert("반려 실패");
            return false;
        }
    },
    searchBtn: function (w1) {
        $("form").submit();
    }
}
