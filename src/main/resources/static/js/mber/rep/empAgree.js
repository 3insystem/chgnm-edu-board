var pageObj = {

    search: function () {
        $form.getData();
    },
    /* 직원목록에서 체크 유무 확인후 권한 등록 테이블에서 ON/OFF
    *
    *  */
    empAgreeCheck: function (checked,mberId){
        let targetTr = $("#agreeTable").find("#" + mberId);
        //체크시
        if($(checked).prop('checked')){
            let td = $(checked).parent().parent().parent();
            var userNm = td.find("#userNm").text()
            if(targetTr.length == 0){
                $ajax.post({
                    url: "/mber/rep/selEmpAgree",
                    data: {mberId : mberId},
                    success: function (res){
                        if(res.result == "S"){
                            let jobCd;
                            let rowStart = `
                            <tr id="${mberId}">
                                <td class="center">${userNm}</td>
                                <td class="justify-center">
                                    <div class="chk-list" name="jobSe">
                                        <label class="chk-item" for="${mberId}1">
                                            <input type="checkbox" id="${mberId}1" value="MNTMG00001">
                                            <span>장애</span>
                                        </label>
                                        <label class="chk-item" for="${mberId}2">
                                            <input type="checkbox" id="${mberId}2" value="MNTMG00002">
                                            <span>설치</span>
                                        </label>
                                        <label class="chk-item" for="${mberId}3">
                                            <input type="checkbox" id="${mberId}3" value="MNTMG00003">
                                            <span>점검</span>
                                        </label>
                                        <label class="chk-item" for="${mberId}4">
                                            <input type="checkbox" id="${mberId}4" value="MNTMG00004">
                                            <span>기타</span>
                                        </label>
                                    </div>
                                </td>`
                            let rowMain =
                                `<td>
                                <div class="chk-list" id="main">`;
                            //jobSe의 종류 수 + 1
                            cnt = 5;
                            res.dto.forEach(function(item){
                                if(item.mainYn == 'Y'){
                                    rowMain = rowMain + `
                                    <label class="chk-item" for="${mberId}${cnt}">
                                        <input type="checkbox" id="${mberId}${cnt}" value="${item.orgCd}" checked>
                                        <span>${item.orgNm}</span>
                                    </label>`
                                    cnt = cnt + 1;
                                } else {
                                    rowMain = rowMain + `
                                    <label class="chk-item" for="${mberId}${cnt}">
                                        <input type="checkbox" id="${mberId}${cnt}" value="${item.orgCd}">
                                        <span>${item.orgNm}</span>
                                    </label>`
                                    cnt = cnt + 1;
                                }
                            });
                            rowMain = rowMain +
                                `   </div>
                             </td>`;

                            $("#agreeTable").append(rowStart + rowMain);

                            if(res.dto[0].chrgJob != null) {
                                jobCd = res.dto[0].chrgJob.split(',');
                                //jobSe의 종류 수
                                jobChkbox = $("#agreeTable > tr").last().find("[name=jobSe] > > [type=checkbox]");
                                jobChkbox.each(function(idx,i){
                                    jobCd.forEach(function (item){
                                        if(i.value == item){
                                            i.checked = true;
                                        }
                                    });
                                });
                            }
                        } else {
                            alert("멤버 조회에 실패했습니다.");
                        }
                    }
                });
            }
            //체크해제
        } else {
            if(targetTr.length > 0){
                $("#agreeTable").find("#" + mberId).remove();
            }
        }
    },
    delete: function (id){
        if(!confirm("해당 버튼을 누르면 권한이 해제됩니다.")){
            return false;
        } else {
            $ajax.post({
                url: "/mber/rep/deleteEmpAgree",
                data: {'usid': id},
                success: function (res){
                    if(res.result == 'S'){
                        $view.setSearchParam();
                        $view.main();
                    }
                }
            });
        }
    },
    save: function () {
        let data = {};
        let emAllList = [];
        let ciAllList = [];
        //row 단위로 저장
        if ($("#agreeTable > tr").length > 0) {
            $("#agreeTable > tr").each(function (idx, item) {
                let em = {};
                //em 데이터 작성
                emTemp = $(item).find("[name=jobSe] > > [type=checkbox]:checked");
                if( emTemp.length > 0){
                    let jobSe="";
                    let jobList = [];
                    emTemp.each(function (emIdx, emItem) {
                        jobList.push(emItem.value)
                    })
                    jobSe = jobList.toString();
                    em["chrgJob"] = jobSe;
                } else {
                    em["chrgJob"] = null;
                }
                em["chrgId"] = item.id;
                //ci 데이터 작성
                $(item).find("#main > > [type=checkbox]:checked").each(function (emIdx, emItem) {
                    let ci = {chrgId: item.id,
                        orgCode: emItem.value,
                        job: 'main'}
                    ciAllList.push(ci);
                });

                $(item).find("#cons > > [type=checkbox]:checked").each(function (emIdx, emItem) {
                    let ci = {chrgId: item.id,
                        orgCode: emItem.value,
                        job: 'cons'}
                    ciAllList.push(ci);
                });
                emAllList.push(em);
            });

            data["em"] = emAllList;
            data["ci"] = ciAllList;

            $ajax.post({
                url: "/mber/rep/saveEmpAgree",
                data: {'data': data},
                success: function (res){
                    if(res.result == 'S'){
                        $view.setSearchParam();
                        $view.main();
                    }
                }
            });
        } else {
            alert("저장할 항목이 없습니다");
        }
    }
}
const domain = $url.getPath();
pageObj.pageStart = function () {
    // selCmmnCode('CONFM', 'confmSe');
};
