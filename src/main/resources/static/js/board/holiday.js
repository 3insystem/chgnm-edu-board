let submitFlag = false;
const doubleSubmit = () => {
    if (!submitFlag) {
        submitFlag = true;
        return false;
    } else {
        return true;
    }
}

var pageObj = {
    addModal: function () {
        modal.open({
            title: "공휴일/휴일 등록",
            path: `/adm/edu/boardMng/holiday/add`
        })
    },
    detail: function (boardId) {
        modal.open({
            title: "공휴일/휴일 수정",
            path: `/adm/edu/boardMng/holiday/add/` + boardId
        })
    },
    save: function () {
        let data = modal.getData();
        if(valueAbleChk()) {
            if(!doubleSubmit()) {
                $ajax.post({
                    url: "/adm/edu/boardMng/holiday/add",
                    data: data,
                    success: function (res) {
                        console.log(res)
                        if (!isNull(res)) {
                            alert("휴일 등록이 완료되었습니다.");
                            location.reload();
                        } else {
                            alert("휴일 등록 중 에러가 발생했습니다.");
                            return false;
                        }
                    }
                })
            }
        }
    },
    delete: function () {
        let data = [];
        let list = $checkBox.getAllChecked($("#tb_holiday"));

        if (list.length > 0) {
            if (!confirm(list.length + "개의 공휴일을 삭제 하시겠습니까?")) {
                return false;
            }
        } else {
            alert("삭제할 공휴일을 선택 해주세요.");
            return false;
        }

        list.forEach(no => {
            let obj = {};
            obj.holidayId = no;
            data.push(obj);
        })

        $ajax.post({
            url: "/adm/edu/boardMng/holiday/del",
            data: data,
            success: function () {
                alert("삭제가 완료되었습니다.");
                location.reload();
            }
        })
    }
}

let modal = $modal();
$(document).ready(function () {
    trClickEvent();
    modal.init($("#modal"), {
        width: 1200,
        height: 1600
    });
})

function trClickEvent() {
    $("#faqList > td").on('click', function () {
        let boardId = $(this).parent().attr("value");
        let chk = $(this).attr("id") /* 체크 버튼 확인 */

        if (chk != "chkId") {
            pageObj.detail(boardId);
        }
    })
}

function valueAbleChk() {
    if($('#modalStartDate').val() == null || $('#modalStartDate').val() == "") {
        alert("시작일을 입력해 주세요.")
        return false;
    }
    
    if($('#modalEndDate').val() == null || $('#modalEndDate').val() == "") {
        alert("종료일을 입력해 주세요.")
        return false;
    }
    
    if($('#title').val() == null || $('#title').val() == "") {
        alert("휴일명을 입력해 주세요.")
        return false;
    }

    return true;
}