var domain;
var pageObj = {
    mainPage: function () {
        domain = `${defaultPageUrl}/${categoryId}`;
        $view.main();
    },
    addPage: function () {
        $ajax.post({
            url: "/api/v1/board/getCategoryCount/"+boardMngId,
            success: function (res) {
                if (res <= 0) {
                    alert("등록된 카테고리가 없습니다. 카테고리를 설정해 주세요.");
                    location.href = "/adm/edu/boardMng/"+boardMngId+"/edit";
                } else {
                    domain = `${defaultPageUrl}/${categoryId}`;
                    $view.add();
                }
            }
        })
    },
    detailPage: function (boardId) {
        domain = `${defaultPageUrl}/${categoryId}`;
        $view.detail(boardId)
    },
    updatePage: function (boardId) {
        domain = `${defaultPageUrl}/${categoryId}`;
        $view.edit(boardId);
    },
    save: function () {
        let data = $form.getData();
        if (this.validation(data)) {
            $ajax.postMultiPart({
                url: "/api/v1/board/board/add",
                data: data,
                success: function (res) {
                    if (!isNull(res)) {
                        alert("게시물 저장이 완료되었습니다.");
                        pageObj.detailPage(res);
                    } else if(res === 0) {
                        alert("개인정보보호법 제24조 및 제29조에 따라 홈페이지를 통한 고유식별번호 등록을 차단하고 있습니다. \n 귀하가 작성하신 글은 고유식별번호를 포함하므로 등록을 제한합니다.");
                        return false;
                    } else {
                        alert("게시물 저장 중 에러가 발생했습니다.");
                        return false;
                    }
                }
            })
        }
    },
    update: function (boardId) {
        let data = $form.getData();
        data.append("boardId", boardId);
        if (this.validation(data)) {
            $ajax.postMultiPart({
                url: "/api/v1/board/board/edit",
                data: data,
                success: function (res) {
                    if (!isNull(res)) {
                        alert("게시물 저장이 완료되었습니다.");
                        pageObj.detailPage(boardId)
                    } else if(res === 0) {
                        alert("개인정보보호법 제24조 및 제29조에 따라 홈페이지를 통한 고유식별번호 등록을 차단하고 있습니다. \n 귀하가 작성하신 글은 고유식별번호를 포함하므로 등록을 제한합니다.");
                        return false;
                    } else {
                        alert("게시물 저장 중 에러가 발생했습니다.");
                        return false;
                    }
                }
            })
        }
    },
    delete: function (boardId) {
        if (confirm("삭제하시겠습니까?")) {
            let reason = prompt("삭제사유", "");

            if(reason !== null && reason !== undefined) {
                let data = {
                    boardId: boardId,
                    delReason: reason
                };

                $ajax.post({
                    url: "/api/v1/board/board/delete",
                    data: data,
                    success: function (res) {
                        if (isNull(res) || res <= 0) {
                            alert("게시물 삭제 중 에러가 발생했습니다.");
                            return false;
                        } else {
                            alert("게시물 삭제가 완료되었습니다.");
                            pageObj.mainPage();
                        }
                    }
                })
            } else {
                alert("삭제사유를 입력해 주세요.");
                return false;
            }

        }
    },
    deleteAll: function () {
        let checked = $checkBox.getAllChecked();
        if (checked.length > 0) {
            if (confirm("삭제하시겠습니까?")) {
                let reason = prompt("삭제사유", "");

                if(reason.length > 0) {
                    let data = {
                        boardIdList: checked,
                        delReason: reason
                    };
                    $ajax.post({
                        url: "/api/v1/board/board/deleteAll",
                        data: data,
                        success: function (res) {
                            if (isNull(res) || res == 0) {
                                alert("게시물 삭제 과정에서 오류가 발생했습니다.");
                                return false;
                            } else {
                                alert("게시물 삭제가 완료되었습니다.");
                                location.reload();
                            }
                        }
                    })
                } else {
                    alert("삭제사유를 입력해 주세요.");
                    return false;
                }
            }
        } else {
            alert("삭제할 항목을 선택해 주세요.");
        }
    },
    restore: function (boardId) {
        if (confirm("복구하시겠습니까?")) {
            let reason = prompt("복구사유", "");

            if(reason !== null && reason !== undefined) {
                let data = {
                    boardId: boardId,
                    restoreReason: reason
                };

                $ajax.post({
                    url: "/api/v1/board/board/restore",
                    data: data,
                    success: function (res) {
                        if (isNull(res) || res <= 0) {
                            alert("게시물 복구 중 에러가 발생했습니다.");
                            return false;
                        } else {
                            alert("게시물 복구가 완료되었습니다.");
                            pageObj.mainPage();
                        }
                    }
                })
            } else {
                alert("복구사유를 입력해 주세요.");
                return false;
            }
        }
    },
    restoreAll: function () {
        let checked = $checkBox.getAllChecked();
        if (checked.length > 0) {
            if (confirm("복구하시겠습니까?")) {
                let reason = prompt("복구사유", "");

                if(reason.length > 0) {
                    let data = {
                        boardIdList: checked,
                        restoreReason: reason
                    };
                    $ajax.post({
                        url: "/api/v1/board/board/restoreAll",
                        data: data,
                        success: function (res) {
                            if (isNull(res) || res == 0) {
                                alert("게시물 복구 과정에서 오류가 발생했습니다.");
                                return false;
                            } else {
                                alert("게시물 복구가 완료되었습니다.");
                                location.reload();
                            }
                        }
                    })
                } else {
                    alert("복구사유를 입력해 주세요.");
                    return false;
                }
            }
        } else {
            alert("복구할 항목을 선택해 주세요.");
        }
    },
    comment: function (boardId) {
        if (isNull($("#comment").val())) {
            alert("댓글을 입력해 주세요.");
            $("#comment").focus();
            return false;
        }

        let data = {
            status: $("[name=status]:checked").val(),
            comment: $("#comment").val(),
            boardId: boardId
        };

        $ajax.post({
            url: "/api/v1/board/board/comment",
            data: data,
            success: function (res) {
                if (isNull(res) || res == 0) {
                    alert("댓글 저장 과정에서 오류가 발생했습니다..");
                    return false;
                } else {
                    alert("게시물 댓글 저장이 완료되었습니다.");
                    location.reload();
                }
            }
        })
    },
    validation: function (data) {
        let result = true;
        let banWord = setup.banWord.split(",");
        for (const [key, value] of data.entries()) {
            if (key == 'boardCategoryId' && isNull(value)) {
                alert("카테고리를 선택해 주세요.");
                $("#boardCategoryId").focus();
                result = false;
                break;
            }

            if (key == 'title' && isNull(value)) {
                alert("제목을 선택해 주세요.");
                $("#title").focus();
                result = false;
                break;
            }

            if (key == 'content') {
                if (isNull(banWord)) {
                    result = true;
                } else {
                    for (let i = 0; i < banWord.length; i++) {
                        let v = value.replace(/(<([^>]+)>)/gi, '');
                        if (v.includes(banWord[i])) {
                            alert("금지 단어가 포함되어 있습니다. [\""+banWord[i]+"\"]");
                            $("#content").focus();
                            result = false;
                            break;
                        }
                    }
                    if (result == false) break;
                }
            }
        }

        if (isNull($("[name=nuriNum]:checked").val())) {
            alert("공공누리 유형을 선택해 주세요.");
            return false;
        }

        return result;
    }
}

pageObj.pageStart = function () {
    boardEvent.init(boardMngId);
};