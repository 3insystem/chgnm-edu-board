let submitFlag = false;
const doubleSubmit = () => {
    if (!submitFlag) {
        submitFlag = true;
        return false;
    } else {
        return true;
    }
}

var pageObj = {
    mainPage: function () {
        domain = `${defaultPageUrl}/${categoryId}`;
        $view.main();
    },
    addPage: function () {
        localStorage.setItem("queryString", new URLSearchParams(location.search).toString()); // 스토리지에 페이지 정보 저장
        location.href = `${defaultPageUrl}/${categoryId}/add`;
    },
    detail: function (boardId) {
        localStorage.setItem("queryString", new URLSearchParams(location.search).toString()); // 스토리지에 페이지 정보 저장
        location.href = `${defaultPageUrl}/${categoryId}/` + boardId;
    },
    close: function () {
        let url = $url.getPath();
        let urldetail = url.indexOf("all");
        url = url.substring(0,urldetail);
        window.location.href = url + "all?" + localStorage.getItem("queryString"); // 이전 페이지로 이동
        localStorage.setItem("queryString","");
    },
    save: function () {
        let url = $url.getPath();
        let data = $form.getData();
        if (this.validation(data)) {
            if(!doubleSubmit()) {
                $ajax.postMultiPart({
                    url: "/api/v1/board/faq/add",
                    data: data,
                    success: function (res) {
                        console.log(res)
                        if (!isNull(res)) {
                            let urldetail = url.indexOf("all");
                            url = url.substring(0,urldetail);
                            alert("게시물 저장이 완료되었습니다.");
                            window.location.href = url + "all?" + localStorage.getItem("queryString"); // 이전 페이지로 이동
                        } else {
                            alert("개인정보보호법 제24조 및 제29조에 따라 홈페이지를 통한 고유식별번호 등록을 차단하고 있습니다. \n 귀하가 작성하신 글은 고유식별번호를 포함하므로 등록을 제한합니다.");
                            submitFlag = false;
                            return false;
                        }
                    }
                })
            }
        }
    },
    delete: function () {
        let data = [];
        let reason = "";
        let list = $checkBox.getAllChecked($("#tb_faq"));

        if (list.length > 0) {
            //if (!confirm(list.length + "개의 게시물을 삭제 하시겠습니까?")) {
            if (!confirm("삭제하시겠습니까?")) {
                return false;
            } else {

                reason = prompt("삭제사유", "");

                if(reason.length > 0) {
                    list.forEach(no => {
                        let obj = {};
                        obj.boardId = no;
                        obj.delReason = reason;
                        data.push(obj);
                    })

                    $ajax.post({
                        url: "/adm/edu/boardMng/faq/del",
                        data: data,
                        success: function () {
                            alert("삭제가 완료되었습니다.");
                            location.reload();
                        }
                    })
                } else {
                    alert("삭제사유를 입력해 주세요.");
                    return false;
                }
            }
        } else {
            alert("삭제할 게시물을 선택 해주세요.");
            return false;
        }

    },
    restoreAll: function () {
        let data = [];
        let reason = "";
        let list = $checkBox.getAllChecked($("#tb_faq"));

        if (list.length > 0) {
            if (!confirm(list.length + "개의 게시물을 복구 하시겠습니까?")) {
                return false;
            } else {
                reason = prompt("복구사유", "");

                if(reason.length > 0) {
                    list.forEach(no => {
                        let obj = {};
                        obj.boardId = no;
                        obj.rstReason = reason;
                        data.push(obj);
                    })

                    $ajax.post({
                        url: "/adm/edu/boardMng/faq/restoreAll",
                        data: data,
                        success: function () {
                            alert("복구가 완료되었습니다.");
                            location.reload();
                        }
                    })
                } else {
                    alert("복구사유를 입력해 주세요.");
                    return false;
                }

            }
        } else {
            alert("복구할 게시물을 선택 해주세요.");
            return false;
        }

    },
    restore: function () {
        if (confirm("복구하시겠습니까?")) {
            let reason = prompt("복구사유", "");

            if(reason.length > 0) {
                let data = {
                    boardId: $("#boardId").val(),
                    rstReason: reason
                };

                $ajax.post({
                    url: "/api/v1/board/faq/restore",
                    data: data,
                    success: function (res) {
                        if (isNull(res) || res <= 0) {
                            alert("게시물 복구 중 에러가 발생했습니다.");
                            return false;
                        } else {
                            alert("게시물 복구가 완료되었습니다.");
                            pageObj.mainPage();
                        }
                    }
                })
            } else {
                alert("복구사유를 입력해 주세요.");
                return false;
            }
        }
    },
    validation: function (data) {

        if (isNull($('#boardCategoryId').val()) || $('#boardCategoryId').val() == "") {
            alert("카테고리를 선택해 주세요.");
            $("#boardCategoryId").focus();
            return false;
        }

        if (isNull($('#title').val()) || $('#title').val() == "") {
            alert("제목을 입력해 주세요.");
            $("#title").focus();
            return false;
        }

        return true;
    }
}


$(document).ready(function () {
    boardEvent.init(boardMngId);
    trClickEvent();
})

function trClickEvent() {
    $("#faqList > td").on('click', function () {
        let boardId = $(this).parent().attr("value");
        let chk = $(this).attr("id") /* 체크 버튼 확인 */

        if (chk != "chkId") {
            pageObj.detail(boardId);
        }
    })
}