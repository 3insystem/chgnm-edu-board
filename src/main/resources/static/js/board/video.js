var pageObj = {
    mainPage: function () {
        domain = `${defaultPageUrl}/${categoryId}`;
        $view.main();
    },
    communityMainPage: function () {
        let mainPageUrl = `${defaultPageUrl}/${categoryId}`;
        mainPageUrl = mainPageUrl.replace("boardMng","community");
        domain = mainPageUrl;
        $view.main();
    },
    addPage: function () {
        domain = `${defaultPageUrl}/${categoryId}`;
        $view.add();
    },
    detailPage: function (boardId) {
        domain = `${defaultPageUrl}/${categoryId}`;
        $view.detail(boardId)
    },
    updatePage: function (boardId) {
        domain = `${defaultPageUrl}/${categoryId}`;
        $view.edit(boardId);
    },
    preview: function () {
        let link = $("#videoLink").val();
        if (isNull(link)) {
            alert("동영상 링크를 입력해 주세요.");
            $("#videoLink").focus();
            return false;
        }

        if (!link.includes('https://')) {
            alert("동영상 링크가 잘못되었습니다.\n다시 입력해 주세요.");
            $("#videoLink").focus();
            return false;
        }

        let url = new URL(link);
        let queryString = url.searchParams;
        let v = queryString.get('v');

        if (isNull(v)) {
            v = link.substring(17, link.indexOf("?"));
        }
        let src = "https://www.youtube.com/embed/" + v;
        $("#div_preview").remove();
        $("#div_link").after(`
         <div class="input-list" id="div_preview">
            <div class="label"><span>미리보기</span></div>
            <div class="input-box">
                <span class="hiddentext">미리보기 설명으로 동영상을 미리 볼 수 있습니다.</span>
                <iframe width="560" height="315" id="preview" src="${src}"
                        title="YouTube video player" frameBorder="0" allowFullScreen
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"></iframe>
            </div>
        </div>
        `);
    },
    save: function () {
        if (this.validation()) {
            let data = $form.getData();
            data['videoLink'] = pageObj.getLinkCode(data['videoLink']);

            $ajax.post({
                url: "/api/v1/board/video/add",
                data: data,
                success: function (res) {
                    if (!isNull(res)) {
                        alert("게시물 저장이 완료되었습니다.");
                        pageObj.detailPage(res);
                    } else {
                        alert("게시물 저장 중 에러가 발생했습니다.");
                        return false;
                    }
                }
            })
        }
    },
    update: function (boardId) {
        let data = $form.getData();
        data['videoLink'] = pageObj.getLinkCode(data['videoLink']);
        data['boardVideoId'] = boardId;
        if (this.validation(data)) {
            $ajax.post({
                url: "/api/v1/board/video/edit",
                data: data,
                success: function (res) {
                    if (isNull(res) || res <= 0) {
                        alert("게시물 저장 중 에러가 발생했습니다.");
                        return false;
                    } else {
                        alert("게시물 저장이 완료되었습니다.");
                        pageObj.detailPage(boardId)
                    }
                }
            })
        }
    },
    delete: function (boardId) {
        if (confirm("삭제하시겠습니까?")) {
            let reason = prompt("삭제사유", "");

            if(reason !== null && reason !== undefined) {
                let data = {
                    boardId: boardId,
                    delReason: reason
                };

                $ajax.post({
                    url: "/api/v1/board/video/delete",
                    data: data,
                    success: function (res) {
                        if (isNull(res) || res <= 0) {
                            alert("게시물 삭제 중 에러가 발생했습니다.");
                            return false;
                        } else {
                            alert("게시물 삭제가 완료되었습니다.");
                            pageObj.mainPage();
                        }
                    }
                })
            } else {
                alert("삭제사유를 입력해 주세요.");
                return false;
            }
        }
    },
    deleteAll: function () {
        let checked = $checkBox.getAllChecked();
        console.log(checked)
        if (checked.length > 0) {
            if (confirm("삭제하시겠습니까?")) {
                let reason = prompt("삭제사유", "입력");

                if(reason.length > 0) {
                    let data = {
                        boardIdList: checked,
                        delReason: reason
                    };
                    $ajax.post({
                        url: "/api/v1/board/video/deleteAll",
                        data: data,
                        success: function (res) {
                            if (isNull(res) || res == 0) {
                                alert("게시물 삭제 과정에서 오류가 발생했습니다.");
                                return false;
                            } else {
                                alert("게시물 삭제가 완료되었습니다.");
                                location.reload();
                            }
                        }
                    })
                } else {
                    alert("삭제사유를 입력해 주세요.");
                    return false;
                }
            }
        } else {
            alert("삭제할 항목을 선택해 주세요.");
        }
    },
    restore: function (videoId) {
        if (confirm("복구하시겠습니까?")) {
            let reason = prompt("복구사유", "");

            if(reason !== null && reason !== undefined) {
                let data = {
                    boardId: boardId,
                    restoreReason: reason
                };

                $ajax.post({
                    url: "/api/v1/board/video/restore",
                    data: data,
                    success: function (res) {
                        if (isNull(res) || res <= 0) {
                            alert("게시물 복구 중 에러가 발생했습니다.");
                            return false;
                        } else {
                            alert("게시물 복구가 완료되었습니다.");
                            pageObj.mainPage();
                        }
                    }
                })
            } else {
                alert("복구사유를 입력해 주세요.");
                return false;
            }
        }
    },
    restoreAll: function () {
        let checked = $checkBox.getAllChecked();
        if (checked.length > 0) {
            if (confirm("복구하시겠습니까?")) {
                let reason = prompt("복구사유", "");

                if(reason.length > 0) {
                    let data = {
                        boardIdList: checked,
                        restoreReason: reason
                    };
                    $ajax.post({
                        url: "/api/v1/board/video/restoreAll",
                        data: data,
                        success: function (res) {
                            if (isNull(res) || res == 0) {
                                alert("게시물 복구 과정에서 오류가 발생했습니다.");
                                return false;
                            } else {
                                alert("게시물 복구가 완료되었습니다.");
                                location.reload();
                            }
                        }
                    })
                } else {
                    alert("복구사유를 입력해 주세요.");
                    return false;
                }
            }
        } else {
            alert("삭제할 항목을 선택해 주세요.");
        }
    },
    validation: function () {
        if (isNull($("#boardCategoryId").val())) {
            alert("카테고리를 선택해 주세요.");
            $("#boardCategoryId").focus();
            return false;
        }

        if (isNull($("#title").val())) {
            alert("제목을 입력해 주세요.");
            $("#title").focus();
            return false;
        }

        if (isNull($("#videoLink").val())) {
            alert("동영상 링크를 입력해 주세요.");
            $("#videoLink").focus();
            return false;
        }

        if (isNull($("[name=nuriNum]").val())) {
            alert("공공누리 유형을 선택해 주세요.");
            return false;
        }

        return true;
    },
    getLinkCode: function (link) {
        let url = new URL(link);
        let queryString = url.searchParams;
        let v = queryString.get('v');
        if (isNull(v)) {
            v = link.substring(17, link.indexOf("?"));
        }
        return v;
    }
}

pageObj.pageStart = function () {
    boardEvent.init(boardMngId);
};