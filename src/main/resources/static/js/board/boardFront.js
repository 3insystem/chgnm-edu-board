var domain = "/adm/edu/community";

var pageObj = {
    mainPage: function () {
        domain = `${defaultPageUrl}/${categoryId}`;
        $view.main();
    },
    addPage: function () {
        $ajax.post({
            url: "/api/v1/board/getCategoryCount/"+boardMngId,
            success: function (res) {
                if (res <= 0) {
                    alert("등록된 카테고리가 없습니다. 카테고리를 설정해 주세요.");
                    location.href = "/adm/edu/boardMng/"+boardMngId+"/edit";
                } else {
                    domain = `${defaultPageUrl}/${categoryId}`;
                    $view.add();
                }
            }
        })
    },
    detailPage: function (boardId) {
        domain = `${defaultPageUrl}/${categoryId}`;
        $view.detail(boardId)
    },
    updatePage: function (boardId) {
        domain = `${defaultPageUrl}/${categoryId}`;
        $view.edit(boardId);
    },
    save: function () {
        let data = $form.getData();
        if (this.validation(data)) {
            $ajax.postMultiPart({
                url: "/api/v1/board/board/add",
                data: data,
                success: function (res) {
                    if (!isNull(res)) {
                        alert("게시물 저장이 완료되었습니다.");
                        pageObj.detailPage(res);
                    } else {
                        alert("게시물 저장 중 에러가 발생했습니다.");
                        return false;
                    }
                }
            })
        }
    },
    update: function (boardId) {
        let data = $form.getData();
        data.append("boardId", boardId);
        if (this.validation(data)) {
            $ajax.postMultiPart({
                url: "/api/v1/board/board/edit",
                data: data,
                success: function (res) {
                    if (isNull(res) || res <= 0) {
                        alert("게시물 저장 중 에러가 발생했습니다.");
                        return false;
                    } else {
                        alert("게시물 저장이 완료되었습니다.");
                        pageObj.detailPage(boardId)
                    }
                }
            })
        }
    },
    delete: function (boardId) {
        if (confirm("삭제하시겠습니까?")) {
            let reason = prompt("삭제사유", "");
            let data = {
                boardId: boardId,
                delReason: reason
            };

            $ajax.post({
                url: "/api/v1/board/board/delete",
                data: data,
                success: function (res) {
                    if (isNull(res) || res <= 0) {
                        alert("게시물 삭제 중 에러가 발생했습니다.");
                        return false;
                    } else {
                        alert("게시물 삭제가 완료되었습니다.");
                        pageObj.mainPage();
                    }
                }
            })
        }
    },
    deleteAll: function () {
        let checked = $checkBox.getAllChecked();
        if (checked.length > 0) {
            if (confirm("삭제하시겠습니까?")) {
                let reason = prompt("삭제사유", "입력");
                let data = {
                    boardIdList: checked,
                    delReason: reason
                };
                $ajax.post({
                    url: "/api/v1/board/board/deleteAll",
                    data: data,
                    success: function (res) {
                        if (isNull(res) || res == 0) {
                            alert("게시물 삭제 과정에서 오류가 발생했습니다.");
                            return false;
                        } else {
                            alert("게시물 삭제가 완료되었습니다.");
                            location.reload();
                        }
                    }
                })
            }
        } else {
            alert("삭제할 항목을 선택해 주세요.");
        }
    },
    comment: function (boardId) {
        if (isNull($("#comment").val())) {
            alert("댓글을 입력해 주세요.");
            $("#comment").focus();
            return false;
        }

        let data = {
            status: $("[name=status]:checked").val(),
            comment: $("#comment").val(),
            boardId: boardId
        };

        $ajax.post({
            url: "/api/v1/board/board/comment",
            data: data,
            success: function (res) {
                if (isNull(res) || res == 0) {
                    alert("댓글 저장 과정에서 오류가 발생했습니다..");
                    return false;
                } else {
                    alert("게시물 댓글 저장이 완료되었습니다.");
                    location.reload();
                }
            }
        })
    },
    validation: function (data) {
        let result = true;
        let banWord = setup.banWord.split(",");
        for (const [key, value] of data.entries()) {
            if (key == 'boardCategoryId' && isNull(value)) {
                alert("카테고리를 선택해 주세요.");
                $("#boardCategoryId").focus();
                result = false;
                break;
            }

            if (key == 'title' && isNull(value)) {
                alert("제목을 선택해 주세요.");
                $("#title").focus();
                result = false;
                break;
            }

            if (key == 'content') {
                if (isNull(banWord)) {
                    result = true;
                } else {
                    for (let i = 0; i < banWord.length; i++) {
                        let v = value.replace(/(<([^>]+)>)/gi, '');
                        if (v.includes(banWord[i])) {
                            alert("금지 단어가 포함되어 있습니다. [\""+banWord[i]+"\"]");
                            $("#content").focus();
                            result = false;
                            break;
                        }
                    }
                    if (result == false) break;
                }
            }
        }

        if (isNull($("[name=nuriNum]:checked").val())) {
            alert("공공누리 유형을 선택해 주세요.");
            return false;
        }

        return result;
    }
    // view: function (boardMngId, ncnm) {
    //     let url = `${domain}/${ncnm}/${boardMngId}/all`;
    //     location.href = url;
    // },
    // change: function () {
    //     if ($("input[type=checkbox]").is(":checked").length == 0) {
    //         alert("사용 또는 미사용 처리할 항목을 선택해 주세요.");
    //         return false;
    //     }
    //
    //     let list = [];
    //     $.each($("#tb_board").find("input:checkbox"), function () {
    //         if ($(this).is(":checked")) {
    //             list.push(this.id);
    //         }
    //     })
    //
    //     $ajax.post({
    //         url: "/api/v1/board/change",
    //         data: list,
    //         success: function (res) {
    //             if (res > 0) {
    //                 alert("처리되었습니다.");
    //                 location.reload();
    //             } else {
    //                 alert("오류가 발생했습니다.");
    //                 return false;
    //             }
    //         }
    //     })
    // },
    // copyView: function () {
    //     if ($("input[type=checkbox]:checked").length == 0) {
    //         alert("복사할 게시판을 선택해 주세요.");
    //         return false;
    //     }
    //
    //     if ($("input[type=checkbox]:checked").length > 1) {
    //         alert("게시판 복사는 1개씩 가능합니다.");
    //         return false;
    //     }
    //
    //     let boardMngId;
    //     $.each($("#tb_board").find("input:checkbox"), function () {
    //         if ($(this).is(":checked")) {
    //             boardMngId = this.id;
    //         }
    //     })
    //
    //     location.href = domain + "/" + boardMngId + "/copy";
    // },
    // changeCtgryUseAt: function (target) {
    //     if ($(target).is(':checked')) {
    //         $("#div_ctgry").css('display', 'block');
    //     } else {
    //         $("#div_ctgry").css('display', 'none');
    //     }
    // },
    // addCtgry: function () {
    //     $("#tb_ctgry > #tbody_ctgry").append(`
    //         <tr>
    //             <td class="order1">
    //                 <label class="chk-item"><input type="checkbox" class="chk_ctgry" title="체크"></label>
    //             </td>
    //             <td id="td_ctgryNm">
    //                 <input type="text" id="ctgryNm" maxlength="8">
    //             </td>
    //         </tr>
    //     `);
    // },
    // delCtgry: function () {
    //     $.each($(".chk_ctgry"), function () {
    //         if ($(this).is(":checked")) {
    //             $(this).parent().parent().parent().remove();
    //         }
    //     })
    // },
    // save: function () {
    //
    //     if (this.validation()) {
    //         $ajax.post({
    //             url: "/api/v1/community/add",
    //             data: this.getData(),
    //             success: function (res) {
    //                 if (res > 0) {
    //                     alert("저장되었습니다.");
    //                     $view.main();
    //                 } else {
    //                     alert("게시판 저장 중 오류가 발생했습니다.");
    //                     return false;
    //                 }
    //             }
    //         })
    //     }
    // },
    // update: function (id) {
    //     if (this.validation()) {
    //         $ajax.post({
    //             url: "/api/v1/board/"+id+"/edit",
    //             data: this.getData(),
    //             success: function (res) {
    //                 if (res > 0) {
    //                     alert("수정되었습니다.");
    //                     $view.main();
    //                 } else {
    //                     alert("게시판 수정 중 오류가 발생했습니다.");
    //                     return false;
    //                 }
    //             }
    //         })
    //     }
    // },
    // getData: function () {
    //     let data = {};
    //     $.each($("[data*=data]"), function () {
    //         let tagNm = this.tagName.toLowerCase();
    //         if (tagNm == 'select' || tagNm == 'textarea') {
    //             data[this.id] = this.value
    //         } else if (tagNm == 'input') {
    //             let tagTp = $(this).prop('type');
    //             if (tagTp == 'checkbox') {
    //                 data[this.id] = $(this).is(":checked") ? "Y" : "N";
    //             } else {
    //                 let v = this.value;
    //                 if (this.id == 'newWrtDtCnt' || this.id == 'postDtCnt' || this.id == 'fileCnt'
    //                     || this.id == 'fileSize' || this.id == 'fileFullSize') {
    //                     v = v.replaceAll(",", "");
    //                 }
    //                 data[this.id] = v;
    //             }
    //         }
    //     })
    //
    //     if (data.ctgryUseAt == "Y") {
    //         let list = [];
    //         $.each($("#tbody_ctgry").find("input:checkbox"), function () {
    //             let obj = {"boardCategoryId": this.id};
    //             let categoryNm = $(this).parent()
    //                 .parent()
    //                 .siblings('#td_ctgryNm')
    //                 .children('#ctgryNm')
    //                 .val();
    //
    //             obj["categoryNm"] = categoryNm;
    //             list.push(obj);
    //         })
    //         data.ctgryList = list;
    //     }
    //
    //     return data;
    // },
    // validation: function () {
    //     let result = true;
    //
    //     $.each($("[data*=essential]"), function () {
    //         if (isNull(this.value)) {
    //             if (this.tagNm == "select") {
    //                 alert(this.title + "을 선택해 주세요.");
    //             } else {
    //                 alert(this.title + "을 입력해 주세요.");
    //             }
    //             $(this).focus();
    //             return result = false;
    //         }
    //     })
    //
    //     if ($("#ctgryUseAt").is(":checked")) {
    //         if ($("#tbody_ctgry").find('tr').length == 0) {
    //             alert("카테고리를 추가해 주세요.");
    //             return false;
    //         }
    //
    //         $.each($("#tbody_ctgry").find("input:text"), function () {
    //             if (isNull(this.value)) {
    //                 alert("카테고리명을 선택해 주세요.");
    //                 $(this).focus();
    //                 return result = false;
    //             }
    //         })
    //     }
    //
    //     return result;
    // }
}

pageObj.pageStart = function () {
    boardEvent.init(boardMngId);
};