var setup = {};

let url = $url.getPath();
let urlChk = url.split("/");

boardFile = {

    fileCnt: 0,
    fileSize: 10485760,
    fileFullSize: 10485760,
    init: function () {
        let fileExt = setup.fileExt.split(",");

        if (fileExt.length > 0) {
            var reFileExt = setup.fileExt.replace(/"/g, "'");
            $files.types = reFileExt;
        } else {
            $files.types = 'gif, jpg, jpeg, png, bmp, xlsx, xls, doc, docx, pdf, hwp, hwpx, ppt, pptx, txt, zip, mp4';
        }

        if (!isNull(setup.fileCnt) && setup.fileCnt > 0) {
            this.fileCnt = setup.fileCnt;
            $files.fileCnt = setup.fileCnt;
        }

        if (!isNull(setup.fileSize) && setup.fileSize > 0) {
            this.fileSize = setup.fileSize;
            $files.maxSize = setup.fileSize;
        }

        if (!isNull(setup.fileFullSize) && setup.fileFullSize > 0) {
            this.fileFullSize = setup.fileFullSize;
        }
    }
}

var calendar;
boardCalendar = {
    init: function () {
        const target = document.getElementById('calendar');
        const useSe = $user.getSessionUser().useSe;
        var calendarViewType;

        calendar = new FullCalendar.Calendar(target, {
            locale: 'ko',
            themeSystem: 'bootstrap',
            firstDay: 0,
            navLinks: false,
            allDayText: '종일',
            selectable: enable(useSe),
            editable: enable(useSe),
            eventStartEditable: false,
            contentHeight: "auto",
            noEventsContent: '일정이 없습니다.',
            googleCalendarApiKey: "AIzaSyAtJQ6Y40Ro9cQRSWNahCFipT7wLx-t5n0",
            initialView: calendarViewType = (window.innerWidth < 768) ? 'listWeek' : 'dayGridMonth',
            windowResize: function (arg) {
                let view = (window.innerWidth < 768) ? 'listWeek' : 'dayGridMonth';
                calendarViewType = view;
                calendar.changeView(view)
            },
            datesSet: function (info) {
                calendarViewType = info.view.type;
            },
            eventOrder: function (target) {
                return target.publicId;
            },
            moreLinkContent: function (args) {
                return '+' + args.num + '개';
            },
            headerToolbar: {
                left: 'listWeek',
                center: 'prev,title,next',
                right: 'dayGridMonth,timeGridWeek,timeGridDay'
            },
            buttonText: {
                listWeek : '일정목록',
                dayGridMonth: '월',
                timeGridWeek: '주',
                timeGridDay: '일'
            },
            views: {
                month: {
                    dayMaxEventRows: 10
                },
                week: {
                    dayMaxEventRows: 20
                },
                day: {
                    dayMaxEventRows: 20
                },
                timeGrid: {
                    dayMaxEventRows: 20
                }
            },
            eventClassNames: function (args) {
                let target = args.event._def;
                let color = setColor(target.groupId);

                args.backgroundColor = color.color;
                args.borderColor = color.borderColor;
                args.textColor = color.textColor;
            },
            dateClick: function (date) {
                //alert(urlChk[3]);
                if (urlChk[3] === "community") {
                    if (disable(useSe)) {
                        modal.open({
                            title: "일정등록",
                            path: `/adm/edu/boardMng/calendar/${boardMngId}/add/${date.dateStr}`
                        })
                    }
                } else {
                    if (enable(useSe)) {
                        modal.open({
                            title: "일정등록",
                            path: `/adm/edu/boardMng/calendar/${boardMngId}/add/${date.dateStr}`
                        })
                    }
                }
            },
            eventClick: function (info) {
                info.jsEvent.stopPropagation();
                info.jsEvent.preventDefault();

                let chkTitle, chkFolder, chkUrl;
                if (urlChk[3] === "community") {
                    chkTitle = "일정상세";
                    chkFolder = "/mber/emp/";
                    chkUrl = "community";
                } else {
                    chkTitle =  "일정등록";
                    chkFolder = "/adm/edu/";
                    chkUrl = "boardMng";
                }

                const id = info.event._def.publicId;
                const type = info.event._def.groupId;
                let url = chkFolder + chkUrl + `/calendar/${boardMngId}/${type}/${id}`;

                if (!isNull(mngType)) {
                    url += '?mngType='+mngType;
                }

                modal.open({
                    title: chkTitle,
                    path: url
                })
            },
            eventSources: [{
                events: function (info, successCallback, failureCallback) {
                    let scheduleData = [];
                    let startDate = moment(info.start).format("YYYY-MM-DD");
                    let endDate = moment(info.end).format("YYYY-MM-DD");

                    let url = `/api/v1/board/calendar`
                    let data = {
                        boardMngId: boardMngId,
                        categoryId: categoryId,
                        startDate: startDate,
                        endDate: endDate,
                        selectKey1: mngType
                    }
                    $ajax.post({
                        url: url,
                        data: data,
                        success: function (scheduleList) {
                            scheduleList.forEach(schedule => {
                                let s = {
                                    id: schedule.id,
                                    groupId: schedule.groupId,
                                    title: schedule.title,
                                    allDay: true,
                                    location: schedule.location,
                                    content: schedule.content,
                                    start: moment(schedule.startDt).format("YYYY-MM-DD"),
                                    end: moment(schedule.endDt).add(1, 'days').format("YYYY-MM-DD")
                                };
                                scheduleData.push(s);
                            });
                            successCallback(scheduleData);
                        }
                    })
                }
            }
                // ,{
                //     googleCalendarId: "ko.south_korea#holiday@group.v.calendar.google.com",
                //     className: "ko_event",
                //     color: "#ccc",
                //     textColor: '#000'
                // }
            ]
        });

        calendar.render();

        function enable(useSe) {
            return useSe == "USESE00008";
        }

        function compareDate(dateObj, holiday) {
            if (holiday) {
                return false;
            }

            return dateObj.startStr == moment(dateObj.endStr).subtract(1, 'days').format("YYYY-MM-DD");
        }

        function setColor(groupId) {
            if (groupId != 'ETC') {
                return {color : "#F6F8FB", textColor : "#1c3eb3", borderColor: "#8ca5ff"};
            } else {
                return {color : "#f1f1f1", textColor : "#3b3b3b", borderColor: "#d3d3d3"};
            }
        }
    },
    save: function () {
        let data = this.getData();
        if (data.flag == false) {
            return false;
        }

        data.data.boardCategoryId = $("#boardCategoryId").val();
        $ajax.post({
            url: "/api/v1/board/calendar/add",
            data: data.data,
            success: function (res) {
                if (res > 0) {
                    alert("일정 저장이 완료되었습니다.");
                    modal.close();
                    calendar.refetchEvents();
                } else {
                    alert("일정 저장 중 에러가 발생했습니다.");
                    return false;
                }
            }
        })
    },
    update: function () {
        let data = this.getData();
        if (data.flag == false) {
            return false;
        }

        data.data.boardCategoryId = $("#categoryId").val();
        data.data.id = $("#boardScheduleId").val();
        $ajax.post({
            url: "/api/v1/board/calendar/edit",
            data: data.data,
            success: function (res) {
                if (res > 0) {
                    alert("일정 수정이 완료되었습니다.");
                    modal.close();
                    calendar.refetchEvents();
                } else {
                    alert("일정 저장 중 에러가 발생했습니다.");
                    return false;
                }
            }
        })
    },
    delete: function (id) {
        if (!isNull(id)) {
            if (confirm("삭제하시겠습니까?")) {
                let reason = prompt("삭제사유", "");
                let data = {
                    scheduleId: id,
                    delReason: reason
                };

                $ajax.post({
                    url: "/api/v1/board/calendar/delete",
                    data: data,
                    success: function (res) {
                        if (res > 0) {
                            alert("일정 삭제가 완료되었습니다.")
                            modal.close();
                            calendar.refetchEvents();
                        } else {
                            alert("일정 삭제 중 에러가 발생했습니다.");
                            return false;
                        }
                    }
                })
            }
        } else {
            alert("일정을 삭제하는 과정에서 에러가 발생했습니다.");
            return false;
        }
    },
    restore: function (id) {
        if (!isNull(id)) {
            if (confirm("복구하시겠습니까?")) {
                let reason = prompt("복구사유", "");
                let data = {
                    scheduleId: id,
                    restoreReason: reason
                };

                $ajax.post({
                    url: "/api/v1/board/calendar/restore",
                    data: data,
                    success: function (res) {
                        if (res > 0) {
                            alert("일정 복구가 완료되었습니다.")
                            modal.close();
                            calendar.refetchEvents();
                        } else {
                            alert("일정 복구 중 에러가 발생했습니다.");
                            return false;
                        }
                    }
                })
            }
        } else {
            alert("일정을 복구하는 과정에서 에러가 발생했습니다.");
            return false;
        }
    },
    updateForm: function () {
        $("#boardCategoryId").prop('disabled', false);
        $("#startDt").prop('readonly', true).datepicker('option', 'disabled', false);
        $("#endDt").prop('readonly', true).datepicker('option', 'disabled', false);
        $("#title").prop('readonly', false);
        $("#location").prop('readonly', false);
        $("#content").prop('readonly', false);
        $("#div_category").addClass("essential");
        $("#div_date").addClass("essential");
        $("#div_title").addClass("essential");

        $("#transport").css('display', 'none');
        $("#cancel").css('display', 'none');
        $("#delete").css('display', 'none');
        $("#updateForm").css('display', 'none');
        $("#updateCancel").css('display', 'block');
        $("#update").css('display', 'block');

        boardEvent.datepicker();
    },
    updateCancel: function () {
        $("#boardCategoryId").prop('disabled', true);
        $("#startDt").prop('readonly', true).datepicker('option', 'disabled', true);
        $("#endDt").prop('readonly', true).datepicker('option', 'disabled', true);
        $("#title").prop('readonly', true);
        $("#location").prop('readonly', true);
        $("#content").prop('readonly', true);
        $("#div_category").removeClass("essential");
        $("#div_date").removeClass("essential");
        $("#div_title").removeClass("essential");

        $("#transport").css('display', 'block');
        $("#cancel").css('display', 'block');
        $("#delete").css('display', 'block');
        $("#updateForm").css('display', 'block');
        $("#updateCancel").css('display', 'none');
        $("#update").css('display', 'none');
    },
    getData: function () {
        if (isNull($("#boardCategoryId").val())) {
            alert("카테고리를 선택해 주세요.");
            $("#boardCategoryId").focus();
            return {flag: false};
        }

        if (isNull($("#startDt").val())) {
            alert("시작일을 선택해 주세요.");
            $("#startDt").focus();
            return {flag: false};
        }

        if (isNull($("#endDt").val())) {
            alert("종료일을 선택해 주세요.");
            $("#endDt").focus();
            return {flag: false};
        }

        if (isNull($("#title").val())) {
            alert("제목을 입력해 주세요.");
            $("#title").focus();
            return {flag: false};
        }

        let data = modal.getData();

        let start = new Date(data.startDt);
        let end = new Date(data.endDt);
        if (start > end) {
            alert("일정을 확인해 주세요.");
            $("#endDt").focus();
            return {flag: false};
        }

        return {flag: true, data: data};
    },
    transView: function (boardId) {
        modal2.open({
            title: "게시물 이동",
            path: `${defaultPageUrl}/transport/`
        })
    },
    transport: function () {
        if (isNull($("#boardScheduleId").val())) {
            console.log("일정 게시물을 이동 과정에서 오류가 발생했습니다.");
            return false;
        }

        let data = {
            boardMngId: $("#boardMng").val(),
            boardCategoryId: $("#boardCtgry").val(),
            boardIdList: [$("#boardScheduleId").val()]
        }

        $ajax.post({
            url: `/api/v1/board/${boardType}/transport`,
            data: data,
            success: function (res) {
                if (isNull(res) || res <= 0) {
                    alert("게시물 이동 과정에서 오류가 발생했습니다.");
                    return false;
                } else {
                    if (confirm("게시물이동이 완료되었습니다.\n해당 페이지로 이동하시겠습니까?")) {
                        let user = $user.getSessionUser();
                        let bt = "boardMng";
                        if (!`${defaultPageUrl}`.includes(bt)) {
                            bt = "community";
                        }

                        let path = `/${user.auth}/${user.useSeNm}/${bt}/${boardType}/${$("#boardMng").val()}/all`;
                        location.href = path;
                    } else {
                        alert("게시물 이동이 완료되었습니다.");
                        location.reload();
                    }
                }
            }
        })
    }
}

board = {
    init: function () {

        if (setup.fileCnt == 0) {
            $("#fileCntAt").remove();
        }

        //썸네일 이미지
        if (setup.boardType === "BOARD00004" || setup.boardType === "BOARD00008") {
            $("#contentAt").remove();
        }

        if (setup.boardType === "BOARD00004") {
            $("#ebookAt").remove();
        }

        if (setup.boardType === "BOARD00008") {
            $("#fileAt").remove();
        }

        if (isNull(setup.ctgryUseAt) || setup.ctgryUseAt == "N") {
            $("#ctgryUseAt").remove();
        }

        if (isNull(setup.postAt) || setup.postAt == "N") {
            $("#postAt").remove();
        }

        if (isNull(setup.allPostAt) || setup.allPostAt == "N") {
            $("#allPostAt").remove();
        }

        if ((isNull(setup.postAt) || setup.postAt == "N") && (isNull(setup.allPostAt) || setup.allPostAt == "N")) {
            $("#board_post").remove();
        }

        if (isNull(setup.secretAt) || setup.secretAt == "N") {
            $("#board_secretAt").remove();
            $("#board_password").remove();
        }

        if (isNull(setup.postResAt) || setup.postResAt == "N") {
            $("#board_reserveDt").remove();
        }

        if (isNull(setup.wrtrSmsAt) || setup.wrtrSmsAt == "N") {
            $("#wrtrSmsAt").remove();
        }

        if (isNull(setup.wrtrEmailAt) || setup.wrtrEmailAt == "N") {
            $("#wrtrEmailAt").remove();
        }

        if ((isNull(setup.wrtrSmsAt) || setup.wrtrSmsAt == "N") && (isNull(setup.wrtrEmailAt) || setup.wrtrEmailAt == "N")) {
            $("#board_wrtr").remove();
        }

        if (!isNull(setup.editorAt) && setup.editorAt == "Y") {
            $editor.init('content');
        }
    },
    transView: function () {
        let checked = $checkBox.getAllChecked();
        if (checked.length <= 0) {
            alert("이동할 게시물을 선택해 주세요.");
            return false;
        }
        modal.open({
            title: "게시물 이동",
            path: `${defaultPageUrl}/transport/`
        })
    },
    mngChange: function (target) {
        let id = target.value;
        let empty = `<option value="">선택</option>`;

        $ajax.post({
            url: "/api/v1/board/getCategory/"+id,
            success: function (res) {
                $("#boardCtgry").empty();
                $("#boardCtgry").append(empty);
                for (let i = 0; i < res.length; i++) {
                    $("#boardCtgry").append(`<option value="${res[i].boardCategoryId}">${res[i].categoryNm}</option>`);
                }
            }
        })
    },
    transport: function () {
        if (isNull($("#boardMng").val())) {
            alert("이동할 게시판을 선택해 주세요.");
            return false;
        }
        if (isNull($("#boardCtgry").val())) {
            alert("이동할 카테고리를 선택해 주세요.");
            return false;
        }
        let checked = $checkBox.getAllChecked();
        if (checked.length <= 0) {
            alert("이동할 게시물이 선택되어있지 않습니다.");
            return false;
        }

        let data = {
            boardMngId: $("#boardMng").val(),
            boardCategoryId: $("#boardCtgry").val(),
            boardIdList: checked
        }

        $ajax.post({
            url: `/api/v1/board/${boardType}/transport`,
            data: data,
            success: function (res) {
                if (isNull(res) || res <= 0) {
                    alert("게시물 이동 과정에서 오류가 발생했습니다.");
                    return false;
                } else {
                    if (confirm("게시물이동이 완료되었습니다.\n해당 페이지로 이동하시겠습니까?")) {
                        let user = $user.getSessionUser();
                        let bt = "boardMng";
                        if (!`${defaultPageUrl}`.includes(bt)) {
                            bt = "community";
                        }

                        let path = `/${user.auth}/${user.useSeNm}/${bt}/${boardType}/${$("#boardMng").val()}/all`;
                        location.href = path;
                    } else {
                        alert("게시물 이동이 완료되었습니다.");
                        location.reload();
                    }
                }
            }
        })
    }
}

var modal = $modal();
var modal2 = $modal();
boardEvent = {
    init: function (boardMngId) {
        this.setup(boardMngId);

        let user = $user.getSessionUser();
        $("#content_tr > td").on('click', function () {
            let id = $(this).parent().attr("value");
            if ($(this).attr('class') != 'order1') {
                if (boardType == 'qna' || boardType == 'gen' || boardType == 'ref') {
                    $ajax.post({
                        url: "/api/v1/board/board/secret",
                        data: {boardId: id},
                        success: function (map) {
                            if (map.usid == user.orgUserId) {
                                pageObj.detailPage(id);
                            } else {
                                if (map.secretAt == 'N') {
                                    pageObj.detailPage(id);
                                } else {
                                    console.log("map")
                                    alert("비밀글로 등록된 게시물입니다.");
                                    return false;
                                }
                            }
                        }
                    })
                } else {
                    pageObj.detailPage(id);
                }
            }
        })

        $("#content_tr > div").on('click', function () {
            let id = $(this).parent().attr("value");
            if ($(this).attr('class') != 'order1') {
                pageObj.detailPage(id);
            }
        })


        $("#content_ntr > td").on('click', function () {
            let id = $(this).parent().attr("value");
            if ($(this).attr('class') != 'order1') {
                pageObj.detailPage(id);
            }
        })


        if ($("#org0").length > 0) {
            this.orgCdSelectSetting()
            this.orgCdDefaultCodeSetting();
        }

        modal.init($("#modal"), {
            width: 1200,
            height: 1600
        });
        modal2.init($("#modal2"), {
            width: 1200,
            height: 1600
        });
    },
    datepicker: function () {
        $(".datepicker, #startDt, #endDt").datepicker();
        $.datepicker.setDefaults({
            dateFormat: 'yy-mm-dd',
            closeText: "닫기",
            currentText: "오늘",
            prevText: '이전 달',
            nextText: '다음 달',
            monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
            monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
            dayNames: ['일', '월', '화', '수', '목', '금', '토'],
            dayNamesShort: ['일', '월', '화', '수', '목', '금', '토'],
            dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
            weekHeader: "주",
            yearSuffix: '년',
            showMonthAfterYear: true,
            onClose: function (selectedDate) {
                if (!isNull(selectedDate)) {
                    $('#endDt').attr('disabled', false);
                    $('#endDt').datepicker('option', 'minDate', selectedDate);
                }
            }
        });
    },
    setup: function (boardMngId) {
        $ajax.post({
            url: "/api/v1/board/setup",
            data: {boardMngId: boardMngId},
            success: function (res) {
                for (const prop in res) {
                    if (prop == 'banWord' && isNull(res[prop])) {
                        setup[prop] = res[prop];
                    } else {
                        setup[prop] = res[prop];
                    }
                }
                console.log(setup)
                switch (setup.boardType) {
                    case "BOARD00001": case "BOARD00002": case "BOARD00003": case "BOARD00007":
                        board.init();
                        boardFile.init();
                        break;
                    case "BOARD00004": case "BOARD00008":
                        board.init();
                        boardFile.init();
                        break;
                    case "BOARD00005":
                        break;
                    case "BOARD00006":
                        boardCalendar.init();
                        break;
                }
            }
        })
    },
    orgCdSelectSetting: function () {
        let sessionUser = $user.getSessionUser();
        $("#org0").on('change', function () {
            let orgCd = this.value;
            if (!isNull(orgCd)) {
                $ajax.post({
                    url: "/common/selAduOrgCd",
                    data: {},
                    success: function (list) {
                        $("#org1").empty();
                        $("#org2").empty();
                        $("#org1").append(`<option value="">선택</option>`);
                        $("#org2").append(`<option value="">선택</option>`);

                        let value = $("#org0 option:checked").val();

                        $.each(list, function () {
                            $("#org1").append(`<option value="${this.orgCd}">${this.orgNm}</option>`)
                        })

                        if (isNull(value)) {
                            $("#viewCdNm").val("");
                            $("#viewCd").val("");
                        } else {
                            $("#viewCdNm").val($("#org0 option:checked").text());
                            $("#viewCd").val($("#org0 option:checked").val());
                        }

                        if (sessionUser.upperOrgCd == $("#viewCd").val()) {
                            $("#allYn").attr("disabled", false);
                        } else {
                            $("#allYn").attr("disabled", true);
                        }
                    }
                })
            } else {
                $("#org1").empty();
                $("#org2").empty();
                $("#viewCdNm").val("");
                $("#viewCd").val("");

                $("#org1").append('<option>선택</option>');
                $("#org2").append('<option>선택</option>');
            }
        })

        $("#org1").on('change', function () {
            $ajax.post({
                url: "/common/selNomalOrgCd",
                data: {orgCd: $("#org1").val()},
                success: function (list) {
                    $("#org2").empty();

                    $("#org2").append(`<option value="">선택</option>`);
                    if(list.length > 0){
                        $("#org2").attr("disabled", false);
                        $.each(list, function () {
                            $("#org2").append(`<option value="${this.orgCd}">${this.orgNm}</option>`)
                        })
                    } else {
                        $("#org2").attr("disabled", true);
                    }

                    let value = $("#org1 option:checked").val();
                    if (isNull(value)) {
                        if (sessionUser.orgType == 'sup' || sessionUser.orgType == 'sch') {
                            $("#viewCdNm").val((""));
                            $("#viewCd").val((""));
                        } else {
                            $("#viewCdNm").val($("#org0 option:checked").text());
                            $("#viewCd").val($("#org0 option:checked").val());
                        }
                    } else {
                        $("#viewCdNm").val($("#org1 option:checked").text());
                        $("#viewCd").val($("#org1 option:checked").val());
                    }

                    if (sessionUser.upperOrgCd == $("#viewCd").val()) {
                        $("#allYn").attr("disabled", false);
                    } else {
                        $("#allYn").attr("disabled", true);
                    }
                }
            })
        })

        $("#org2").on('change', function () {
            let value = $("#org2 option:checked").val();
            if (isNull(value)) {
                if (sessionUser.orgType == 'sch') {
                    $("#viewCdNm").val((""));
                    $("#viewCd").val((""));
                } else {
                    $("#viewCdNm").val($("#org1 option:checked").text());
                    $("#viewCd").val($("#org1 option:checked").val());
                }
            } else {
                $("#viewCdNm").val($("#org2 option:checked").text());
                $("#viewCd").val($("#org2 option:checked").val());
            }

            if (sessionUser.upperOrgCd == $("#viewCd").val()) {
                $("#allYn").attr("disabled", false);
            } else {
                $("#allYn").attr("disabled", true);
            }
        })
    },
    orgCdDefaultCodeSetting: function () {
        let sessionUser = $user.getSessionUser();
        let orgCd = $("#viewCd").val();
        const maxDepth = 3;
        var orgnztList = []
        $("#org0").val("");
        $("#org1").empty();
        $("#org2").empty();
        $("#org1").append('<option>선택</option>');
        $("#org2").append('<option>선택</option>');
        if (isNull(orgCd)) {
            orgCd = sessionUser.upperOrgCd;
        }

        $ajax.post({
            url: "/common/selUpperOrgByOrgCd",
            data: {orgCd: orgCd},
            async: false,
            success: function (res) {
                if (res.result == "S") {
                    if (!isNull(res.data)) {
                        orgnztList = res.data;
                        let org0Cd = orgnztList[0].orgCd;
                        $("#org0").val(org0Cd);
                        let s = orgnztList.length;
                        for (var i = 0; i < s; i++) {
                            $("#org" + i).val(orgnztList[i].orgCd);
                            if (i == 0) {
                                url = "/common/selAduOrgCd";
                            } else if (i == 1) {
                                url = "/common/selNomalOrgCd";
                            }
                            $ajax.post({
                                url: url,
                                data: {orgCd: orgnztList[i].orgCd},
                                async: false,
                                success: function (list) {
                                    $("#org" + (i + 1)).empty();
                                    $("#org" + (i + 1)).append(`<option value="">선택</option>`);
                                    if (i == 0) {
                                        $.each(list, function () {
                                            $("#org" + (i + 1)).append(`<option value="${this.orgCd}">${this.orgNm}</option>`);
                                        })
                                    } else if (i == 1) {
                                        if (list.length > 0) {
                                            $.each(list, function () {
                                                $("#org" + (i + 1)).append(`<option value="${this.orgCd}">${this.orgNm}</option>`);
                                            })
                                        } else {
                                        }
                                    }
                                    $("#org" + (i + 1)).val(orgnztList[i].orgCd);
                                    if (maxDepth <= i || i == s - 1) {
                                        let value = $("#org" + i + " option:checked").val();
                                        if (isNull(value)) {
                                            $("#viewCdNm").val("");
                                            $("#viewCd").val("");
                                        } else {
                                            $("#viewCdNm").val($("#org" + i + " option:checked").text());
                                            $("#viewCd").val($("#org" + i + " option:checked").val());
                                        }
                                        if ($("#org" + (i + 1)).length > 0) {
                                            $("#org" + (i + 1)).val("");
                                        }
                                    }
                                }
                            });
                        }
                    }
                } else {
                    alert(res.msg);
                }
            }
        });
    }
}