//유형 선택에 따른 공공누리 유형 미리보기 처리
//<![CDATA[
$(document).ready(function(){
    $('#koglType1').click(function(){
        $(".codeView02, .codeView03,.codeView04,.codeView05").addClass('hide');
        $(".codeView01").removeClass('hide');
    }); //type1 check
    $('#koglType2').click(function(){
        $(".codeView01, .codeView03,.codeView04,.codeView05").addClass('hide');
        $(".codeView02").removeClass('hide');
    }); //type2 check
    $('#koglType3').click(function(){
        $(".codeView02, .codeView01,.codeView04,.codeView05").addClass('hide');
        $(".codeView03").removeClass('hide');
    }); //type3 check
    $('#koglType4').click(function(){
        $(".codeView02, .codeView03,.codeView01,.codeView05").addClass('hide');
        $(".codeView04").removeClass('hide');
    }); //type4 check
    $('#koglType5').click(function(){
        $(".codeView02, .codeView03,.codeView04,.codeView01").addClass('hide');
        $(".codeView05").removeClass('hide');
    }); //적용불가 check
});
//]]>

//등록시 입력값 유효성 체크
function checkFrm(){
    var frm = document.form1;
    try{
        if(frm.title.value ==""){
            alert("제목을 입력하세요");
            return;
        }else if(frm.content.value ==""){
            alert("내용을 입력하세요");
            return;
            //free_use_desc : 공공누리 유형표시를 데이터베이스에 입력할 경우 사용
        } else if(frm.free_use_desc.value =="") {
            alert("공공저작물 자유이용 조건을 선택하세요");
            return;
        }else{
            frm.submit();
        }
    }catch(e){
        alert(e.massage);
        return;
    }
}

//공공누리 자유이용 스크립트 적용
function fnKoglTypeSet(sel){
    var type = sel.value ;
    var sOrgName = "충남교육청" ; //기관명은 필수입니다.
    var sKoglTitle = document.getElementById("title").value;
    //게시판의 제목(저작물) 입력란의 id를 매칭시켜주세요.
    if(sOrgName.length == 0 ) {
        //console.debug("기관명을 셋팅해주세요.");
        alert("기관명을 셋팅해주세요.");
        sel.checked = false;
        return;
    } else if(sKoglTitle.length == 0){
        //console.debug("게시물의 제목이 작성되어야 합니다.");
        alert("게시물의 제목이 작성되어야 합니다.");
        sel.checked = false;
        return;
    }

    var sInfoUrl = "";
    var sInfoAlt = "";
    var sImgUrl ;
    var sLpadd ;
    if(type == "1"){
        sLpadd = 150;
        sInfoAlt = "\"출처표시\"";
        sInfoUrl = "<a href='http://www.kogl.or.kr/open/info/license_info/by.do' target='_blank'> 				"+sInfoAlt+" </a>";
        sImgUrl ="'http://www.kogl.or.kr/open/web/images/images_2014/codetype/img_opentype01.png' style='position:absolute;left:20px;top:20%;vertical-align:middle;width:115px;height:43px;'"; //
    }else if(type == "2") {
        sLpadd = 180;
        sInfoAlt = "\"출처표시+상업적 이용금지\"";
        sInfoUrl = "<a href='http://www.kogl.or.kr/open/info/license_info/nc.do' target='_blank'> 				"+sInfoAlt+"</a>";
        sImgUrl ="'http://www.kogl.or.kr/open/web/images/images_2014/codetype/img_opentype02.png' style='position:absolute;left:20px;top:25%;vertical-align:middle;width:140px;height:43px;'";
    }else if(type == "3") {
        sLpadd = 180;
        sInfoAlt = "\"출처표시+변경금지\"";
        sInfoUrl = "<a href='http://www.kogl.or.kr/open/info/license_info/nd.do' target='_blank'> 				"+sInfoAlt+"</a>";
        sImgUrl ="'http://www.kogl.or.kr/open/web/images/images_2014/codetype/img_opentype03.png' style='position:absolute;left:20px;top:25%;vertical-align:middle;width:140px;height:43px;'";
    }else if(type == "4") {
        sLpadd = 210;
        sInfoAlt = "\"출처표시+상업적 이용금지+변경금지\"";
        sInfoUrl = "<a href='http://www.kogl.or.kr/open/info/license_info/all.do' target='_blank'> 				"+sInfoAlt+"</a>";
        sImgUrl ="'http://www.kogl.or.kr/open/web/images/images_2014/codetype/img_opentype04.png' style='position:absolute;left:20px;top:25%;vertical-align:middle;width:166px;height:43px;'";
    }else if(type == "5") {
        sLpadd = 60;
        sInfoAlt = "공공저작물 자유이용 허락표시 적용 안함";
        sInfoUrl = "<a href='http://www.law.go.kr/lsInfoP.do?lsiSeq=148848&efYd=20140701#0000' 				target='_blank'> "+sInfoAlt+"</a>";
        sImgUrl ="'http://www.kogl.or.kr/open/web/images/images_2014/codetype/img_opencode0_1.jpg' style='position:absolute;left:20px;top:25%;vertical-align:middle;width:109px;height:27px;'";
    }

    //데이터 베이스에 등록되는 자유이용 스크립트 완성
    var koglHtml = "<div class='codeView0"+type+"' style='position:relative;margin:0;padding:0;margin-top:5px;background:#fff;border:1px solid #dbdbdb;padding:30px 15px 30px "+sLpadd+"px;font-size:12px;color:#292929;font-weight:bold;'>";
    koglHtml += "<img src="+sImgUrl+" alt='"+sInfoAlt+"'/>";
    if(type != "5") {
        koglHtml += "<span>"+sOrgName+"</span>이(가) 창작한 <span>"+sKoglTitle+"</span> 			저작물은 공공누리&nbsp;";
    }
    koglHtml += sInfoUrl;
    if(type != "5") {
        koglHtml += "&nbsp;조건에 따라 이용할 수 있습니다.</div>";
    }
}