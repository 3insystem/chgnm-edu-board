var domain = "/adm/edu/boardMng";

var pageObj = {
    view: function (boardMngId, ncnm) {
        let url = `${domain}/${ncnm}/${boardMngId}/all`;
        location.href = url;
        // window.open(url);
    },
    holidayView: function () {
      location.href = domain + "/holiday";
    },
    change: function () {
        if ($("input[type=checkbox]:checked").length == 0) {
            alert("사용 또는 미사용 처리할 항목을 선택해 주세요.");
            return false;
        }

        let list = [];
        $.each($("#tb_board").find("input:checkbox"), function () {
            if ($(this).is(":checked")) {
                list.push(this.id);
            }
        })

        $ajax.post({
            url: "/api/v1/board/change",
            data: list,
            success: function (res) {
                if (res > 0) {
                    alert("처리되었습니다.");
                    location.reload();
                } else {
                    alert("오류가 발생했습니다.");
                    return false;
                }
            }
        })
    },
    copyView: function () {
        if ($("input[type=checkbox]:checked").length == 0) {
            alert("복사할 게시판을 선택해 주세요.");
            return false;
        }

        if ($("input[type=checkbox]:checked").length > 1) {
            alert("게시판 복사는 1개씩 가능합니다.");
            return false;
        }

        let boardMngId;
        $.each($("#tb_board").find("input:checkbox"), function () {
            if ($(this).is(":checked")) {
                boardMngId = this.id;
            }
        })

        location.href = domain + "/" + boardMngId + "/copy";
    },
    changeCtgryUseAt: function (target) {
        if ($(target).is(':checked')) {
            $("#div_ctgry").css('display', 'block');
        } else {
            $("#div_ctgry").css('display', 'none');
        }
    },
    addCtgry: function () {
        $("#tb_ctgry > #tbody_ctgry").append(`
            <tr>
                <td class="order1">
                    <label class="chk-item"><input type="checkbox" class="chk_ctgry" title="체크"></label>
                </td>
                <td id="td_ctgryNm">
                    <input type="text" id="ctgryNm" maxlength="8">
                </td>
            </tr>
        `);
    },
    delCtgry: function () {
        $.each($(".chk_ctgry"), function () {
            if ($(this).is(":checked")) {
                $(this).parent().parent().parent().remove();
            }
        })
    },
    save: function () {
        if (this.validation()) {
            $ajax.post({
                url: "/api/v1/board/add",
                data: this.getData(),
                success: function (res) {
                    if (res > 0) {
                        alert("저장되었습니다.");
                        $view.main();
                    } else {
                        alert("게시판 저장 중 오류가 발생했습니다.");
                        return false;
                    }
                }
            })
        }
    },
    update: function (id) {
        if (this.validation()) {
            $ajax.post({
                url: "/api/v1/board/"+id+"/edit",
                data: this.getData(),
                success: function (res) {
                    if (res > 0) {
                        alert("수정되었습니다.");
                        $view.main();
                    } else {
                        alert("게시판 수정 중 오류가 발생했습니다.");
                        return false;
                    }
                }
            })
        }
    },
    getData: function () {
        let data = {};
        $.each($("[data*=data]"), function () {
            let tagNm = this.tagName.toLowerCase();
            if (tagNm == 'select' || tagNm == 'textarea') {
                data[this.id] = this.value
            } else if (tagNm == 'input') {
                let tagTp = $(this).prop('type');
                if (tagTp == 'checkbox') {
                    data[this.id] = $(this).is(":checked") ? "Y" : "N";
                } else {
                    let v = this.value;
                    if (this.id == 'newWrtDtCnt' || this.id == 'postDtCnt' || this.id == 'fileCnt'
                        || this.id == 'fileSize' || this.id == 'fileFullSize') {
                        v = v.replaceAll(",", "");
                    }
                    data[this.id] = v;
                }
            }
        })

        if (data.ctgryUseAt == "Y") {
            let list = [];
            $.each($("#tbody_ctgry").find("input:checkbox"), function () {
                let obj = {"boardCategoryId": this.id};
                let categoryNm = $(this).parent()
                                        .parent()
                                        .siblings('#td_ctgryNm')
                                        .children('#ctgryNm')
                                        .val();

                obj["categoryNm"] = categoryNm;
                list.push(obj);
            })
            data.ctgryList = list;
        }

        return data;
    },
    validation: function () {
        let result = true;

        $.each($("[data*=essential]"), function () {
            if (isNull(this.value)) {
                if (this.tagNm == "select") {
                    alert(this.title + "을 선택해 주세요.");
                } else {
                    alert(this.title + "을 입력해 주세요.");
                }
                $(this).focus();
                return result = false;
            }
        })

        if ($("#ctgryUseAt").is(":checked")) {
            if ($("#tbody_ctgry").find('tr').length == 0) {
                alert("카테고리를 추가해 주세요.");
                return false;
            }

            $.each($("#tbody_ctgry").find("input:text"), function () {
                if (isNull(this.value)) {
                    alert("카테고리명을 선택해 주세요.");
                    $(this).focus();
                    return result = false;
                }
            })
        }

        return result;
    },
    delView: function (boardMngId, ncnm){
        let url = `${domain}/del/${ncnm}/${boardMngId}/all`;
        location.href = url;
    }
}

pageObj.pageStart = function () {
};