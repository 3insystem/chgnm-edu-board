$(document).ready(function() {
    $("#imageFile").on("change", function(e) {
        $("#preview").show();
        var file = e.target.files[0];

        if(pageObj.isImageFile(file)) {
            var reader = new FileReader();
            reader.onload = function(e) {

               // console.log('===== reader.onload =====')
                var img = new Image();
                img.src = e.target.result;

                img.onload = function() {
                    var imgWidth = this.width;
                    var imgHeight = this.height;
                    //console.log('imgWidth : ' + imgWidth);
                    //console.log('imgHeight : ' + imgHeight);
                    if(imgWidth > 360 || imgHeight.height > 230) {
                        alert('이미지의 크기는 최대 360px x 230px 이하이어야 합니다.');
                        $("#imageFile").val('');
                        $("#preview").hide();
                    } else {
                        $("#preview").attr("src", e.target.result).css({
                            'margin-bottom': '1rem',
                            'border': `1px solid #E5E5E5`,
                            'border-radius': '0.5rem',
                            'padding': '0.625rem'
                        });
                    }
                }
            }
            reader.readAsDataURL(file);
        } else {
            alert("이미지 파일만 첨부 가능합니다.");
            $("#imageFile").val("");
            $("#preview").attr("src", "");
        }
    });
});

var domain;
var pageObj = {
    mainPage: function () {
        domain = `${defaultPageUrl}/${categoryId}`;
        $view.main();
    },
    communityMainPage: function () {
        let mainPageUrl = `${defaultPageUrl}/${categoryId}`;
        mainPageUrl = mainPageUrl.replace("boardMng","community");
        domain = mainPageUrl;
        $view.main();
    },
    addPage: function () {
        $ajax.post({
            url: "/api/v1/board/getCategoryCount/"+boardMngId,
            success: function (res) {
                if (res <= 0) {
                    alert("등록된 카테고리가 없습니다. 카테고리를 설정해 주세요.");
                    location.href = "/adm/edu/boardMng/"+boardMngId+"/edit";
                } else {
                    domain = `${defaultPageUrl}/${categoryId}`;
                    $view.add();
                }
            }
        })
    },
    detailPage: function (boardId) {
        domain = `${defaultPageUrl}/${categoryId}`;
        $view.detail(boardId)
    },
    updatePage: function (boardId) {
        domain = `${defaultPageUrl}/${categoryId}`;
        $view.edit(boardId);
    },
    // 업로드 파일 이미지 파일인지 확인
    isImageFile(file) {
        // 파일명에서 확장자를 가져옴
        var ext = file.name.split(".").pop().toLowerCase();
        return ($.inArray(ext, ["jpg", "jpeg", "gif", "png"]) === -1) ? false : true;
    },

    save: function () {
        //let formData = $form.getData();
        let form =  $("form[id=theForm]");
        let formData = new FormData(form[0]);
        //console.log(formData);

        let obj = {
            boardCategoryId: $('#boardCategoryId').val(), //게시판카테고리번호
            bIdx: $('#idx').val(),
            title: $('#title').val(),
            nuriNum: $("input[name='nuriNum']:checked").val()
        }

        const fileInput = document.getElementById('imageFile');
        const selectedFiles = fileInput.files[0];

        //alert($files.getFiles().length);
         //formData.append("files", selectedFiles);
        formData.append("thumFile", selectedFiles);

         if ($files.getFiles().length > 0) {
            for (var i = 0; i < $files.getFiles().length; i++) {
                formData.append("files", $files.getFiles()[i]);
            }
         }

        formData.append("saveData", new Blob([JSON.stringify(obj)], {
            type: "application/json"
        }));

       // console.log(formData);
        if (this.validation(formData)) {
            $ajax.postMultiPart({
                url: "/api/v1/board/thumbnail/add",
                data: formData,
                success: function (res) {
                   // console.log(res)
                    if (!isNull(res)) {
                        alert("게시물 저장이 완료되었습니다.");
                        pageObj.detailPage(res);
                    } else {
                        alert("게시물 저장 중 에러가 발생했습니다.");
                        return false;
                    }
                }
            })
        }
    },
    update: function (boardId) {
        //let data = $form.getData();

        //let formData = $form.getData();
        let form =  $("form[id=theForm]");
        let formData = new FormData(form[0]);
        //console.log(formData);

        let obj = {
            boardCategoryId: $('#boardCategoryId').val(), //게시판카테고리번호
            bIdx: $('#idx').val(),
            title: $('#title').val(),
            nuriNum: $("input[name='nuriNum']:checked").val()
        }

        const fileInput = document.getElementById('imageFile');
        const selectedFiles = fileInput.files[0];

        //alert($files.getFiles().length);
        //formData.append("files", selectedFiles);
       // formData.append("thumFile", selectedFiles);
       // if (isNull($("[name=savedFileSns]").val())) {

        if ($files.getFiles().length > 0) {
            for (var i = 0; i < $files.getFiles().length; i++) {
                formData.append("files", $files.getFiles()[i]);
            }
        }

        if (!isNull($("[name=savedThumFileSns]").val())) {
            // 서버로 전송할 JSON 배열을 생성
            var savedThumFileIdsArray = [$("[name=savedThumFileSns]").val()];
            obj['savedThumFileSns'] = savedThumFileIdsArray;
        }

        var file_val = $("#imageFile").val();
        if(file_val) {
            formData.append("thumFile", selectedFiles);
        } else {
            if (!isNull($("[name=savedFileSns]").val())) {
                // 서버로 전송할 JSON 배열을 생성
                var savedFileIdsArray = [$("[name=savedFileSns]").val()];
                obj['savedFileSns'] = savedFileIdsArray;
            }
        }

        formData.append("updateData", new Blob([JSON.stringify(obj)], {
            type: "application/json"
        }));

        formData.append("boardThumbnailId", boardId);

        //console.log(formData);
        if (this.validation(formData)) {
            $ajax.postMultiPart({
                url: "/api/v1/board/thumbnail/edit",
                data: formData,
                success: function (res) {
                    if (isNull(res) || res <= 0) {
                        alert("게시물 저장 중 에러가 발생했습니다.");
                        return false;
                    } else {
                        alert("게시물 저장이 완료되었습니다.");
                        pageObj.detailPage(boardId)
                    }
                }
            })
        }
    },
    delete: function (boardId) {
        if (confirm("삭제하시겠습니까?")) {
            let reason = prompt("삭제사유", "");

            if(reason !== null && reason !== undefined) {
                let data = {
                    boardId: boardId,
                    delReason: reason
                };

                $ajax.post({
                    url: "/api/v1/board/thumbnail/delete",
                    data: data,
                    success: function (res) {
                        if (isNull(res) || res <= 0) {
                            alert("게시물 삭제 중 에러가 발생했습니다.");
                            return false;
                        } else {
                            alert("게시물 삭제가 완료되었습니다.");
                            pageObj.mainPage();
                        }
                    }
                })
            } else {
                alert("삭제사유를 입력해 주세요.");
                return false;
            }
        }
    },
    deleteAll: function () {
        let checked = $checkBox.getAllChecked();
        if (checked.length > 0) {
            if (confirm("삭제하시겠습니까?")) {
                let reason = prompt("삭제사유", "");

                if(reason.length > 0) {
                    let data = {
                        boardIdList: checked,
                        delReason: reason
                    };
                    $ajax.post({
                        url: "/api/v1/board/thumbnail/deleteAll",
                        data: data,
                        success: function (res) {
                            if (isNull(res) || res == 0) {
                                alert("게시물 삭제 과정에서 오류가 발생했습니다.");
                                return false;
                            } else {
                                alert("게시물 삭제가 완료되었습니다.");
                                location.reload();
                            }
                        }
                    })
                } else {
                    alert("삭제사유를 입력해 주세요.");
                    return false;
                }
            }
        } else {
            alert("삭제할 항목을 선택해 주세요.");
        }
    },
    restore: function (boardId) {
        if (confirm("복구하시겠습니까?")) {
            let reason = prompt("복구사유", "");

            if(reason !== null && reason !== undefined) {
                let data = {
                    boardId: boardId,
                    restoreReason: reason
                };

                $ajax.post({
                    url: "/api/v1/board/thumbnail/restore",
                    data: data,
                    success: function (res) {
                        if (isNull(res) || res <= 0) {
                            alert("게시물 복구 중 에러가 발생했습니다.");
                            return false;
                        } else {
                            alert("게시물 복구가 완료되었습니다.");
                            pageObj.mainPage();
                        }
                    }
                })
            } else {
                alert("복구사유를 입력해 주세요.");
                return false;
            }
        }
    },
    restoreAll: function () {
        let checked = $checkBox.getAllChecked();
        if (checked.length > 0) {
            if (confirm("복구하시겠습니까?")) {
                let reason = prompt("복구사유", "");

                if(reason.length > 0) {
                    let data = {
                        boardIdList: checked,
                        restoreReason: reason
                    };
                    $ajax.post({
                        url: "/api/v1/board/thumbnail/restoreAll",
                        data: data,
                        success: function (res) {
                            if (isNull(res) || res == 0) {
                                alert("게시물 복구 과정에서 오류가 발생했습니다.");
                                return false;
                            } else {
                                alert("게시물 복구가 완료되었습니다.");
                                location.reload();
                            }
                        }
                    })
                } else {
                    alert("복구사유를 입력해 주세요.");
                    return false;
                }
            }

        } else {
            alert("삭제할 항목을 선택해 주세요.");
        }
    },
    validation: function (data) {
       // alert(data);
        let result = true;
        let banWord = setup.banWord.split(",");
        for (const [key, value] of data.entries()) {
            if (key == 'boardCategoryId' && isNull(value)) {
                alert("카테고리를 선택해 주세요.");
                $("#boardCategoryId").focus();
                result = false;
                break;
            }

            if (key == 'title' && isNull(value)) {
                alert("제목을 선택해 주세요.");
                $("#title").focus();
                result = false;
                break;
            }

            if ($("#mode").val() == 'reg') {
                var file_val = $("#imageFile").val();

                if(!file_val) {
                    alert("목록이미지를 등록해 주세요.");
                    $("#imageFile").focus();
                    result = false;
                    break;
                }
            } else {
                var file_val = $("#imageFile").val();
                if (key == 'savedThumFileSns' && isNull(value)) {

                    if(!file_val) {
                        alert("목록이미지를 등록해 주세요.");
                        $("#imageFile").focus();
                        result = false;
                        break;
                    }
                }
            }

            if (setup.boardType === "BOARD00008") {
                if (key == 'ebookLink' && isNull(value)) {
                    alert("E-Book 주소를 입력 주세요.");
                    $("#ebookLink").focus();
                    result = false;
                    break;
                }
            }

            if (key == 'content') {
                for (let i = 0; i < banWord.length; i++) {
                    let v = value.replace(/(<([^>]+)>)/gi, '');
                    if (v.includes(banWord[i])) {
                        alert("금지 단어가 포함되어 있습니다. [\""+banWord[i]+"\"]");
                        $("#content").focus();
                        result = false;
                        break;
                    }
                }
                if (result == false) break;
            }
        }

        if (isNull($("[name=nuriNum]:checked").val())) {
            alert("공공누리 유형을 선택해 주세요.");
            return false;
        }

        return result;
    }
}

pageObj.pageStart = function () {
    boardEvent.init(boardMngId);
};