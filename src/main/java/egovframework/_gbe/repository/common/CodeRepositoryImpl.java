package egovframework._gbe.repository.common;

import com.querydsl.core.Tuple;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.common.util.PagingUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import static egovframework._gbe.domain.code.QCmmnCode.cmmnCode;

@RequiredArgsConstructor
public class CodeRepositoryImpl implements CodeRepositoryQueryDsl {

    private final JPAQueryFactory queryFactory;

    @Override
    public Page<Tuple> findClCodeList(SearchParam searchParam, Pageable pageable) {
        JPAQuery<Tuple> query = queryFactory
                .select(
                        cmmnCode.clCode,
                        cmmnCode.clNm
                )
                .from(cmmnCode)
                .where(
                    CodeRepositoryConditions.containsWord(searchParam)
                )
                .groupBy(cmmnCode.clCode, cmmnCode.clNm);

        return PagingUtils.getPage(query, pageable);
    }
}
