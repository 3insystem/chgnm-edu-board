package egovframework._gbe.repository.common;

import com.querydsl.core.Tuple;
import egovframework._gbe.common.parameter.SearchParam;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CodeRepositoryQueryDsl {

    Page<Tuple> findClCodeList(SearchParam searchParam, Pageable pageable);
}
