package egovframework._gbe.repository.common;

import com.querydsl.core.types.dsl.BooleanExpression;
import egovframework._gbe.common.parameter.SearchParam;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import static egovframework._gbe.domain.code.QCmmnCode.cmmnCode;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CodeRepositoryConditions {

    public static BooleanExpression containsWord(SearchParam searchParam) {
        if (StringUtils.hasLength(searchParam.getSelectKey1())) {
            if (searchParam.getSelectKey1().equals("category")) {
                return cmmnCode.clCode.contains(searchParam.getWord1());
            } else {
                return cmmnCode.clNm.contains(searchParam.getWord1());
            }
        } else {
            if (StringUtils.hasLength(searchParam.getWord1())) {
                return cmmnCode.clCode.contains(searchParam.getWord1());
            } else {
                return null;
            }
        }
    }
}
