package egovframework._gbe.repository.common;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface CommonDao {

    /*
     * 유저 ID로 사업자번호 찾기
     * bizrno 사업자번호
     */
    String selBizrnoByUsid(String bizrno);

    List<Map<String, String>> selCodeList(String clCode);

    Map<String, String> selOrgCdSection(String orgCd);

    List<Map<String, String>> selMainOrgCd(Map<String, String> param);

    /*
     * Select box용, 지원청 노출 박스
     * param : orgCd 조직코드
     */
    List<Map<String, String>> selAduOrgCd();

    /*
     * Select box용, 제일 마지막 박스
     * param : orgCd 조직코드
     */
    List<Map<String, String>> selNomalOrgCd(Map<String, String> param);

    /*
     * 조직코드로 상위코드 조회(계층형)
     * orgCd 조직코드
     */
    List<Map<String, String>> selUpperOrgByOrgCd(String orgCd);

    /*
     * 조직코드로 회원유형 조회(계층형)
     * orgCd 조직코드
     */
    Map<String, String> selOrgCdToUseSe(String usid);
}