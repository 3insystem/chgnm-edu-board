package egovframework._gbe.repository.common;

import egovframework._gbe.domain.code.CmmnCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CodeRepository extends JpaRepository<CmmnCode, String>, CodeRepositoryQueryDsl {

    /* TB_CMMN_CODE 에서 C_CODE, CODE_NM을 반환 */
    List<CmmnCode> findByClCode(String clCode);
}
