package egovframework._gbe.repository.msg;

import egovframework._gbe.domain.msg.messenger.Messenger;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MessengerRepository extends JpaRepository<Messenger, Long> {
}
