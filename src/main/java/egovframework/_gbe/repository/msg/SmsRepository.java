package egovframework._gbe.repository.msg;

import egovframework._gbe.domain.msg.sms.Sms;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SmsRepository extends JpaRepository<Sms, Long> {
}
