package egovframework._gbe.repository.user.auth;

import egovframework._gbe.domain.user.auth.Auth;
import egovframework._gbe.repository.user.UserDao;
import egovframework._gbe.repository.user.UserRepositoryQueryDsl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthRepository extends JpaRepository<Auth, String>, AuthRepositoryQueryDsl, AuthDao {
}
