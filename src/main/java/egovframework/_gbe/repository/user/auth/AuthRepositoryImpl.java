package egovframework._gbe.repository.user.auth;

import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class AuthRepositoryImpl {

    private final JPAQueryFactory queryFactory;
}
