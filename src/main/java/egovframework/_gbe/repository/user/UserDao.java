package egovframework._gbe.repository.user;

import egovframework._gbe.web.dto.user.UserDto;
import egovframework._gbe.web.dto.user.cmpny.BizrnoCheckDto;
import egovframework._gbe.web.dto.user.cmpny.CmpnyDto;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface UserDao {

    // 메소드와 Sql Mapper(xml)와 매핑 규칙
    // 1. 인터페이스의 FQN과 동일한 네임스페이스를 가지고
    //    (ex: <mapper namespace="egovframework._gbe.repository.user.UserDao">)
    // 2. 동일한 메소드 이름의 ID를 가지는 구문
    //    (ex: <select id="selectUserDetail"/>)
    /*
     * 유저 세션정보 조회
     * param userId 유저 ID
     */
    public UserDto selectUserDetail(String userId);

    public BizrnoCheckDto selectCmpnyBizrno(String bizrno);

    public CmpnyDto selCmpny(String bizrno);

    public int updUserSe(Object o);

    public int updAuthSe(Object o);

    public String selAhrztNoVal(String usid);
}