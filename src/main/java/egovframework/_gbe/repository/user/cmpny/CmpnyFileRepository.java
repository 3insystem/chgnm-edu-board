package egovframework._gbe.repository.user.cmpny;

import egovframework._gbe.domain.user.cmpny.CmpnyFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CmpnyFileRepository extends JpaRepository<CmpnyFile, Long> {
}
