package egovframework._gbe.repository.user.cmpny;

import egovframework._gbe.domain.user.cmpny.Cmpny;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CmpnyRepository extends JpaRepository<Cmpny, String> {
}
