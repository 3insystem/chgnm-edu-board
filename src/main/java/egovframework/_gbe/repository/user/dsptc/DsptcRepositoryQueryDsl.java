package egovframework._gbe.repository.user.dsptc;

public interface DsptcRepositoryQueryDsl {

    String findDsptcId();
}
