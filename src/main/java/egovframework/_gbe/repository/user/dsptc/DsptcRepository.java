package egovframework._gbe.repository.user.dsptc;

import egovframework._gbe.domain.user.dsptc.Dsptc;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DsptcRepository extends JpaRepository<Dsptc, String>, DsptcRepositoryQueryDsl {

}
