package egovframework._gbe.repository.user.dsptc;

import com.querydsl.jpa.impl.JPAQueryFactory;
import egovframework._gbe.domain.user.dsptc.Dsptc;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static egovframework._gbe.domain.user.dsptc.QDsptc.dsptc;

@RequiredArgsConstructor
public class DsptcRepositoryQueryDslImpl implements DsptcRepositoryQueryDsl {

    private final JPAQueryFactory queryFactory;


    @Override
    public String findDsptcId() {
        String now = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
        String prefix = "DSPTC-"+now+"-";

        Dsptc find = queryFactory
                .selectFrom(dsptc)
                .where(dsptc.dsptcNo.startsWith(prefix))
                .orderBy(dsptc.dsptcNo.desc())
                .limit(1)
                .fetchOne();

        if (find != null) {
            int no = Integer.parseInt(find.getDsptcNo().replaceAll(prefix, ""))+1;
            String dsptcNo = prefix + String.format("%03d", no);
            return dsptcNo;
        } else {
            return prefix + "001";
        }
    }
}
