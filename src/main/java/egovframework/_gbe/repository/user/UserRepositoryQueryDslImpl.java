package egovframework._gbe.repository.user;

import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.common.util.CryptUtils;
import egovframework._gbe.common.util.PagingUtils;
import egovframework._gbe.domain.user.User;
import egovframework._gbe.web.dto.user.FindDto;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

import static egovframework._gbe.domain.user.QUser.user;
import static egovframework._gbe.repository.user.UserRepositoryConditions.findIdOrPwdConditions;

@RequiredArgsConstructor
public class UserRepositoryQueryDslImpl implements UserRepositoryQueryDsl {

    private final JPAQueryFactory queryFactory;
    private final CryptUtils cryptUtils;

    @Override
    public int findByIdCnt(String userId, String suffix) {

        List<User> fetch = queryFactory
                .selectFrom(user)
                .where(
                        user._super.usid.eq(userId)
                                .or(user._super.usid.eq(userId + suffix))
                )
                .fetch();

        return fetch.size();
    }

/*
    @Override
    public User findByEmail(FindDto findDto) {
        String userNm = cryptUtils.encrypt(findDto.getUserNm());
        String email = cryptUtils.encrypt(findDto.getEmail());

        User fetch = queryFactory
                .selectFrom(user)
                .where(
                        findIdOrPwdConditions(findDto, userNm, email)
                ).fetchFirst();

        return fetch;
    }
*/

    @Override
    public User findByMbtlnum(FindDto findDto) {
        String userNm = cryptUtils.encrypt(findDto.getUserNm());
        String mbtlnum = cryptUtils.encrypt(findDto.getMbtlnum());

        User fetch = queryFactory
                .selectFrom(user)
                .where(
                        findIdOrPwdConditions(findDto, userNm, mbtlnum)
                ).fetchFirst();

        return fetch;
    }

    // 예시
    @Override
    public Page<User> findAll(SearchParam searchParam, Pageable pageable) {
        JPAQuery<User> query = queryFactory
                .selectFrom(user);

        return PagingUtils.getPage(query, pageable);
    }
}
