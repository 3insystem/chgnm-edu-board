package egovframework._gbe.repository.user;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.Expressions;
import egovframework._gbe.common.util.CommonUtils;
import egovframework._gbe.domain.user.QUser;
import egovframework._gbe.web.dto.user.FindDto;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import static egovframework._gbe.domain.user.QUser.user;


@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class UserRepositoryConditions {

    public static BooleanExpression findIdOrPwdConditions(FindDto dto, String userNm, String mbtlnum) {
        if (dto.getUserId() != null) {

            String userId = "";

            if("cmpny".equals(CommonUtils.getServerType())){
                userId = dto.getUserId() + "_CMPNY!";
            } else {
                userId = dto.getUserId();
            }

            return Expressions.allOf(
                    equalsUserId(userId),
                    equalsUserNm(userNm),
                    equalsMbtlnum(mbtlnum)
            );
        } else {
            return Expressions.allOf(
                    equalsUserNm(userNm),
                    equalsMbtlnum(mbtlnum)
            );
        }
    }

    private static BooleanExpression equalsUserId(String userId) {
        return user.usid.eq(userId);
    }

    private static BooleanExpression equalsUserNm(String userNm) {
        return user.userNm.eq(userNm);
    }

    private static BooleanExpression equalsMbtlnum(String mbtlnum) {
        return user.mbtlnum.eq(mbtlnum);
    }
}
