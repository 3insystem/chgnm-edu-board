package egovframework._gbe.repository.user.organization;

import egovframework._gbe.domain.user.organization.Organizaion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface OrganizationRepository extends JpaRepository<Organizaion, String> {

    @Query("SELECT O FROM Organizaion O WHERE O.orgUseYn = 'Y' AND O.delYn = 'N' AND O.orgCd = :orgCd")
    Optional<Organizaion> findOrgByOrgCd(@Param("orgCd") String orgCd);
}
