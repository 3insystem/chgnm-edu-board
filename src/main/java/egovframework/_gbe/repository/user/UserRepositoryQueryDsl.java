package egovframework._gbe.repository.user;

import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.domain.user.User;
import egovframework._gbe.web.dto.user.FindDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserRepositoryQueryDsl {

    int findByIdCnt(String userId, String suffix);

    //User findByEmail(FindDto findDto);

    User findByMbtlnum(FindDto findDto);
    // 예시
    Page<User> findAll(SearchParam searchParam, Pageable pageable);
}
