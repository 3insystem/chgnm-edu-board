package egovframework._gbe.repository.board;

import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.web.dto.board.*;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Mapper
public interface BoardMngDao {

    List<BoardNavDto> findNavList();

    List<BoardMngDto> findAll(SearchParam searchParam);

    List<Map<String, String>> findBoardMngByBoardType(String boardType);

    List<BoardListDto> allMngNotice(SearchParam searchParam);

    List<BoardListDto> selMngNotice(SearchParam searchParam);

    List<BoardListDto> selRepNotice(SearchParam searchParam);

    List<BoardListDto> selEmpNotice(SearchParam searchParam);

    List<BoardListDto> findBoardAll(SearchParam searchParam);

    List<BoardListDto> findInsttBoardAll(SearchParam searchParam);

    List<BoardListDto> findRepBoardAll(SearchParam searchParam);

    List<BoardListDto> findEmpBoardAll(SearchParam searchParam);

    List<BoardListDto> findInsttQBoardAll(SearchParam searchParam);

    List<BoardListDto> findRepQBoardAll(SearchParam searchParam);

    List<BoardListDto> findEmpQBoardAll(SearchParam searchParam);

    List<VideoListDto> findVideoAll(SearchParam searchParam);

    VideoSave findVideo(Long boardId);

    List<ScheduleDto> findHolidayAll();

    ScheduleDto findHoliday(Long scheduleId);

    List<ScheduleDto> findScheduleAll(Map<String, String> map);

    ScheduleDto findSchedule(Long scheduleId);

    ScheduleDto findDelSchedule(Map searchMap);

    BoardSave findBoard(Long boardId);

    BoardMngDto findById(Long boardMngId);

    Map<String, Object> getSecretAt(Map<String, Object> map);

    List<CtgryDto> findCtgryByBoardMngId(Long boardMngId);

    int saveBoardMng(BoardMngSave saveData);

    int saveBoardCtgry(List<Map<String, String>> ctgryList);

    int updateBoardMng(BoardMngSave updateData);

    int updateBoardCtgry(List<Map<String, String>> updateList);

    int change(Long boardMngId);

    Long saveBoard(BoardSave saveData);

    int updateBoard(BoardSave updateData);

    int deleteBoard(Map<String, Object> map);

    int restoreBoard(Map<String, Object> map);

    int comment(Map<String, Object> map);

    int saveSchedule(ScheduleDto saveData);

    int updateSchedule(ScheduleDto updateData);

    int deleteSchedule(Map<String, Object> map);

    int restoreSchedule(Map<String, Object> map);

    int saveVideo(VideoSave saveData);

    int updateVideo(VideoSave updateData);

    int deleteVideo(Map<String, Object> map);

    int restoreVideo(Map<String, Object> map);

    /*
     * 썸네일 게시판 목록 조회
     * */
    List<ThumbnailListDto> findThumbnailList(SearchParam searchParam);
    
    /*
     * 썸네일 게시판 상세 조회
     * */
    ThumbnailSave findThumbnail(Long boardId);

    /*
     * 썸네일 게시판 저장
     * */
    Long saveThumbnail(ThumbnailSave seveData);
    
    /*
     * 썸네일 게시판 수정
     * */
    int updateThumbnail(ThumbnailSave updateData);
    
    /*
     * 썸네일 게시판 삭제
     * */
    int deleteThumbnail(Map<String, Object> delMap);

    void saveThumbnailFiles(List<Map<String, Object>> files);

    /*
     * 썸네일 게시판 복구
     * */
    int restoreThumbnail(Map<String, Object> rstMap);

    /**
     * 공통
     * */
    String selectFileId();

    List<BoardFileDto> selectFiles(Long fileId);

    /**
     * 첨부파일 (파일아이디, 파일순번)
     */
    BoardFileDto findFiles(Long fileId, Long fileSn);

    void saveFiles(List<Map<String, Object>> files);

    void deleteFile(Long fileId);

    /* FAQ 게시판 저장 */
    Long saveFaq(FaqSave faqSave);

    /* FAQ 게시판 수정 */
    Long updateFaq(FaqSave faqSave);

    /* FAQ 게시판 사용 안함 처리 */
    void delFaq(FaqSave delList);

    /* FAQ 게시판 사용 함 처리 */
    int rstFaq(FaqSave faqSave);

    /* FAQ 게시판 목록 조회 */
    List<FaqListDto> findFaqList(SearchParam searchParam);

    /* FAQ 상세 조회 */
    FaqSave faqDetail(Long boardId);

    /* 공휴일 게시판 저장 */
    Long saveHoliday(HolidaySave holidaySave);

    /* 공휴일 게시판 수정 */
    Long updateHoliday(HolidaySave holidaySave);

    /* 공휴일 게시판 사용 안함 처리 */
    void delHoliday(HolidayListDto delList);

    /* 공휴일 게시판 목록 조회 */
    List<HolidayListDto> holidayList(SearchParam searchParam);

    /* 공휴일 게시판 상세 조회 */
    HolidaySave holidayDetail(Long holidayId);

    int transportBoard(List<Map<String, Object>> map);

    int transportFaq(List<Map<String, Object>> map);

    int transportThumbnail(List<Map<String, Object>> map);

    int transportVideo(List<Map<String, Object>> map);

    int transportSchedule(List<Map<String, Object>> map);
}
