package egovframework._gbe.repository.print;

import egovframework._gbe.web.dto.print.PrintReportChrgDto;
import egovframework._gbe.web.dto.print.PrintReportDetailDto;
import egovframework._gbe.web.dto.print.PrintReportResultDto;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PrintDao {

    PrintReportResultDto getPrintReportData(Long reprtNo);
    List<PrintReportDetailDto> getPrintReportDetails(Long reprtNo);
    List<PrintReportChrgDto> getInChargeReportInfo(Long reprtNo);
}
