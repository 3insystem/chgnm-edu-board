package egovframework._gbe.repository.mber.rep;

import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.web.dto.mber.rep.cmpds.CmpdsListDto;
import egovframework._gbe.web.dto.mber.rep.mntmgt.MntmgtListDto;
import egovframework._gbe.web.dto.mber.rep.report.ReportListDto;
import egovframework._gbe.web.dto.mber.rep.report.ReportOrgListDto;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface ReportDao {

    List<ReportListDto> getReportList(SearchParam searchParam);  /* 업체 대표 리포트 리스트 */

    List<ReportOrgListDto> getReportOrgList(String userId);  /* 업체 대표 학교 리스트 */

    List<MntmgtListDto> getmntmgtList(Map map);  /* 업체 대표 유지관리 리스트 */

    List<MntmgtListDto> getrtdetaillist(String reprtNo);
}
