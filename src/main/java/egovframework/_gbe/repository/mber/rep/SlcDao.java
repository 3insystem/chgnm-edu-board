package egovframework._gbe.repository.mber.rep;

import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.web.dto.mber.rep.SlctnDto;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface SlcDao {
    public List<SlctnDto> selSlcList(SearchParam searchParam);
    public int insertSlc(Map map);
    public int deleteSlc(Map map);
    public int slcConfm(Map map);
    public int slcStopBtn(Map map);
    public int checkSlc(Map map);
    public List<SlctnDto> selSchList(Map map);
    public List<SlctnDto> selCpn(Map map);
    int deleteChrgInstt(Map map);
    public String selOrg(Map map);
}
