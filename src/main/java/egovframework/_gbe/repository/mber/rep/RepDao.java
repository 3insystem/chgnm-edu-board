package egovframework._gbe.repository.mber.rep;

import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.web.dto.common.ChrgInsttDto;
import egovframework._gbe.web.dto.common.EmpManageDto;
import egovframework._gbe.web.dto.mber.rep.DeleteEmpAgreeDto;
import egovframework._gbe.web.dto.mber.rep.SelCmpnyAgreeListDto;
import egovframework._gbe.web.dto.mber.rep.SelEmpListDto;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface RepDao {
    /*
    * 업체 직원 리스트 호출
    * bizrno 사업자 번호
    */
    public List<SelEmpListDto> selEmpList(SearchParam searchParam);
    public SelEmpListDto selEmpHist(SearchParam searchParam);
    public List<SelCmpnyAgreeListDto> selCmpnyAgreeList(Map map);

    public int insEmpManage(EmpManageDto dto);

    public int insEmpManage(DeleteEmpAgreeDto dto);

    public int updInitChrgInstt(EmpManageDto dto);

    public int updInitChrgInstt(DeleteEmpAgreeDto dto);

    public int insChrgInsttList(List list);

}