package egovframework._gbe.repository.mber.rep;

import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.web.dto.mber.rep.cmpds.CmpdsListDto;
import egovframework._gbe.web.dto.mber.rep.dashboard.DashboardCmpdsDto;
import egovframework._gbe.web.dto.mber.rep.dashboard.DashboardCountDto;
import egovframework._gbe.web.dto.mber.rep.dashboard.DashboardMntmgtDto;
import egovframework._gbe.web.dto.mber.rep.mntmgt.MntmgtDetailChargerList;
import egovframework._gbe.web.dto.mber.rep.mntmgt.MntmgtDetailDto;
import egovframework._gbe.web.dto.mber.rep.mntmgt.MntmgtListCountDto;
import egovframework._gbe.web.dto.mber.rep.mntmgt.MntmgtListDto;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface RepMntmgtDao {
    List<MntmgtListDto> getRepMntmgtList(SearchParam searchParam);  /* 업체 대표 유지관리 리스트 */
    MntmgtListCountDto getRepMntmgtListCount(SearchParam searchParam); /* 업체 대표 유지관리 리스트 카운트 */
    List<MntmgtListDto> getEmpMntmgtList(SearchParam searchParam);  /* 업체 담당자 유지관리 리스트 */
    MntmgtListCountDto getEmpMntmgtListCount(SearchParam searchParam);  /* 업체 담당자 유지관리 카운트 */
    MntmgtDetailDto getMntmgtDetail(Map data);  /* 유지관리 상세 */
    DashboardCountDto getDashboardMntmgtCount(String bizrno); /* 업체 대표 대시보드 카운트 */
    List<DashboardMntmgtDto> getDashboardMntmgt(String bizrno); /* 업체 대표 대시보드 리스트 */
    DashboardCountDto getDashboardMntmgtCountEmp(Map data); /* 업체 담당자 대시보드 카운트 */
    List<DashboardMntmgtDto> getDashboardMntmgtEmp(Map data); /* 업체 담당자 대시보드 리스트 */
    List<MntmgtDetailChargerList> getMntmgtDetailChargerList (Map data); /* 유지관리 담당자 리스트 */
}
