package egovframework._gbe.repository.mber.rep;

import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.web.dto.mber.rep.cmpds.CmpdsDetailDto;
import egovframework._gbe.web.dto.mber.rep.cmpds.CmpdsListDto;
import egovframework._gbe.web.dto.mber.rep.dashboard.DashboardCmpdsDto;
import egovframework._gbe.web.dto.mber.rep.dashboard.DashboardCountDto;
import egovframework._gbe.web.dto.mber.rep.mntmgt.MntmgtDetailChargerList;
import egovframework._gbe.web.dto.mber.rep.mntmgt.MntmgtListCountDto;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface RepCmpdsDao {
    List<CmpdsListDto> getRepCmpdsList(SearchParam searchParam);  /* 업체 대표 소모품 리스트 */
    MntmgtListCountDto getRepCmpdsListCount(SearchParam searchParam);  /* 업체 대표 소모품 리스트 카운트 */
    List<CmpdsListDto> getEmpCmpdsList(SearchParam searchParam);  /* 업체 담당자 소모품 리스트 */
    MntmgtListCountDto getEmpCmpdsListCount(SearchParam searchParam);  /* 업체 담당자 소모품 리스트 카운트 */
    DashboardCountDto getDashboardCmpdsCount(String bizrno);  /* 업체 대표 대시보드 소모품 카운트 */
    List<DashboardCmpdsDto> getDashboardCmpds(String bizrno);  /* 업체 대표 대시보드 소모품 리스트 */
    DashboardCountDto getDashboardCmpdsCountEmp(Map data);  /* 업체 담당자 대시보드 소모품 카운트 */
    List<DashboardCmpdsDto> getDashboardCmpdsEmp(Map data);  /* 업체 담당자 대시보드 소모품 리스트 */
    CmpdsDetailDto getCmpdsDetail(Map data);  /* 소모품 상세 */
    List<MntmgtDetailChargerList> getCmpdsDetailChargerList (Map data); /* 유지관리 담당자 리스트 */
}
