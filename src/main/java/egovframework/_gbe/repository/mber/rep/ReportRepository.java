package egovframework._gbe.repository.mber.rep;

import egovframework._gbe.domain.report.Report;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReportRepository extends JpaRepository<Report, Long> {
}
