package egovframework._gbe.repository.mber.cmpds;

import egovframework._gbe.domain.cmpds.Cmpds;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CmpdsCmpnyRepository extends JpaRepository<Cmpds, String> {
}
