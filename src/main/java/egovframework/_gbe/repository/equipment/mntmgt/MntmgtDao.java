package egovframework._gbe.repository.equipment.mntmgt;

import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.web.dto.equipment.EquipmentDetailDto;
import egovframework._gbe.web.dto.equipment.EquipmentListDto;
import egovframework._gbe.web.dto.equipment.EquipmentUnprocessedDto;
import egovframework._gbe.web.dto.user.UserDto;
import egovframework._gbe.web.dto.user.cmpny.CmpnyDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface MntmgtDao {
    List<EquipmentListDto> getMntmgtList(SearchParam searchParam);
    List<EquipmentListDto> getAdminMntmgtList(SearchParam searchParam);
    EquipmentDetailDto getMntmgt(String recepNo);
    List<CmpnyDto> getCompanyInstitutionsMatching(String orgCd);
    List<EquipmentUnprocessedDto> getUnprocessedStatus(String orgCd);
    int getUnprocessedCount(SearchParam searchParam);
    int getAdminUnprocessedCount(SearchParam searchParam);
    UserDto getCompanyRepresentativeInfo(@Param("bizrno") String bizrno, @Param("orgCd") String orgCd);
}
