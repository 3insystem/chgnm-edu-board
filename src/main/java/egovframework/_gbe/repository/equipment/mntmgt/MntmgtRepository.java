package egovframework._gbe.repository.equipment.mntmgt;

import egovframework._gbe.domain.mntmgt.Mntmgt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface MntmgtRepository extends JpaRepository<Mntmgt, String>, MntmgtRepositoryQueryDsl {

    Optional<Mntmgt> findTop1ByRceptNoContainingOrderByRceptNoDesc(@Param("rceptNo") String rceptNo);


}
