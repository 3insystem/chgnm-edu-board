package egovframework._gbe.repository.equipment.mntmgt;

import egovframework._gbe.domain.mntmgt.Mntmgt;

import java.util.List;

public interface MntmgtRepositoryQueryDsl {

    List<Mntmgt> findByBizrno(String bizrno);
}
