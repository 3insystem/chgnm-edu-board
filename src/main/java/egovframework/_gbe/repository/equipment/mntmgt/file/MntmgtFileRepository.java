package egovframework._gbe.repository.equipment.mntmgt.file;

import egovframework._gbe.domain.mntmgt.Mntmgt;
import egovframework._gbe.domain.mntmgt.file.MntmgtFile;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MntmgtFileRepository extends JpaRepository<MntmgtFile, Long> {
    List<MntmgtFile> findByMntmgt(Mntmgt mntmgt);
}
