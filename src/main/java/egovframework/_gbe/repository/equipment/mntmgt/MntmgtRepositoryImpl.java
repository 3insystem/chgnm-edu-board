package egovframework._gbe.repository.equipment.mntmgt;

import com.querydsl.jpa.impl.JPAQueryFactory;
import egovframework._gbe.domain.mntmgt.Mntmgt;
import lombok.RequiredArgsConstructor;

import java.util.List;

import static egovframework._gbe.domain.mntmgt.QMntmgt.mntmgt;

@RequiredArgsConstructor
public class MntmgtRepositoryImpl implements MntmgtRepositoryQueryDsl {

    private final JPAQueryFactory queryFactory;

    @Override
    public List<Mntmgt> findByBizrno(String bizrno) {
        return queryFactory
                .selectFrom(mntmgt)
                .where(mntmgt.bizrno.bizrno.eq(bizrno))
                .fetch();
    }
}
