package egovframework._gbe.repository.equipment.cmpds.file;

import egovframework._gbe.domain.cmpds.Cmpds;
import egovframework._gbe.domain.cmpds.file.CmpdsFile;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CmpdsFileRepository extends JpaRepository<CmpdsFile, Long> {
    List<CmpdsFile> findByCmpds(Cmpds cmpds);
}
