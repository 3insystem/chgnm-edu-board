package egovframework._gbe.repository.equipment.cmpds;

import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.web.dto.equipment.EquipmentDetailDto;
import egovframework._gbe.web.dto.equipment.EquipmentListDto;
import egovframework._gbe.web.dto.equipment.EquipmentUnprocessedDto;
import egovframework._gbe.web.dto.user.cmpny.CmpnyDto;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface CmpdsDao {
    List<EquipmentListDto> getCmpdsList(SearchParam searchParam);
    List<EquipmentListDto> getAdminCmpdsList(SearchParam searchParam);
    EquipmentDetailDto getCmpds(String recepNo);
    List<CmpnyDto> getCompanyInstitutionsMatching(String orgCd);
    List<EquipmentUnprocessedDto> getUnprocessedStatus(String orgCd);
    int getUnprocessedCount(SearchParam searchParam);
    int getAdminUnprocessedCount(SearchParam searchParam);
}
