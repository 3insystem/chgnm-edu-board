package egovframework._gbe.repository.equipment.cmpds;

import egovframework._gbe.domain.cmpds.Cmpds;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface CmpdsRepository extends JpaRepository<Cmpds, String> {

    Optional<Cmpds> findTop1ByRceptNoContainingOrderByRceptNoDesc(@Param("rceptNo") String rceptNo);


}
