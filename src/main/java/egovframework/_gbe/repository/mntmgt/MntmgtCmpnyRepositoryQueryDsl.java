package egovframework._gbe.repository.mntmgt;

import egovframework._gbe.web.dto.mber.rep.mntmgt.MntmgtDetailChargerList;

import java.util.List;

public interface MntmgtCmpnyRepositoryQueryDsl {

    String getMntmgtUseSe (String userId);

    List<MntmgtDetailChargerList> getMntmgtChargerList(String bizrno);
}
