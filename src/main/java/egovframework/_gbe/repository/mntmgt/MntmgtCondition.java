package egovframework._gbe.repository.mntmgt;

import com.querydsl.core.types.dsl.BooleanExpression;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import static egovframework._gbe.domain.mntmgt.QMntmgt.mntmgt;
import static egovframework._gbe.domain.user.QUser.user;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class MntmgtCondition {

    public static BooleanExpression containRceptNo(String recepNo) {
        return StringUtils.hasText(recepNo) ? mntmgt.rceptNo.contains(recepNo) : null;
    }

    public static BooleanExpression containOrgNm(String orgNm) {
        return StringUtils.hasText(orgNm) ? user.orgNm.contains(orgNm) : null;
    }

    public static BooleanExpression containCmpNm(String cmpNm) {
        return StringUtils.hasText(cmpNm) ? mntmgt.bizrno.cmpnyNm.contains(cmpNm) : null;
    }

    public static BooleanExpression containSubject(String subject) {
        return StringUtils.hasText(subject) ? mntmgt.subject.contains(subject) : null;
    }

    public static BooleanExpression containChargerNm(String chargerNm) {
        return StringUtils.hasText(chargerNm) ? user.userNm.contains(chargerNm) : null;
    }

//    public static BooleanExpression containApplicant(String applicant) {
//        return StringUtils.hasText(applicant) ? mntmgt.userNm.contains(applicant) : null;
//    }

    public static BooleanExpression equalsStatusType(String statusType) {
        return StringUtils.hasText(statusType) ? mntmgt.statusType.eq(statusType) : null;
    }

    public static BooleanExpression equalsMaintenanceType(String word) {
        return StringUtils.hasText(word) ? mntmgt.maintenanceType.eq(word) : null;
    }

    public static BooleanExpression betweenDate(LocalDate startDate, LocalDate endDate) {
        LocalDateTime start = null;
        LocalDateTime end = null;

        if (startDate != null && endDate == null) {
            start = startDate.atStartOfDay();
            end = LocalDateTime.now();
        }

        if(startDate != null && endDate != null) {
            start = startDate.atStartOfDay(); // 등록 시작일
            end = endDate.atTime(LocalTime.MAX);  // 등록 종료일
        }

        return startDate == null ? null : mntmgt.registDt.between(start, end);
    }


}
