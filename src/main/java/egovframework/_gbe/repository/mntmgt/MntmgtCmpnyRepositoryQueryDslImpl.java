package egovframework._gbe.repository.mntmgt;

import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQueryFactory;
import egovframework._gbe.web.dto.mber.rep.mntmgt.MntmgtDetailChargerList;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.List;

import static egovframework._gbe.domain.user.QUser.user;

@Repository
@RequiredArgsConstructor
public class MntmgtCmpnyRepositoryQueryDslImpl implements MntmgtCmpnyRepositoryQueryDsl {

    private final JPAQueryFactory queryFactory;

    /* 유지보수 상세 담당자 리스트 */
    @Override
    public List<MntmgtDetailChargerList> getMntmgtChargerList(String bizrno) {

        List<MntmgtDetailChargerList> result = queryFactory.select(Projections.fields(
                MntmgtDetailChargerList.class,
                        ExpressionUtils.as(user.userNm, "chargerNm"),
                        ExpressionUtils.as(user.usid, "chargerId")))
                .from(user)
                .where(user.cmpny.bizrno.eq(bizrno).and(user.useSe.code.eq("USESE00002").or(user.useSe.code.eq("USESE00003"))) )
                .fetch();
        return result;
    }

    /* 권한 구분 */
    @Override
    public String getMntmgtUseSe (String userId) {
        String result = queryFactory.select(
                        ExpressionUtils.as(user.useSe.code, "useSe")
                )
                .from(user)
                .where(user.usid.eq(userId))
                .fetchOne();
        return result;
    }
}
