package egovframework._gbe.repository.mntmgt;

import egovframework._gbe.domain.mntmgt.Mntmgt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface MntmgtCmpnyRepository extends JpaRepository<Mntmgt, String> {

    Optional<Mntmgt> findTop1ByRceptNoContainingOrderByRceptNoDesc(@Param("rceptNo") String rceptNo);



}
