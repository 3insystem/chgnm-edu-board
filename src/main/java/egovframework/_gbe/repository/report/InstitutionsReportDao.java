package egovframework._gbe.repository.report;

import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.web.dto.report.ReportChrgDto;
import egovframework._gbe.web.dto.report.ReportDetailDto;
import egovframework._gbe.web.dto.report.ReportDto;
import egovframework._gbe.web.dto.user.cmpny.CmpnyDto;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface InstitutionsReportDao {

    List<ReportDto> getReportList(SearchParam searchParam);
    List<ReportDto> getAdminReportList(SearchParam searchParam);
    List<CmpnyDto> getCompanyInstitutionsMatching(String orgCd);
    ReportDto getReportDetail(Long reprtNo);
    List<ReportDetailDto> getReportDetails(Long reprtNo);
    List<ReportChrgDto> getInChargeInfo(Long reprtNo);
}
