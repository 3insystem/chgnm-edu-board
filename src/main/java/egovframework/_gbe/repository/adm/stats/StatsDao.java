package egovframework._gbe.repository.adm.stats;

import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.web.dto.adm.stats.StatsCmpdsDto;
import egovframework._gbe.web.dto.adm.stats.StatsMntmgtDto;
import org.apache.ibatis.annotations.Mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Mapper
public interface StatsDao {

    public ArrayList<String> selYear();

    public List<StatsMntmgtDto> statsMntmgt(SearchParam searchParam);

    public List<StatsCmpdsDto> statsCmpds(SearchParam searchParam);

    public List<Map<String, String>> statsCommon(SearchParam searchParam);

    public List<Map<String, String>> statsJoin(SearchParam searchParam);

    public List<Map<String, String>> statsMenu(SearchParam searchParam);
}
