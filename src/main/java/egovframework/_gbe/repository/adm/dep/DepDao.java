package egovframework._gbe.repository.adm.dep;

import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.web.dto.adm.dep.DepListDto;
import egovframework._gbe.web.dto.adm.dep.DepUpdDto;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface DepDao {

    List<DepListDto> selDepList(SearchParam searchParam);

    int updDepCd(DepUpdDto depUpdDto);

    int updUpperOrg(DepUpdDto depUpdDto);
}
