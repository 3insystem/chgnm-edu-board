package egovframework._gbe.repository.adm.auth;

import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.web.dto.adm.auth.AuthInfoDto;
import egovframework._gbe.web.dto.adm.auth.AuthListDto;
import egovframework._gbe.web.dto.adm.auth.AuthSaveDto;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface AuthDao {

    List<AuthListDto> selAuthList(SearchParam searchParam);

    AuthInfoDto selChangeOrgCdToUseSe(String userId);
    int updDsptcSave(AuthSaveDto dto);

    String selOrgNm(String orgCd);

}
