package egovframework._gbe.repository.adm.slc;

import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.web.dto.mber.rep.SlctnDto;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface InsttSlcDao {
    public List<SlctnDto> insttSelSlcList(SearchParam searchParam);

    public int slcConfm(Map map);

    public int slcStopBtn(Map map);

    public List<SlctnDto> slcConfmFlag(Map map);

    public List<SlctnDto> insttSchSelSlcList(SearchParam searchParam);

    public String slcBizrno(String slctnNo);

    public List<SlctnDto> selSms(String bizrno);

    public String selOrgNm(String slctnNo);
}
