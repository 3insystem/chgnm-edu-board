package egovframework._gbe.repository.log;

import egovframework._gbe.web.dto.log.LogGuestDto;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface LogGuestDao {

    int insSysLog(LogGuestDto dto);
}
