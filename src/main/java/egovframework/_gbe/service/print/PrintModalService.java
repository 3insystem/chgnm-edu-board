package egovframework._gbe.service.print;

import egovframework._gbe.repository.print.PrintDao;
import egovframework._gbe.web.dto.print.PrintReportChrgDto;
import egovframework._gbe.web.dto.print.PrintReportDetailDto;
import egovframework._gbe.web.dto.print.PrintReportResultDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PrintModalService {

    private final PrintDao printDao;

    /**
     * 점검보고서 인쇄 데이터
     */
    public PrintReportResultDto getReportPrint(Long reprtNo) {
        PrintReportResultDto resultDto = printDao.getPrintReportData(reprtNo);
        List<PrintReportDetailDto> reportDetailDtos = printDao.getPrintReportDetails(reprtNo);
        List<PrintReportChrgDto> inChargeInfos = printDao.getInChargeReportInfo(reprtNo);

        resultDto.setDetailDtos(reportDetailDtos);
        resultDto.setInChargeInfos(inChargeInfos);

        return resultDto;
    }
}
