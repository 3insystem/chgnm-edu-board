package egovframework._gbe.service.adm.stats;

import egovframework._gbe.common.excpetion.UnableChangeStatusException;
import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.repository.adm.stats.StatsDao;
import egovframework._gbe.web.dto.adm.stats.StatsCmpdsDto;
import egovframework._gbe.web.dto.adm.stats.StatsMntmgtDto;
import egovframework._gbe.web.dto.adm.stats.StatsJsonDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


@Service
@RequiredArgsConstructor
@Transactional
public class StatsService {

    private final StatsDao statsDao;

    public StatsJsonDto statsRec(SearchParam searchParam){

        StatsJsonDto JsonDto = new StatsJsonDto();
        //유지보수
        List<StatsMntmgtDto> mList = statsDao.statsMntmgt(searchParam);
        String mntSe[] = {"장애", "설치", "점검", "기타"};
        ArrayList<ArrayList<Integer>> mntList = new ArrayList<>();
        /*
        //소모품
        List<StatsCmpdsDto> cList = statsDao.statsCmpds(searchParam);
        String cmpSe[] = {"인쇄용품", "컴퓨터용품"};
        ArrayList<ArrayList<Integer>> cmpList = new ArrayList<>();
        */
        //분류 수 만큼 초기화
        for (int i=0; i < mntSe.length; i++){
            mntList.add(new ArrayList<>());
        }
        /*
        for (int i=0; i < cmpSe.length; i++){
            cmpList.add(new ArrayList<>());
        }
        */
        //데이터 추가
        //유지보수
        for (StatsMntmgtDto dto: mList) {
            JsonDto.setCategories(dto.getYmd());
            mntList.get(0).add(dto.getMnt001());
            mntList.get(1).add(dto.getMnt002());
            mntList.get(2).add(dto.getMnt003());
            mntList.get(3).add(dto.getMnt004());
        }
        /*
        //소모품
        for (StatsCmpdsDto dto: cList) {
            cmpList.get(0).add(dto.getCmp001());
            cmpList.get(1).add(dto.getCmp002());
        }
        */
        //분류 수 만큼 객체 생성
        //유지보수
        for (int i=0; i < mntSe.length; i++){
            Map series = new HashMap();
            series.put("name", mntSe[i]);
            series.put("data", mntList.get(i));
            JsonDto.setSeries(series);
        }
        /*
        //소모품
        for (int i=0; i < cmpSe.length; i++){
            Map series = new HashMap();
            series.put("name", cmpSe[i]);
            series.put("data", cmpList.get(i));
            JsonDto.setSeries(series);
        }
        */
        return JsonDto;
    }

    public StatsJsonDto statsCommon(SearchParam searchParam) throws UnableChangeStatusException {

        StatsJsonDto JsonDto = new StatsJsonDto();
        ArrayList<String> dateList  = new ArrayList<>();
        if ("d".equals(searchParam.getSelectKey1())) {
            LocalDate s = searchParam.getStartDate();
            LocalDate e = searchParam.getEndDate();

            while (s.compareTo(e) != 1) {
                dateList.add(s.toString());
                s = s.plusDays(1);
            }
            searchParam.setSelectList(dateList);
        } else if ("m".equals(searchParam.getSelectKey1())) {
            LocalDate m = LocalDate.parse(searchParam.getSel0() + "-01-01");

            for(int i=0; i<12; i++){
                dateList.add(m.format(DateTimeFormatter.ofPattern("YYYY-MM")).toString());
                m = m.plusMonths(1);
            }
        } else {
            throw new UnableChangeStatusException("일/월별 선택 값 없음.");
        }
        //유지보수
        List<Map<String, String>> sList = statsDao.statsCommon(searchParam);

        for (Map<String, String> map: sList) {
            Map series = new HashMap();
            ArrayList<Integer> dataList = new ArrayList<>();

            for (Map.Entry<String, String> entry : map.entrySet()) {
                if(!entry.getKey().equals("statsNm")){
                    dataList.add(Integer.valueOf(String.valueOf(entry.getValue())));
                }
            }

            series.put("name", map.get("statsNm"));
            series.put("data", dataList);
            JsonDto.setSeries(series);
        }

        JsonDto.setCategories(dateList);

        return JsonDto;
    }

    public StatsJsonDto statsJoin(SearchParam searchParam) throws UnableChangeStatusException {

        StatsJsonDto JsonDto = new StatsJsonDto();
        ArrayList<String> dateList  = new ArrayList<>();
        if ("d".equals(searchParam.getSelectKey1())) {
            LocalDate s = searchParam.getStartDate();
            LocalDate e = searchParam.getEndDate();

            while (s.compareTo(e) != 1) {
                dateList.add(s.toString());
                s = s.plusDays(1);
            }
            searchParam.setSelectList(dateList);
        } else if ("m".equals(searchParam.getSelectKey1())) {
            LocalDate m = LocalDate.parse(searchParam.getSel0() + "-01-01");

            for(int i=0; i<12; i++){
                dateList.add(m.format(DateTimeFormatter.ofPattern("YYYY-MM")).toString());
                m = m.plusMonths(1);
            }
        } else {
            throw new UnableChangeStatusException("일/월별 선택 값 없음.");
        }
        //가입자수
        List<Map<String, String>> sList = statsDao.statsJoin(searchParam);

        for (Map<String, String> map: sList) {
            Map series = new HashMap();
            ArrayList<Integer> dataList = new ArrayList<>();

            for (Map.Entry<String, String> entry : map.entrySet()) {
                if(!entry.getKey().equals("statsNm")){
                    dataList.add(Integer.valueOf(String.valueOf(entry.getValue())));
                }
            }

            series.put("name", map.get("statsNm"));
            series.put("data", dataList);
            JsonDto.setSeries(series);
        }

        JsonDto.setCategories(dateList);

        return JsonDto;
    }

    public StatsJsonDto statsMenu(SearchParam searchParam) throws UnableChangeStatusException {

        StatsJsonDto JsonDto = new StatsJsonDto();
        ArrayList<String> dateList  = new ArrayList<>();
        if ("d".equals(searchParam.getSelectKey1())) {
            LocalDate s = searchParam.getStartDate();
            LocalDate e = searchParam.getEndDate();

            while (s.compareTo(e) != 1) {
                dateList.add(s.toString());
                s = s.plusDays(1);
            }
            searchParam.setSelectList(dateList);
        } else if ("m".equals(searchParam.getSelectKey1())) {
            LocalDate m = LocalDate.parse(searchParam.getSel0() + "-01-01");

            for(int i=0; i<12; i++){
                dateList.add(m.format(DateTimeFormatter.ofPattern("YYYY-MM")).toString());
                m = m.plusMonths(1);
            }
        } else {
            throw new UnableChangeStatusException("일/월별 선택 값 없음.");
        }
        //메뉴수
        List<Map<String, String>> sList = statsDao.statsMenu(searchParam);

        for (Map<String, String> map: sList) {
            Map series = new HashMap();
            ArrayList<Integer> dataList = new ArrayList<>();

            for (Map.Entry<String, String> entry : map.entrySet()) {
                if(!entry.getKey().equals("statsNm")){
                    dataList.add(Integer.valueOf(String.valueOf(entry.getValue())));
                }
            }

            series.put("name", map.get("statsNm"));
            series.put("data", dataList);
            JsonDto.setSeries(series);
        }

        JsonDto.setCategories(dateList);

        return JsonDto;
    }

    public ArrayList<String> selYear() {

        ArrayList<String> result = statsDao.selYear();

        return result;
    }
}
