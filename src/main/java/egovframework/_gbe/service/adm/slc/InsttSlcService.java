package egovframework._gbe.service.adm.slc;

import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.common.util.SecurityUtils;
import egovframework._gbe.repository.adm.slc.InsttSlcDao;
import egovframework._gbe.web.dto.mber.rep.SlctnDto;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class InsttSlcService {

    private final InsttSlcDao insttslcDao;


    public Page<SlctnDto> insttselSlcList(SearchParam searchParam, Pageable pageable) {
        //검색값이 없다면 자신이 가진 코드값으로 조회
        if(searchParam.getViewCd() == null){
            searchParam.setViewCd(SecurityUtils.getLoginUser().getUpperOrgCd());
        }
        searchParam.settingPage(pageable);

        List<SlctnDto> dto = insttslcDao.insttSelSlcList(searchParam);

        Integer totalCnt = 0;
        if (dto.size() > 0) {
            totalCnt = dto.get(0).getTotalCnt();
        }

        return new PageImpl<>(dto, pageable, totalCnt);
    }

    public Map slcConfm(String slctnNo, String userId, String orgCd, String bizrNo) {
        Map map = new HashMap<>();
        map.put("orgCd", orgCd);
        map.put("userId", userId);
        map.put("slctnNo", slctnNo);
        map.put("viewCd", orgCd);

        List<SlctnDto> flag = insttslcDao.slcConfmFlag(map);
        if( flag.get(0).getRm().equals("0")) {
            map.put("rm", "1");
        } else {
            map.put("rm", "0");
        }
        int result = insttslcDao.slcConfm(map);
        Map resultMap = new HashMap<>();
        if(result == 0){
            resultMap.put("result", "F");
            resultMap.put("msg", "요청을 실패했습니다.");
        } else {
            resultMap.put("result", "S");
        }
        return resultMap;
    }

    public Map slcStopBtn(String slctnNo, String userId) {
        Map map = new HashMap<>();
        map.put("userId", userId);
        map.put("slctnNo", slctnNo);
        int result = insttslcDao.slcStopBtn(map);
        Map resultMap = new HashMap<>();
        if(result == 0){
            resultMap.put("result", "F");
            resultMap.put("msg", "요청을 실패했습니다.");
        } else {
            resultMap.put("result", "S");
        }
        return resultMap;
    }

    public Page<SlctnDto> insttSupSelSlcList(SearchParam searchParam, Pageable pageable) {
        if(searchParam.getViewCd() == null){
            searchParam.setViewCd(SecurityUtils.getLoginUser().getUpperOrgCd());
        }

        searchParam.settingPage(pageable);

        List<SlctnDto> dto = insttslcDao.insttSelSlcList(searchParam);

        Integer totalCnt = 0;
        if (dto.size() > 0) {
            totalCnt = dto.get(0).getTotalCnt();
        }

        return new PageImpl<>(dto, pageable, totalCnt);
    }

    public Page<SlctnDto> insttSchSelSlcList(SearchParam searchParam, Pageable pageable) {
        String orgCd = SecurityUtils.getLoginUser().getOrgCd();
        List<SlctnDto> dto = new ArrayList<>();
        searchParam.setViewCd(orgCd);
        searchParam.settingPage(pageable);
        dto = insttslcDao.insttSchSelSlcList(searchParam);

        Integer totalCnt = 0;
        if (dto.size() > 0) {
            totalCnt = dto.get(0).getTotalCnt();
        }

        return new PageImpl<>(dto, pageable, totalCnt);
    }

    public String slcBizrno(String slctnNo) {
        return insttslcDao.slcBizrno(slctnNo);
    }

    public List<SlctnDto> selSms(String bizrno) {
        return insttslcDao.selSms(bizrno);
    }

    public String selOrgNm(String slctnNo) {
        return insttslcDao.selOrgNm(slctnNo);
    }
}
