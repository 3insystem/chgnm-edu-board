package egovframework._gbe.service.adm.auth;

import egovframework._gbe.common.excpetion.UnableChangeStatusException;
import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.common.util.SecurityUtils;
import egovframework._gbe.repository.adm.auth.AuthDao;
import egovframework._gbe.repository.user.UserDao;
import egovframework._gbe.web.dto.adm.auth.AuthInfoDto;
import egovframework._gbe.web.dto.adm.auth.AuthListDto;
import egovframework._gbe.web.dto.adm.auth.AuthSaveDto;
import egovframework._gbe.web.dto.msg.MessengerDto;
import egovframework._gbe.web.dto.msg.NoticeMessengerDto;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
@RequiredArgsConstructor
@Transactional
public class AuthService {

    private final UserDao userDao;
    private final AuthDao authDao;

    public Page<AuthListDto> EduUserList(SearchParam searchParam, Pageable pageable){
        //검색값이 없다면 자신이 가진 코드값으로 조회
        if(searchParam.getViewCd() == null){
            searchParam.setViewCd(SecurityUtils.getLoginUser().getUpperOrgCd());
        }

        searchParam.settingPage(pageable);

        List<AuthListDto> list = authDao.selAuthList(searchParam);

        Integer totalCnt = 0;
        if (list.size() > 0) {
            totalCnt = list.get(0).getTotalCnt();
        }

        return new PageImpl<AuthListDto>(list, pageable, totalCnt);
    }

    public Map updAuthSaveList(List<AuthSaveDto>list) throws UnableChangeStatusException {

        int resultUser = 0;
        int resultAuth = 0;
        Map resultMap = new HashMap<>();
        List<NoticeMessengerDto> userList = new ArrayList();
        //유저들이 가진 기관코드로 권한 파악
        for (AuthSaveDto dto: list) {
            String usid = dto.getUsid();
            AuthInfoDto authInfoDto = authDao.selChangeOrgCdToUseSe(usid);
            dto.setUpdusrId();
            //메신저 관련 변수
            NoticeMessengerDto noticeMessengerDto = new NoticeMessengerDto();
            String orgNm = "";
            String authType = "";
            //파견 승인의 경우, 파견처의 타입에 따름
            if("DSPTC00002".equals(dto.getDsptcSe())){
                authInfoDto.setType(authInfoDto.getDsptcType());
            }
            if("mber".equals(dto.getUseSe())){
                dto.setUseSe("USESE00001");
                dto.setAuthSe("AUTH000001");
                switch (authInfoDto.getType()){
                    case "edu":
                        orgCdSetter(dto, authInfoDto.getUpperOrgCd(), authInfoDto.getOrgCd(), authInfoDto.getDsptcOrgCd());
                        break;
                    case "sup":
                        orgCdSetter(dto, authInfoDto.getUpperOrgCd(), authInfoDto.getOrgCd(), authInfoDto.getDsptcOrgCd());
                        break;
                    case "sch":
                        orgCdSetter(dto, authInfoDto.getOrgCd(), authInfoDto.getOrgCd(), authInfoDto.getDsptcOrgCd());
                        break;
                    default:
                        throw new UnableChangeStatusException("회원 정보에 문제가 발생했습니다");
                }
                //메신저 변수
                authType = "일반";
                // 관리자의 경우, 해당하는 기관관리자 코드,
                // 자기가 속한 부서의 상위 코드를 지정
            } else if("adm".equals(dto.getUseSe())){
                dto.setAuthSe("AUTH000002");
                switch (authInfoDto.getType()){
                    case "edu":
                        dto.setUseSe("USESE00008");
                        orgCdSetter(dto, authInfoDto.getUpperOrgCd(), authInfoDto.getOrgCd(), authInfoDto.getDsptcOrgCd());
                        break;
                    case "sup":
                        dto.setUseSe("USESE00007");
                        orgCdSetter(dto, authInfoDto.getUpperOrgCd(), authInfoDto.getOrgCd(), authInfoDto.getDsptcOrgCd());
                        break;
                    case "sch":
                        dto.setUseSe("USESE00006");
                        orgCdSetter(dto, authInfoDto.getOrgCd(), authInfoDto.getOrgCd(), authInfoDto.getDsptcOrgCd());
                        break;
                    default:
                        throw new UnableChangeStatusException("회원 정보에 문제가 발생했습니다");
                }
                //메신저 변수
                authType = "관리자";
            } else {
                throw new UnableChangeStatusException("데이터를 다시 확인 해 주십시오");
            }
            //데이터 편집후 바로 쿼리 실행
            if(!("N".equals(dto.getDsptcSe()))){
                if("DSPTC00003".equals(dto.getDsptcSe())){
                    dto.setDelAt("Y");
                } else {
                    dto.setDelAt("N");
                    //메신저 변수
                    authType = "파견";
                }
                authDao.updDsptcSave(dto);
            }
            resultUser = resultUser + userDao.updUserSe(dto);
            resultAuth = resultAuth + userDao.updAuthSe(dto);

            //메신저 설정

            orgNm = authDao.selOrgNm(dto.getOrgCd());
            noticeMessengerDto.setSender(usid);
            noticeMessengerDto.setReceiver(dto.getUpdusrId());
            noticeMessengerDto.setTitle("[충청남도교육청]권한/파견 변경");
            noticeMessengerDto.setContent(orgNm + "의\n" + authType + "권한으로 지정(변경) 되었습니다.\n권한이 변경된 경우 재로그인 해주시면 정상적으로 이용이 가능하십니다");
            userList.add(noticeMessengerDto);
        }

        if(resultUser == 0){
            throw new UnableChangeStatusException("회원 저장에 실패하였습니다");
        } else if(resultAuth == 0){
            throw new UnableChangeStatusException("권한 저장에 실패하였습니다");
        } else {
            resultMap.put("result", "S");
            resultMap.put("userList", userList);
        }

        return resultMap;
    }

    /* 기관 코드 설정
     *  dto : 저장용 dto
     *  upperOrgCd : 상위 기관 코드
     *  orgCd : 본인 기관 코드
     *  dsptcOrgCd : 파견처 코드
     *  DSPTC00002(파견)의 경우 파견처 코드
     *  DSPTC00003(파견종료)의 경우 본인 기관 코드
     *  (else)파견중이 아닐시에는 상위기관 코드(혹은 본인코드)를 지정
     */
    void orgCdSetter(AuthSaveDto dto, String upperOrgCd, String orgCd, String dsptcOrgCd){
        if("DSPTC00002".equals(dto.getDsptcSe())){
            dto.setOrgCd(dsptcOrgCd);
        } else if ("DSPTC00003".equals(dto.getDsptcSe())){
            dto.setOrgCd(orgCd);
        } else {
            dto.setOrgCd(upperOrgCd);
        }
    }
}
