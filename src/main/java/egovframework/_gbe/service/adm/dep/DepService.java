package egovframework._gbe.service.adm.dep;

import egovframework._gbe.common.excpetion.UnableChangeStatusException;
import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.common.util.SecurityUtils;
import egovframework._gbe.repository.adm.dep.DepDao;
import egovframework._gbe.repository.user.UserDao;
import egovframework._gbe.web.dto.adm.dep.DepListDto;
import egovframework._gbe.web.dto.adm.dep.DepUpdDto;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
@RequiredArgsConstructor
@Transactional
public class DepService {

    private final DepDao depDao;

    public Page<DepListDto> depList(SearchParam searchParam, Pageable pageable){
        //검색값이 없다면 자신이 가진 코드값으로 조회
        if(searchParam.getViewCd() == null){
            searchParam.setViewCd(SecurityUtils.getLoginUser().getUpperOrgCd());
        }

        searchParam.settingPage(pageable);

        List<DepListDto> list = depDao.selDepList(searchParam);

        Integer totalCnt = 0;
        if (list.size() > 0) {
            totalCnt = list.get(0).getTotalCnt();
        }

        return new PageImpl<DepListDto>(list, pageable, totalCnt);
    }

    public Map<String, String> depUpdate(DepUpdDto depUpdDto) throws Exception {

        int result = 0;
        Map resultMap = new HashMap();

        result = depDao.updDepCd(depUpdDto);
        if(result == 0){
            throw new UnableChangeStatusException("기관 저장에 실패했습니다");
        }

        result = depDao.updUpperOrg(depUpdDto);
        if(result == 0){
            throw new UnableChangeStatusException("기관 백업에 실패했습니다");
        }

        resultMap.put("result", "S");

        return resultMap;
    }
}
