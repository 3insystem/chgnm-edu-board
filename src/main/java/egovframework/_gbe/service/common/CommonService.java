package egovframework._gbe.service.common;

import com.querydsl.core.Tuple;
import egovframework._gbe.common.excpetion.UnableChangeStatusException;
import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.common.util.SecurityUtils;
import egovframework._gbe.domain.code.CmmnCode;
import egovframework._gbe.domain.user.SessionUser;
import egovframework._gbe.repository.common.CodeRepository;
import egovframework._gbe.repository.common.CommonDao;
import egovframework._gbe.repository.user.UserDao;
import egovframework._gbe.web.dto.adm.code.ClCodeDto;
import egovframework._gbe.web.dto.adm.code.CodeDto;
import egovframework._gbe.web.dto.user.UserDto;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional
public class CommonService {

    private final CommonDao commonDao;
    private final UserDao userDao;
    private final CodeRepository codeRepository;

    public Page<ClCodeDto> findClCodeList(SearchParam searchParam, Pageable pageable) {
        Page<Tuple> result = codeRepository.findClCodeList(searchParam, pageable);

        return new PageImpl<>(
                result.stream().map(ClCodeDto::new).collect(Collectors.toList()),
                pageable,
                result.getTotalElements()
        );
    }

    public List<CmmnCode> findAll(String clCode) {
        List<Map<String, String>> maps = commonDao.selCodeList(clCode);
        return maps
                .stream()
                .map(CmmnCode::new)
                .collect(Collectors.toList());
    }

    public CmmnCode findById(String cCode) {
        return codeRepository.findById(cCode)
                .orElseThrow(EntityNotFoundException::new);
    }
    /*
     * 유저 ID로 사업자번호 찾기
     * bizrno 사업자번호
     */
    public String selBizrnoByUsid(String userId){

        String bizrno = commonDao.selBizrnoByUsid(userId);

        return bizrno;
    }
    /*
     * 공통코드 selectBox 데이터 가공
     * clCode 분류코드
     */
    public Map selCmmnCode(String clCode){

        List<CmmnCode> listDto = findAll(clCode);
        Map resultMap = new HashMap<>();

        /* TB_CMMN_CODE에서 조회한 값을 키(코드) 값(코드명)으로 변환 */
        for (CmmnCode cmmnCode : listDto) {
            resultMap.put(cmmnCode.getCode(), cmmnCode.getCodeNm());
        }
        return resultMap;
    }

    public Map<String, String> selOrgCdSection(String orgCd) {
        return commonDao.selOrgCdSection(orgCd);
    }

    /*
     * 조직코드로 상위코드 조회(계층형)
     * orgCd 조직코드
     */
    public List<Map<String, String>> selUpperOrgByOrgCd(String orgCd){

        return commonDao.selUpperOrgByOrgCd(orgCd);
    }


    /*
     * Select box용, 지원청 노출 박스
     * param : orgCd 조직코드
     */
    public List<Map<String, String>> selAduOrgCd(){

        return commonDao.selAduOrgCd();
    }

    /*
     * Select box용, 제일 마지막 박스
     * param : orgCd 조직코드
     */
    public List<Map<String, String>> selNomalOrgCd(Map<String, String> param){

        return commonDao.selNomalOrgCd(param);
    }
    public List<Map<String, String>> selMainOrgCd(Map<String, String> param) {
        return commonDao.selMainOrgCd(param);
    }

    /*
     * 조직코드로 회원유형 조회
     * orgCd 조직코드
     */
    public Map<String, String> selOrgCdToUseSe(String usid){

        return commonDao.selOrgCdToUseSe(usid);
    }

    public void saveList(List<CodeDto> codeDtoList) {
        codeDtoList.forEach(dto -> {
            CmmnCode code = CmmnCode.of(dto);
            codeRepository.save(code);
        });
    }

    public void delete(String clCode) {
        List<CmmnCode> list = codeRepository.findByClCode(clCode);
        for (CmmnCode cmmnCode : list) {
            cmmnCode.delete();
        }
    }

    public void update(List<CodeDto> codeDtoList) {
        for (CodeDto codeDto : codeDtoList) {
            if (codeDto.getOrgCode() != null && !codeDto.getOrgCode().equals("")) {
                CmmnCode find = findById(codeDto.getOrgCode());
                find.update(codeDto);
            } else {
                CmmnCode cmmnCode = CmmnCode.of(codeDto);
                codeRepository.save(cmmnCode);
            }
        }
    }

    public boolean sessionUserChk() throws UnableChangeStatusException {

        SessionUser loginUser = SecurityUtils.getLoginUser();
        UserDto userDto = userDao.selectUserDetail(loginUser.getUserId());

        if(userDto.getAuthSe() == null){
            throw new UnableChangeStatusException("권한 정보가 없습니다");
        } else if ("AUTH000001".equals(userDto.getAuthSe())){
            return false;
        } else if ("AUTH000002".equals(userDto.getAuthSe())){
            return true;
        } else {
            throw new UnableChangeStatusException("알 수 없는 권한 정보입니다.");
        }
    }

    public void sessionUserChg() {

        SessionUser loginUser = SecurityUtils.getLoginUser();
        UserDto userDto = userDao.selectUserDetail(loginUser.getUserId());

        loginUser.setAuth(userDto.getRole());
        loginUser.setRole("ROLE_"+userDto.getAuthSe());

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        List<GrantedAuthority> ua = new ArrayList<>(auth.getAuthorities());
        ua.add(new SimpleGrantedAuthority("ROLE_"+userDto.getAuthSe()));

        Authentication newAuth = new UsernamePasswordAuthenticationToken(auth.getPrincipal(), auth.getCredentials(), ua);
        SecurityContextHolder.getContext().setAuthentication(newAuth);
    }

    public String selAhrztNoVal(String usid){
        return userDao.selAhrztNoVal(usid);
    }
}
