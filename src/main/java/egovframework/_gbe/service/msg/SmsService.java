package egovframework._gbe.service.msg;

import egovframework._gbe.domain.msg.sms.Sms;
import egovframework._gbe.repository.msg.SmsRepository;
import egovframework._gbe.service.user.UserService;
import egovframework._gbe.web.dto.msg.SmsDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class SmsService {

    private final SmsRepository smsRepository;
    private final UserService userService;

    public void sendSms(SmsDto dto) {
        dto.setPhnNum();
        dto.setReservedFg();

        // 장문
        if (dto.getSndMsg().getBytes().length > 70 || dto.getMsgTitle().getBytes().length > 70) {
            Sms LongSms = Sms.ofLong(dto);
            smsRepository.save(LongSms);
        } else { // 단문
            Sms ShortSms = Sms.ofShort(dto);
            smsRepository.save(ShortSms);
        }
    }

    public void sendSmsList(List<SmsDto> list) {
        for (SmsDto dto : list) {
            sendSms(dto);
        }
    }
}
