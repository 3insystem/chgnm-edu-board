package egovframework._gbe.service.msg;

import egovframework._gbe.domain.msg.messenger.Messenger;
import egovframework._gbe.domain.user.User;
import egovframework._gbe.repository.msg.MessengerRepository;
import egovframework._gbe.service.user.UserService;
import egovframework._gbe.web.dto.msg.MessengerDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class MessengerService {

    private final MessengerRepository messengerRepository;
    private final UserService userService;

    @Transactional
    public void sendOne(MessengerDto dto) {
        User sndr = userService.findById(dto.getSndId());
        User rcvr = userService.findById(dto.getRcvId());

        Messenger messenger = Messenger.of(dto, sndr, rcvr);
        messengerRepository.save(messenger);
    }

    @Transactional
    public void sendList(List<MessengerDto> list) {
        for (MessengerDto dto : list) {
            sendOne(dto);
        }
    }
}
