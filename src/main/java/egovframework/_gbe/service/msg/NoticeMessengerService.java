package egovframework._gbe.service.msg;

import egovframework._gbe.web.dto.msg.MessengerDao;
import egovframework._gbe.web.dto.msg.NoticeMessengerDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class NoticeMessengerService {

    private final MessengerDao messengerDao;

    public int sendOne(NoticeMessengerDto dto) {
        int result = messengerDao.sendMessenger(dto);

        return result;
    }

    @Transactional
    public void sendList(List<NoticeMessengerDto> list) {
        for (NoticeMessengerDto dto : list) {
            sendOne(dto);
        }
    }
}
