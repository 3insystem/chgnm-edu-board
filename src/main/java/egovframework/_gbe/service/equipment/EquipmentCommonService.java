package egovframework._gbe.service.equipment;

import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.common.response.RestResponse;
import egovframework._gbe.domain.code.CmmnCode;
import egovframework._gbe.web.dto.equipment.EquipmentDetailDto;
import egovframework._gbe.web.dto.equipment.EquipmentListDto;
import egovframework._gbe.web.dto.equipment.EquipmentSave;
import egovframework._gbe.web.dto.user.cmpny.CmpnyDto;
import org.json.simple.parser.ParseException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public interface EquipmentCommonService {
    String CODE_TYPE = "STTUS";

    RestResponse equipmentDataSave(EquipmentSave dto, HttpServletRequest request) throws Exception;

    Page<EquipmentListDto> getEquipmentDatas(Pageable pageable, SearchParam searchParam);

    EquipmentDetailDto getEquipmentDataDetail(String rceptNo);

    EquipmentSave getEquipmentData(String rceptNo);

    String cancelEquipmentData(String rceptNo);

    String equipmentDataLastComplete(String rceptNo);

    RestResponse equipmentDataUpdate(EquipmentSave equipmentSave, HttpServletRequest httpServletRequest) throws IOException, ParseException;

    List<CmpnyDto> getCompanyInstitutionsMatching(String rceptNo);

    default void isStatusType(EquipmentDetailDto equipmentDetailDto) {
        switch (equipmentDetailDto.getStatusType()) {
            case "접수":
                equipmentDetailDto.setRcept(true);
                break;
            case "취소":
                if(equipmentDetailDto.getRceptNm() != null) {
                    equipmentDetailDto.setRcept(true);
                }
                equipmentDetailDto.setCancel(true);
                break;
            case "보류":
                equipmentDetailDto.setHold(true);
                break;
            case "업체완료":
                equipmentDetailDto.setCompt(true);
                break;
            case "최종완료":
                equipmentDetailDto.setLastCompt(true);
                break;
        }
    }

    /**
     * 상태구분 코드 가져오기
     */
    default String getStatusType(List<CmmnCode> cmmnCodes, String codeNm) {
        return cmmnCodes.stream()
                .filter(code -> code.getCodeNm().equals(codeNm))
                .map(CmmnCode::getCode)
                .collect(Collectors.joining());
    }
}
