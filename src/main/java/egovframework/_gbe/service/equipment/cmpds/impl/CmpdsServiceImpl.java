package egovframework._gbe.service.equipment.cmpds.impl;

import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.common.response.RestResponse;
import egovframework._gbe.domain.cmpds.Cmpds;
import egovframework._gbe.domain.user.SessionUser;
import egovframework._gbe.domain.user.cmpny.Cmpny;
import egovframework._gbe.domain.user.organization.Organizaion;
import egovframework._gbe.repository.common.CodeRepository;
import egovframework._gbe.repository.equipment.cmpds.CmpdsDao;
import egovframework._gbe.repository.equipment.cmpds.CmpdsRepository;
import egovframework._gbe.repository.equipment.cmpds.file.CmpdsFileRepository;
import egovframework._gbe.repository.user.cmpny.CmpnyRepository;
import egovframework._gbe.repository.user.organization.OrganizationRepository;
import egovframework._gbe.service.equipment.EquipmentCommonService;
import egovframework._gbe.web.dto.equipment.EquipmentDetailDto;
import egovframework._gbe.web.dto.equipment.EquipmentListDto;
import egovframework._gbe.web.dto.equipment.EquipmentSave;
import egovframework._gbe.web.dto.equipment.file.EquipmentFileDto;
import egovframework._gbe.web.dto.user.cmpny.CmpnyDto;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.web.authentication.session.SessionAuthenticationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

import static egovframework._gbe.common.util.SecurityUtils.getLoginUser;
import static egovframework._gbe.type.StatusType.APPLY;
import static egovframework._gbe.type.StatusType.CANCEL;

@Service
@Qualifier(value = "CmpdsService")
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class CmpdsServiceImpl implements EquipmentCommonService {

    private final CmpdsDao cmpdsDao;
    private final CmpdsRepository cmpdsRepository;
    private final CmpdsFileRepository cmpdsFileRepository;
    private final CmpnyRepository cmpnyRepository;
    private final CodeRepository codeRepository;
    private final OrganizationRepository organizationRepository;

    /**
     * 소모품 저장
     */
    @Override
    @Transactional
    public RestResponse equipmentDataSave(EquipmentSave equipmentSave, HttpServletRequest request) throws IOException {

        Cmpny cmpny = cmpnyRepository.findById(equipmentSave.getBizrno())
                .orElseThrow(EntityNotFoundException::new);

        Organizaion organizaion = organizationRepository.findById(getLoginUser().getUpperOrgCd())
                .orElseThrow(EntityNotFoundException::new);

        if(equipmentSave.getExpendablesType().equals("CMPD100000")) {
            equipmentSave.setExpendablesType(equipmentSave.getSecondExpendablesType());
        }

        String statusType = getStatusType(codeRepository.findByClCode(CODE_TYPE), APPLY.getStatus());
        equipmentSave.setStatusType(statusType);
        Cmpds cmpds = Cmpds.of(createRceptNo(), equipmentSave, organizaion, cmpny);

        // 파일 업로드
        cmpds.addFile(equipmentSave.getFiles(), true);

        cmpdsRepository.save(cmpds);

        return null;
    }

    /**
     * 소모품 목록
     */
    @Override
    public Page<EquipmentListDto> getEquipmentDatas(Pageable pageable, SearchParam searchParam) {
        List<EquipmentListDto> cmpdss = new ArrayList<>();
        int unprocessedCount;
        SessionUser sessionUser = getLoginUser();
        searchParam.settingPage(pageable);
        searchParam.setOrgCd(sessionUser.getUpperOrgCd());

        if(searchParam.getViewCd() != null && !searchParam.getViewCd().equals(sessionUser.getUpperOrgCd())) {
            searchParam.setOrgCd(searchParam.getViewCd());
        }

        if(searchParam.getAllYn() != null && !searchParam.getAllYn().isEmpty()) {
            cmpdss = cmpdsDao.getAdminCmpdsList(searchParam);
            unprocessedCount = cmpdsDao.getAdminUnprocessedCount(searchParam);
        } else {
            cmpdss = cmpdsDao.getCmpdsList(searchParam);
            unprocessedCount = cmpdsDao.getUnprocessedCount(searchParam);
        }


        Integer totalCnt = 0;
        if (cmpdss.size() > 0) {
            cmpdss.get(0).setUnprocessedCount(unprocessedCount);
            totalCnt = cmpdss.get(0).getTotalCnt();
        }

        return new PageImpl<>(cmpdss, pageable, totalCnt);
    }

    /**
     * 소모품 상세
     */
    @Override
    public EquipmentDetailDto getEquipmentDataDetail(String rceptNo) {
        EquipmentDetailDto mntmgtDetailDto = cmpdsDao.getCmpds(rceptNo);

        if (mntmgtDetailDto == null) {
            throw new NoSuchElementException("잘못된 접근입니다.");
        }

        isStatusType(mntmgtDetailDto);
        mntmgtDetailDto.setFiles(getMntmgtFileDtos(rceptNo));

        return mntmgtDetailDto;
    }

    /**
     * 소모품 저장 데이터
     */
    @Override
    public EquipmentSave getEquipmentData(String rceptNo) {
        Cmpds cmpds = getCmpds(rceptNo);
        SessionUser loginUser = getLoginUser();

        if(!loginUser.getUserId().equals(cmpds.getRegisterId()) && !loginUser.getUseSe().equals("USESE00008")
                && !loginUser.getUseSe().equals("USESE00007") && !loginUser.getUseSe().equals("USESE00006")
                || !cmpds.getStatusType().equals("STTUS00001")) {
            throw new SessionAuthenticationException("잘못된 접근입니다.");
        }

        List<EquipmentFileDto> files = getMntmgtFileDtos(rceptNo);
        return EquipmentSave.entityToDto(cmpds, files);
    }

    /**
     * 소모품 신청내역 수정
     */
    @Override
    @Transactional
    public RestResponse equipmentDataUpdate(EquipmentSave equipmentSave, HttpServletRequest request) throws IOException {
        SessionUser loginUser = getLoginUser();
        Cmpds cmpds = getCmpds(equipmentSave.getRceptNo());
        Cmpny cmpny = cmpnyRepository.findById(equipmentSave.getBizrno())
                .orElseThrow(EntityNotFoundException::new);

        if(!loginUser.getUserId().equals(cmpds.getRegisterId()) && !loginUser.getUseSe().equals("USESE00008")
                && !loginUser.getUseSe().equals("USESE00007") && !loginUser.getUseSe().equals("USESE00006")) {
            throw new RuntimeException("잘못된 접근입니다.");
        }

        if(equipmentSave.getExpendablesType().equals("CMPD100000")) {
            equipmentSave.setExpendablesType(equipmentSave.getSecondExpendablesType());
        }

        cmpds.updateCmpds(equipmentSave, cmpny);
        cmpds.updateFile(equipmentSave.getFiles(), equipmentSave.getSavedFileIds(), true);

        return null;
    }

    /**
     * 소모품 신청내역 취소
     */
    @Override
    @Transactional
    public String cancelEquipmentData(String rceptNo) {
        Cmpds cmpds = getCmpds(rceptNo);
        String statusType = getStatusType(codeRepository.findByClCode(CODE_TYPE), CANCEL.getStatus());
        cmpds.changeStatus(statusType);
        return "신청내역이 취소되었습니다.";
    }

    /**
     * 소모품 최종완료
     */
    @Override
    public String equipmentDataLastComplete(String rceptNo) {
        return null;
    }

    /**
     * 기관 회사 매칭
     */
    @Override
    public List<CmpnyDto> getCompanyInstitutionsMatching(String rceptNo) {
        String orgCd = getLoginUser().getUpperOrgCd();

        if(rceptNo != null) {
            Cmpds cmpds = getCmpds(rceptNo);
            orgCd = cmpds.getOrgCd().getOrgCd();
        }

        return cmpdsDao.getCompanyInstitutionsMatching(orgCd);
    }

    /**
     * 접수번호 생성
     */
    private String createRceptNo() {
        String now = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
        final String NUMBERING_CODE = "E-".concat(now).concat("-");
        Optional<Cmpds> cmpdsOpt = cmpdsRepository.findTop1ByRceptNoContainingOrderByRceptNoDesc(NUMBERING_CODE);

        return !cmpdsOpt.isPresent() ? NUMBERING_CODE.concat("001") :
                NUMBERING_CODE.concat(String.format("%03d", (Integer.parseInt(cmpdsOpt.get().getRceptNo().split("-")[2]) + 1)));
    }

    private Cmpds getCmpds(String rceptNo) {
        return cmpdsRepository.findById(rceptNo)
                .orElseThrow(EntityNotFoundException::new);
    }

    private List<EquipmentFileDto> getMntmgtFileDtos(String rceptNo) {
        return cmpdsFileRepository.findByCmpds(getCmpds(rceptNo)).stream()
                .map(EquipmentFileDto::new)
                .collect(Collectors.toList());
    }

}
