package egovframework._gbe.service.equipment.mntmgt.impl;

import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.common.response.RestResponse;
import egovframework._gbe.common.util.CryptUtils;
import egovframework._gbe.common.util.WebFilterUtils;
import egovframework._gbe.common.util.file.FileUtils;
import egovframework._gbe.domain.mntmgt.Mntmgt;
import egovframework._gbe.domain.mntmgt.file.MntmgtFile;
import egovframework._gbe.domain.user.SessionUser;
import egovframework._gbe.domain.user.User;
import egovframework._gbe.domain.user.cmpny.Cmpny;
import egovframework._gbe.domain.user.organization.Organizaion;
import egovframework._gbe.repository.common.CodeRepository;
import egovframework._gbe.repository.equipment.mntmgt.MntmgtDao;
import egovframework._gbe.repository.equipment.mntmgt.MntmgtRepository;
import egovframework._gbe.repository.equipment.mntmgt.file.MntmgtFileRepository;
import egovframework._gbe.repository.user.UserRepository;
import egovframework._gbe.repository.user.cmpny.CmpnyRepository;
import egovframework._gbe.repository.user.organization.OrganizationRepository;
import egovframework._gbe.service.equipment.EquipmentCommonService;
import egovframework._gbe.web.dto.equipment.EquipmentDetailDto;
import egovframework._gbe.web.dto.equipment.EquipmentListDto;
import egovframework._gbe.web.dto.equipment.EquipmentSave;
import egovframework._gbe.web.dto.equipment.file.EquipmentFileDto;
import egovframework._gbe.web.dto.msg.MessageRequestDto;
import egovframework._gbe.web.dto.user.UserDto;
import egovframework._gbe.web.dto.user.cmpny.CmpnyDto;
import lombok.RequiredArgsConstructor;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.web.authentication.session.SessionAuthenticationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

import static egovframework._gbe.common.message.MessageDto.createSmsInfo;
import static egovframework._gbe.common.message.MessageDto.createSmsMessage;
import static egovframework._gbe.common.util.SecurityUtils.getLoginUser;
import static egovframework._gbe.common.util.file.FileUtils.deleteFile;
import static egovframework._gbe.type.StatusType.APPLY;

@Service
@Qualifier(value = "MntmgtService")
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class MntmgtServiceImpl implements EquipmentCommonService {

    private final MntmgtDao mntmgtDao;
    private final MntmgtRepository mntmgtRepository;
    private final MntmgtFileRepository mntmgtFileRepository;
    private final CmpnyRepository cmpnyRepository;
    private final CodeRepository codeRepository;
    private final OrganizationRepository organizationRepository;
    private final CryptUtils cryptUtils;
    private final UserRepository userRepository;

    /**
     * 유지관리 저장
     */
    @Override
    @Transactional
    public RestResponse equipmentDataSave(EquipmentSave equipmentSave, HttpServletRequest request) throws Exception {

        Cmpny cmpny = cmpnyRepository.findById(equipmentSave.getBizrno())
                .orElseThrow(EntityNotFoundException::new);

        Organizaion organizaion = organizationRepository.findById(getLoginUser().getUpperOrgCd())
                .orElseThrow(EntityNotFoundException::new);

        String statusType = getStatusType(codeRepository.findByClCode(CODE_TYPE), APPLY.getStatus());
        equipmentSave.setStatusType(statusType);
        equipmentSave.setCttpcl(cryptUtils.encrypt(equipmentSave.getCttpcl()));
        Mntmgt mntmgt = Mntmgt.of(createRceptNo(), equipmentSave, organizaion, cmpny);

        // 파일 업로드
        mntmgt.addFile(equipmentSave.getFiles(), true);
        String result = webFilterCheck(mntmgt, request);

        // 웹 필터에 걸렸을 경우
        if (!result.isEmpty()) {
            for (MntmgtFile file : mntmgt.getMntmgtFile()) {
                deleteFile(file.getStreFileNm());
            }

            return RestResponse.builder()
                    .message(result)
                    .build();
        }

        mntmgtRepository.save(mntmgt);

        // SMS 정보 생성
        UserDto userDto = mntmgtDao.getCompanyRepresentativeInfo(cmpny.getBizrno(), organizaion.getOrgCd());
        String cttpcl = cryptUtils.decrypt(mntmgt.getCttpcl());
        MessageRequestDto requestDto = new MessageRequestDto(cttpcl, createSmsInfo(userDto), getLoginUser().getUserId(), "장애관리", createSmsMessage(mntmgt, organizaion), "0", "");

        return RestResponse.builder()
                .message("등록")
                .data(requestDto)
                .build();
    }

    /**
     * 유지관리 목록
     */
    @Override
    public Page<EquipmentListDto> getEquipmentDatas(Pageable pageable, SearchParam searchParam) {
        List<EquipmentListDto> mntmgts;
        int unprocessedCount;
        SessionUser loginUser = getLoginUser();
        searchParam.settingPage(pageable);
        searchParam.setOrgCd(loginUser.getUpperOrgCd());

        if(searchParam.getWord4() != null && !searchParam.getWord4().isEmpty()) {
            searchParam.setWord4(cryptUtils.encrypt(searchParam.getWord4()));
        }

        if (searchParam.getViewCd() != null && !searchParam.getViewCd().equals(loginUser.getUpperOrgCd())) {
            searchParam.setOrgCd(searchParam.getViewCd());
        }

        if (searchParam.getViewCd() != null && loginUser.getAuth().equals("adm")) {
            mntmgts = mntmgtDao.getAdminMntmgtList(searchParam);
            unprocessedCount = mntmgtDao.getAdminUnprocessedCount(searchParam);
        } else {
            mntmgts = mntmgtDao.getMntmgtList(searchParam);
            unprocessedCount = mntmgtDao.getUnprocessedCount(searchParam);
        }

        // 정렬
        mntmgts = getReversedSort(mntmgts);
        searchParam.setWord4(cryptUtils.decrypt(searchParam.getWord4()));

        Integer totalCnt = 0;
        if (mntmgts.size() > 0) {
            mntmgts.get(0).setUnprocessedCount(unprocessedCount);
            totalCnt = mntmgts.get(0).getTotalCnt();
        }

        return new PageImpl<>(mntmgts, pageable, totalCnt);
    }

    /**
     * 유지관리 상세
     */
    @Override
    public EquipmentDetailDto getEquipmentDataDetail(String rceptNo) {
        EquipmentDetailDto mntmgtDetailDto = mntmgtDao.getMntmgt(rceptNo);

        if (mntmgtDetailDto == null) {
            throw new NoSuchElementException("잘못된 접근입니다.");
        }

        isStatusType(mntmgtDetailDto);
        mntmgtDetailDto.setFiles(getMntmgtFileDtos(rceptNo));

        return mntmgtDetailDto;
    }

    /**
     * 유지관리 저장 데이터
     */
    @Override
    public EquipmentSave getEquipmentData(String rceptNo) {
        Mntmgt mntmgt = getMntmgt(rceptNo);
        SessionUser loginUser = getLoginUser();
        String cttpcl = cryptUtils.decrypt(mntmgt.getCttpcl());
        if ((!loginUser.getUserId().equals(mntmgt.getRegisterId()) && !loginUser.getAuth().equals("adm"))
                || !mntmgt.getStatusType().equals("STTUS00001")) {
            throw new SessionAuthenticationException("잘못된 접근입니다.");
        }

        List<EquipmentFileDto> files = getMntmgtFileDtos(rceptNo);
        return EquipmentSave.entityToDto(mntmgt, cttpcl, files);
    }

    /**
     * 유지관리 신청내역 수정
     */
    @Override
    @Transactional
    public RestResponse equipmentDataUpdate(EquipmentSave equipmentSave, HttpServletRequest request) throws IOException, ParseException {
        SessionUser sessionUser = getLoginUser();
        Mntmgt mntmgt = getMntmgt(equipmentSave.getRceptNo());
        Cmpny cmpny = cmpnyRepository.findById(equipmentSave.getBizrno())
                .orElseThrow(EntityNotFoundException::new);

        if (!sessionUser.getUserId().equals(mntmgt.getRegisterId()) && !sessionUser.getAuth().equals("adm")) {
            throw new RuntimeException("잘못된 접근입니다.");
        }

        // 웹필터를 위한 조건 검사
        String result = fakeDataWebFilter(equipmentSave, request);

        // 웰필터 조건에 걸렸을 경우
        if (!result.isEmpty()) {
            return RestResponse.builder()
                    .message(result)
                    .build();
        }

        mntmgt.updateFile(equipmentSave.getFiles(), equipmentSave.getSavedFileIds(), true);
        mntmgt.updateInsttMntmgt(equipmentSave, cmpny);

        return RestResponse.builder()
                .message("수정")
                .data(mntmgt.getRceptNo())
                .build();
    }


    /**
     * 유지관리 신청내역 취소
     */
    @Override
    @Transactional
    public String cancelEquipmentData(String rceptNo) {
        Mntmgt mntmgt = getMntmgt(rceptNo);

        if(mntmgt.getStatusType().equals("STTUS00004") || mntmgt.getStatusType().equals("STTUS00005")) {
            return "잘못된 요청입니다.";
        }

        mntmgt.changeCancelStatus();
        return "신청내역이 취소되었습니다.";
    }


    /**
     * 유지관리 신청내역 최종완료
     */
    @Override
    public String equipmentDataLastComplete(String rceptNo) {
        Mntmgt mntmgt = getMntmgt(rceptNo);
        User user = userRepository.findById(getLoginUser().getUserId())
                .orElseThrow(EntityNotFoundException::new);

        if(!mntmgt.getStatusType().equals("STTUS00005")) {
            return "잘못된 요청입니다.";
        }

        mntmgt.changeCompleteStatus(user);
        return "완료처리 되었습니다.";
    }

    /**
     * 기관 회사 매칭
     */
    @Override
    public List<CmpnyDto> getCompanyInstitutionsMatching(String rceptNo) {
        String orgCd = getLoginUser().getUpperOrgCd();

        if (rceptNo != null) {
            Mntmgt mntmgt = getMntmgt(rceptNo);
            orgCd = mntmgt.getOrgCd().getOrgCd();
        }

        return mntmgtDao.getCompanyInstitutionsMatching(orgCd);
    }

    /**
     * 접수번호 생성
     */
    private String createRceptNo() {
        String now = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
        final String NUMBERING_CODE = "AS-".concat(now).concat("-");
        Optional<Mntmgt> mntmgtOpt = mntmgtRepository.findTop1ByRceptNoContainingOrderByRceptNoDesc(NUMBERING_CODE);

        return !mntmgtOpt.isPresent() ? NUMBERING_CODE.concat("001") :
                NUMBERING_CODE.concat(String.format("%03d", (Integer.parseInt(mntmgtOpt.get().getRceptNo().split("-")[2]) + 1)));
    }

    private Mntmgt getMntmgt(String rceptNo) {
        return mntmgtRepository.findById(rceptNo)
                .orElseThrow(EntityNotFoundException::new);
    }

    private List<EquipmentFileDto> getMntmgtFileDtos(String rceptNo) {
        return mntmgtFileRepository.findByMntmgt(getMntmgt(rceptNo)).stream()
                .map(EquipmentFileDto::new)
                .collect(Collectors.toList());
    }

    private List<EquipmentListDto> getReversedSort(List<EquipmentListDto> mntmgts) {
        return mntmgts.stream()
                .sorted(Comparator.comparing(EquipmentListDto::getRceptNo).reversed())
                .collect(Collectors.toList());
    }

    /**
     * 웹필터
     */
    private String webFilterCheck(Mntmgt mntmgt, HttpServletRequest request) throws IOException, ParseException {
        String listUrl = request.getRequestURI().substring(0, request.getRequestURI().lastIndexOf('/'));
        String filePath = "";
        String fileName = "";

        for (MntmgtFile file : mntmgt.getMntmgtFile()) {
            filePath += file.getFileStreCours() + "|";
            fileName += file.getOrignlFileNm() + "|";
        }

        return WebFilterUtils.getWfsend(request.getRequestURI(), listUrl, getLoginUser().getUserId(),
                mntmgt.getSubject(), mntmgt.getContent(), filePath, fileName, request.getRemoteAddr());
    }

    private String fakeDataWebFilter(EquipmentSave equipmentSave, HttpServletRequest request) throws IOException, ParseException {
        // 웹 필터 전용 데이터 생성
        Mntmgt fakeData = Mntmgt.of("1", equipmentSave, null, null);
        fakeData.addFile(equipmentSave.getFiles(), true);
        equipmentSave.getFiles().clear();
        // 웹필터 체크
        String result = webFilterCheck(fakeData, request);

        for (MntmgtFile file : fakeData.getMntmgtFile()) {
            MultipartFile multipartFile = copyMultipartFile(file);
            equipmentSave.getFiles().add(multipartFile);
            // 웹필터 전용 데이터 업로드 파일 삭제
            FileUtils.deleteFile(file.getStreFileNm());
        }

        fakeData.getMntmgtFile().clear();
        return result;
    }

    private MultipartFile copyMultipartFile(MntmgtFile mntmgtFile) throws IOException {
        File file = new File(mntmgtFile.getFileStreCours()); // 파일 경로
        DiskFileItem fileItem = new DiskFileItem("file", Files.probeContentType(file.toPath()), false, mntmgtFile.getOrignlFileNm(), (int) file.length(), file.getParentFile());
        try (InputStream input = new FileInputStream(file);
             OutputStream output = fileItem.getOutputStream()) {
            IOUtils.copy(input, output);
            output.flush();
        }
        return new CommonsMultipartFile(fileItem);
    }
}
