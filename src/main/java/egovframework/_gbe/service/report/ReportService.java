package egovframework._gbe.service.report;

import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.domain.user.SessionUser;
import egovframework._gbe.domain.user.organization.Organizaion;
import egovframework._gbe.repository.report.InstitutionsReportDao;
import egovframework._gbe.repository.user.organization.OrganizationRepository;
import egovframework._gbe.web.dto.report.ReportChrgDto;
import egovframework._gbe.web.dto.report.ReportDetailDto;
import egovframework._gbe.web.dto.report.ReportDetailResultDto;
import egovframework._gbe.web.dto.report.ReportDto;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

import static egovframework._gbe.common.util.SecurityUtils.getLoginUser;
import static egovframework._gbe.web.dto.report.ReportDetailResultDto.changeData;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class ReportService {

    private final InstitutionsReportDao institutionsReportDao;
    private final OrganizationRepository organizationRepository;

    /**
     * 점검보고서 목록
     */
    public Page<ReportDto> getReportList(Pageable pageable, SearchParam searchParam) {
        List<ReportDto> reports = new ArrayList<>();
        searchParam.settingPage(pageable);
        SessionUser loginUser = getLoginUser();
        searchParam.setOrgCd(loginUser.getUpperOrgCd());

        if (searchParam.getViewCd() != null && !searchParam.getViewCd().equals(loginUser.getUpperOrgCd())) {
            searchParam.setOrgCd(searchParam.getViewCd());
        }

        if (searchParam.getViewCd() != null && (loginUser.getUseSe().equals("USESE00008") || loginUser.getUseSe().equals("USESE00007"))) {
            reports = institutionsReportDao.getAdminReportList(searchParam);
        } else {
            reports = institutionsReportDao.getReportList(searchParam);
        }

        Integer totalCnt = 0;

        if (reports.size() > 0) {
            totalCnt = reports.get(0).getTotalCnt();
        }

        return new PageImpl<>(reports, pageable, totalCnt);
    }

    /**
     * 점검보고서 상세
     */
    public ReportDetailResultDto getReportDetail(Long reprtNo) {
        ReportDto reportDto = institutionsReportDao.getReportDetail(reprtNo);
        List<ReportDetailDto> reportDetailDtos = institutionsReportDao.getReportDetails(reprtNo);
        List<ReportChrgDto> inChargeInfos = institutionsReportDao.getInChargeInfo(reprtNo);

        ReportDetailResultDto resultDto = changeData(reportDto);
        resultDto.setDetailDtos(reportDetailDtos);
        resultDto.setInChargeInfos(inChargeInfos);

        return resultDto;
    }

    public String getInstitutionsName() {
         Organizaion organizaion = organizationRepository.findOrgByOrgCd(getLoginUser().getUpperOrgCd())
                .orElseThrow(EntityNotFoundException::new);
        return organizaion.getOrgNm();
    }

}
