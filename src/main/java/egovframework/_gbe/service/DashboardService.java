package egovframework._gbe.service;

import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.domain.user.SessionUser;
import egovframework._gbe.repository.equipment.cmpds.CmpdsDao;
import egovframework._gbe.repository.equipment.mntmgt.MntmgtDao;
import egovframework._gbe.web.dto.dashboard.DashBoardDto;
import egovframework._gbe.web.dto.equipment.EquipmentListDto;
import egovframework._gbe.web.dto.equipment.EquipmentUnprocessedDto;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static egovframework._gbe.common.util.SecurityUtils.getLoginUser;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class DashboardService {

    private final MntmgtDao mntmgtDao;
    private final CmpdsDao cmpdsDao;

    public DashBoardDto getDashBoard() {
        DashBoardDto dashBoardDto = new DashBoardDto();
        SessionUser sessionUser = getLoginUser();
        SearchParam searchParam = settingsSearchPram(sessionUser.getUpperOrgCd());

        // 유지 관리 최근 5개
        List<EquipmentListDto> mntmgtsDtos = mntmgtDao.getMntmgtList(searchParam);
        // 유지관리 미처리 현황
        List<EquipmentUnprocessedDto> unprocessedDtos = mntmgtDao.getUnprocessedStatus(sessionUser.getUpperOrgCd());
        // 유지관리 총 미처리 건수
        int totalCount = unprocessedDtos.stream().mapToInt(EquipmentUnprocessedDto::getUnprocessedCount).sum();

        // 소모품 최근 5개
       // List<EquipmentListDto> cmpdsDtos = cmpdsDao.getCmpdsList(searchParam);
        // 소모품 총 미처리 건수
       // int cmpdsUnprocessedCount = cmpdsDao.getUnprocessedCount(searchParam);

        dashBoardDto.setUnprocessedDtos(unprocessedDtos);
        dashBoardDto.setTotalCount(totalCount);
        dashBoardDto.setInsttMntmgtDtos(mntmgtsDtos);
       // dashBoardDto.setInsttCmpdsDtos(cmpdsDtos);
       // dashBoardDto.setCmpdsUnprocessedCount(cmpdsUnprocessedCount);

        return dashBoardDto;
    }

    private SearchParam settingsSearchPram(String orgCd) {
        SearchParam searchParam = new SearchParam();
        searchParam.settingPage(PageRequest.of(0, 10));
        searchParam.setOrgCd(orgCd);
        return searchParam;
    }
}
