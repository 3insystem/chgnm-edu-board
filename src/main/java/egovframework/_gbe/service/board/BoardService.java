package egovframework._gbe.service.board;

import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.common.util.SecurityUtils;
import egovframework._gbe.common.util.file.BoardFileUtils;
import egovframework._gbe.common.util.file.UploadFile;
import egovframework._gbe.domain.user.SessionUser;
import egovframework._gbe.repository.board.BoardMngDao;
import egovframework._gbe.web.dto.board.*;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections.CollectionUtils;
import org.json.simple.parser.ParseException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static egovframework._gbe.common.util.CommonUtils.getClientIp;
import static egovframework._gbe.common.util.WebFilterUtils.getWfsend;


@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class BoardService {

    private final BoardMngDao boardMngDao;

    public List<BoardNavDto> findNavList() {
        return boardMngDao.findNavList();
    }

    public Page<BoardMngDto> findAll(SearchParam searchParam, Pageable pageable) {
        searchParam.settingPage(pageable);

        List<BoardMngDto> result = boardMngDao.findAll(searchParam);

        Integer totalCnt = 0;
        if (result.size() > 0) {
            totalCnt = result.get(0).getTotalCnt();
        }

        return new PageImpl<>(result, pageable, totalCnt);
    }

    public BoardMngDto findById(Long boardMngId) {
        BoardMngDto result = boardMngDao.findById(boardMngId);
        if (result == null) {
            throw new EntityNotFoundException();
        }

        return result;
    }

    public List<Map<String, String>> findBoardMngByBoardType(String boardType) {
        List<Map<String, String>> result = boardMngDao.findBoardMngByBoardType(boardType);
        return result;
    }

    public List<CtgryDto> findCtgryByBoardMngId(Long boardMngId) {
        return boardMngDao.findCtgryByBoardMngId(boardMngId);
    }

    //관리자 리스트 목록
    public List<BoardListDto> findAPostAll(SearchParam searchParam, Long boardMngId, String categoryId) {
        SessionUser user = SecurityUtils.getLoginUser();
        searchParam.setId1(boardMngId);
        searchParam.setId2(categoryId);
        searchParam.setUsid(user.getUserId());
        searchParam.setBizrno(user.getBizrno());
        //    searchParam.setOrgCd(user.getOrgCd());

        //   System.out.println(user.getUpperOrgCd());
//추가 한거

        searchParam.setViewCd(user.getUpperOrgCd());
        searchParam.setOrgCd(user.getUpperOrgCd());


        List<BoardListDto> result;

        switch (user.getUseSe()) {
            case "USESE00001": case "USESE00005": case "USESE00006":
            case "USESE00007": case "USESE00008":
                result = boardMngDao.allMngNotice(searchParam);
                break;
            case "USESE00002":
                result = boardMngDao.selRepNotice(searchParam);
                break;
            case "USESE00003":
                result = boardMngDao.selEmpNotice(searchParam);
                break;
            default:
                result = new ArrayList<>();
                break;
        }

        return result;
    }


    public List<BoardListDto> findPostAll(SearchParam searchParam, Long boardMngId, String categoryId) {
        SessionUser user = SecurityUtils.getLoginUser();
        searchParam.setId1(boardMngId);
        searchParam.setId2(categoryId);
        searchParam.setUsid(user.getUserId());
        searchParam.setBizrno(user.getBizrno());
    //    searchParam.setOrgCd(user.getOrgCd());

     //   System.out.println(user.getUpperOrgCd());
//추가 한거

        searchParam.setViewCd(user.getUpperOrgCd());
        searchParam.setOrgCd(user.getUpperOrgCd());


        List<BoardListDto> result;

        switch (user.getUseSe()) {
            case "USESE00001": case "USESE00005": case "USESE00006":
            case "USESE00007": case "USESE00008":
                result = boardMngDao.selMngNotice(searchParam);
                break;
            case "USESE00002":
                result = boardMngDao.selRepNotice(searchParam);
                break;
            case "USESE00003":
                result = boardMngDao.selEmpNotice(searchParam);
                break;
            default:
                result = new ArrayList<>();
                break;
        }

        return result;
    }

    public Page<BoardListDto> findBoardAll(SearchParam searchParam, Long boardMngId, String categoryId, Pageable pageable) {
        searchParam.settingPage(pageable);
        searchParam.setId1(boardMngId);
        searchParam.setId2(categoryId);
        List<BoardListDto> result = boardMngDao.findBoardAll(searchParam);

        Integer totalCnt = 0;
        if (result.size() > 0) {
            totalCnt = result.get(0).getTotalCnt();
        }

        return new PageImpl<>(result, pageable, totalCnt);
    }

    public Page<BoardListDto> findBoardAllByUserType(SearchParam searchParam, Long boardMngId, String categoryId, Pageable pageable) {
        SessionUser user = SecurityUtils.getLoginUser();
        searchParam.settingPage(pageable);
        searchParam.setId1(boardMngId);
        searchParam.setId2(categoryId);
        searchParam.setUsid(user.getUserId());
        searchParam.setBizrno(user.getBizrno());
        //searchParam.setOrgCd(user.getOrgCd());

        searchParam.setOrgCd(user.getUpperOrgCd());

        List<BoardListDto> result;

        switch (user.getUseSe()) {
            case "USESE00001": case "USESE00005": case "USESE00006":
            case "USESE00007": case "USESE00008":
                result = boardMngDao.findInsttBoardAll(searchParam);
                break;
            case "USESE00002":
                result = boardMngDao.findRepBoardAll(searchParam);
                break;
            case "USESE00003":
                result = boardMngDao.findEmpBoardAll(searchParam);
                break;
            default:
                result = new ArrayList<>();
                break;
        }

        Integer totalCnt = 0;
        if (result.size() > 0) {
            totalCnt = result.get(0).getTotalCnt();
        }

        return new PageImpl<>(result, pageable, totalCnt);
    }

    /*QNA 관련 */
    public Page<BoardListDto> findQBoardAllByUserType(SearchParam searchParam, Long boardMngId, String categoryId, Pageable pageable) {
        SessionUser user = SecurityUtils.getLoginUser();
        searchParam.settingPage(pageable);
        searchParam.setId1(boardMngId);
        searchParam.setId2(categoryId);
        searchParam.setUsid(user.getUserId());
        searchParam.setBizrno(user.getBizrno());
        //searchParam.setOrgCd(user.getOrgCd());

        searchParam.setOrgCd(user.getUpperOrgCd());

        List<BoardListDto> result;

        switch (user.getUseSe()) {
            case "USESE00001": case "USESE00005": case "USESE00006":
            case "USESE00007": case "USESE00008":
                result = boardMngDao.findInsttQBoardAll(searchParam);
                break;
            case "USESE00002":
                result = boardMngDao.findRepQBoardAll(searchParam);
                break;
            case "USESE00003":
                result = boardMngDao.findEmpQBoardAll(searchParam);
                break;
            default:
                result = new ArrayList<>();
                break;
        }

        Integer totalCnt = 0;
        if (result.size() > 0) {
            totalCnt = result.get(0).getTotalCnt();
        }

        return new PageImpl<>(result, pageable, totalCnt);
    }

    public BoardSave findBoard(Long boardId) {
        BoardSave result = boardMngDao.findBoard(boardId);
        if (result == null) {
            throw new EntityNotFoundException();
        }
        if (result.getFileId() != null) {
            List<BoardFileDto> files = boardMngDao.selectFiles(Long.valueOf(result.getFileId()));
            result.setSavedFiles(files);
        }

        return result;
    }

    public Map<String, Object> getSecretAt(Map<String, Object> map) {
        return boardMngDao.getSecretAt(map);
    }

    /* FAQ 관리자 조회 */
    public Page<FaqListDto> findFaqAll(SearchParam searchParam, Long boardMngId, String categoryId, Pageable pageable) {
        searchParam.settingPage(pageable);
        searchParam.setId1(boardMngId);
        searchParam.setId2(categoryId);
        List<FaqListDto> result = boardMngDao.findFaqList(searchParam);

        Integer totalCnt = 0;
        if (result.size() > 0) {
            totalCnt = result.get(0).getTotalCnt();
        }

        return new PageImpl<>(result, pageable, totalCnt);
    }

    /* FAQ 사용자 조회 */
    public Page<FaqListDto> findUserFaqAll(SearchParam searchParam, Long boardMngId, String categoryId, Pageable pageable) {
        String orgCdChk = SecurityUtils.getLoginUser().getOrgCd();

        searchParam.settingPage(pageable);
        searchParam.setId1(boardMngId);
        searchParam.setId2(categoryId);
        if(orgCdChk != null) {
            searchParam.setId3("기관");
        } else {
            searchParam.setId3("업체");
        }

        List<FaqListDto> result = boardMngDao.findFaqList(searchParam);

        Integer totalCnt = 0;
        if (result.size() > 0) {
            totalCnt = result.get(0).getTotalCnt();
        }

        return new PageImpl<>(result, pageable, totalCnt);
    }

    public Page<ThumbnailListDto> findThumbnailAll(SearchParam searchParam, Long boardMngId, String categoryId, Pageable pageable) {
        searchParam.settingPage(pageable);
        searchParam.setId1(boardMngId);
        searchParam.setId2(categoryId);

        List<ThumbnailListDto> result = boardMngDao.findThumbnailList(searchParam);

        Integer totalCnt = 0;
        if (result.size() > 0) {
            totalCnt = result.get(0).getTotalCnt();
        }

        return new PageImpl<>(result, pageable, totalCnt);
    }

    /* 썸네일 상세 */
    public ThumbnailSave findThumbnail(Long boardId) {
        ThumbnailSave result = boardMngDao.findThumbnail(boardId);
        if (result == null) {
            throw new EntityNotFoundException();
        }

        //썸네일파일
        if (result.getThumbnailId() != null) {
            List<BoardFileDto> thumFiles = boardMngDao.selectFiles(Long.valueOf(result.getThumbnailId()));
            result.setSavedThumFiles(thumFiles);
        }

        //일반파일
        if (result.getFileId() != null) {
            List<BoardFileDto> files = boardMngDao.selectFiles(Long.valueOf(result.getFileId()));
            result.setSavedFiles(files);
        }

        return result;
    }

    public Page<VideoListDto> findVideoAll(SearchParam searchParam, Long boardMngId, String categoryId, Pageable pageable) {
        searchParam.settingPage(pageable);
        searchParam.setId1(boardMngId);
        searchParam.setId2(categoryId);
        List<VideoListDto> result = boardMngDao.findVideoAll(searchParam);

        Integer totalCnt = 0;
        if (result.size() > 0) {
            totalCnt = result.get(0).getTotalCnt();
        }

        return new PageImpl<>(result, pageable, totalCnt);
    }

    public VideoSave findVideo(Long boardId) {
        VideoSave result = boardMngDao.findVideo(boardId);
        if (result == null) {
            throw new EntityNotFoundException();
        }

        return result;
    }

    public List<ScheduleDto> findScheduleAll(Map<String, String> map) {
        List<ScheduleDto> holidayList = boardMngDao.findHolidayAll();
        List<ScheduleDto> scheduleList = boardMngDao.findScheduleAll(map);
        scheduleList.addAll(holidayList);
        return scheduleList;
    }

    public ScheduleDto findSchedule(String type, Long scheduleId) {
        if (!type.equals("ETC")) {
            return boardMngDao.findSchedule(scheduleId);
        } else {
            return boardMngDao.findHoliday(scheduleId);
        }
    }

    public ScheduleDto findDelSchedule(String type, Map searchMap) {
        if (!type.equals("ETC")) {
            return boardMngDao.findDelSchedule(searchMap);
        } else {
            return boardMngDao.findHoliday((Long)searchMap.get("scheduleId"));
        }
    }

    @Transactional
    public int save(BoardMngSave saveData) throws Exception {
        String usid = SecurityUtils.getUserId();
        saveData.setUsid(usid);
        int result = boardMngDao.saveBoardMng(saveData);

        if (saveData.getCtgryUseAt().equals("Y")) {
            for (Map<String, String> map : saveData.getCtgryList()) {
                map.put("boardMngId", String.valueOf(saveData.getBoardMngId()));
                map.put("usid", usid);
            }

            int ctgry = boardMngDao.saveBoardCtgry(saveData.getCtgryList());
            if (ctgry == 0) {
                throw new Exception("카테고리 저장 중 에러가 발생했습니다.");
            }
            result += ctgry;
        }
        return result;
    }

    @Transactional
    public int update(Long boardMngId, BoardMngSave saveData) throws Exception {
        String usid = SecurityUtils.getUserId();
        saveData.setUsid(usid);
        saveData.setBoardMngId(boardMngId);

        int result = boardMngDao.updateBoardMng(saveData);

        if (saveData.getCtgryUseAt().equals("Y")) {
            List<CtgryDto> ctgryList = this.findCtgryByBoardMngId(boardMngId);
            List<Map<String, String>> list = CtgryDto.getUpdateList(ctgryList, saveData.getCtgryList(), boardMngId, usid);
            int ctgry = boardMngDao.updateBoardCtgry(list);

            result += ctgry;
        }

        return result;
    }

    @Transactional
    public int change(List<Long> idList) {
        int result = 0;
        for (Long id : idList) {
            int r = boardMngDao.change(id);
            result += r;
        }
        return result;
    }

    /**
     * 게시물 저장
     */
    @Transactional
    public Long save(BoardSave saveData, HttpServletRequest request) throws Exception {
        String usid = SecurityUtils.getUserId();
        saveData.setUsid(usid);

        //qna 일 경우 추가
        saveData.setBoardType(saveData.getBoardType());

        if (saveData.getBoardType().equals("qna")) {
            saveData.setStatus("A");
        }

        String orgCd = SecurityUtils.getLoginUser().getUpperOrgCd();
        if (saveData.getViewCd() != null) {
            orgCd = saveData.getViewCd();
        }
        saveData.setOrgCd(orgCd);
        Long boardCateId = saveData.getBoardCategoryId(); //게시판 폴더 생성시 필요로 인해 추가
        String fileId = boardMngDao.selectFileId();
        saveData.setFileId(fileId);

        //웹필터 시작
        String actionUrl = request.getRequestURL().toString();
        String url = request.getRequestURL().toString();
        String listUrl = request.getRequestURI().substring(0, request.getRequestURI().lastIndexOf('/'));

        String writer = usid;
        String subject = saveData.getTitle();
        String contents =  saveData.getContent() == null ? "" : saveData.getContent();
        String userIp = getClientIp(request);

        List<Map<String, Object>> files = setFiles(saveData.getFiles(), saveData.getBoardId(), fileId, boardCateId);

        // 웹필터를 위한 조건 검사
        String filtResult = webFilterBCheck(saveData, files, fileId, boardCateId, actionUrl, listUrl, writer, subject, contents, userIp);

        String upload = String.valueOf(boardCateId);

        // 웰필터 조건에 걸렸을 경우
        if (!filtResult.isEmpty()) {
            if (!files.isEmpty()) {
                for(Map<String , Object> file : files){
                    String streFileNm = (String) file.get("streFileNm") ;

                    // 웹필터 전용 데이터 업로드 파일 삭제
                    BoardFileUtils.deleteFile(streFileNm, upload);
                }
            }

            return 0L;
        } else {
            boardMngDao.saveBoard(saveData);
            saveFiles(files);

            return saveData.getBoardId();
        }
    }

    @Transactional
    public int update(BoardSave updateData, HttpServletRequest request) throws Exception {
        int result = 0;
        BoardSave board = findBoard(updateData.getBoardId());
        if (board != null) {
            String usid = SecurityUtils.getUserId();
            updateData.setUsid(usid);
            updateData.setOrgCd(updateData.getViewCd());

            //웹필터 시작
            String actionUrl = request.getRequestURL().toString();
            String url = request.getRequestURL().toString();
            String listUrl = request.getRequestURI().substring(0, request.getRequestURI().lastIndexOf('/'));

            String writer = usid;
            String subject = updateData.getTitle();
            String contents =  updateData.getContent() == null ? "" : updateData.getContent();
            String userIp = getClientIp(request);

            // 웹필터를 위한 조건 검사
            Long filtResult = webFilterUPCheck(updateData, updateData.getSavedFileSns(), updateData.getFiles(), updateData.getBoardId(), board.getFileId(), updateData.getBoardCategoryId(), actionUrl, listUrl, writer, subject, contents, userIp);

            // 웹필터 조건에 걸렸을 경우
            if (filtResult.longValue() == 0) {
                result = result;
            } else {
                updateData.setFileId(String.valueOf(filtResult));
                result += boardMngDao.updateBoard(updateData);
            }
        } else {
            throw new Exception("게시물이 존재하지 않습니다.");
        }
        return result;
    }

    @Transactional
    public int deleteBoard(Map<String, Object> map) {
        map.put("usid", SecurityUtils.getUserId());
        return boardMngDao.deleteBoard(map);
    }

    @Transactional
    public int deleteBoardAll(Map<String, Object> map) {
        int result = 0;
        List<String> boardIdList = (List<String>) map.get("boardIdList");
        for (String boardId : boardIdList) {
            Map<String, Object> delMap = new HashMap<>();
            delMap.put("boardId", boardId);
            delMap.put("delReason", map.get("delReason"));
            delMap.put("usid", SecurityUtils.getUserId());
            result += boardMngDao.deleteBoard(delMap);
        }
        return result;
    }

    @Transactional
    public int restoreBoard(Map<String, Object> map) {
        map.put("usid", SecurityUtils.getUserId());
        return boardMngDao.restoreBoard(map);
    }

    @Transactional
    public int restoreBoardAll(Map<String, Object> map) {
        int result = 0;
        List<String> boardIdList = (List<String>) map.get("boardIdList");
        for (String boardId : boardIdList) {
            Map<String, Object> rstMap = new HashMap<>();
            rstMap.put("boardId", boardId);
            rstMap.put("restoreReason", map.get("restoreReason"));
            rstMap.put("usid", SecurityUtils.getUserId());
            result += boardMngDao.restoreBoard(rstMap);
        }
        return result;
    }

    @Transactional
    public int comment(Map<String, Object> map) {
        map.put("usid", SecurityUtils.getUserId());
        return boardMngDao.comment(map);
    }

    /**
     * 일정 저장
     */
    @Transactional
    public int save(ScheduleDto saveData) {
        String usid = SecurityUtils.getUserId();
        saveData.setUsid(usid);

        return boardMngDao.saveSchedule(saveData);
    }

    @Transactional
    public int update(ScheduleDto updateData) {
        String usid = SecurityUtils.getUserId();
        updateData.setUsid(usid);

        return boardMngDao.updateSchedule(updateData);
    }

    @Transactional
    public int deleteShceule(Map<String, Object> map) {
        map.put("usid", SecurityUtils.getUserId());
        return boardMngDao.deleteSchedule(map);
    }

    @Transactional
    public int restoreSchedule(Map<String, Object> map) {
        map.put("usid", SecurityUtils.getUserId());
        return boardMngDao.restoreSchedule(map);
    }

    /**
     * 비디오 저장
     * */
    @Transactional
    public Long save(VideoSave saveData) {
        String usid = SecurityUtils.getUserId();
        saveData.setUsid(usid);
        boardMngDao.saveVideo(saveData);

        return saveData.getBoardVideoId();
    }

    @Transactional
    public int update(VideoSave updateData) {
        String usid = SecurityUtils.getUserId();
        updateData.setUsid(usid);

        return boardMngDao.updateVideo(updateData);
    }

    @Transactional
    public int deleteVideo(Map<String, Object> map) {
        map.put("usid", SecurityUtils.getUserId());
        return boardMngDao.deleteVideo(map);
    }

    @Transactional
    public int deleteVideoAll(Map<String, Object> map) {
        int result = 0;
        List<String> videoIdList = (List<String>) map.get("videoIdList");
        for (String videoId : videoIdList) {
            Map<String, Object> delMap = new HashMap<>();
            delMap.put("videoId", videoId);
            delMap.put("delReason", map.get("delReason"));
            delMap.put("usid", SecurityUtils.getUserId());
            result += boardMngDao.deleteVideo(delMap);
        }
        return result;
    }

    @Transactional
    public int restoreVideo(Map<String, Object> map) {
        map.put("usid", SecurityUtils.getUserId());
        return boardMngDao.restoreVideo(map);
    }

    @Transactional
    public int restoreVideoAll(Map<String, Object> map) {
        int result = 0;
        List<String> videoIdList = (List<String>) map.get("videoIdList");
        for (String videoId : videoIdList) {
            Map<String, Object> rstMap = new HashMap<>();
            rstMap.put("videoId", videoId);
            rstMap.put("restoreReason", map.get("restoreReason"));
            rstMap.put("usid", SecurityUtils.getUserId());
            result += boardMngDao.restoreVideo(rstMap);
        }
        return result;
    }

    /**
     * 썸네일 저장
     * */
    @Transactional
    public Long save(ThumbnailSave saveData, List<MultipartFile> thumFile, HttpServletRequest request) throws IOException, ParseException {
        String usid = SecurityUtils.getUserId();
        saveData.setUsid(usid);

        Long boardCateId = saveData.getBoardCategoryId(); //게시판 폴더 생성시 필요로 인해 추가

        String thumbnailId = boardMngDao.selectFileId();
        saveData.setThumbnailId(thumbnailId);
        int nextFId = Integer.parseInt(thumbnailId);

        int ecFileId = nextFId + 1;
        String fileId = String.valueOf(ecFileId);

        //e-book이 아닐 경우
        if (StringUtils.isEmpty(saveData.getEbookLink())) {
            saveData.setFileId(fileId);
        }
        //String fileId = boardMngDao.selectFileId();

        Long boardId = boardMngDao.saveThumbnail(saveData);

        //웹필터 시작
        String actionUrl = request.getRequestURL().toString();
        String url = request.getRequestURL().toString();
        String listUrl = request.getRequestURI().substring(0, request.getRequestURI().lastIndexOf('/'));

        String writer = usid;
        String subject = saveData.getTitle();
        String contents =  saveData.getContent() == null ? "" : saveData.getContent();
        String userIp = getClientIp(request);

        // 파일 업로드
        //if (thumFile != null && !thumFile.isEmpty()) { // 파일이 첨부 되었을 경우
        //    List<Map<String, Object>> files1 = setFiles(thumFile, saveData.getBoardThumbnailId(), thumbnailId, boardCateId);
        //    saveThumbnailFiles(files1);
        //}

        List<Map<String, Object>> files1 = setFiles(thumFile, saveData.getBoardThumbnailId(), thumbnailId, boardCateId);

        // 웹필터를 위한 조건 검사
        String filtResult = webFilterCheck(saveData, files1, fileId, boardCateId, actionUrl, listUrl, writer, subject, contents, userIp);

        String filtResult2 = "";
        List<Map<String, Object>> files2 = null;

        //e-book이 아닐 경우
        if (StringUtils.isEmpty(saveData.getEbookLink())) {

            files2 =  setFiles(saveData.getFiles(), saveData.getBoardThumbnailId(), fileId, boardCateId);

            // 웹필터를 위한 조건 검사
             filtResult2 = webFilterCheck(saveData, files2, fileId, boardCateId, actionUrl, listUrl, writer, subject, contents, userIp);

        } else {
             filtResult2 = "";
        }

        // 웰필터 조건에 걸렸을 경우
        if (!filtResult.isEmpty() && !filtResult2.isEmpty()) {
            return 0L;
        } else {
            saveThumbnailFiles(files1);
            if (StringUtils.isEmpty(saveData.getEbookLink())) {
              saveFiles(files2);
            }

            return saveData.getBoardThumbnailId();
        }

    }

    /**
     * 썸네일 수정
     * */
    @Transactional
    public int update(ThumbnailSave updateData, List<MultipartFile> thumFile, HttpServletRequest request) throws Exception {
        int result = 0;
        ThumbnailSave board = findThumbnail(updateData.getBoardThumbnailId());

        if (board != null) {
            String usid = SecurityUtils.getUserId();
            updateData.setUsid(usid);

            //웹필터 시작
            String actionUrl = request.getRequestURL().toString();
            String url = request.getRequestURL().toString();
            String listUrl = request.getRequestURI().substring(0, request.getRequestURI().lastIndexOf('/'));

            String writer = usid;
            String subject = updateData.getTitle();
            String contents =  updateData.getContent() == null ? "" : updateData.getContent();
            String userIp = getClientIp(request);

            //썸네일목록
            /*
            if(CollectionUtils.isNotEmpty(thumFile)) {
                Long thumbnailId = updateThumFiles(updateData.getBoardThumbnailId(), updateData.getBoardCategoryId(),
                        board.getThumbnailId(), updateData.getSavedThumFileSns(), thumFile);
                updateData.setThumbnailId(String.valueOf(thumbnailId));
            } else if (updateData.getSavedThumFileSns().size() > 0 || updateData.getThumFiles().size() > 0) {
                Long thumbnailId = updateThumFiles(updateData.getBoardThumbnailId(), updateData.getBoardCategoryId(),
                        board.getThumbnailId(), updateData.getSavedThumFileSns(), updateData.getThumFiles());
                updateData.setThumbnailId(String.valueOf(thumbnailId));
            }
*/
            // 웹필터를 위한 조건 검사

            Long filtResult = 0L;
            if(CollectionUtils.isNotEmpty(thumFile)) {
                filtResult = webFilterThumUPCheck(updateData, updateData.getSavedFileSns(), thumFile, updateData.getBoardThumbnailId(), board.getFileId(),
                        updateData.getBoardCategoryId(), actionUrl, listUrl, writer, subject, contents, userIp);
            } else if (updateData.getSavedThumFileSns().size() > 0 || updateData.getThumFiles().size() > 0) {
                filtResult = webFilterThumUPCheck(updateData, updateData.getSavedFileSns(), updateData.getThumFiles(), updateData.getBoardThumbnailId(), board.getFileId(),
                        updateData.getBoardCategoryId(), actionUrl, listUrl, writer, subject, contents, userIp);
            }

            Long filtResult2 = 0L;
            //e-book이 아닐 경우
            if (StringUtils.isEmpty(updateData.getEbookLink())) {
                if (updateData.getSavedFileSns().size() > 0 || updateData.getFiles().size() > 0) {
                    Long fileId = updateFiles(updateData.getBoardThumbnailId(), updateData.getBoardCategoryId(),
                            board.getFileId(), updateData.getSavedFileSns(), updateData.getFiles());
                    updateData.setFileId(String.valueOf(fileId));
                }

                filtResult2 = webFilterThumUPCheck(updateData, updateData.getSavedFileSns(), updateData.getFiles(), updateData.getBoardThumbnailId(), board.getFileId(), updateData.getBoardCategoryId(), actionUrl, listUrl, writer, subject, contents, userIp);
            } else {
                filtResult2 = 0L;
            }

            // 웹필터 조건에 걸렸을 경우
            if ((filtResult.longValue() == 0) && !(filtResult2.longValue() == 0)) {
                result = result;
            } else {
                updateData.setThumbnailId(String.valueOf(filtResult));
                if (StringUtils.isEmpty(updateData.getEbookLink())) {
                    updateData.setFileId(String.valueOf(filtResult2));
                }

                result += boardMngDao.updateThumbnail(updateData);
            }


        } else {
            throw new Exception("게시물이 존재하지 않습니다.");
        }

        return result;
    }

    /**
     * 썸네일 삭제
     * */
    @Transactional
    public int deleteThumbnail(Map<String, Object> map) {
        map.put("usid", SecurityUtils.getUserId());
        return boardMngDao.deleteThumbnail(map);
    }

    @Transactional
    public int deleteThumbnailAll(Map<String, Object> map) {
        int result = 0;
        List<String> boardIdList = (List<String>) map.get("boardIdList");
        for (String boardId : boardIdList) {
            Map<String, Object> rstMap = new HashMap<>();
            rstMap.put("boardId", boardId);
            rstMap.put("restoreReason", map.get("restoreReason"));
            rstMap.put("usid", SecurityUtils.getUserId());
            result += boardMngDao.deleteThumbnail(rstMap);
        }
        return result;
    }

    /**
     * 썸네일 복구
     * */
    @Transactional
    public int restoreThumbnail(Map<String, Object> map) {
        map.put("usid", SecurityUtils.getUserId());
        return boardMngDao.restoreThumbnail(map);
    }

    @Transactional
    public int restoreThumbnailAll(Map<String, Object> map) {
        int result = 0;
        List<String> boardIdList = (List<String>) map.get("boardIdList");
        for (String boardId : boardIdList) {
            Map<String, Object> delMap = new HashMap<>();
            delMap.put("boardId", boardId);
            delMap.put("delReason", map.get("delReason"));
            delMap.put("usid", SecurityUtils.getUserId());
            result += boardMngDao.restoreThumbnail(delMap);
        }
        return result;
    }


    /**
     * 공통
     * */
    public List<Map<String, Object>> setFiles(List<MultipartFile> fileList, Long boardId, String fileId, Long boardCateId) throws IOException {
        List<Map<String, Object>> files = new ArrayList<>();
        if (fileList.size() > 0) {
            // if (boardId == null) {
            //     throw new IOException("파일 업로드 중 에러가 발생했습니다.");
            // }

            String id = String.valueOf(boardId);
            String cateId = String.valueOf(boardCateId);
            String usid = SecurityUtils.getUserId();

            for (MultipartFile file : fileList) {
                UploadFile uploadFile = BoardFileUtils.uploadFile(file, cateId);
                Map<String, Object> map = new HashMap<>();
                map.put("fileId", fileId);
                map.put("fileStreCours", uploadFile.getFileStreCours());
                map.put("streFileNm", uploadFile.getStreFileNm());
                map.put("originlFileNm", uploadFile.getOriginlFileNm());
                map.put("fileExtsn", uploadFile.getFileExtsn());
                map.put("fileCn", uploadFile.getFileCn());
                map.put("fileSize", uploadFile.getFileSize());
                map.put("usid", usid);

                files.add(map);
            }
        }

        return files;
    }

    public List<BoardFileDto> selectFiles(Long fileId) {
        return boardMngDao.selectFiles(fileId);
    }

    public void saveFiles(List<Map<String, Object>> files) {
        if (files.size() > 0) {
            boardMngDao.saveFiles(files);
        }
    }

    /* 썸네일용 */
    public void saveThumbnailFiles(List<Map<String, Object>> files) {
        if (files.size() > 0) {
            boardMngDao.saveThumbnailFiles(files);
        }
    }

    public Long updateFiles(Long boardId, Long boardCateId, String fileId, List<Long> savedFileSns, List<MultipartFile> files) throws IOException {
        List<Map<String, Object>> result = new ArrayList<>();
        String usid = SecurityUtils.getUserId();
        String id = boardMngDao.selectFileId();
        List<BoardFileDto> boardFileDtos = selectFiles(Long.valueOf(fileId));

        for (Long sn : savedFileSns) {
            Map<String, Object> fileMap = new HashMap<>();
            for (BoardFileDto file : boardFileDtos) {
                if (sn.intValue() == file.getFileSn()) {
                    fileMap.put("fileId", id);
                    fileMap.put("fileStreCours", file.getFileStreCours());
                    fileMap.put("streFileNm", file.getStreFileNm());
                    fileMap.put("originlFileNm", file.getOrignlFileNm());
                    fileMap.put("fileExtsn", file.getFileExtsn());
                    fileMap.put("fileCn", file.getFileCn());
                    fileMap.put("fileSize", file.getFileSize());
                    fileMap.put("usid", usid);

                    result.add(fileMap);
                }
            }
        }

        List<Map<String, Object>> newFiles = setFiles(files, boardId, id, boardCateId);
        result.addAll(newFiles);

        for (BoardFileDto dto : boardFileDtos) {
            BoardFileUtils.deleteFile(dto.getStreFileNm(), String.valueOf(boardId));
        }
        deleteAllFile(boardFileDtos);
        saveFiles(result);

        return Long.valueOf(id);
    }



    /* 썸네일 업데이트용 */
    public Long updateThumFiles(Long boardId, Long boardCateId, String fileId, List<Long> savedFileSns, List<MultipartFile> files) throws IOException {
        List<Map<String, Object>> result = new ArrayList<>();
        String usid = SecurityUtils.getUserId();
        String id = boardMngDao.selectFileId();
        List<BoardFileDto> boardFileDtos = selectFiles(Long.valueOf(fileId));

        for (Long sn : savedFileSns) {
            Map<String, Object> fileMap = new HashMap<>();
            for (BoardFileDto file : boardFileDtos) {
                if (sn.intValue() == file.getFileSn()) {
                    fileMap.put("fileId", id);
                    fileMap.put("fileStreCours", file.getFileStreCours());
                    fileMap.put("streFileNm", file.getStreFileNm());
                    fileMap.put("originlFileNm", file.getOrignlFileNm());
                    fileMap.put("fileExtsn", file.getFileExtsn());
                    fileMap.put("fileCn", file.getFileCn());
                    fileMap.put("fileSize", file.getFileSize());
                    fileMap.put("usid", usid);

                    result.add(fileMap);
                }
            }
        }

        List<Map<String, Object>> newFiles = setFiles(files, boardId, id, boardCateId);
        // result.addAll(newFiles);

        for (BoardFileDto dto : boardFileDtos) {
            BoardFileUtils.deleteFile(dto.getStreFileNm(), String.valueOf(boardCateId));
        }

        for (BoardFileDto dto : boardFileDtos) {
            deleteFile(dto.getBoardFileId());
        }

        saveThumbnailFiles(newFiles);

        return Long.valueOf(id);
    }


    public void deleteFile(Long fileId) {
        boardMngDao.deleteFile(fileId);
    }

    public void deleteAllFile(List<BoardFileDto> list) {
        for (BoardFileDto dto : list) {
            deleteFile(dto.getBoardFileId());
        }
    }
    /**
     * FAQ 저장
     */
    @Transactional
    public Long faqSave(FaqSave saveData, HttpServletRequest request) throws IOException, ParseException {
        String usid = SecurityUtils.getUserId();
        saveData.setUsid(usid);

        String url = request.getRequestURL().toString();
        int target = url.indexOf("/add");
        String urlList = url.substring(0,target);
        //웹필터 시작
        String actionUrl = url;
        String listUrl = urlList;
        String writer = usid;
        String subject = saveData.getTitle();
        String contents = saveData.getContent() == null ? "" : saveData.getContent();
        String userIp = request.getRequestURI();
        String file_path = "";
        String file_name = "";
        String webFiterChk = "";

        webFiterChk = getWfsend(actionUrl, listUrl, writer, subject, contents, file_path, file_name, userIp );

        if (webFiterChk == "") { //내용이 있으면 파일 삭제

            if(saveData.getCmpnyAt() == null) {
                saveData.setCmpnyAt("N");
            }
            if(saveData.getInsttAt() == null) {
                saveData.setInsttAt("N");
            }

            if(saveData.getBoardId() != null) { /* 저장 수정 기능 구분 */
                boardMngDao.updateFaq(saveData);
            } else {
                boardMngDao.saveFaq(saveData);
            }
            return saveData.getBoardId();
        }
        return null;
    }

    /**
     * FAQ 사용 안함 처리
     */
    @Transactional
    public void faqDel(List<FaqSave> saveData) {
        String usid = SecurityUtils.getUserId();
        saveData.forEach(inData -> {
            inData.setUsid(usid);
            boardMngDao.delFaq(inData);
        });
    }

    /**
     * FAQ 사용 함 처리
     */
    @Transactional
    public void faqRestoreAll(List<FaqSave> saveData) {
        String usid = SecurityUtils.getUserId();
        saveData.forEach(inData -> {
            inData.setUsid(usid);
            boardMngDao.rstFaq(inData);
        });
    }

    @Transactional
    public int faqRestore(FaqSave saveData) {
        String usid = SecurityUtils.getUserId();
        saveData.setUsid(usid);
        return boardMngDao.rstFaq(saveData);
    }

    /**
     * FAQ 상세
     */
    public FaqSave faqDetail(Long boardId) {
        FaqSave detail = boardMngDao.faqDetail(boardId);
        return detail;
    }

    /**
     * 공휴일 저장
     */
    @Transactional
    public boolean holidaySave(HolidaySave saveData) {
        String usid = SecurityUtils.getUserId();
        saveData.setUsid(usid);

        if(saveData.getHolidayId() != null) {
            boardMngDao.updateHoliday(saveData);
        } else {
            boardMngDao.saveHoliday(saveData);
        }

        return true;
    }

    /**
     * 공휴일 사용 안함 처리
     */
    @Transactional
    public void holidayDel(List<HolidayListDto> saveData) {
        String usid = SecurityUtils.getUserId();
        saveData.forEach(inData -> {
            inData.setUsid(usid);
            boardMngDao.delHoliday(inData);
        });
    }

    /**
     * 공휴일 목록 조회
     */
    public List<HolidayListDto> holidayList(SearchParam searchParam) {
        List<HolidayListDto> holidayList = boardMngDao.holidayList(searchParam);

        return holidayList;
    }

    /**
     * 공휴일 목록 조회
     */
    public HolidaySave holidayDetail(Long boardId) {
        HolidaySave detail = boardMngDao.holidayDetail(boardId);
        detail.setStartDateChk("true");

        return detail;
    }

    @Transactional
    public int transport(String boardType, Map<String, Object> map) {
        List<Map<String, Object>> list = new ArrayList<>();
        List<String> boardIdList = (List<String>) map.get("boardIdList");
        for (String boardId : boardIdList) {
            Map<String, Object> m = new HashMap<>();
            m.put("boardId", boardId);
            m.put("boardMngId", map.get("boardMngId"));
            m.put("boardCategoryId", map.get("boardCategoryId"));
            m.put("usid", SecurityUtils.getUserId());
            list.add(m);
        }

        int result = 0;
        switch (boardType) {
            case "gen": case "qna": case "ref":
                result += boardMngDao.transportBoard(list);
                break;
            case "faq":
                result += boardMngDao.transportFaq(list);
                break;
            case "thu": case "new":
                result += boardMngDao.transportThumbnail(list);
                break;
            case "vid":
                result += boardMngDao.transportVideo(list);
                break;
            case "cal":
                result += boardMngDao.transportSchedule(list);
                break;
            default:
                result += boardMngDao.transportBoard(list);
                break;
        }

        return result;
    }


    /**
     * 웹필터
     */
    private String webFilterCheck(ThumbnailSave thumbnailSave, List<Map<String, Object>> fileList, String fileId, Long boardCateId, String actionUrl, String listUrl, String writer, String subject, String contents, String userIp) throws IOException, ParseException {
        String filePath = "";
        String fileName = "";

        if (fileList.size() > 0) {
            for(Map<String , Object> file : fileList){
                filePath += (String) file.get("fileStreCours") + "|";
                fileName += (String) file.get("originlFileNm") + "|";
            }
        }

        return getWfsend(actionUrl, listUrl, writer,
                subject, contents, filePath, fileName, userIp);
    }

    //일반첨부파일 체크
    private String webFilterBCheck(BoardSave boardSave, List<Map<String, Object>> fileList, String fileId, Long boardCateId, String actionUrl, String listUrl, String writer, String subject, String contents, String userIp) throws IOException, ParseException {
        String filePath = "";
        String fileName = "";

        if (fileList.size() > 0) {
            for(Map<String , Object> file : fileList){
                filePath += (String) file.get("fileStreCours") + "|";
                fileName += (String) file.get("originlFileNm") + "|";
            }
        }

        return getWfsend(actionUrl, listUrl, writer,
                subject, contents, filePath, fileName, userIp);
    }

    public Long webFilterUPCheck(BoardSave updateData, List<Long> savedFileSns, List<MultipartFile> files, Long boardId, String fileId, Long boardCateId, String actionUrl, String listUrl, String writer, String subject, String contents, String userIp) throws IOException, ParseException {
        List<Map<String, Object>> result = new ArrayList<>();
        String usid = SecurityUtils.getUserId();
        String id = boardMngDao.selectFileId();
        if (!StringUtils.isEmpty(fileId)) {  // 등록된 첨부 파일이 있을 경우에만
            List<BoardFileDto> boardFileDtos = selectFiles(Long.valueOf(fileId));

            for (Long sn : savedFileSns) {
                Map<String, Object> fileMap = new HashMap<>();
                for (BoardFileDto file : boardFileDtos) {
                    if (sn.intValue() == file.getFileSn()) {
                        fileMap.put("fileId", id);
                        fileMap.put("fileStreCours", file.getFileStreCours());
                        fileMap.put("streFileNm", file.getStreFileNm());
                        fileMap.put("originlFileNm", file.getOrignlFileNm());
                        fileMap.put("fileExtsn", file.getFileExtsn());
                        fileMap.put("fileCn", file.getFileCn());
                        fileMap.put("fileSize", file.getFileSize());
                        fileMap.put("usid", usid);

                        result.add(fileMap);
                    }
                }
            }

            for (BoardFileDto dto : boardFileDtos) {
                BoardFileUtils.deleteFile(dto.getStreFileNm(), String.valueOf(boardId));
            }

          //  deleteAllFile(boardFileDtos);
        }

        String file_path = "";
        String file_name = "";

        List<Map<String, Object>> newFiles = setFiles(files, boardId, id, boardCateId);
        result.addAll(newFiles);

        if (!result.isEmpty()) {
            for(Map<String , Object> file : result){
                file_path += (String) file.get("fileStreCours") + "|";
                file_name += (String) file.get("originlFileNm") + "|";
            }
        }

        // 웹필터 체크
        String fiterResult = webFilterBCheck(updateData, newFiles, fileId, boardCateId, actionUrl, listUrl, writer, subject, contents, userIp );

        updateData.getFiles().clear();

        // 웰필터 조건에 걸렸을 경우
        if (!fiterResult.isEmpty()) {
            return 0L;
        } else {

            saveFiles(result);
            return Long.valueOf(id);
        }

    }



    /* 썸네일 웹필터 업데이트용 */
    public Long webFilterThumUPCheck(ThumbnailSave updateData, List<Long> savedFileSns, List<MultipartFile> files, Long boardId, String fileId, Long boardCateId, String actionUrl, String listUrl, String writer, String subject, String contents, String userIp) throws IOException, ParseException {
        List<Map<String, Object>> result = new ArrayList<>();
        String usid = SecurityUtils.getUserId();
        String id = boardMngDao.selectFileId();

        if (!StringUtils.isEmpty(fileId)) {  // 등록된 첨부 파일이 있을 경우에만
            List<BoardFileDto> boardFileDtos = selectFiles(Long.valueOf(fileId));

            for (Long sn : savedFileSns) {
                Map<String, Object> fileMap = new HashMap<>();
                for (BoardFileDto file : boardFileDtos) {
                    if (sn.intValue() == file.getFileSn()) {
                        fileMap.put("fileId", id);
                        fileMap.put("fileStreCours", file.getFileStreCours());
                        fileMap.put("streFileNm", file.getStreFileNm());
                        fileMap.put("originlFileNm", file.getOrignlFileNm());
                        fileMap.put("fileExtsn", file.getFileExtsn());
                        fileMap.put("fileCn", file.getFileCn());
                        fileMap.put("fileSize", file.getFileSize());
                        fileMap.put("usid", usid);

                        result.add(fileMap);
                    }
                }
            }

            for (BoardFileDto dto : boardFileDtos) {
                BoardFileUtils.deleteFile(dto.getStreFileNm(), String.valueOf(boardCateId));
            }

            for (BoardFileDto dto : boardFileDtos) {
                deleteFile(dto.getBoardFileId());
            }
        }

        String file_path = "";
        String file_name = "";

        List<Map<String, Object>> newFiles = setFiles(files, boardId, id, boardCateId);

        if (!result.isEmpty()) {
            for(Map<String , Object> file : result){
                file_path += (String) file.get("fileStreCours") + "|";
                file_name += (String) file.get("originlFileNm") + "|";
            }
        }

        // 웹필터 체크
        String fiterResult = webFilterCheck(updateData, newFiles, fileId, boardCateId, actionUrl, listUrl, writer, subject, contents, userIp );

        updateData.getFiles().clear();

        // 웰필터 조건에 걸렸을 경우
        if (!fiterResult.isEmpty()) {
            return 0L;
        } else {
            saveThumbnailFiles(newFiles);
            return Long.valueOf(id);
        }

    }

}