package egovframework._gbe.service.board;

import egovframework._gbe.common.util.SecurityUtils;
import egovframework._gbe.repository.board.BoardDao;
import egovframework._gbe.web.dto.board.BoardSave;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class BoardFService {

    private final BoardDao boardDao;

    public String slcBoardNm(Long boardMngId) {
        return boardDao.slcBoardNm(boardMngId);
    }

    @Transactional
    public int save(BoardSave saveData) throws Exception {
        String usid = SecurityUtils.getUserId();
        saveData.setUsid(usid);

        int result = BoardDao.saveBoard(saveData);
        /*
        if (saveData.getCtgryUseAt().equals("Y")) {
            for (Map<String, String> map : saveData.getCtgryList()) {
                map.put("boardMngId", String.valueOf(saveData.getBoardMngId()));
                map.put("usid", usid);
            }

            int ctgry = boardMngDao.saveBoardCtgry(saveData.getCtgryList());
            if (ctgry == 0) {
                throw new Exception("카테고리 저장 중 에러가 발생했습니다.");
            }
            result += ctgry;
        }

         */
        return result;
    }
}
