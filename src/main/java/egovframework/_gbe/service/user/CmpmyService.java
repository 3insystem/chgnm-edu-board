package egovframework._gbe.service.user;

import egovframework._gbe.repository.user.cmpny.CmpnyRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional
public class CmpmyService {

    private final CmpnyRepository cmpnyRepository;
}
