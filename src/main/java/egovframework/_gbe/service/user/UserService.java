package egovframework._gbe.service.user;

import egovframework._gbe.common.excpetion.UnableChangeStatusException;
import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.common.util.CryptUtils;
import egovframework._gbe.common.util.SecurityUtils;
import egovframework._gbe.domain.code.CmmnCode;
import egovframework._gbe.domain.mntmgt.Mntmgt;
import egovframework._gbe.domain.user.SessionUser;
import egovframework._gbe.domain.user.User;
import egovframework._gbe.domain.user.auth.Auth;
import egovframework._gbe.domain.user.cmpny.Cmpny;
import egovframework._gbe.domain.user.dsptc.Dsptc;
import egovframework._gbe.domain.user.organization.Organizaion;
import egovframework._gbe.repository.equipment.mntmgt.MntmgtRepository;
import egovframework._gbe.repository.user.UserDao;
import egovframework._gbe.repository.user.UserRepository;
import egovframework._gbe.repository.user.cmpny.CmpnyRepository;
import egovframework._gbe.repository.user.dsptc.DsptcRepository;
import egovframework._gbe.repository.user.organization.OrganizationRepository;
import egovframework._gbe.service.common.CommonService;
import egovframework._gbe.web.dto.user.*;
import egovframework._gbe.web.dto.user.cmpny.BizrnoCheckDto;
import egovframework._gbe.web.dto.user.cmpny.CmpnyDto;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static egovframework._gbe.common.util.CommonUtils.userId;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class UserService {
    private final UserRepository userRepository;
    private final UserDao userDao;
    private final CommonService commonService;
    private final CmpnyRepository cmpnyRepository;
    private final DsptcRepository dsptcRepository;
    private final OrganizationRepository organizationRepository;
    private final MntmgtRepository mntmgtRepository;
    private final PasswordEncoder passwordEncoder;
    private final CryptUtils cryptUtils;

    // 예시
    public Page<UserExDto> findAll(SearchParam searchParam, Pageable pageable) {
        Page<User> result = userRepository.findAll(searchParam, pageable);

        return new PageImpl<>(
                result.stream().map(UserExDto::new).collect(Collectors.toList()),
                pageable,
                result.getTotalElements()
        );
    }

    public String idCheck(String userId) {
        int cnt = userRepository.findByIdCnt(userId, "_CMPNY!");
        return (cnt > 0) ? "Y" : "N";
    }

    public User findById(String userId) {
        User user = userRepository.findById(userId)
                .orElseThrow(EntityNotFoundException::new);
        return user;
    }

    public UserInfo getUser(String userId) {
        User user = findById(userId);
        String userNm = cryptUtils.decrypt(user.getUserNm());
        String mbtlnum = cryptUtils.decrypt(user.getMbtlnum());

        return new UserInfo(user, userNm, mbtlnum);
    }

    public UserUpdate getUserUpdate(String userId) {
        return new UserUpdate(findById(userId));
    }

    public UserInfo findByMbtlnum(FindDto findDto) {
//        User find = userRepository.findByEmail(findDto);
        User find = userRepository.findByMbtlnum(findDto);

        if (find == null) {
            return null;
        }

        String userNm = cryptUtils.decrypt(find.getUserNm());
        String mbtlnum = cryptUtils.decrypt(find.getMbtlnum());

        UserInfo userInfo = new UserInfo(find,userNm,mbtlnum);
        return userInfo;
    }

    /*
    public UserInfo findByEmail(FindDto findDto) {
        User find = userRepository.findByEmail(findDto);
        if (find == null) {
            return null;
        }

        String userNm = cryptUtils.decrypt(find.getUserNm());
        String mbtlnum = cryptUtils.decrypt(find.getMbtlnum());
        String email = cryptUtils.decrypt(find.getEmail());

        UserInfo userInfo = new UserInfo(find,userNm,mbtlnum,email);
        return userInfo;
    }
    */

    public Cmpny findCmpny(String bizrno) throws Exception {
        Optional<Cmpny> cmpny = cmpnyRepository.findById(bizrno);
        if (cmpny.isPresent()) {
            return cmpny.get();
        } else {
            throw new UnableChangeStatusException("none.cmpny");
        }
    }

    public BizrnoCheckDto selectCmpnyBizrno(String bizrno) {
        BizrnoCheckDto dto = userDao.selectCmpnyBizrno(bizrno);
        return dto;
    }

    public CmpnyDto selectCmpny(String bizrno) {
        return userDao.selCmpny(bizrno);
    }

    @Transactional
    public User save(UserSave saveData, String useSe) throws Exception {
        if (saveData.getPassword() != null) {
            saveData.setPassword(passwordEncoder.encode(saveData.getPassword()));
        }

        if (StringUtils.hasLength(saveData.getUserNm())) {
            saveData.setUserNm(cryptUtils.encrypt(saveData.getUserNm()));
        }

        if (StringUtils.hasLength(saveData.getMbtlnum())) {
            saveData.setMbtlnum(cryptUtils.encrypt(saveData.getMbtlnum()));
        }

        // 사용자 구분 & 권한 구분
        CmmnCode useSeCmmnCode = commonService.findById(useSe);
        CmmnCode authCmmnCode = commonService.findById("AUTH000001");
        String userId = saveData.getUserId() + "_CMPNY!";
        Auth auth = new Auth(userId, authCmmnCode);
        User user = User.of(userId, auth, saveData, useSeCmmnCode);

        //대표인 경우
        if (useSe.equals("USESE00002")) {
            CmmnCode rspofCmmnCode = commonService.findById("RSPOF00010");
            user.setRspoFcSe(rspofCmmnCode);

            Cmpny cmpny = Cmpny.of(saveData);
            if (saveData.getFiles() != null) {
                cmpny.setFile(saveData.getFiles());
            }
            Cmpny saveCmpny = cmpnyRepository.save(cmpny);
            user.setCmpny(saveCmpny);
        } else {
            // 직원인 경우
            CmmnCode rspofCmmnCode;
            if(saveData.getPosition() != null && !"".equals(saveData.getPosition())){
                rspofCmmnCode = commonService.findById(saveData.getPosition());
                user.setRspoFcSe(rspofCmmnCode);
            }
            Cmpny cmpny = findCmpny(saveData.getBizrnoFilter());
            user.setCmpny(cmpny);
        }

        return userRepository.save(user);
    }

    @Transactional
    public User passwordReset(LoginDto loginDto) {
        if (loginDto.getPassword() != null) {
            loginDto.setPassword(passwordEncoder.encode(loginDto.getPassword()));
            loginDto.setUserId(loginDto.getUserId() + "_CMPNY!");
        }

        User user = findById(loginDto.getUserId());
        user.updatePassword(loginDto.getPassword());

        return user;
    }

    @Transactional
    public User update(UserUpdate updateData, MultipartFile file) throws IOException {
        if (updateData.getPassword() != null && !updateData.getPassword().equals("")) {
            updateData.setPassword(passwordEncoder.encode(updateData.getPassword()));
        }

        if (StringUtils.hasLength(updateData.getUserNm())) {
            updateData.setUserNm(cryptUtils.encrypt(updateData.getUserNm()));
        }

        if (StringUtils.hasLength(updateData.getMbtlnum())) {
            updateData.setMbtlnum(cryptUtils.encrypt(updateData.getMbtlnum()));
        }

        User user = findById(userId(updateData.getUserId()));
        String cmpnyBizrno = user.getCmpny().getBizrno();
        CmmnCode useSeCmmnCode = commonService.findById(updateData.getUseSe());
        user.oldUserUpdate(updateData, useSeCmmnCode);

        Optional<Cmpny> findCmpny = cmpnyRepository.findById(updateData.getBizrnoFilter());
        if (updateData.getUseSe().equals("USESE00002")) {
            if (findCmpny.isPresent()) {
                Cmpny cmpny = findCmpny.get();
                cmpny.oldUserUpdate(updateData, file);
                user.setCmpny(cmpny);
            } else {
                Cmpny cmpny = Cmpny.of(updateData);
                if (file != null) {
                    cmpny.setFile(file);
                }
                Cmpny saveCmpny = cmpnyRepository.save(cmpny);
                user.setCmpny(saveCmpny);
            }
            if (cmpnyBizrno.length() < 10) {
                // 유지관리에서 companyId로 된 항목들은 변경된 사업자번호로 바꿔야함
                // 유지관리 entity가 만들어지면 가능
                // companyId = null로 update

                List<Mntmgt> list = mntmgtRepository.findByBizrno(cmpnyBizrno);
                for (Mntmgt mntmgt : list) {
                    mntmgt.updateCmpny(user.getCmpny());
                }

                user.updateCompanyId();
            }
        } else {
            if (StringUtils.hasLength(updateData.getPosition())) {
                CmmnCode position = commonService.findById(updateData.getPosition());
                user.setRspoFcSe(position);
            } else {
                user.setRspoFcSe(null);
            }

            if (findCmpny.isPresent()) {
                Cmpny cmpny = findCmpny.get();
                user.setCmpny(cmpny);
                user.updateCompanyId();
            }
        }
        return user;
    }

    @Transactional
    public User insttUpdate(String userId, InsttUpdate updateData) {
        User user = findById(userId);
        if (updateData.getPassword() != null && !updateData.getPassword().equals("")) {
            updateData.setPassword(passwordEncoder.encode(updateData.getPassword()));
        }

        if (StringUtils.hasLength(updateData.getUserNm())) {
            updateData.setUserNm(cryptUtils.encrypt(updateData.getUserNm()));
        }

        if (StringUtils.hasLength(updateData.getMbtlnum())) {
            updateData.setMbtlnum(cryptUtils.encrypt(updateData.getMbtlnum()));
        }

        user.oldInsttUpdate(updateData);

        if (updateData.getChkDsptcAt().equals("N")) { //파견근무중 체크추가
            for (Dsptc dsptc : user.getDsptcList()) {
                dsptc.updateDsptcAt(updateData.getDsptcAt());
            }

            if (updateData.getDsptcAt().equals("Y")) {
                String orgCd = updateData.getOrgCd();
                String dsptcId = getDsptcId();
                String dsptcBgnde = updateData.getDsptcBgnde();
                String dsptcEndde = updateData.getDsptcEndde();
                CmmnCode cmmnCode = commonService.findById("DSPTC00001");
                Organizaion orgCdCode = organizationRepository.findById(orgCd)
                        .orElseThrow(EntityNotFoundException::new);
                Dsptc dsptc = Dsptc.of(dsptcId, user, orgCdCode, cmmnCode, dsptcBgnde, dsptcEndde);
                Dsptc saveDsptc = dsptcRepository.save(dsptc);
                user.setDsptc(saveDsptc);
            }
        }
        return user;
    }

    private String getDsptcId() {
        return dsptcRepository.findDsptcId();
    }
}
