package egovframework._gbe.service.mber.cmpds;

import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.common.util.SecurityUtils;
import egovframework._gbe.domain.cmpds.Cmpds;
import egovframework._gbe.domain.mntmgt.Mntmgt;
import egovframework._gbe.domain.user.SessionUser;
import egovframework._gbe.domain.user.User;
import egovframework._gbe.repository.equipment.cmpds.CmpdsRepository;
import egovframework._gbe.repository.equipment.cmpds.file.CmpdsFileRepository;
import egovframework._gbe.repository.mber.cmpds.CmpdsCmpnyRepository;
import egovframework._gbe.repository.equipment.mntmgt.MntmgtRepository;
import egovframework._gbe.repository.equipment.mntmgt.file.MntmgtFileRepository;
import egovframework._gbe.repository.mber.rep.RepCmpdsDao;
import egovframework._gbe.repository.mber.rep.RepMntmgtDao;
import egovframework._gbe.repository.mntmgt.MntmgtCmpnyRepository;
import egovframework._gbe.repository.mntmgt.MntmgtCmpnyRepositoryQueryDsl;
import egovframework._gbe.repository.user.UserRepository;
import egovframework._gbe.web.dto.equipment.file.EquipmentFileDto;
import egovframework._gbe.web.dto.mber.rep.cmpds.CmpdsDetailDto;
import egovframework._gbe.web.dto.mber.rep.cmpds.CmpdsUpdateDto;
import egovframework._gbe.web.dto.mber.rep.dashboard.DashboardMntmgtDto;
import egovframework._gbe.web.dto.mber.rep.mntmgt.*;
import egovframework._gbe.web.dto.mber.rep.cmpds.CmpdsListDto;
import egovframework._gbe.web.dto.mber.rep.dashboard.DashboardCmpdsDto;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.ejb.NoSuchEntityException;
import javax.persistence.EntityExistsException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class CmpdsCmpnyServiceImpl implements CmpdsCmpnyService {

    private final MntmgtCmpnyRepositoryQueryDsl mntmgtCmpnyRepositoryQueryDsl;
    private final CmpdsCmpnyRepository cmpdsCmpnyRepository;
    private final MntmgtFileRepository mntmgtFileRepository;

    private final CmpdsFileRepository cmpdsFileRepository;
    private final CmpdsRepository cmpdsRepository;
    private final RepCmpdsDao repCmpdsDao;
    private final UserRepository userRepository;


    /* 업체 대표 리스트 */
    @Override
    public Page<CmpdsListDto> cmpdsList (Pageable pageable, SearchParam searchParam) {

        String userId = SecurityUtils.getLoginUser().getUserId();

        //statusTypeCheck(searchParam);
        //maintenanceTypeCheck(searchParam);

        searchParam.setWord6(userId); // 권한 구분
        searchParam.settingPage(pageable);
        //Page<MntmgtListDto> result = repMntmgtDao.getRepMntmgtList(pageable, userId, searchParam);
        List<CmpdsListDto> cmpdss = repCmpdsDao.getRepCmpdsList(searchParam);
        //repCmpdsDao
        Integer totalCnt = 0;
        if (cmpdss.size() > 0) {
            totalCnt = cmpdss.get(0).getTotalCnt();
        }

        return new PageImpl<>(cmpdss, pageable, totalCnt);
    }

    /* 업체 대표 리스트 카운트 */
    @Override
    public MntmgtListCountDto getRepCmpdsListCount(SearchParam searchParam) {
        MntmgtListCountDto result = repCmpdsDao.getRepCmpdsListCount(searchParam);
        //repCmpdsDao
        return result;
    }


    /* 업체 상세 (대표, 담당자) */
    @Override
    public CmpdsDetailDto cmpdsDetail (String rceptNo) {

        String userId = SecurityUtils.getLoginUser().getUserId();

        Map map = new HashMap();
        map.put("userId",userId);
        map.put("rceptNo",rceptNo);

        CmpdsDetailDto result = repCmpdsDao.getCmpdsDetail(map);
        result.setFiles(getMCmpdsFileDtos(rceptNo)); // 학교 첨부 파일 부분
        result.setSaveFiles(getMCmpdsFileDtos(rceptNo)); // 업체 첨부 파일 부분

        Map charmap = new HashMap();
        charmap.put("bizrno",result.getBizrno());
        charmap.put("rceptNo",rceptNo);

        List<MntmgtDetailChargerList> chargerList = repCmpdsDao.getCmpdsDetailChargerList(charmap);

        result.setChargerLists(chargerList);

        return result;
    }

    /* 업체 상세 업데이트 */
    @Override
    @Transactional
    public void cmpdsUpdate (String rceptNo, CmpdsUpdateDto saveData) throws IOException {

        String userId = SecurityUtils.getLoginUser().getUserId();
        Cmpds cmpds = findByCmpds(rceptNo);

        if (saveData.getChargerId() == null || saveData.getChargerId().equals("")) { // 업체 담당자 용도
            saveData.setChargerId(userId);
        }

        User chargerUser = userRepository.findById(saveData.getChargerId())
                .orElseThrow(NoSuchEntityException::new);

        User rceptUser = userRepository.findById(saveData.getChargerId())
                .orElseThrow(NoSuchEntityException::new);

        User comptUser = userRepository.findById(saveData.getChargerId())
                .orElseThrow(NoSuchEntityException::new);

        cmpds.UpdateRepCmpds(saveData, chargerUser, rceptUser, comptUser);

        cmpds.updateFile(saveData.getFiles(), saveData.getSavedFileIds(), false); // 업체 참고 파일 업데이트
    }

    @Override
    /* 업체 직원 리스트 */
    public Page<CmpdsListDto> cmpdsEmpList (Pageable pageable, SearchParam searchParam) {

        String userId = SecurityUtils.getLoginUser().getUserId();

        //statusTypeCheck(searchParam);
        //maintenanceTypeCheck(searchParam);

        searchParam.setWord6(userId); // 권한 구분
        searchParam.settingPage(pageable);
        //Page<MntmgtListDto> result = repMntmgtDao.getRepMntmgtList(pageable, userId, searchParam);
        List<CmpdsListDto> cmpdss = repCmpdsDao.getEmpCmpdsList(searchParam);

        Integer totalCnt = 0;
        if (cmpdss.size() > 0) {
            totalCnt = cmpdss.get(0).getTotalCnt();
        }

        return new PageImpl<>(cmpdss, pageable, totalCnt);
    }

    /* 업체 담당자 리스트 카운트 */
    @Override
    public MntmgtListCountDto getEmpCmpdsListCount(SearchParam searchParam) {
        MntmgtListCountDto result = repCmpdsDao.getEmpCmpdsListCount(searchParam);
        //repCmpdsDao
        return result;
    }


    /* 공용 */
    public Cmpds findByCmpds (String rceptNo) {
        return cmpdsCmpnyRepository.findById(rceptNo)
                .orElseThrow(EntityExistsException::new);
    }

    public List<DashboardCmpdsDto> cmpnyDashboardCmpds() {
        SessionUser user = SecurityUtils.getLoginUser();

        if(user.getUseSe().equals("USESE00002")) {
            List<DashboardCmpdsDto> result = repCmpdsDao.getDashboardCmpds(user.getBizrno());
            return result;
        } else {
            Map map = new HashMap();
            map.put("bizrno",user.getBizrno());
            map.put("charerId",user.getUserId());
            List<DashboardCmpdsDto> result = repCmpdsDao.getDashboardCmpdsEmp(map);
            return result;
        }
    }

    /*@Override
    public void statusTypeCheck (SearchParam searchParam) {
        if (searchParam.getSelectKey2() != null && !searchParam.getSelectKey2().isEmpty()) {
            if (searchParam.getSelectKey2().equals("APPLY")) {
                searchParam.setSelectKey2("STTUS00001");
            } else if (searchParam.getSelectKey2().equals("RECEIPT")) {
                searchParam.setSelectKey2("STTUS00002");
            } else if (searchParam.getSelectKey2().equals("CANCEL")) {
                searchParam.setSelectKey2("STTUS00003");
            } else if (searchParam.getSelectKey2().equals("HOLD")) {
                searchParam.setSelectKey2("STTUS00004");
            } else if (searchParam.getSelectKey2().equals("COMPLETE")) {
                searchParam.setSelectKey2("STTUS00005");
            }
        }
    }*/

    /*@Override
    public void maintenanceTypeCheck (SearchParam searchParam) {
        if (searchParam.getSelectKey3() != null && !searchParam.getSelectKey3().isEmpty()) {
            if (searchParam.getSelectKey3().equals("FLAW")) {
                searchParam.setSelectKey3("MNTMG00001");
            } else if (searchParam.getSelectKey3().equals("INSTALL")) {
                searchParam.setSelectKey3("MNTMG00002");
            } else if (searchParam.getSelectKey3().equals("INSPECTION")) {
                searchParam.setSelectKey3("MNTMG00003");
            } else if (searchParam.getSelectKey3().equals("ETC")) {
                searchParam.setSelectKey3("MNTMG00004");
            }
        }
    }*/

    private List<EquipmentFileDto> getMCmpdsFileDtos(String rceptNo) { // 학교 첨부 파일
        return cmpdsFileRepository.findByCmpds(getCmpds(rceptNo)).stream()
                .map(EquipmentFileDto::new)
                .collect(Collectors.toList());
    }

    private Cmpds getCmpds(String rceptNo) { // 학교 첨부 파일 관련
        return cmpdsRepository.findById(rceptNo)
                .orElseThrow(() -> new NoSuchElementException("잘못된 접근입니다."));
    }
}
