package egovframework._gbe.service.mber.cmpds;

import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.web.dto.mber.rep.cmpds.CmpdsDetailDto;
import egovframework._gbe.web.dto.mber.rep.cmpds.CmpdsUpdateDto;
import egovframework._gbe.web.dto.mber.rep.mntmgt.MntmgtListCountDto;
import egovframework._gbe.web.dto.mber.rep.mntmgt.MntmgtListDto;
import egovframework._gbe.web.dto.mber.rep.cmpds.CmpdsListDto;
import egovframework._gbe.web.dto.mber.rep.dashboard.DashboardCmpdsDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.IOException;
import java.util.List;

public interface CmpdsCmpnyService {


    /* 업체 대표 리스트 */
    Page<CmpdsListDto> cmpdsList (Pageable pageable, SearchParam searchParam);

    /* 업체 대표 리스트 카운트 */
    MntmgtListCountDto getRepCmpdsListCount(SearchParam searchParam);

    CmpdsDetailDto cmpdsDetail (String rceptNo);

    void cmpdsUpdate (String rceptNo, CmpdsUpdateDto saveData) throws IOException;


    /* 업체 담당자 리스트 */
    Page<CmpdsListDto> cmpdsEmpList (Pageable pageable, SearchParam searchParam);

    /* 업체 담당자 리스트 카운트 */
    MntmgtListCountDto getEmpCmpdsListCount(SearchParam searchParam);

    /* 대시보드 유지관리 리스트 */
    List<DashboardCmpdsDto> cmpnyDashboardCmpds();

    /* 공통 */
    //void statusTypeCheck (SearchParam searchParam);

    //void maintenanceTypeCheck (SearchParam searchParam);
}
