package egovframework._gbe.service.mber.report;

import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.common.util.CryptUtils;
import egovframework._gbe.common.util.SecurityUtils;
import egovframework._gbe.domain.report.Report;
import egovframework._gbe.domain.user.SessionUser;
import egovframework._gbe.domain.user.cmpny.Cmpny;
import egovframework._gbe.domain.user.organization.Organizaion;
import egovframework._gbe.repository.mber.rep.ReportDao;
import egovframework._gbe.repository.mber.rep.ReportRepository;
import egovframework._gbe.repository.user.cmpny.CmpnyRepository;
import egovframework._gbe.repository.user.organization.OrganizationRepository;
import egovframework._gbe.web.dto.mber.rep.mntmgt.MntmgtListDto;
import egovframework._gbe.web.dto.mber.rep.report.ReportDetailDto;
import egovframework._gbe.web.dto.mber.rep.report.ReportDetailListDto;
import egovframework._gbe.web.dto.mber.rep.report.ReportListDto;
import egovframework._gbe.web.dto.mber.rep.report.ReportOrgListDto;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class ReportCmpnyService {
    private final ReportDao reportDao;
    private final OrganizationRepository organizationRepository;
    private final CmpnyRepository cmpnyRepository;
    private final ReportRepository reportRepository;

    private final CryptUtils cryptUtils;

    // ReportListDto
    public Page<ReportListDto> reportList (Pageable pageable, SearchParam searchParam) { /* 점검보고서 리스트 */

        String userId = SecurityUtils.getLoginUser().getUserId();

        searchParam.setWord6(userId); // 권한 구분
        searchParam.settingPage(pageable);
        //Page<MntmgtListDto> result = repMntmgtDao.getRepMntmgtList(pageable, userId, searchParam);
        List<ReportListDto> reports = reportDao.getReportList(searchParam);

        Integer totalCnt = 0;
        if (reports.size() > 0) {
            totalCnt = reports.get(0).getTotalCnt();
        }

        return new PageImpl<>(reports, pageable, totalCnt);
    }

    public List<ReportOrgListDto> orgList () { /* 업체와 연결된 학교 리스트 */
        String userId = SecurityUtils.getLoginUser().getUserId();
        List<ReportOrgListDto> orgList = reportDao.getReportOrgList(userId);
        return orgList;
    }

    public List<ReportOrgListDto> orgsaveList (String reprtNo) { /* 업체와 연결된 학교 리스트 */

        boolean orgChk = true; // 같은 학교 있는지 확인하는 부분
        String userId = SecurityUtils.getLoginUser().getUserId();
        List<ReportOrgListDto> orgList = reportDao.getReportOrgList(userId);
        Report report = reportRepository.findById(Long.parseLong(reprtNo)).orElseThrow(EntityNotFoundException::new);
        Organizaion organizaion = organizationRepository.findById(report.getOrgCd().getOrgCd())
                .orElseThrow(EntityNotFoundException::new);

        for (ReportOrgListDto orgcd : orgList) {
            if (orgcd.getOrgCd().equals(organizaion.getOrgCd())) {
                orgChk = false;
            }
        }

        if (orgChk) {
            ReportOrgListDto reportOrgListDto = new ReportOrgListDto(); // 과거 report에 들어있던 학교 정보
            reportOrgListDto.setOrgNm(organizaion.getOrgNm());
            reportOrgListDto.setOrgCd(organizaion.getOrgCd());
            orgList.add(reportOrgListDto);
        }
        
        return orgList;
    }

    public List<String> yearList () { /* 점검보고서 상세 년도 리스트 */
        LocalDate now = LocalDate.now();
        List<String> yearlist = new ArrayList<>();
        for (int i=0; i<5; i++) {
            yearlist.add(Integer.toString(now.getYear() - i));
        }
        return yearlist;
    }

    public List<String> monthList () { /* 점검보고서 상세 월 리스트 */
        List<String> monthlist = new ArrayList<>();
        for (int i=12; i>=1; i--) {
            monthlist.add(Integer.toString(i));
        }
        return monthlist;
    }

    public List<MntmgtListDto> mntmgtList (ReportDetailDto reportDetailDto) { /* 점검보고서 유지관리 리스트 */
        String userId = SecurityUtils.getLoginUser().getUserId();

        Map map = new HashMap();
        map.put("userId",userId);
        map.put("orgNm",reportDetailDto.getOrgNm());
        map.put("startDate",reportDetailDto.getStartDate());
        map.put("endDate",reportDetailDto.getEndDate());

        List<MntmgtListDto> mntmgts = reportDao.getmntmgtList(map);
        return mntmgts;
    }

    public List<MntmgtListDto> getrtdetailList (String reprtNo) { /* 점검보고서 유지관리 리스트 */
        List<MntmgtListDto> mntmgts = reportDao.getrtdetaillist(reprtNo);
        return mntmgts;
    }

    @Transactional
    public void reportSave (String reprtNo, ReportDetailDto reportDetailDto) { /* 점검보고서 상세 등록 및 업데이트 */

        SessionUser user = SecurityUtils.getLoginUser();

        Organizaion organizaion = organizationRepository.findById(reportDetailDto.getOrgNm())
                .orElseThrow(EntityNotFoundException::new);

        Cmpny cmpny = cmpnyRepository.findById(user.getBizrno())
                .orElseThrow(EntityNotFoundException::new);

        List<ReportDetailListDto> rpdList = new ArrayList<>();

        for (ReportDetailListDto data: reportDetailDto.getDetailList()) { // 점검보고서 신청인 이름 암호화 후 등록
            data.setRegisterNm(cryptUtils.encrypt(data.getRegisterNm()));
            rpdList.add(data);
        }

        reportDetailDto.setDetailList(rpdList);

        if (reprtNo != null) {
            Report report = reportRepository.findById(Long.parseLong(reprtNo))
                    .orElseThrow(EntityNotFoundException::new);

            report.reportUpdate(user.getUserId(), organizaion, cmpny, reportDetailDto);
        } else {
            Report report = Report.reportSave(user.getUserId(), organizaion, cmpny, reportDetailDto);
            reportRepository.save(report);
        }
    }

    public void detailView (String reprtNo ,ReportDetailDto reportDetailDto) {

        if (reportDetailDto.getYear() == null && reportDetailDto.getMonth() == null && reportDetailDto.getChk() == null && reportDetailDto.getStartDate() == null && reportDetailDto.getEndDate() == null) {
            Report report = reportRepository.findById(Long.parseLong(reprtNo))
                    .orElseThrow(EntityNotFoundException::new);
            reportDetailDto.setReprtNo(reprtNo);
            reportDetailDto.setYear(Integer.toString(report.getYear()));
            reportDetailDto.setMonth(Integer.toString(report.getMonth()));
            reportDetailDto.setOrgNm(report.getOrgCd().getOrgCd());
            reportDetailDto.setStartDate(report.getBgnde());
            reportDetailDto.setEndDate(report.getEndde());
            reportDetailDto.setMemo(report.getMemo());
        }
    }

    public void nullChk (ReportDetailDto reportDetailDto) {
        LocalDate now = LocalDate.now();

        if (reportDetailDto.getMonth() == null || reportDetailDto.getMonth().equals("")) { /* 당일 월 체크 */
            reportDetailDto.setMonth(Integer.toString(now.getMonthValue()));
        }

        if (reportDetailDto.getYear() != null) {
            if (reportDetailDto.getYear().equals("")) {
                reportDetailDto.setYear(null);
            }
        }
        if (reportDetailDto.getMonth() != null) {
            if (reportDetailDto.getMonth().equals("")) {
                reportDetailDto.setMonth(null);
            }
        }
        if (reportDetailDto.getChk() != null) {
            if (reportDetailDto.getChk().equals("")) {
                reportDetailDto.setChk(null);
            }
        }
        if (reportDetailDto.getOrgNm() != null) {
            if (reportDetailDto.getOrgNm().equals("")) {
                reportDetailDto.setOrgNm(null);
            }
        }
    }
}
