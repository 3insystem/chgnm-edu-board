package egovframework._gbe.service.mber.cmpnymsg;

import egovframework._gbe.common.message.MessageDto;
import egovframework._gbe.common.message.MessageService;
import egovframework._gbe.common.util.CryptUtils;
import egovframework._gbe.common.util.SecurityUtils;
import egovframework._gbe.domain.cmpds.Cmpds;
import egovframework._gbe.domain.mntmgt.Mntmgt;
import egovframework._gbe.domain.user.SessionUser;
import egovframework._gbe.domain.user.User;
import egovframework._gbe.repository.mber.cmpds.CmpdsCmpnyRepository;
import egovframework._gbe.repository.mntmgt.MntmgtCmpnyRepository;
import egovframework._gbe.repository.user.UserRepository;
import egovframework._gbe.service.msg.MessengerService;
import egovframework._gbe.service.msg.NoticeMessengerService;
import egovframework._gbe.web.dto.mber.rep.CmpMsgDto;
import egovframework._gbe.web.dto.mber.rep.report.ReportDetailDto;
import egovframework._gbe.web.dto.mber.rep.report.ReportDetailListDto;
import egovframework._gbe.web.dto.msg.MessengerDto;
import egovframework._gbe.web.dto.msg.NoticeMessengerDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.ejb.NoSuchEntityException;
import javax.persistence.EntityExistsException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class CmpMsgService {
    private final MessageService messageService; // SMS
    private final MessengerService messengerService;
    private final NoticeMessengerService noticeMessengerService; // Messenger
    private final MntmgtCmpnyRepository mntmgtCmpnyRepository;
    private final CmpdsCmpnyRepository cmpdsCmpnyRepository;
    private final UserRepository userRepository;

    private final CryptUtils cryptUtils;

    @Transactional
    public void sendCmpSms(String rceptNo, String statusChk) throws Exception {
        LocalDateTime date = LocalDateTime.now();
        CmpMsgDto cmpMsgDto;
        SessionUser user = SecurityUtils.getLoginUser(); /* 발신자 번호 */
        User userinfo = userRepository.findById(user.getUserId()).orElseThrow(NoSuchEntityException::new);

        cmpMsgDto = jobSeChk(rceptNo);

        if ((!(cmpMsgDto.getStatusSe().equals(statusChk)) == (cmpMsgDto.getStatusSe().equals("STTUS00002"))) && !(userinfo.getMbtlnum().equals(cmpMsgDto.getUser().getMbtlnum()))) {
            String sndMsg = cmpMsgDto.getJobSe() + " 접수번호 " + rceptNo + " 건이 접수되었으며, 해당건 담당자로 배정되었습니다.";

            List<Map<String, String>> list = new ArrayList<>();
            Map<String, String> map = new HashMap<>();
            map.put("name", cryptUtils.decrypt(cmpMsgDto.getUser().getUserNm()));
            map.put("phone", cryptUtils.decrypt(cmpMsgDto.getUser().getMbtlnum()));
            list.add(map);

            messageService.sendMsg(cryptUtils.decrypt(userinfo.getMbtlnum()), list, userinfo.getUsid(), cmpMsgDto.getJobSe(), sndMsg, "0", "");

        }
    }

    @Transactional
    public void sendCmpMessenger(String rceptNo) {
        NoticeMessengerDto dto = new NoticeMessengerDto();
        CmpMsgDto cmpMsgDto;
        String callerId = "gbeadmin"; /* 발신자 번호(교육청) */

        cmpMsgDto = jobSeChk(rceptNo);

        if (cmpMsgDto.getStatusSe().equals("STTUS00002")) {
            cmpMsgDto.setStatusSe("접수");
        } else if (cmpMsgDto.getStatusSe().equals("STTUS00004")) {
            cmpMsgDto.setStatusSe("보류");
        } else {
            cmpMsgDto.setStatusSe("완료");
        }

        String sndMsg = cmpMsgDto.getJobSe() + " 접수번호 " + rceptNo + " 건이(" + cmpMsgDto.getStatusSe() + ")되었습니다." + System.lineSeparator()
                + "업체명 : " + cmpMsgDto.getUser().getCmpny().getCmpnyNm() + System.lineSeparator()
                + "담당자 " + cryptUtils.decrypt(cmpMsgDto.getUser().getUserNm()) + "(" + cryptUtils.decrypt(cmpMsgDto.getUser().getMbtlnum()) + ")";

        dto.setSender(callerId); /* 발신자 아이디 */
        dto.setSendername("충청남도교육청");
        dto.setReceiver(cmpMsgDto.getRcvId()); /* 수신자 아이디 */
        dto.setTitle(cmpMsgDto.getJobSe()); /* 제목 */
        dto.setContent(sndMsg); /* 내용 */
        dto.setOpt1("https://as.cne.go.kr/member/edu/dashboard");
        dto.setLinkmessage("접수신청 확인");

        noticeMessengerService.sendOne(dto);
    }


    /* 점검보고서 메신저 */
    @Transactional
    public void sendCmpReportMessenger(ReportDetailDto reportDetailDto) {
        SessionUser user = SecurityUtils.getLoginUser();
        List<ReportDetailListDto> reportDetailListDtos = reportDetailDto.getDetailList();
        Set<String> uniqueChargerIds = new HashSet<>();
        List<ReportDetailListDto> uniqueReportDetailListDtos = new ArrayList<>();
        List<NoticeMessengerDto> dtoList = new ArrayList<>();

        String callerId = "gbeadmin"; /* 발신자 번호(교육청) */
        LocalDate date = LocalDate.now();

        User userInfo = userRepository.findById(user.getUserId()).orElseThrow(EntityExistsException::new);

        reportDetailDto.setChk("유지관리");

        String sndMsg = userInfo.getCmpny().getCmpnyNm() + " 업체에서 " + date.getYear() + "년 " + date.getMonthValue()
                + "월 (" + reportDetailDto.getChk() + ") 점검보고서가 등록되었습니다." + System.lineSeparator()
                + "로그인>점검보고서 메뉴에서 확인해주세요.";


        for (ReportDetailListDto reportDetailListDto : reportDetailListDtos) {  /* 담당자 아이디 키 중복 제거 */
            String chargerId = reportDetailListDto.getChargerId();
            if (!uniqueChargerIds.contains(chargerId)) {
                uniqueChargerIds.add(chargerId);
                uniqueReportDetailListDtos.add(reportDetailListDto);
            }
        }


        if (uniqueReportDetailListDtos.size() > 0) {

            uniqueReportDetailListDtos.forEach(us -> {
                NoticeMessengerDto dto = new NoticeMessengerDto();

                dto.setSender(callerId); /* 발신자 아이디 */
                dto.setReceiver(us.getChargerId()); /* 수신자 아이디 */
                dto.setTitle(reportDetailDto.getChk()); /* 제목 */
                dto.setContent(sndMsg); /* 내용 */

                dtoList.add(dto);
            });
            noticeMessengerService.sendList(dtoList);
        }
    }

    // 유지관리 소모품 구분
    private CmpMsgDto jobSeChk(String rceptNo) {
        CmpMsgDto cmpMsgDto = new CmpMsgDto();
        String rcvId = "";
        String chargerInfo = "";
        String jobSe = "";
        String statusSe = "";

        Mntmgt mntmgt = mntmgtCmpnyRepository.findById(rceptNo)
                .orElseThrow(EntityExistsException::new);
        chargerInfo = mntmgt.getChargerUser().getUsid();
        jobSe = "유지관리";
        rcvId = mntmgt.getRegisterId();
        statusSe = mntmgt.getStatusType();

        User user = userRepository.findById(chargerInfo) /* 담당자 전화번호 */
                .orElseThrow(EntityExistsException::new);

        cmpMsgDto.setUser(user); /* 담당자 정보 */
        cmpMsgDto.setJobSe(jobSe);
        cmpMsgDto.setRcvId(rcvId);
        cmpMsgDto.setStatusSe(statusSe);

        return cmpMsgDto;
    }

}
