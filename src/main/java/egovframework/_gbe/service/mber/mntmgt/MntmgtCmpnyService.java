package egovframework._gbe.service.mber.mntmgt;

import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.web.dto.mber.rep.dashboard.DashboardCountDto;
import egovframework._gbe.web.dto.mber.rep.dashboard.DashboardMntmgtDto;
import egovframework._gbe.web.dto.mber.rep.mntmgt.MntmgtDetailDto;
import egovframework._gbe.web.dto.mber.rep.mntmgt.MntmgtListCountDto;
import egovframework._gbe.web.dto.mber.rep.mntmgt.MntmgtListDto;
import egovframework._gbe.web.dto.mber.rep.mntmgt.MntmgtUpdateDto;
import org.json.simple.parser.ParseException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

public interface MntmgtCmpnyService {

    /* 업체 대표 */
    Page<MntmgtListDto> mntmgtList (Pageable pageable, SearchParam searchParam);
    MntmgtListCountDto getRepMntmgtListCount(SearchParam searchParam);


    /* 업체 직원 */
    Page<MntmgtListDto> mntmgtEmpList (Pageable pageable, SearchParam searchParam);
    MntmgtListCountDto getEmpMntmgtListCount(SearchParam searchParam);



    /* 대시보드 카운트 */
    DashboardCountDto cmpnyDashboardCount();

    /* 대시보드 유지관리 리스트 */
    List<DashboardMntmgtDto> cmpnyDashboardMntgmt();

    /* 공통 */

    String cmpnyUseSe ();

    MntmgtDetailDto mntmgtDetail (String rceptNo);

    void mntmgtUpdate (String rceptNo, MntmgtUpdateDto saveData) throws IOException, ParseException;

    String statusChkb (String rceptNo);

    Boolean webFilter(String userId, String rceptNo, MntmgtUpdateDto mntmgtUpdateDto, HttpServletRequest request) throws IOException, ParseException;
    //void statusTypeCheck (SearchParam searchParam);

    //void maintenanceTypeCheck (SearchParam searchParam);
}
