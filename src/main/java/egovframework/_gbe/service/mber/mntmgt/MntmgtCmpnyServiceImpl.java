package egovframework._gbe.service.mber.mntmgt;

import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.common.util.CryptUtils;
import egovframework._gbe.common.util.SecurityUtils;
import egovframework._gbe.common.util.file.FileUtils;
import egovframework._gbe.domain.mntmgt.Mntmgt;
import egovframework._gbe.domain.mntmgt.file.MntmgtFile;
import egovframework._gbe.domain.user.SessionUser;
import egovframework._gbe.domain.user.User;
import egovframework._gbe.repository.equipment.mntmgt.MntmgtRepository;
import egovframework._gbe.repository.equipment.mntmgt.file.MntmgtFileRepository;
import egovframework._gbe.repository.mber.rep.RepCmpdsDao;
import egovframework._gbe.repository.mber.rep.RepMntmgtDao;
import egovframework._gbe.repository.mntmgt.MntmgtCmpnyRepository;
import egovframework._gbe.repository.mntmgt.MntmgtCmpnyRepositoryQueryDsl;
import egovframework._gbe.repository.user.UserRepository;
import egovframework._gbe.web.dto.equipment.EquipmentSave;
import egovframework._gbe.web.dto.equipment.file.EquipmentFileDto;
import egovframework._gbe.web.dto.mber.rep.dashboard.DashboardCountDto;
import egovframework._gbe.web.dto.mber.rep.dashboard.DashboardMntmgtDto;
import egovframework._gbe.web.dto.mber.rep.mntmgt.*;
import lombok.RequiredArgsConstructor;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.json.simple.parser.ParseException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.ejb.NoSuchEntityException;
import javax.persistence.EntityExistsException;
import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.nio.file.Files;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import static egovframework._gbe.common.util.CommonUtils.getClientIp;
import static egovframework._gbe.common.util.WebFilterUtils.getWfsend;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class MntmgtCmpnyServiceImpl implements MntmgtCmpnyService {

    private final MntmgtCmpnyRepositoryQueryDsl mntmgtCmpnyRepositoryQueryDsl;

    private final MntmgtCmpnyRepository mntmgtCmpnyRepository;

    private final MntmgtFileRepository mntmgtFileRepository;

    private final MntmgtRepository mntmgtRepository;

    private final RepMntmgtDao repMntmgtDao;

    private final RepCmpdsDao repCmpdsDao;

    private final UserRepository userRepository;

    private final CryptUtils cryptUtils;


    /* 업체 대표 리스트 */
    @Override
    public Page<MntmgtListDto> mntmgtList (Pageable pageable, SearchParam searchParam) {

        String userId = SecurityUtils.getLoginUser().getUserId();

        searchParam.setWord4(cryptUtils.encrypt(searchParam.getWord4()));
        searchParam.setWord6(userId); // 권한 구분
        searchParam.settingPage(pageable);
        //Page<MntmgtListDto> result = repMntmgtDao.getRepMntmgtList(pageable, userId, searchParam);
        List<MntmgtListDto> mntmgts = repMntmgtDao.getRepMntmgtList(searchParam);

        Integer totalCnt = 0;
        if (mntmgts.size() > 0) {
            totalCnt = mntmgts.get(0).getTotalCnt();
        }

        return new PageImpl<>(mntmgts, pageable, totalCnt);
    }

    /* 업체 대표 리스트 카운트 */
    @Override
    public MntmgtListCountDto getRepMntmgtListCount(SearchParam searchParam) {
        MntmgtListCountDto result = repMntmgtDao.getRepMntmgtListCount(searchParam);
        searchParam.setWord4(cryptUtils.decrypt(searchParam.getWord4())); // 암호화 된 글자 복호화 후 검색창으로 전달
        return result;
    }

    /* 업체 상세 (대표, 담당자) */
    @Override
    public MntmgtDetailDto mntmgtDetail (String rceptNo) {

        String userId = SecurityUtils.getLoginUser().getUserId();
        List<MntmgtDetailChargerList> chargerList = new ArrayList<>();

        Map map = new HashMap();
        map.put("userId",userId);
        map.put("rceptNo",rceptNo);

        MntmgtDetailDto result = repMntmgtDao.getMntmgtDetail(map);
        result.setFiles(getMntmgtFileDtos(rceptNo)); // 학교 첨부 파일 부분
        result.setSaveFiles(getMntmgtFileDtos(rceptNo)); // 업체 첨부 파일 부분

        Map charmap = new HashMap();
        charmap.put("bizrno",result.getBizrno());
        charmap.put("rceptNo",rceptNo);

        Mntmgt mntmgt = mntmgtCmpnyRepository.findById(rceptNo).orElseThrow(EntityExistsException::new);

        if (mntmgt.getChargerUser() == null) { /* 담당자 권한 변경시 담당자 조회 용도 */
            chargerList = repMntmgtDao.getMntmgtDetailChargerList(charmap);
        } else {
            MntmgtDetailChargerList charger = new MntmgtDetailChargerList();
            charger.setChargerId(mntmgt.getChargerUser().getUsid());
            charger.setChargerNm(cryptUtils.decrypt(mntmgt.getChargerUser().getUserNm()));
            chargerList.add(charger);
        }

        result.setChargerLists(chargerList);

        return result;
    }

    /* 업체 상세 업데이트 */
    @Override
    @Transactional
    public void mntmgtUpdate (String rceptNo, MntmgtUpdateDto saveData) throws IOException, ParseException {

        String userId = SecurityUtils.getLoginUser().getUserId();
        String chargerId = "";
        Mntmgt mntmgt = findByMntmgt(rceptNo);

        if ( mntmgt.getChargerUser() == null && (saveData.getChargerId() == null || saveData.getChargerId().equals("")) ) { // 업체 담당자 용도
            saveData.setChargerId(userId);
        }

        if (mntmgt.getChargerUser() == null) {
            chargerId = saveData.getChargerId();
        } else {
            chargerId = mntmgt.getChargerUser().getUsid();
        }

        User chargerUser = userRepository.findById(chargerId)
                .orElseThrow(NoSuchEntityException::new);

        User rceptUser = userRepository.findById(chargerId)
                .orElseThrow(NoSuchEntityException::new);

        User comptUser = userRepository.findById(userId)
                .orElseThrow(NoSuchEntityException::new);

        mntmgt.UpdateRepMntmgt(saveData, chargerUser, rceptUser, comptUser);
        mntmgt.updateFile(saveData.getFiles(), saveData.getSavedFileIds(), false); // 업체 참고 파일 업데이트
    }

    @Override
    /* 업체 직원 리스트 */
    public Page<MntmgtListDto> mntmgtEmpList (Pageable pageable, SearchParam searchParam) {

        String userId = SecurityUtils.getLoginUser().getUserId();

        //statusTypeCheck(searchParam);
        //maintenanceTypeCheck(searchParam);

        searchParam.setWord6(userId); // 권한 구분
        searchParam.settingPage(pageable);
        //Page<MntmgtListDto> result = repMntmgtDao.getRepMntmgtList(pageable, userId, searchParam);
        List<MntmgtListDto> mntmgts = repMntmgtDao.getEmpMntmgtList(searchParam);

        Integer totalCnt = 0;
        if (mntmgts.size() > 0) {
            totalCnt = mntmgts.get(0).getTotalCnt();
        }

        return new PageImpl<>(mntmgts, pageable, totalCnt);
    }

    /* 업체 담당자 리스트 카운트 */
    @Override
    public MntmgtListCountDto getEmpMntmgtListCount(SearchParam searchParam) {
        MntmgtListCountDto result = repMntmgtDao.getEmpMntmgtListCount(searchParam);

        return result;
    }


    /* 공용 */
    public Mntmgt findByMntmgt (String rceptNo) {
        return mntmgtCmpnyRepository.findById(rceptNo)
                .orElseThrow(EntityExistsException::new);
    }

    /* 업체 권한 */
    @Override
    public String cmpnyUseSe () {

        String userId = SecurityUtils.getLoginUser().getUserId();
        String useSe = mntmgtCmpnyRepositoryQueryDsl.getMntmgtUseSe(userId);

        return useSe;
    }

    /* 업체 대시보드 유지관리 + 소모품 카운트 */
    @Override
    public DashboardCountDto cmpnyDashboardCount() {
        SessionUser user = SecurityUtils.getLoginUser();

        if(user.getUseSe().equals("USESE00002")) {
            DashboardCountDto result = repMntmgtDao.getDashboardMntmgtCount(user.getBizrno()); // 유지관리
            //DashboardCountDto suc = repCmpdsDao.getDashboardCmpdsCount(user.getBizrno()); // 소모품
            //result.setSuc(suc.getSuc()); // 병합
            return result;
        } else {
            Map map = new HashMap();
            map.put("bizrno",user.getBizrno());
            map.put("charerId",user.getUserId());
            DashboardCountDto result = repMntmgtDao.getDashboardMntmgtCountEmp(map); // 유지관리
            //DashboardCountDto suc = repCmpdsDao.getDashboardCmpdsCountEmp(map); // 소모품
            //result.setSuc(suc.getSuc()); // 병합
            return result;
        }
    }

    public List<DashboardMntmgtDto> cmpnyDashboardMntgmt() {
        SessionUser user = SecurityUtils.getLoginUser();

        if(user.getUseSe().equals("USESE00002")) {
            List<DashboardMntmgtDto> result = repMntmgtDao.getDashboardMntmgt(user.getBizrno());
            return result;
        } else {
            Map map = new HashMap();
            map.put("bizrno",user.getBizrno());
            map.put("charerId",user.getUserId());
            List<DashboardMntmgtDto> result = repMntmgtDao.getDashboardMntmgtEmp(map);
            return result;
        }
    }

    private List<EquipmentFileDto> getMntmgtFileDtos(String rceptNo) { // 학교 첨부 파일
        return mntmgtFileRepository.findByMntmgt(getMntmgt(rceptNo)).stream()
                .map(EquipmentFileDto::new)
                .collect(Collectors.toList());
    }

    private Mntmgt getMntmgt(String rceptNo) { // 학교 첨부 파일 관련
        return mntmgtRepository.findById(rceptNo)
                .orElseThrow(() -> new NoSuchElementException("잘못된 접근입니다."));
    }

    @Override
    public String statusChkb (String rceptNo) {
        Mntmgt mntmgt = findByMntmgt(rceptNo);
        String statusChk = mntmgt.getStatusType();
        return statusChk;
    }

    @Override
    public Boolean webFilter(String userId, String rceptNo, MntmgtUpdateDto mntmgtUpdateDto, HttpServletRequest request) throws IOException, ParseException {

        User user = userRepository.findById(userId).orElseThrow(EntityExistsException::new);
        Mntmgt mntmgt = mntmgtCmpnyRepository.findById(rceptNo).orElseThrow(EntityExistsException::new); /* 파일 확인용 객체 */

        EquipmentSave equipmentSave = chkData(mntmgt);

        Mntmgt fakeData = Mntmgt.of("", equipmentSave, null, null);


        //웹필터 시작
        String actionUrl = "/mntmgt/detail/" + rceptNo;
        String listUrl = "/mntmgt";
        String writer = user.getUserNm();
        String subject = "";
        String contents = mntmgtUpdateDto.getProcessCn() == null ? "false*@$@$#%@5113@#$@#@" : mntmgtUpdateDto.getProcessCn();
        String userIp = request.getRequestURI();
        String webFiterChk = "";

        //파일 경로 및 파일명 추출
        String file_path = "";
        String file_name = "";

        if (mntmgtUpdateDto.getFiles() != null && !mntmgtUpdateDto.getFiles().isEmpty()) { // 파일이 첨부 되었을 경우
            // 파일 업로드
//            fakeData.getMntmgtFile().clear();
            fakeData.addFile(mntmgtUpdateDto.getFiles(), false);
            mntmgtUpdateDto.getFiles().clear();

            for (MntmgtFile file : fakeData.getMntmgtFile()) {
                file_path += file.getFileStreCours() + "|";
                file_name += file.getOrignlFileNm() + "|";
            }

            webFiterChk = getWfsend(actionUrl, listUrl, writer, subject, contents, file_path, file_name, userIp );

            for (MntmgtFile file : fakeData.getMntmgtFile()) {
                MultipartFile multipartFile = copyMultipartFile(file);
                mntmgtUpdateDto.getFiles().add(multipartFile);
                // 웹필터 전용 데이터 업로드 파일 삭제
                FileUtils.deleteFile(file.getStreFileNm());
            }

        } else {
            file_path = "";
            file_name = "";
            if (!contents.equals("false*@$@$#%@5113@#$@#@")) {
                webFiterChk = getWfsend(actionUrl, listUrl, writer, subject, contents, file_path, file_name, userIp );
            }
        }

        //웹필터링 함수 호출

        fakeData.getMntmgtFile().clear();

        //  System.out.println(webFiterChk);
        if (webFiterChk != "") { //내용이 있으면 파일 삭제
            String delchkFile = file_path;

            String[] filenames = delchkFile.split(" \\| ");

            for (String filename : filenames) {
                // "1번 "을 제거하여 실제 파일 이름 추출
                String actualFilename = filename;

                File fileToDelete = new File(actualFilename);
                fileToDelete.delete();
            }

            return false;

        } else {

            return true;
        }
    }

    /* 2023.12.13 확인용 */

    private MultipartFile copyMultipartFile(MntmgtFile mntmgtFile) throws IOException {
        File file = new File(mntmgtFile.getFileStreCours()); // 파일 경로
        DiskFileItem fileItem = new DiskFileItem("file", Files.probeContentType(file.toPath()), false, mntmgtFile.getOrignlFileNm(), (int) file.length(), file.getParentFile());
        try (InputStream input = new FileInputStream(file);
             OutputStream output = fileItem.getOutputStream()) {
            IOUtils.copy(input, output);
            output.flush();
        }
        return new CommonsMultipartFile(fileItem);
    }
    /* 2023.12.13 확인용 */

    private static String extractExt(String originalFileName) { /* 2023.12.12 psh */
        int pos = originalFileName.lastIndexOf(".");
        return originalFileName.substring(pos + 1);
    }

    private EquipmentSave chkData (Mntmgt mntmgt) {
        EquipmentSave data = new EquipmentSave();
        data.setMaintenanceType(mntmgt.getMaintenanceType());
        data.setClSe(mntmgt.getClSe());
        data.setSubject(mntmgt.getSubject());
        data.setContent(mntmgt.getContent());
        data.setStatusType(mntmgt.getStatusType());
        data.setCttpcl(mntmgt.getCttpcl());

        return data;
    }


//    public static String getFullPath(String filename) {
//        return fileDir + LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd")) +"/" + filename;
//    }
}
