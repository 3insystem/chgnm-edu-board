package egovframework._gbe.service.mber.rep;

import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.common.util.SecurityUtils;
import egovframework._gbe.domain.user.cmpny.Cmpny;
import egovframework._gbe.repository.mber.rep.SlcDao;
import egovframework._gbe.service.msg.MessengerService;
import egovframework._gbe.service.msg.NoticeMessengerService;
import egovframework._gbe.service.user.UserService;
import egovframework._gbe.web.dto.mber.rep.SlctnDto;
import egovframework._gbe.web.dto.msg.MessengerDto;
import egovframework._gbe.web.dto.msg.NoticeMessengerDto;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class SlcService {
    private final NoticeMessengerService noticeMessengerService;
    private final UserService userService;
    private final SlcDao slcDao;

    public Page<SlctnDto> selSlcList(SearchParam searchParam, Pageable pageable) {
        String bizrno = SecurityUtils.getLoginUser().getBizrno();
        searchParam.setWord3(bizrno);
        searchParam.settingPage(pageable);
        List<SlctnDto> dto = slcDao.selSlcList(searchParam);

        Integer totalCnt = 0;
        if (dto.size() > 0) {
            totalCnt = dto.get(0).getTotalCnt();
        }
        return new PageImpl<>(dto, pageable, totalCnt);
    }

    @Transactional
    public Map insertSlc(@RequestParam("userId") String userId, @RequestParam("bizrno") String bizrno, @RequestParam("orgCd") String orgCd)  throws Exception {
        Map map = new HashMap<>();
        map.put("userId", userId);
        map.put("bizrno", bizrno);
        map.put("orgCd", orgCd);
        Map resultMap = new HashMap<>();
        int checkSlc = slcDao.checkSlc(map);

        if (checkSlc > 0) {
            resultMap.put("result", "A");
            resultMap.put("msg", "이미 요청된 기관(학교)입니다.");
            return resultMap;
        }
        int result = slcDao.insertSlc(map);
        if (result == 0) {
            resultMap.put("result", "F");
            resultMap.put("msg", "요청을 실패했습니다.");
        } else {
            resultMap.put("result", "S");
        }
        List<SlctnDto> schUserList = slcDao.selSchList(map);

        List<NoticeMessengerDto> dtoList = new ArrayList<>();
        Cmpny cmpnyInfo = userService.findCmpny(bizrno);
        for (SlctnDto slctnDto: schUserList) {
            NoticeMessengerDto dto = new NoticeMessengerDto();
            String rcvId = slctnDto.getUsid();
            String callerId = "gbeadmin"; /* 발신자 번호(교육청) */
            dto.setSender(callerId);
            dto.setSendername("충청남도 교육청 지정 ID");
            dto.setReceiver(rcvId);
            dto.setTitle(cmpnyInfo.getCmpnyNm() +" 업체에서 기관(학교) 추가 요청");
            dto.setContent(cmpnyInfo.getCmpnyNm() + "업체에서 기관(학교) 추가 요청이 접수되었으며, 승인대기 상태입니다.\n" +
                    "업체관리 메뉴에서 확인해주세요.");
            dto.setOpt1("https://as.cne.go.kr/member/edu/dashboard");
            dtoList.add(dto);
        }
        noticeMessengerService.sendList(dtoList);
        return resultMap;
    }

    public Map deleteSlc(@RequestParam("slctnNo") String slctnNo, @RequestParam("userId") String userId, @RequestParam("bizrno") String bizrno) {

        Map map = new HashMap<>();
        map.put("userId", userId);
        map.put("slctnNo", slctnNo);
        map.put("bizrno", bizrno);

        int result = slcDao.deleteSlc(map);
        Map resultMap = new HashMap<>();
        if (result == 0) {
            resultMap.put("result", "F");
            resultMap.put("msg", "요청을 실패했습니다.");
        } else {
            resultMap.put("result", "S");
        }
        List<SlctnDto> selCpnList = slcDao.selCpn(map);
        String orgCd = slcDao.selOrg(map);
        for (SlctnDto list: selCpnList) {
            map.put("userId", list.getUsid());
            map.put("orgCd", orgCd);
            int res = slcDao.deleteChrgInstt(map);
            if (res == 0) {
                resultMap.put("result", "F");
                resultMap.put("msg", "요청을 실패했습니다.");
            } else {
                resultMap.put("result", "S");
            }
        }

        return resultMap;
    }

    public Map slcConfm(@RequestParam("slctnNo") String slctnNo, @RequestParam("userId") String userId) {
        Map map = new HashMap<>();
        map.put("userId", userId);
        map.put("slctnNo", slctnNo);
        int result = slcDao.slcConfm(map);
        Map resultMap = new HashMap<>();
        if (result == 0) {
            resultMap.put("result", "F");
            resultMap.put("msg", "요청을 실패했습니다.");
        } else {
            resultMap.put("result", "S");
        }
        return resultMap;
    }

    public Map slcStopBtn(String slctnNo, String userId) {
        Map map = new HashMap<>();
        map.put("userId", userId);
        map.put("slctnNo", slctnNo);
        int result = slcDao.slcStopBtn(map);
        Map resultMap = new HashMap<>();
        if(result == 0){
            resultMap.put("result", "F");
            resultMap.put("msg", "요청을 실패했습니다.");
        } else {
            resultMap.put("result", "S");
        }
        return resultMap;
    }
}
