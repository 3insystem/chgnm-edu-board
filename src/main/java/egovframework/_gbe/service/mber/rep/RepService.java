package egovframework._gbe.service.mber.rep;

import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.common.util.SecurityUtils;
import egovframework._gbe.domain.user.SessionUser;
import egovframework._gbe.repository.mber.rep.RepDao;
import egovframework._gbe.repository.user.UserDao;
import egovframework._gbe.web.dto.common.ChrgInsttDto;
import egovframework._gbe.web.dto.common.EmpManageDto;
import egovframework._gbe.web.dto.mber.rep.DeleteEmpAgreeDto;
import egovframework._gbe.web.dto.mber.rep.SelCmpnyAgreeListDto;
import egovframework._gbe.web.dto.mber.rep.SelEmpListDto;
import egovframework._gbe.web.dto.user.UserDto;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class RepService {

    private final UserDao userDao;
    private final RepDao repDao;

    public Page<SelEmpListDto> selEmpList(SearchParam searchParam, Pageable pageable){
        String bizrno = SecurityUtils.getLoginUser().getBizrno();
        searchParam.setWord3(bizrno);
        searchParam.settingPage(pageable);

        List<SelEmpListDto> list = repDao.selEmpList(searchParam)
                .stream()
                .map(SelEmpListDto::SelEmpListDto)
                .collect(Collectors.toList());

        Integer totalCnt = 0;
        if (list.size() > 0) {
            totalCnt = list.get(0).getTotalCnt();
        }
        return new PageImpl<>(list, pageable, totalCnt);
    }

    public SelEmpListDto selEmps(SearchParam param) {
        SelEmpListDto result = repDao.selEmpHist(param);

        String chrgJob = chrgJobChange(result.getChrgJob()); // 담당 업무 코드 -> 이름 변경
        result.setChrgJob(chrgJob);

        return result;
    }

    public List<SelCmpnyAgreeListDto> SelCmpnyAgreeList(Map map){

        Map searchMap = new HashMap();
        /* 현재 접속중인 유저의 사업자번호 */
        String bizrno = SecurityUtils.getLoginUser().getBizrno();
        /* 업무권한 조회 유저 */
        String mberId = map.get("mberId") + "_CMPNY!";

        searchMap.put("bizrno", bizrno);
        searchMap.put("userId", mberId);

        List<SelCmpnyAgreeListDto> dto = repDao.selCmpnyAgreeList(searchMap);

        return dto;
    }

    public Map saveEmpAgree(Map<String, Object> map) {
        //SQL 결과 확인용 변수
        int resultEm = 0;
        int resultUser = 0;
        Map resultMap = new HashMap<>();
        List<ChrgInsttDto> ci = new ArrayList<>();
        List<String> empList = new ArrayList<>();
        /* 현재 접속중인 유저(업체) */
        String userId = SecurityUtils.getLoginUser().getUserId();
        if (map.get("em") != null) {
            List<Map> reqEm = (List)map.get("em");
            for (Map reqMap: reqEm) {
                EmpManageDto tempDto = new EmpManageDto();
                tempDto.MapToDto(reqMap);
                tempDto.setDelAt("N");
                tempDto.setConfmSe("CONFM00002");

                /*
                //초기화용 변수 작성
                Map ciInitMap = new HashMap();
                ciInitMap.put("userId", userId);
                ciInitMap.put("mberId", tempDto.getChrgId());
                ciInitList.add(ciInitMap);
                //회원 권한 변경용 변수 작성
                Map userSeMap = new HashMap();
                userSeMap.put("useSe", "USESE00003");
                userSeMap.put("updusrId", userId);
                userSeMap.put("usid", tempDto.getChrgId());
                userSeList.add(userSeMap);
                */

                // 담당기관 초기화
                updInitChrgInstt(tempDto);
                // 직원관리 테이블 값 변경
                resultEm = resultEm + repDao.insEmpManage(tempDto);
                // 유저
                resultUser = resultUser + updUserSe(tempDto);
                //반환용 유저 목록
                empList.add(tempDto.getChrgId());
            }
        } else {
            resultMap.put("result", "F");
            resultMap.put("msg", "직원관리 데이터가 없습니다.");
            return resultMap;
        }

        if (map.get("ci") != null) {
            List<Map> reqCi = (List)map.get("ci");
            for (Map reqMap: reqCi) {
                ChrgInsttDto tempDto = new ChrgInsttDto();
                tempDto.MapToDto(reqMap);
                tempDto.setRegisterId(userId);
                tempDto.setUpdusrId(userId);
                ci.add(tempDto);
            }
        }
        if(ci.size() > 0){
            // 담당기관 추가
            repDao.insChrgInsttList(ci);
        }
        if(resultEm == 0){
            resultMap.put("msg", "직원관리 데이터 생성에 실패했습니다.");
        } else if(resultUser == 0){
            resultMap.put("msg", "유저 권한 변경에 실패했습니다.");
        }

        resultMap.put("result", "S");
        resultMap.put("empList", empList);
        return resultMap;
    }

    public Map deleteEmpAgree(DeleteEmpAgreeDto dto){

        Map resultMap = new HashMap<>();
        int result = 0;
        //유저변경,초기화
        dto.init();
        updInitChrgInstt(dto);
        // 직원관리 테이블 값 변경
        result = repDao.insEmpManage(dto);
        if(result == 0){
            resultMap.put("result", "F");
            resultMap.put("msg", "직원관리 데이터 생성에 실패했습니다.");
            return resultMap;
        }
        result = updUserSe(dto);
        if(result == 0){
            resultMap.put("result", "F");
            resultMap.put("msg", "유저 권한 변경에 실패했습니다.");
            return resultMap;
        }
        resultMap.put("result", "S");

        return resultMap;
    }

    public int updInitChrgInstt(EmpManageDto dto){
        return repDao.updInitChrgInstt(dto);
    }
    public int updInitChrgInstt(DeleteEmpAgreeDto dto){
        return repDao.updInitChrgInstt(dto);
    }
    public int updUserSe(Object o){
        return userDao.updUserSe(o);
    }

    public String chrgJobChange(String chrgjob) {
        String result = "";
        if (!StringUtils.hasLength(chrgjob)) {
            return result;
        }
        if (chrgjob.contains("MNTMG00001")) {
            result = result + "장애";
        }
        if (chrgjob.contains("MNTMG00002")) {
            result = result + ",설치";
        }
        if (chrgjob.contains("MNTMG00003")) {
            result = result + ",점검";
        }
        if (chrgjob.contains("MNTMG00004")) {
            result = result + ",기타";
        }
        if (result.charAt(0) == ',') {
            result = result.substring(1);
        }
        return result;
    }
}
