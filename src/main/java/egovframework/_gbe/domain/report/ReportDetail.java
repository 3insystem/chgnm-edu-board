package egovframework._gbe.domain.report;

import egovframework._gbe.common.entity.BaseEntity;
import egovframework._gbe.common.util.BooleanToYNConverter;
import egovframework._gbe.domain.user.User;
import egovframework._gbe.web.dto.mber.rep.report.ReportDetailListDto;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@Table(name = "TB_REPRT_DETAIL")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ReportDetail extends BaseEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "REPRT_DETAIL_NO")
    private Long reportDetailNo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "REPRT_NO")
    private Report report;
    
    /**
     * 신청자
     */
    @Column(name = "APPLCNT")
    private String applcnt;

    /**
     * 신청일시
     */
    @Column(name = "RQSTDT")
    private LocalDate rqstdt;

    /**
     * 완료자
     */
    @Column(name = "CMPTER")
    private String cmpter;
    
    /**
     * 완료일자
     */
    @Column(name = "COMPT_DE")
    private LocalDate comptDe;

    /**
     *  제목
     */
    @Column(name = "SJ")
    private String subject;

    /**
     * 처리내용
     */
    @Column(name = "PROCESS_CN")
    private String processCn;
    
    
    /**
     * 삭제자
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DLTR_ID")
    private User dltrId;

    /**
     * 삭제일시
     */
    @Column(name = "DEL_DE")
    private LocalDateTime delDe;
    
    /**
     * 삭제여부
     */
    @Column(name = "DEL_AT")
    @Convert(converter = BooleanToYNConverter.class)
    private boolean delYn;

    /**
     * 정렬순서
     */
    @Column(name = "SORT")
    private int sort;

    public void setReportDetail(Report report, ReportDetailListDto reportDetailListDto, int i){
        this.report = report;
        this.applcnt = reportDetailListDto.getRegisterNm();
        this.rqstdt = reportDetailListDto.getRegistDt();
        this.cmpter = reportDetailListDto.getChargerId();
        this.comptDe = reportDetailListDto.getComptDe();
        this.subject = reportDetailListDto.getSj();
        this.processCn = reportDetailListDto.getProcessCn();
        this.sort = i;
        this.delYn = false;
    }
}
