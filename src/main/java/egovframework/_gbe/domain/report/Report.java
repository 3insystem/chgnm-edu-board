package egovframework._gbe.domain.report;

import egovframework._gbe.common.entity.BaseEntity;
import egovframework._gbe.common.util.BooleanToYNConverter;
import egovframework._gbe.domain.user.User;
import egovframework._gbe.domain.user.cmpny.Cmpny;
import egovframework._gbe.domain.user.organization.Organizaion;
import egovframework._gbe.web.dto.mber.rep.report.ReportDetailDto;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Table(name = "TB_REPRT")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Report extends BaseEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "REPRT_NO")
    private Long reprtNo;

    /**
     * 리포트 상세
     */
    @OneToMany(mappedBy = "report", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    private List<ReportDetail> reportDetails = new ArrayList<>();
   
    /**
     * 기관(학교)코드
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ORG_CD")
    private Organizaion orgCd;


    /**
     * 연도
     */
    @Column(name = "YEAR", length = 4)
    private int year;

    /**
     * 월
     */
    @Column(name = "MT", length = 2)
    private int month;
    
    /**
     * 시작일자
     */
    @Column(name = "SJ", length = 200)
    private String sj;

    /*
    * 제목
    * */
    @Column(name = "BGNDE")
    private LocalDate bgnde;

    /**
     * 종료일자
     */
    @Column(name = "ENDDE")
    private LocalDate endde;

    /**
     * 사업자 번호
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "BIZRNO")
    private Cmpny bizrno;
    
    /**
     * 담당자 아이디
     */
    @Column(name = "CHRG_ID")
    private String chrdId;

    /**
     * 메모
     */
    @Column(name = "MEMO")
    private String memo;

    /**
     * 삭제여부
     */
    @Column(name = "DEL_AT")
    @Convert(converter = BooleanToYNConverter.class)
    private boolean delAt;

    /**
     * 삭제일
     */
    @Column(name = "DEL_DE")
    private LocalDateTime delDe;

    /**
     * 삭제자 아이디
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DLTR_ID")
    private User dltrId;



    public static Report reportSave(String userId, Organizaion organizaion, Cmpny cmpny, ReportDetailDto reportDetailDto) {
        Report report = new Report();
        report.orgCd = organizaion;
        report.bizrno = cmpny;
        report.year = Integer.parseInt(reportDetailDto.getYear());
        report.month = Integer.parseInt(reportDetailDto.getMonth());
        report.sj = reportDetailDto.getSj();
        report.bgnde = reportDetailDto.getStartDate();
        report.endde = reportDetailDto.getEndDate();
        report.chrdId = userId;
        report.memo = reportDetailDto.getMemo();
        report.delAt = false;

        if (reportDetailDto.getDetailList().size() > 0) {

            final int[] i = {1};

            reportDetailDto.getDetailList().forEach(us -> {
                ReportDetail reportDetail = new ReportDetail();
                reportDetail.setReportDetail(report, us, i[0]);
                report.reportDetails.add(reportDetail);
                i[0]++;
            });

        }
        return report;
    }

    public void reportUpdate(String userId, Organizaion organizaion, Cmpny cmpny, ReportDetailDto reportDetailDto) {

        this.orgCd = organizaion;
        this.bizrno = cmpny;
        this.year = Integer.parseInt(reportDetailDto.getYear());
        this.month = Integer.parseInt(reportDetailDto.getMonth());
        this.sj = reportDetailDto.getSj();
        this.bgnde = reportDetailDto.getStartDate();
        this.endde = reportDetailDto.getEndDate();
        this.chrdId = userId;
        this.memo = reportDetailDto.getMemo();
        this.delAt = false;

        this.reportDetails.clear();

        if (reportDetailDto.getDetailList().size() > 0) {

            final int[] i = {1};

            reportDetailDto.getDetailList().forEach(us -> {
                ReportDetail reportDetail = new ReportDetail();
                reportDetail.setReportDetail(this, us, i[0]);
                this.reportDetails.add(reportDetail);
                i[0]++;
            });

        }
    }
}
