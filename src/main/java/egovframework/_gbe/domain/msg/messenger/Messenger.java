package egovframework._gbe.domain.msg.messenger;

import egovframework._gbe.domain.user.User;
import egovframework._gbe.web.dto.msg.MessengerDto;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * 메신저(알림)
 *
 * */
@Entity
@Table(name = "MSG_NOTI")
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Messenger {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "MSG_ID")
    private Long id;

    private String sysCode;

    private String sysName;

    private String sndName;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RCV_ID")
    private User user;

    private String subject;

    private String content;

    private String url;

    @Column(name = "SEND_Y_N", length = 1)
    private String sended;

    private LocalDateTime dateTime;

    public static Messenger of(MessengerDto dto, User sndr, User rcvr) {
        if (StringUtils.isEmpty(dto.getSubject()))
            throw new RuntimeException("알림 제목이 비어있습니다.");
        if (StringUtils.isEmpty(dto.getContent()))
            throw new RuntimeException("알림 내용이 비어있습니다.");

        Messenger m = new Messenger();
        m.sysCode = "ITE";
        m.sysName = "정보화장비관리";
        m.sended = "N";
        m.dateTime = LocalDateTime.now();

        m.sndName = sndr.getUserNm();
        m.user = rcvr;
        m.subject = dto.getSubject();
        m.content = dto.getContent();
        m.url = dto.getUrl();

        return m;
    }
}
