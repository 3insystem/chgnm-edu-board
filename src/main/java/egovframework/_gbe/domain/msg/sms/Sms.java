package egovframework._gbe.domain.msg.sms;

import egovframework._gbe.domain.user.User;
import egovframework._gbe.web.dto.msg.SmsDto;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * SMS
 *
 * */
@Table(name = "TBL_SUBMIT_QUEUE")
@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Sms {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CMP_MSG_ID")
    private Long id;

    private String cmpMsgGroupId;

    private String usrId;

    private String smsGb;

    private String usedCd;

    /* 예약 전송 여부 I:즉시 발송, R:예약 발송 */
    private String reservedFg;

    /* 예약 전송 : 전송을 원하는 시간(reserved_fg=”R”), 즉시 전송 : 현재시간(reserved_fg=”I”)
       example : "20210330174124"
    */
    private String reservedDttm;

    private String savedFg;

    /* 수신자 번호 */
    private String rcvPhnId;

    /* 발신자 번호 */
    private String sndPhnId;

    private String natCd;

    private String assignCd;

    /* 문자 내용 */
    private String sndMsg;

    private String callbackUrl;

    private Integer contentCnt;

    private String contentMimeType;

    private String contentPath;

    /* 오늘날짜(YYYYMMDDHH24MISS) */
    private String cmpSndDttm;

    private String cmpRcvDttm;

    private String regSndDttm;

    private String regRcvDttm;

    private String machineId;

    private String smsStatus;

    private String rsltVal;

    /* 장문 문자 제목 16bytes 이내 권장 */
    private String msgTitle;

    private String telcoId;

    private String etcChar1;

    private String etcChar2;

    private String etcChar3;

    private String etcChar4;

    private Integer etcInt5;

    private Integer etcInt6;

    public static Sms ofShort(SmsDto dto) {
        Sms s = new Sms();
        s.usrId = "0000";
        s.usedCd = "00";
        s.reservedFg = dto.getReservedFg();
        s.sndPhnId = dto.getSndPhnId();
        s.rcvPhnId = dto.getRcvPhnId();
        s.sndMsg = dto.getSndMsg();
        if (!StringUtils.isBlank(dto.getReservedDttm())) {
            s.reservedDttm = dto.getReservedDttm();
        } else {
            s.reservedDttm = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
        }
        s.cmpSndDttm = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));

        return s;
    }

    public static Sms ofLong(SmsDto dto) {
        Sms s = new Sms();
        s.usrId = "0000";
        s.usedCd = "10";
        s.reservedFg = dto.getReservedFg();
        s.sndPhnId = dto.getSndPhnId();
        s.rcvPhnId = dto.getRcvPhnId();
        s.msgTitle = dto.getMsgTitle();
        s.sndMsg = dto.getSndMsg();
        if (!StringUtils.isBlank(dto.getReservedDttm())) {
            s.reservedDttm = dto.getReservedDttm();
        } else {
            s.reservedDttm = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
        }
        s.cmpSndDttm = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));

        return s;
    }
}
