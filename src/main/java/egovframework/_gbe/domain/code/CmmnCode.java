package egovframework._gbe.domain.code;

import egovframework._gbe.common.entity.BaseEntity;
import egovframework._gbe.web.dto.adm.code.CodeDto;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Map;

@Table(name = "TB_CMMN_CODE")
@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CmmnCode extends BaseEntity {

    @Id
    @Column(name = "C_CODE")
    private String code;

    @Column(name = "CL_CODE")
    private String clCode;

    @Column(name = "CL_NM")
    private String clNm;

    @Column(name = "CODE_NM")
    private String codeNm;

    @Column(name = "CODE_DC")
    private String codeDc;

    @Column(name = "PARNTS_CODE")
    private String parntsCode;

    @Column(name = "CODE_DP")
    private String codeDp;

    @Column(name = "CODE_SN")
    private String codeSn;

    @Column(name = "USE_BEGIN_DE")
    private String useBeginDe;

    @Column(name = "USE_END_DE")
    private String useEndDe;

    @Column(name = "NCNM")
    private String ncnm;

    @Column(name = "DEL_AT")
    private String delAt;

    public CmmnCode(Map map) {
        this.clCode = StringUtils.hasLength((String) map.get("clCode")) ? (String) map.get("clCode") : "";
        this.code = StringUtils.hasLength((String) map.get("cCode")) ? (String) map.get("cCode") : "";
        this.clNm = StringUtils.hasLength((String) map.get("clNm")) ? (String) map.get("clNm") : "";
        this.codeNm = StringUtils.hasLength((String) map.get("codeNm")) ? (String) map.get("codeNm") : "";
        this.codeDc = StringUtils.hasLength((String) map.get("codeDc")) ? (String) map.get("codeDc") : "";
        this.parntsCode = StringUtils.hasLength((String) map.get("parntsCode")) ? (String) map.get("parntsCode") : "";
        //this.codeDp = StringUtils.hasLength(((BigDecimal) map.get("codeDp")).toString()) ? ((BigDecimal) map.get("codeDp")).toString() : "";
        this.codeDp = (map.get("codeDp") != null) ? ((Integer) map.get("codeDp")).toString() : "";

        //this.codeSn = StringUtils.hasLength(((BigDecimal) map.get("codeSn")).toString()) ? ((BigDecimal) map.get("codeSn")).toString() : "";
        this.codeSn = (map.get("codeSn") != null) ? ((Integer) map.get("codeSn")).toString() : "";
        this.useBeginDe = StringUtils.hasLength((String) map.get("useBeginDe")) ? (String) map.get("useBeginDe") : "";
        this.useEndDe = StringUtils.hasLength((String) map.get("useEndDe")) ? (String) map.get("useEndDe") : "";
        this.ncnm = StringUtils.hasLength((String) map.get("ncnm")) ? (String) map.get("ncnm") : "";
        this.delAt = StringUtils.hasLength((String) map.get("delAt")) ? (String) map.get("delAt") : "";
    }

    public static CmmnCode of(CodeDto dto) {
        CmmnCode cmmnCode = new CmmnCode();
        cmmnCode.clCode = dto.getClCode();
        cmmnCode.clNm = dto.getClNm();
        cmmnCode.code = dto.getCode();
        cmmnCode.codeNm = dto.getCodeNm();
        cmmnCode.codeDc = dto.getCodeDc();
        cmmnCode.parntsCode = dto.getParntsCode();
        cmmnCode.codeDp = dto.getCodeDp();
        cmmnCode.codeSn = dto.getCodeSn();
        cmmnCode.useBeginDe = "19001111";
        cmmnCode.useEndDe = "9999999";
        cmmnCode.ncnm = dto.getNcnm();
        cmmnCode.delAt = dto.getDelAt();

        return cmmnCode;
    }

    public void update(CodeDto dto) {
        this.codeNm = dto.getCodeNm();
        this.codeDc = dto.getCodeDc();
        this.parntsCode = dto.getParntsCode();
        this.codeDp = dto.getCodeDp();
        this.codeSn = dto.getCodeSn();
        this.ncnm = dto.getNcnm();
        this.delAt = dto.getDelAt();
    }

    public void delete() {
        this.delAt = "Y";
    }
}
