package egovframework._gbe.domain.cmpds;

import egovframework._gbe.common.entity.BaseEntity;
import egovframework._gbe.common.util.BooleanToYNConverter;
import egovframework._gbe.common.util.file.FileUtils;
import egovframework._gbe.common.util.file.UploadFile;
import egovframework._gbe.domain.cmpds.file.CmpdsFile;
import egovframework._gbe.domain.user.User;
import egovframework._gbe.domain.user.cmpny.Cmpny;
import egovframework._gbe.domain.user.organization.Organizaion;
import egovframework._gbe.web.dto.equipment.EquipmentSave;
import egovframework._gbe.web.dto.mber.rep.cmpds.CmpdsUpdateDto;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Getter
@Table(name = "TB_CMPDS")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Cmpds extends BaseEntity {

    /**
     * 접수번호
     */
    @Id
    @Column(name = "RCEPT_NO")
    private String rceptNo;
    /**
     * 기관(학교)코드
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ORG_CODE")
    private Organizaion orgCd;

    /**
     * 업체사업자 번호
     */
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "BIZRNO", foreignKey = @ForeignKey(ConstraintMode.NO_CONSTRAINT))
    private Cmpny bizrno;

    /**
     * 제목
     */
    @Column(name = "SJ")
    private String subject;

    /**
     * 내용
     */
    @Column(name = "CN")
    private String content;

    /**
     * 상태 구분
     */
    @Column(name = "STTUS_SE")
    private String statusType;

    /**
     * 소모품 구분
     */
    @Column(name = "CMPDS_SE")
    private String expendablesType;

    /**
     * 담당자 아이디
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CHARGER_ID")
    private User chargerUser;

    /**
     * 접수일
     */
    @Column(name = "RCEPT_DE")
    private LocalDateTime rceptDe;

    /**
     * 접수자 아이디
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RCEPT_ID")
    private User rceptUser;

    /**
     * 완료일
     */
    @Column(name = "COMPT_DE")
    private LocalDateTime comptDe;

    /**
     * 완료자 아이디
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COMPT_ID")
    private User comptUser;

    /**
     * 첨부파일
     */
    @OneToMany(mappedBy = "cmpds", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    private List<CmpdsFile> cmpdsFiles = new ArrayList<>();

    /**
     * 처리내용
     */
    @Column(name = "PROCESS_CN")
    private String processCn;

    /**
     * 참고파일
     */
    @OneToMany(mappedBy = "cmpds", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    private List<CmpdsFile> referAtchmnfl = new ArrayList<>();


    /**
     * 삭제여부
     */
    @Column(name = "DEL_AT")
    @Convert(converter = BooleanToYNConverter.class)
    private boolean delAt;

    /**
     * 연락처
     */
    @Column(name = "CTTPCL")
    private String cttpcl;


    public void deleted() {
        this.delAt = true;
    }

    public void updateCmpds(EquipmentSave equipmentSave, Cmpny cmpny) {
        this.subject = equipmentSave.getSubject();
        this.content = equipmentSave.getContent();
        this.expendablesType = equipmentSave.getExpendablesType();
        this.cttpcl = equipmentSave.getCttpcl();
        this.bizrno = cmpny;
    }

    public void UpdateRepCmpds(CmpdsUpdateDto saveData, User chargerUser, User rceptUser, User comptUser) { //값은 넘어 오는데 업데이트가 안됨 확인 필요
        this.statusType = saveData.getStatusType();
        this.chargerUser = chargerUser;
        this.processCn = saveData.getProcessCn();

        if (saveData.getStatusType().equals("STTUS00002")) { // 접수 시 접수일자 아이디 등록
            this.rceptUser = rceptUser;
            this.rceptDe = this.getUpdtDt();
        }

        if (saveData.getStatusType().equals("STTUS00005")) { // 완료 시 완료일자 아이디 등록
            this.comptUser = comptUser;
            this.comptDe = this.getUpdtDt();
        }
    }

    public void changeStatus(String statusType) {
        this.statusType = statusType;
    }

    public static Cmpds of(String rceptNo, EquipmentSave equipmentSave, Organizaion organizaion, Cmpny cmpny) {
        Cmpds cmpds = new Cmpds();
        cmpds.rceptNo = rceptNo;
        cmpds.orgCd = organizaion;
        cmpds.bizrno = cmpny;
        cmpds.expendablesType = equipmentSave.getExpendablesType();
        cmpds.subject = equipmentSave.getSubject();
        cmpds.content = equipmentSave.getContent();
        cmpds.statusType = equipmentSave.getStatusType();
        cmpds.cttpcl = equipmentSave.getCttpcl();
        cmpds.delAt = false;
        return cmpds;
    }

    public void updateFile(List<MultipartFile> files, List<Long> savedFileIds, boolean isInstt) throws IOException {

        if ((savedFileIds == null && this.cmpdsFiles.size() == 0 && files.size() > 0) ||
                (savedFileIds != null && this.cmpdsFiles.size() == savedFileIds.size() && files.size() > 0)) {

            addFile(files, isInstt);
            return;
        }

        if ((savedFileIds != null && this.cmpdsFiles.size() > savedFileIds.size()) ||
                (savedFileIds == null && files.size() > 0 && this.cmpdsFiles.size() > 0) || (savedFileIds == null && files.size() == 0)) {
            deleteFile(savedFileIds, isInstt);

            if (files.size() > 0) {
                addFile(files, isInstt);
            }
        }

    }

    public void addFile(List<MultipartFile> files, boolean isInstt) throws IOException {
        if (files.size() > 0) {
            for (MultipartFile file : files) {
                UploadFile uploadFile = FileUtils.uploadFile(file);
                String type = isInstt ? "O" : "C";
                CmpdsFile cmpdsFile = CmpdsFile.of(uploadFile, type);
                cmpdsFile.setCmpds(this);
                this.cmpdsFiles.add(cmpdsFile);
            }
        }
    }


    private void deleteFile(List<Long> savedFileIds, boolean isInstt) {
        List<CmpdsFile> condistionsList = fileTypeClassification(isInstt);

        if (savedFileIds != null) {
            for (Long savedFileId : savedFileIds) {
                condistionsList = condistionsList.stream()
                        .filter(file -> !file.getFileNo().equals(savedFileId))
                        .collect(Collectors.toList());
            }
        }

        int fileIndex = this.cmpdsFiles.size() - 1;
        int removeIndex = condistionsList.size() - 1;

        while (this.cmpdsFiles.iterator().hasNext() && fileIndex > -1 && removeIndex > -1) {
            if (condistionsList.get(removeIndex).getFileNo().equals(this.cmpdsFiles.get(fileIndex).getFileNo())) {
                FileUtils.deleteFile(this.cmpdsFiles.get(fileIndex).getStreFileNm());
                this.cmpdsFiles.get(fileIndex).delete();
                this.cmpdsFiles.remove(fileIndex);
                if (fileIndex > 0) fileIndex--;
                if (condistionsList.size() > 0) removeIndex--;
                continue;
            }

            if (condistionsList.get(removeIndex).getFileNo() != this.cmpdsFiles.get(fileIndex).getFileNo()) {
                fileIndex--;
            }
        }
    }

    private List<CmpdsFile> fileTypeClassification(boolean isInstt) {
        List<CmpdsFile> condistionsList = this.cmpdsFiles;
        if (isInstt) {
            return condistionsList.stream().filter(file -> file.getAtchAt().equals("O")).collect(Collectors.toList());
        } else {
            return condistionsList.stream().filter(file -> file.getAtchAt().equals("C")).collect(Collectors.toList());
        }
    }
}
