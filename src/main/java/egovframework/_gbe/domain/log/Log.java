package egovframework._gbe.domain.log;

import egovframework._gbe.common.entity.BaseEntity;
import egovframework._gbe.domain.user.User;
import lombok.*;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Table(name = "TB_SYS_LOG")
@Getter
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Log extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "LOG_NO")
    private Long logNo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USID")
    private User user;

    @Column(name = "URL")
    private String url;

    @Column(name = "SYS_NM")
    private String sysNm;

    @Column(name = "SYS_DETAIL")
    private String sysDetail;

    @Column(name = "MTH")
    private String mth;

    @Column(name = "IP")
    private String ip;
}
