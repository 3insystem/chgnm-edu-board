package egovframework._gbe.domain.user.organization;

import egovframework._gbe.common.entity.BaseEntity;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Table(name = "TB_ORGNZT")
@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Organizaion extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ORG_CD")
    private String orgCd;

    @Column(name = "ORG_NM")
    private String orgNm;

    @Column(name = "UPPER_ORG_CD")
    private String upperOrgCd;

    @Column(name = "UPPER_ORG_CD2")
    private String upperOrgCd2;

    @Column(name = "ORG_USE_YN")
    private String orgUseYn;

    @Column(name = "DEL_YN")
    private String delYn;

    @Column(name = "ORG_CDWPTR_CD")
    private String orgCdwptrCd;

    @Column(name = "ORG_SE_CD")
    private String orgSeCd;

    //게시판 조직코드
    public Organizaion(String orgCd) {
        this.orgCd = orgCd;
    }
}
