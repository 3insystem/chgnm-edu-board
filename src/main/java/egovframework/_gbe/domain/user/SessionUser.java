package egovframework._gbe.domain.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Arrays;
import java.util.Collection;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SessionUser implements UserDetails {

    /**
     * 로그인 아이디
     * */
    private String userId;

    /**
     * 로그인 비밀번호
     * */
    private String password;

    private String userNm;

    private String bizrno;

    private String cmpnyNm;

    private String role;

    private String auth;

    private String useSe;

    private String useSeNm;

    private String orgCd;

    private String orgNm;

    private String upperOrgCd;

    private String orgType;

    private String changeAt;

    private boolean enabled;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Arrays.<GrantedAuthority>asList(new SimpleGrantedAuthority(this.role));
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.getUserId();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return this.enabled;
    }

    public void setUserNm(String userNm) {
        this.userNm = userNm;
    }

    public void setOrgCd(String orgCd) {
        this.orgCd = orgCd;
    }

    public void setOrgType(String type) {
        this.orgType = type;
    }

    public void setUpperOrgCd(String upperOrgCd) {
        this.upperOrgCd = upperOrgCd;
    }

    public void setBizrno(String bizrno) {
        this.bizrno = bizrno;
    }

    public void setCmpnyNm(String cmpnyNm) { this.cmpnyNm = cmpnyNm; }

    public void setUseSe(String useSe) {
        this.useSe = useSe;
    }

    public void setChangeAt() {
        this.changeAt = null;
    }

    public void setRole(String role){this.role = role;}

    public void setAuth(String auth){this.auth = auth;}
}
