package egovframework._gbe.domain.user;

import egovframework._gbe.domain.code.CmmnCode;
import egovframework._gbe.domain.user.auth.Auth;
import egovframework._gbe.domain.user.cmpny.Cmpny;
import egovframework._gbe.domain.user.dsptc.Dsptc;
import egovframework._gbe.domain.user.organization.Organizaion;
import egovframework._gbe.web.dto.user.InsttUpdate;
import egovframework._gbe.web.dto.user.UserSave;
import egovframework._gbe.web.dto.user.UserUpdate;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Table(name = "TB_USER")
@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class User extends Auth {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USE_SE")
    private CmmnCode useSe;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "USER_NM")
    private String userNm;

    @Column(name = "AHRZT_ESNTL_NM")
    private String ahrztEsntlNm;

    @Column(name = "AHRZT_NO_VAL")
    private String ahrztNoVal;

    @Column(name = "LOGIN_FAIL_CNT")
    private Integer loginFailCnt;

    @Column(name = "STPLAT_AGRE_AT", length = 1)
    private String stplatAgreAt;

    @Column(name = "INDVDLINFO_AGRE_AT", length = 1)
    private String indvdlinfoAgreAt;

    @Column(name = "ESNTL_IDNTFC_AGRE_AT", length = 1)
    private String esntlIdntfcAgreAt;

    @Column(name = "MBTLNUM")
    private String mbtlnum;

    @Column(name = "EMAIL")
    private String email;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "BIZRNO")
    private Cmpny cmpny;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RSPOFC_SE")
    private CmmnCode rspofcSe;

    @Column(name = "SANCTN_OFCPS_NM")
    private String sanctnOfcpsNm;

    @Column(name = "USE_YN", length = 1)
    private String useYn;

    @Column(name = "DEL_YN", length = 1)
    private String delYn;

    @Column(name = "TELNO")
    private String telno;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ORG_CD")
    private Organizaion orgCode;

    @Column(name = "ORG_NM")
    private String orgNm;

    @Column(name = "OFCPS_NM")
    private String ofcpsNm;

    @Column(name = "OFCPS_CD")
    private String ofcpsCd;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CMPS_ORG_CD")
    private Organizaion cmpsOrgCd;

    @Column(name = "CMPS_ORG_NM")
    private String cmpsOrgNm;

    @Column(name = "CLSF_CD")
    private String clsfCd;

    @Column(name = "CLSF_NM")
    private String clsfNm;

    @Column(name = "COMPANY_ID")
    private Long companyId;

    @Column(name = "CHANGE_AT")
    private String changeAt;

    @Column(name = "REGISTER_ID")
    private String rgstId;

    @Column(name = "REGIST_DT")
    private LocalDateTime rgstDt;

    @Column(name = "UPDUSR_ID")
    private String updtId;

    @Column(name = "UPDT_DT")
    private LocalDateTime updusrDt;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Dsptc> dsptcList = new ArrayList<>();

    private User(String userId, Auth auth) {
        super(userId, auth.getAuthSe());
    }

    public static User of(String userId, Auth auth, UserSave saveData, CmmnCode useSe) {
        User user = new User(userId, auth);
        user.password = saveData.getPassword();
        user.userNm = saveData.getUserNm();
        user.mbtlnum = saveData.getMbtlnum();
        user.email = saveData.getEmail();
        user.stplatAgreAt = "Y";
        user.indvdlinfoAgreAt = "Y";
        user.esntlIdntfcAgreAt = "Y";
        user.useYn = "Y";
        user.delYn = "N";
        user.loginFailCnt = 0;
        user.rgstId = saveData.getUserId();
        user.rgstDt = LocalDateTime.now();

        if (useSe != null) {
            user.useSe = useSe;
        }

        return user;
    }

    public void setCmpny(Cmpny cmpny) {
        this.cmpny = cmpny;
    }

    public void updateCompanyId() {
        this.companyId = null;
    }

    public void setRspoFcSe(CmmnCode cmmnCode) {
        this.rspofcSe = cmmnCode;
    }

    public void updatePassword(String password) {
        this.password = password;
    }

    public void oldUserUpdate(UserUpdate updateData, CmmnCode useSe) {
        if (updateData.getPassword() != null && !updateData.getPassword().equals("")) {
            this.password = updateData.getPassword();
        }
        this.userNm = updateData.getUserNm();
        this.mbtlnum = updateData.getMbtlnum();
        this.email = updateData.getEmail();
        this.useSe = useSe;
        this.indvdlinfoAgreAt = "Y";
        this.esntlIdntfcAgreAt = "Y";
        this.stplatAgreAt = "Y";
        this.changeAt = null;
    }

    public void oldInsttUpdate(InsttUpdate updateData) {
        if (updateData.getPassword() != null && !updateData.getPassword().equals("")) {
            this.password = updateData.getPassword();
        }

        this.email = updateData.getEmail();
        this.changeAt = null;
    }

    public void setDsptc(Dsptc dsptc) {
        this.dsptcList.add(dsptc);
    }

    public Dsptc getCurrentDsptc() {
        if (this.dsptcList.size() > 0) {
            Optional<Dsptc> result = this.dsptcList
                    .stream()
                    .filter(dsptc -> dsptc.getDelAt().equals("N"))
                    .findAny();

            return result.orElse(null);
        }

        return null;
    }

    public String getFormattedUsid() {
        String s = this.getUsid().replaceAll("_CMPNY!", "");
        return s;
    }
}
