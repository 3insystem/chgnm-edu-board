package egovframework._gbe.domain.user.dsptc;

import egovframework._gbe.common.entity.BaseEntity;
import egovframework._gbe.domain.code.CmmnCode;
import egovframework._gbe.domain.user.User;
import egovframework._gbe.domain.user.organization.Organizaion;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Table(name = "TB_DSPTC")
@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Dsptc extends BaseEntity {

    @Id
    @Column(name = "DSPTC_NO")
    private String dsptcNo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DSPTC_ID")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ORG_CD")
    private Organizaion orgCd;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "BASS_ORG_CD")
    private Organizaion bassOrgCd;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DSPTC_ORG_CD")
    private Organizaion dsptcOrgCd;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DSPTC_SE")
    private CmmnCode dsptcSe;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CONFM_ID")
    private User confirmer;

    @Column(name = "CONFM_DE")
    private LocalDate confmDe;

    @Column(name = "AGRE_AT")
    private String agreAt;

    @Column(name = "RM")
    private String rm;

    @Column(name = "DEL_AT")
    private String delAt;

    @Column(name = "DSPTC_BGNDE")
    private String dsptcBgnde;

    @Column(name = "DSPTC_ENDDE")
    private String dsptcEndde;

    public static Dsptc of(String dsptcNo, User user, Organizaion orgCd, CmmnCode cmmnCode, String dsptcBgnde, String dsptcEndde) {
        Dsptc dsptc = new Dsptc();
        dsptc.dsptcNo = dsptcNo;
        dsptc.user = user;
        dsptc.orgCd = orgCd;

        if (user.getOrgCd() != null) {
            dsptc.bassOrgCd = user.getOrgCd();
        }

        dsptc.dsptcOrgCd = orgCd;
        dsptc.dsptcSe = cmmnCode;
        dsptc.agreAt = "Y";
        dsptc.delAt = "N";

        dsptc.dsptcBgnde = dsptcBgnde;
        dsptc.dsptcEndde = dsptcEndde;

        return dsptc;
    }

    public void updateDsptcAt(String dsptcAt) {
        if (dsptcAt.equals("Y")) {
            this.delAt = "Y";
        }
    }


}
