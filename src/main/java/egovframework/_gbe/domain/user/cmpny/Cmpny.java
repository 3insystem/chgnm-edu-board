package egovframework._gbe.domain.user.cmpny;

import egovframework._gbe.common.entity.BaseEntity;
import egovframework._gbe.common.util.CommonUtils;
import egovframework._gbe.common.util.file.FileUtils;
import egovframework._gbe.common.util.file.UploadFile;
import egovframework._gbe.domain.user.User;
import egovframework._gbe.web.dto.user.UserSave;
import egovframework._gbe.web.dto.user.UserUpdate;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.security.core.parameters.P;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.io.IOException;

@Table(name = "TB_CMPNY")
@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Cmpny extends BaseEntity {

    @Id
    @Column(name = "BIZRNO")
    private String bizrno;

    @Column(name = "CMPNY_NM")
    private String cmpnyNm;

    @Column(name = "BIZRNO_ATCHMNFL")
    private String bizrnoAtchmnfl;

    @Column(name = "ZIP")
    private String zip;

    @Column(name = "ADRES")
    private String adres;

    @Column(name = "DETAIL_ADRES")
    private String detailAdres;

    @Column(name = "CTPRVN")
    private String ctprvn;

    @Column(name = "SIGNGU")
    private String signgu;

    @Column(name = "REPRSNT_TELNO")
    private String reprsntTelno;

    @Column(name = "DEL_AT")
    private String delAt;

    @Column(name = "COMPANY_ID")
    private String companyId;

    @OneToOne(mappedBy = "cmpny", cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "BIZRNO")
    private CmpnyFile cmpnyFile;

    public static Cmpny of(UserSave saveData) {
        Cmpny cmpny = new Cmpny();
        cmpny.bizrno = saveData.getBizrnoFilter();
        cmpny.cmpnyNm = saveData.getCmpnyNm();
        cmpny.zip = saveData.getZip();
        cmpny.adres = saveData.getAdres();
        cmpny.detailAdres = saveData.getDetailAdres();
        cmpny.ctprvn = saveData.getCtprvn();
        cmpny.signgu = saveData.getSigngu();
        cmpny.reprsntTelno = saveData.getReprsntTelno();
        cmpny.delAt = "N";

        return cmpny;
    }

    public static Cmpny of(UserUpdate updateData) {
        Cmpny cmpny = new Cmpny();
        cmpny.bizrno = updateData.getBizrnoFilter();
        cmpny.cmpnyNm = updateData.getCmpnyNm();
        cmpny.zip = updateData.getZip();
        cmpny.adres = updateData.getAdres();
        cmpny.detailAdres = updateData.getDetailAdres();
        cmpny.ctprvn = updateData.getCtprvn();
        cmpny.signgu = updateData.getSigngu();
        cmpny.reprsntTelno = updateData.getReprsntTelno();
        cmpny.delAt = "N";

        return cmpny;
    }

    public void setFile(MultipartFile file) throws IOException {
        if (!file.isEmpty()) {
            UploadFile uploadFile = FileUtils.uploadFile(file);
            if (this.getCmpnyFile() == null) {
                this.addFile(uploadFile);
            } else {
                this.cmpnyFile.update(uploadFile);
            }
        } else {
            if (this.getCmpnyFile() != null) {
                this.deleteFile();
            }
        }
    }

    public void addFile(UploadFile uploadFile) {
        CmpnyFile file = CmpnyFile.of(uploadFile);
        file.setCmpny(this);
        this.cmpnyFile = file;
    }

    public void updateFile(Long savedFileNo, MultipartFile updateFile) throws IOException {
        if (this.cmpnyFile != null) {
            if (!this.cmpnyFile.fileNo.equals(savedFileNo)) {
                FileUtils.deleteFile(this.cmpnyFile.getStreFileNm());

                if (updateFile == null || updateFile.isEmpty()) {
                    this.cmpnyFile = null;
                } else {
                    try {
                        this.cmpnyFile = null;
                        UploadFile uploadFile = FileUtils.uploadFile(updateFile);
                        CmpnyFile cmpnyFile = CmpnyFile.of(uploadFile);
                        cmpnyFile.setCmpny(this);
                        this.cmpnyFile = cmpnyFile;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else {
            addFile(FileUtils.uploadFile(updateFile));
        }

    }

    private void deleteFile() {
        FileUtils.deleteFile(this.cmpnyFile.getStreFileNm());
        this.cmpnyFile.delete();
        this.cmpnyFile = null;
    }

    public void oldUserUpdate(UserUpdate updateData, MultipartFile file) throws IOException {
        this.cmpnyNm = updateData.getCmpnyNm();
        this.zip = updateData.getZip();
        this.adres = updateData.getAdres();
        this.detailAdres = updateData.getDetailAdres();
        this.ctprvn = updateData.getCtprvn();
        this.signgu = updateData.getSigngu();
        this.reprsntTelno = updateData.getReprsntTelno();

        updateFile(updateData.getSavedFileIds(), file);
    }

    public String formattedBizrno() {
        String ff = CommonUtils.stringToBizrno(this.bizrno);
        return ff;
    }
}
