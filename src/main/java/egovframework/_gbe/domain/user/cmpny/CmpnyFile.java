package egovframework._gbe.domain.user.cmpny;


import egovframework._gbe.common.entity.File;
import egovframework._gbe.common.util.file.UploadFile;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;

import static javax.persistence.GenerationType.IDENTITY;

@Table(name = "TB_CMPNY_ATCH")
@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CmpnyFile extends File {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "FILE_NO")
    protected Long fileNo;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FILE_ID")
    private Cmpny cmpny;


    public static CmpnyFile of(UploadFile uploadFile) {
        CmpnyFile file = new CmpnyFile();
        file.fileStreCours = uploadFile.getFileStreCours();
        file.streFileNm = uploadFile.getStreFileNm();
        file.orignlFileNm = uploadFile.getOriginlFileNm();
        file.fileExtsn = uploadFile.getFileExtsn();
        file.fileCn = uploadFile.getFileCn();
        file.fileSize = uploadFile.getFileSize();

        return file;
    }

    public void setCmpny(Cmpny cmpny) {
        this.cmpny = cmpny;
    }

    public void update(UploadFile uploadFile) {
        this.fileStreCours = uploadFile.getFileStreCours();
        this.streFileNm = uploadFile.getStreFileNm();
        this.orignlFileNm = uploadFile.getOriginlFileNm();
        this.fileExtsn = uploadFile.getFileExtsn();
        this.fileCn = uploadFile.getFileCn();
        this.fileSize = uploadFile.getFileSize();
    }

    public void delete() {
        this.cmpny = null;
    }

    public Map<String, String> entityToMap()  {
        Map<String, String> map = new HashMap<>();
        map.put("streFileNm", this.streFileNm);
        map.put("orignlFileNm", this.orignlFileNm);
        map.put("fileStreCours", this.fileStreCours);
        return map;
    }
}
