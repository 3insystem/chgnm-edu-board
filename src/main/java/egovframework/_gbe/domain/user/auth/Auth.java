package egovframework._gbe.domain.user.auth;

import egovframework._gbe.domain.code.CmmnCode;
import egovframework._gbe.domain.user.cmpny.Cmpny;
import egovframework._gbe.domain.user.organization.Organizaion;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Table(name = "TB_AUTHOR")
@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Inheritance(strategy = InheritanceType.JOINED)
public class Auth {

    @Id
    @Column(name = "USID")
    private String usid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ORG_CD")
    private Organizaion orgCd;

    @ManyToOne
    @JoinColumn(name = "AUTH_SE")
    private CmmnCode authSe;

    @Column(name = "DEL_AT", length = 1)
    private String delAt;

    @Column(name = "REGISTER_ID")
    private String registerId;

    @Column(name = "REGIST_DT")
    private LocalDate registDt;

    @Column(name = "UPDUSR_ID")
    private String updusrId;

    @Column(name = "UPDT_DT")
    private LocalDate updtDt;

    public Auth(String userId, CmmnCode authSe) {
        this.usid = userId;
        this.orgCd = null;
        this.authSe = authSe;
        this.delAt = "N";
        this.registerId = userId;
        this.registDt = LocalDate.now();
    }


}
