package egovframework._gbe.domain.user;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class PrivateSessionUser {

    private String userId;

    private String orgUserId;

    private String userNm;

    private String bizrno;

    private String cmpnyNm;

    private String role;

    private String auth;

    private String useSe;

    private String useSeNm;

    private String orgCd;

    private String orgNm;

    private String upperOrgCd;

    private String orgType;

    private String changeAt;

    private boolean enabled;

    public PrivateSessionUser(SessionUser sessionUser) {
        String userId = sessionUser.getUserId();
        this.orgUserId = userId;
        if (sessionUser.getUserId().contains("_CMPNY!")) {
            userId = userId.replaceAll("_CMPNY", "");
        }

        this.userId = userId;
        this.userNm = sessionUser.getUserNm();
        this.bizrno = sessionUser.getBizrno();
        this.cmpnyNm = sessionUser.getCmpnyNm();
        this.role = sessionUser.getRole();
        this.auth = sessionUser.getAuth();
        this.useSe = sessionUser.getUseSe();
        this.useSeNm = sessionUser.getUseSeNm();
        this.orgCd = sessionUser.getOrgCd();
        this.orgNm = sessionUser.getOrgNm();
        this.upperOrgCd = sessionUser.getUpperOrgCd();
        this.orgType = sessionUser.getOrgType();
        this.changeAt = sessionUser.getChangeAt();
        this.enabled = sessionUser.isEnabled();
    }
}
