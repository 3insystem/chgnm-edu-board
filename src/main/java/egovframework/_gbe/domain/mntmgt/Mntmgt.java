package egovframework._gbe.domain.mntmgt;

import egovframework._gbe.common.entity.BaseEntity;
import egovframework._gbe.common.util.BooleanToYNConverter;
import egovframework._gbe.common.util.file.FileUtils;
import egovframework._gbe.common.util.file.UploadFile;
import egovframework._gbe.domain.mntmgt.file.MntmgtFile;
import egovframework._gbe.domain.user.User;
import egovframework._gbe.domain.user.cmpny.Cmpny;
import egovframework._gbe.domain.user.organization.Organizaion;
import egovframework._gbe.web.dto.equipment.EquipmentSave;
import egovframework._gbe.web.dto.mber.rep.mntmgt.MntmgtUpdateDto;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Getter
@Table(name = "TB_MNTMGT")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Mntmgt extends BaseEntity {

    /**
     * 접수번호
     */
    @Id
    @Column(name = "RCEPT_NO")
    private String rceptNo;
    /**
     * 기관(학교)코드
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ORG_CD")
    private Organizaion orgCd;

    /**
     * 업체사업자 번호
     */
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "BIZRNO", foreignKey = @ForeignKey(ConstraintMode.NO_CONSTRAINT))
    private Cmpny bizrno;

    /**
     * 제목
     */
    @Column(name = "SJ")
    private String subject;

    /**
     * 내용
     */
    @Column(name = "CN")
    private String content;

    /**
     * 긴급여부
     */
    @Column(name = "EMRGNCY_AT")
    private char emrgncyAt;

    /**
     * 상태 구분
     */
    @Column(name = "STTUS_SE")
    private String statusType;

    /**
     * 유지보수 구분
     */
    @Column(name = "MNTMGT_SE")
    private String maintenanceType;

    /**
     * 분류 상세
     */
    @Column(name = "CL_SE")
    private String clSe;

    /**
     * 담당자 아이디
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CHARGER_ID")
    private User chargerUser;

    /**
     * 접수일
     */
    @Column(name = "RCEPT_DE")
    private LocalDateTime rceptDe;

    /**
     * 접수자 아이디
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RCEPT_ID")
    private User rceptUser;

    /**
     * 업체 완료일
     */
    @Column(name = "COMPT_DE")
    private LocalDateTime comptDe;

    /**
     * 업체 완료자 아이디
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COMPT_ID")
    private User comptUser;

    /**
     * 최종 완료일
     */
    @Column(name = "LAST_COMPT_DE")
    private LocalDateTime lastComptDe;

    /**
     * 최종 완료자 아이디
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "LAST_COMPT_ID")
    private User lastComptUser;

    /**
     * 첨부파일
     */
    @OneToMany(mappedBy = "mntmgt", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    private List<MntmgtFile> mntmgtFile = new ArrayList<>();

    /**
     * 처리내용
     */
    @Column(name = "PROCESS_CN")
    private String processCn;

    /**
     * 참고파일
     */
    @OneToMany(mappedBy = "mntmgt", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    private List<MntmgtFile> referAtchmnfl = new ArrayList<>();


    /**
     * 삭제여부
     */
    @Column(name = "DEL_AT")
    @Convert(converter = BooleanToYNConverter.class)
    private boolean delAt;

    /**
     * 연락처
     */
    @Column(name = "CTTPCL")
    private String cttpcl;

    public void deleted() {
        this.delAt = true;
    }

    public void updateInsttMntmgt(EquipmentSave equipmentSave, Cmpny cmpny) {
        this.subject = equipmentSave.getSubject();
        this.content = equipmentSave.getContent();
        this.maintenanceType = equipmentSave.getMaintenanceType();
        this.cttpcl = equipmentSave.getCttpcl();
        this.clSe = equipmentSave.getClSe();
        this.bizrno = cmpny;
    }

    public void UpdateRepMntmgt(MntmgtUpdateDto saveData, User chargerUser, User rceptUser, User comptUser) { //값은 넘어 오는데 업데이트가 안됨 확인 필요
        LocalDateTime now = LocalDateTime.now();
        this.statusType = saveData.getStatusType();
        this.chargerUser = chargerUser;
        this.processCn = saveData.getProcessCn();

        if (saveData.getStatusType().equals("STTUS00002")) { // 접수 시 접수일자 아이디 등록
            this.rceptUser = rceptUser;
            this.rceptDe = now;
        }

        if (saveData.getStatusType().equals("STTUS00005")) { // 완료 시 완료일자 아이디 등록
            this.comptUser = comptUser;
            this.comptDe = now;
        }
    }

    public void changeStatus(String statusType) {
        this.statusType = statusType;
    }

    public void changeCancelStatus() {
        this.statusType = "STTUS00003";
    }

    public void changeCompleteStatus(User user) {
        this.lastComptUser = user;
        this.lastComptDe = LocalDateTime.now();
        this.statusType = "STTUS00006";
    }

    public static Mntmgt of(String rceptNo, EquipmentSave equipmentSave, Organizaion organizaion, Cmpny cmpny) {
        Mntmgt mntmgt = new Mntmgt();
        mntmgt.rceptNo = rceptNo;
        mntmgt.orgCd = organizaion;
        mntmgt.bizrno = cmpny;
        mntmgt.maintenanceType = equipmentSave.getMaintenanceType();
        mntmgt.clSe = equipmentSave.getClSe();
        mntmgt.subject = equipmentSave.getSubject();
        mntmgt.content = equipmentSave.getContent();
        mntmgt.statusType = equipmentSave.getStatusType();
        mntmgt.cttpcl = equipmentSave.getCttpcl();
        mntmgt.delAt = false;
        return mntmgt;
    }

    public void updateFile(List<MultipartFile> files, List<Long> savedFileIds, boolean isInstt) throws IOException {

        if ((savedFileIds == null && this.mntmgtFile.size() == 0 && files.size() > 0) ||
                (savedFileIds != null && this.mntmgtFile.size() == savedFileIds.size() && files.size() > 0)) {

            addFile(files, isInstt);
            return;
        }

        if ((savedFileIds != null && this.mntmgtFile.size() > savedFileIds.size()) ||
                (savedFileIds == null && files.size() > 0 && this.mntmgtFile.size() > 0) || (savedFileIds == null && files.size() == 0)) {
            deleteFile(savedFileIds, isInstt);

            if (files.size() > 0) {
                addFile(files, isInstt);
            }
        }

    }

    public void addFile(List<MultipartFile> files, boolean isInstt) throws IOException {
        if (files.size() > 0) {
            for (MultipartFile file : files) {
                UploadFile uploadFile = FileUtils.uploadFile(file);
                String type = isInstt ? "O" : "C";
                MntmgtFile mntmgtfile = MntmgtFile.of(uploadFile, type);
                mntmgtfile.setMntmgt(this);
                this.mntmgtFile.add(mntmgtfile);
            }
        }
    }


    private void deleteFile(List<Long> savedFileIds, boolean isInstt) {
        List<MntmgtFile> condistionsList = fileTypeClassification(isInstt);

        if (savedFileIds != null) {
            for (Long savedFileId : savedFileIds) {
                condistionsList = condistionsList.stream()
                        .filter(file -> !file.getFileNo().equals(savedFileId))
                        .collect(Collectors.toList());
            }
        }

        int fileIndex = this.mntmgtFile.size() - 1;
        int removeIndex = condistionsList.size() - 1;

        while (this.mntmgtFile.iterator().hasNext() && fileIndex > -1 && removeIndex > -1) {
            if (condistionsList.get(removeIndex).getFileNo().equals(this.mntmgtFile.get(fileIndex).getFileNo())) {
                FileUtils.deleteFile(this.mntmgtFile.get(fileIndex).getStreFileNm());
                this.mntmgtFile.get(fileIndex).delete();
                this.mntmgtFile.remove(fileIndex);
                if (fileIndex > 0) fileIndex--;
                if (condistionsList.size() > 0) removeIndex--;
                continue;
            }

            if (condistionsList.get(removeIndex).getFileNo() != this.mntmgtFile.get(fileIndex).getFileNo()) {
                fileIndex--;
            }
        }
    }

    private List<MntmgtFile> fileTypeClassification(boolean isInstt) {
        List<MntmgtFile> condistionsList = this.mntmgtFile;
        if (isInstt) {
            return condistionsList.stream().filter(file -> file.getAtchAt().equals("O")).collect(Collectors.toList());
        } else {
            return condistionsList.stream().filter(file -> file.getAtchAt().equals("C")).collect(Collectors.toList());
        }
    }

    public void updateCmpny(Cmpny cmpny) {
        this.bizrno = cmpny;
    }
}
