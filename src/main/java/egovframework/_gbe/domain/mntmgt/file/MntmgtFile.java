package egovframework._gbe.domain.mntmgt.file;

import egovframework._gbe.common.entity.File;
import egovframework._gbe.common.util.file.UploadFile;
import egovframework._gbe.domain.mntmgt.Mntmgt;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Getter
@Table(name = "TB_MNTMGT_ATCH")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MntmgtFile extends File {

    /**
     * 파일번호
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "FILE_NO")
    private Long fileNo;
    /**
     * 파일 아이디
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FILE_ID")
    private Mntmgt mntmgt;

    @Column(name = "ATCH_AT")
    private String atchAt;

    public static MntmgtFile of(UploadFile uploadFile, String type) {
        MntmgtFile file = new MntmgtFile();
        file.fileStreCours = uploadFile.getFileStreCours();
        file.streFileNm = uploadFile.getStreFileNm();
        file.orignlFileNm = uploadFile.getOriginlFileNm();
        file.fileExtsn = uploadFile.getFileExtsn();
        file.fileCn = uploadFile.getFileCn();
        file.fileSize = uploadFile.getFileSize();
        file.atchAt = type;
        return file;
    }

    public void setMntmgt(Mntmgt mntmgt) {
        this.mntmgt = mntmgt;
    }

    public void update(UploadFile uploadFile) {
        this.fileStreCours = uploadFile.getFileStreCours();
        this.streFileNm = uploadFile.getStreFileNm();
        this.orignlFileNm = uploadFile.getOriginlFileNm();
        this.fileExtsn = uploadFile.getFileExtsn();
        this.fileCn = uploadFile.getFileCn();
        this.fileSize = uploadFile.getFileSize();
    }

    public void delete() {
        this.fileNo = null;
    }

    public Map<String, String> entityToMap() {
        Map<String, String> map = new HashMap<>();
        map.put("streFileNm", this.streFileNm);
        map.put("orignlFileNm", this.orignlFileNm);
        map.put("fileStreCours", this.fileStreCours);
        return map;
    }

}
