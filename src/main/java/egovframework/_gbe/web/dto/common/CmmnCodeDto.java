package egovframework._gbe.web.dto.common;

import egovframework._gbe.domain.code.CmmnCode;
import lombok.Data;

@Data
public class CmmnCodeDto {

    /* 그룹분류 코드 */
    String clCode;
    /* 코드 */
    String cCode;
    /* 그룹분류 코드명 */
    String clNm;
    /* 코드 이름 */
    String codeNm;
    /* 코드 설명 */
    String codeDc;
    /* 부모 코드 */
    String parntsCode;
    /* 코드 깊이 */
    String codeDp;
    /* 코드 순번 */
    String codeSn;

    public static CmmnCodeDto entityToDto(CmmnCode cmmnCode) {
        CmmnCodeDto dto = new CmmnCodeDto();
        dto.clCode = cmmnCode.getClCode();
        dto.cCode = cmmnCode.getCode();
        dto.clNm = cmmnCode.getClNm();
        dto.codeNm = cmmnCode.getCodeNm();
        dto.codeDc = cmmnCode.getCodeDc();
        dto.parntsCode = cmmnCode.getParntsCode();
        dto.codeDp = cmmnCode.getCodeDp();
        dto.codeSn = cmmnCode.getCodeSn();
        return dto;
    }
}
