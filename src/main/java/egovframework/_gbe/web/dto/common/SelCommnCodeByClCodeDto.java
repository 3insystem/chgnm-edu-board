package egovframework._gbe.web.dto.common;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SelCommnCodeByClCodeDto {

    /* 코드 */
    String cCode;
    /* 코드 이름 */
    String codeNm;

}
