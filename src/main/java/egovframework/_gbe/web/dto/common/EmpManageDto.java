package egovframework._gbe.web.dto.common;

import egovframework._gbe.common.util.SecurityUtils;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Getter@Setter
public class EmpManageDto {

    //em 테이블
    /* 담당자아이디 */
    private String chrgId;
    /* 사업자번호 */
    private String bizrno;
    /* 담당업무 */
    private String chrgJob;
    /* 승인구분 */
    private String confmSe;
    /* 승인일 */
    private String confmDe;
    /* 삭제여부 */
    private String delAt;
    /* 등록자 아이디 */
    private String registerId;
    /* 등록일시 */
    private String registDt;
    /* 수정자 아이디 */
    private String updusrId;
    /* 수정일시 */
    private String updtDt;
    // ci 초기화용
    /* 수정자 */
    private String userId;
    /* 대상 회원 */
    private String mberId;
    // 회원 권한 변경
    /* 회원 아이디 */
    private String usid;
    /* 회원 유형 */
    private String useSe;
    public void MapToDto(Map map){
        this.chrgId = (String) map.get("chrgId") + "_CMPNY!";
        if(map.get("bizrno") == null){
            this.bizrno = SecurityUtils.getLoginUser().getBizrno();
        } else {
            this.bizrno = (String) map.get("bizrno");
        }
        this.chrgJob = (String) map.get("chrgJob");
        this.confmSe = (String) map.get("confmSe");
        this.confmDe = (String) map.get("confmDe");
        this.delAt = (String) map.get("delAt");
        this.registerId = SecurityUtils.getLoginUser().getUserId();
        this.registDt = (String) map.get("registDt");
        this.updusrId = SecurityUtils.getLoginUser().getUserId();
        this.updtDt = (String) map.get("updtDt");

        //em 이외
        this.userId = this.registerId;
        this.mberId = this.chrgId;
        this.usid = this.chrgId;
        this.useSe = "USESE00003";
    }
}
