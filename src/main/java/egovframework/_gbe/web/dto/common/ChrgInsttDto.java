package egovframework._gbe.web.dto.common;

import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Getter@Setter
public class ChrgInsttDto {

    /* 담당기관번호 */
    private String chrgInsttNo;
    /* 담당자아이디 */
    private String chrgId;
    /* 업무구분 */
    private String jobSe;
    /* 조직기관아이디 */
    private String orgCode;
    /* 삭제여부 */
    private String delAt;
    /* 등록자 아이디 */
    private String registerId;
    /* 등록일시 */
    private String registDt;
    /* 수정자 아이디 */
    private String updusrId;
    /* 수정일시 */
    private String updtDt;

    public void MapToDto(Map map){
        this.chrgInsttNo = (String) map.get("chrgInsttNo");
        this.chrgId = (String) map.get("chrgId") + "_CMPNY!";
        this.jobSe = (String) map.get("jobSe");
        this.orgCode = (String) map.get("orgCode");
        this.delAt = (String) map.get("delAt");
        this.registerId = (String) map.get("registerId");
        this.registDt = (String) map.get("registDt");
        this.updusrId = (String) map.get("updusrId");
        this.updtDt = (String) map.get("updtDt");

        if("main".equals(map.get("job"))){
            this.jobSe = "JOBSE00001";
        } else if ("cons".equals(map.get("job"))){
            this.jobSe = "JOBSE00002";
        }
    }
}
