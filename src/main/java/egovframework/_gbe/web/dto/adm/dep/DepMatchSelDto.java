package egovframework._gbe.web.dto.adm.dep;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;

@Getter
@Setter
public class DepMatchSelDto {

    /* 상위 기관 코드 */
    String upperOrgCd;
    /* 기관 코드 */
    String orgCd;
    /* 기관 이름 */
    String orgNm;
    /* 기관 구분 */
    String depNm;
    /* 지원청 목록 */
    List<Map<String, String>> supList;
}
