package egovframework._gbe.web.dto.adm.dep;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class DepListDto {

    /* 상위 기관 코드 */
    private String upperOrgCd;
    /* 상위 기관 이름 */
    private String upperOrgNm;
    /* 기관 코드 */
    private String orgCd;
    /* 기관 이름 */
    private String orgNm;
    /* 관리자 수 */
    private String authCnt;
    /* 기관 구분 코드 */
    private String depCd;
    /* 기관 구분 이름*/
    private String depNm;
    /* 총 데이터 수 */
    private int totalCnt;
}
