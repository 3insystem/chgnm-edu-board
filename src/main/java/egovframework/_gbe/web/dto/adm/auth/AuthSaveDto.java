package egovframework._gbe.web.dto.adm.auth;

import egovframework._gbe.common.util.SecurityUtils;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthSaveDto {

    /* 유저 ID */
    private String usid;
    /* 파견 상태 */
    private String dsptcSe;
    /* 유저 타입 */
    private String useSe;
    /* 권한 타입 */
    private String authSe;
    /* 기관 코드 */
    private String orgCd;
    /* 삭제 여부 */
    private String delAt;
    /* 수정자 */
    private String updusrId;

    public void setUpdusrId() {
        this.updusrId = SecurityUtils.getUserId();
    }
}
