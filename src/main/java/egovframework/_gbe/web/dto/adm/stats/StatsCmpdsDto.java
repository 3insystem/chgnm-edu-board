package egovframework._gbe.web.dto.adm.stats;

import lombok.Getter;

@Getter
public class StatsCmpdsDto {

    /* 날짜 */
    String ymd;
    /* 인쇄용품 건수 */
    Integer cmp001;
    /* 컴퓨터용품 건수 */
    Integer cmp002;
}
