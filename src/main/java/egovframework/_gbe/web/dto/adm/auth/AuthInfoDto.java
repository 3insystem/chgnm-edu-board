package egovframework._gbe.web.dto.adm.auth;

import egovframework._gbe.common.util.SecurityUtils;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthInfoDto {

    /* 유저 타입 */
    private String type;
    /* 상위기관 코드 */
    private String upperOrgCd;
    /* 기관 코드 */
    private String orgCd;
    /* 파견 기관 타입 */
    private String dsptcType;
    /* 파견 기관 코드 */
    private String dsptcOrgCd;
}
