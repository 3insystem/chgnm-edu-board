package egovframework._gbe.web.dto.adm.code;

import egovframework._gbe.domain.code.CmmnCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CodeDto {

    private String clCode;

    private String clNm;

    private String orgCode;

    private String code; // 필수값

    private String codeNm; // 필수값

    private String codeDc;

    private String parntsCode;

    private String codeDp; // 필수값

    private String codeSn; // 필수값

    private String ncnm;

    private String delAt;

    public CodeDto(CmmnCode cmmnCode) {
        this.clCode = cmmnCode.getClCode();
        this.clNm = cmmnCode.getClNm();
        this.code = cmmnCode.getCode();
        this.orgCode = cmmnCode.getCode();
        this.codeNm = cmmnCode.getCodeNm();
        this.codeDc = cmmnCode.getCodeDc();
        this.parntsCode = cmmnCode.getParntsCode();
        this.codeDp = cmmnCode.getCodeDp();
        this.codeSn = cmmnCode.getCodeSn();
        this.ncnm = cmmnCode.getNcnm();
        this.delAt = cmmnCode.getDelAt();
    }
}
