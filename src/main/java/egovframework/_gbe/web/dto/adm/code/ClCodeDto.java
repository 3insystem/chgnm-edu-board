package egovframework._gbe.web.dto.adm.code;

import com.querydsl.core.Tuple;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class ClCodeDto {

    private String clCode;

    private String clNm;

    public ClCodeDto(Tuple tuple) {
        String clcode = tuple.get(0, String.class);
        String clNm = tuple.get(1, String.class);

        this.clCode = clcode;
        this.clNm = clNm;
    }
}
