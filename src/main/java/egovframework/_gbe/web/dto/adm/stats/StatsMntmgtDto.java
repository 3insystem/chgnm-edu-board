package egovframework._gbe.web.dto.adm.stats;

import lombok.Getter;

@Getter
public class StatsMntmgtDto {

    /* 날짜 */
    String ymd;
    /* 장애 건수 */
    Integer mnt001;
    /* 설치 건수 */
    Integer mnt002;
    /* 점검 건수 */
    Integer mnt003;
    /* 기타 건수 */
    Integer mnt004;
}
