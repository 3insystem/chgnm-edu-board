package egovframework._gbe.web.dto.adm.auth;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;

@Getter @Setter
public class AuthListDto {

    /* 기관 이름 */
    private String orgNm;
    /* 유저 ID */
    private String usid;
    /* 유저 이름 */
    private String userNm;
    /* 전화번호(내선번호) */
    private String telno;
    /* 직급 이름 */
    private String ofcpsNm;
    /* 파견 상태 */
    private String dsptcSe;
    /* 유저 타입 */
    private String useSe;
    /*파견시작일*/
    private String dsptcBgnde;
    /*파견종료일*/
    private String dsptcEndde;

    /*
    *//* 비고 *//*
    private String rm;
    */
    /* 총 데이터 수 */
    private int totalCnt;
}
