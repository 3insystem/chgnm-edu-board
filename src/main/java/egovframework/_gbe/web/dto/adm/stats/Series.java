package egovframework._gbe.web.dto.adm.stats;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

@Getter
@Setter
public class Series {

    private String name;
    private ArrayList<Integer> data;
}
