package egovframework._gbe.web.dto.adm.dep;

import egovframework._gbe.common.util.SecurityUtils;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DepUpdDto {

    /* 상위 기관 코드 */
    private String upperOrgCd;
    /* 상위 기관 코드2 */
    private String upperOrgCd2;
    /* 상위 기관 코드3 */
    private String upperOrgCd3;
    /* 기관 코드 */
    private String orgCd;
    /* 기관 구분 코드 */
    private String depCd;
    /* 기관 구분 이름*/
    private String depNm;
    /* 수정자 아이디 */
    private String updusrId;

    DepUpdDto(){
        this.updusrId = SecurityUtils.getLoginUser().getUserId();
    }
}
