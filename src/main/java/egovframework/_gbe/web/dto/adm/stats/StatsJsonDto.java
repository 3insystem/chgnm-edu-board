package egovframework._gbe.web.dto.adm.stats;

import lombok.Getter;

import java.util.ArrayList;
import java.util.Map;

@Getter
public class StatsJsonDto {

    /* 행 */
    private ArrayList<String> categories;
    /* 차트 데이터 */
    private ArrayList<Series> series;

    public StatsJsonDto(){
        this.categories = new ArrayList<>();
        this.series = new ArrayList<>();
    }
    public void setCategories(ArrayList list) {
        this.categories = list;
    }
    public void setCategories(String s) {
        this.categories.add(s);
    }

    public void setSeries(Map m) {

        String name = (String) m.get("name");
        ArrayList<Integer> data = (ArrayList<Integer>)m.get("data");

        Series series = new Series();
        series.setName(name);
        series.setData(data);

        this.series.add(series);
    }
}
