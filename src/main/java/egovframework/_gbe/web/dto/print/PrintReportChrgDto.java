package egovframework._gbe.web.dto.print;

import lombok.Data;

@Data
public class PrintReportChrgDto {

    /**
     * 담당자
     */
    private String chrgNm;

    /**
     * 담당자 연락처
     */
    private String mbtlnum;

    /**
     * 이메일
     */
    private String email;
}
