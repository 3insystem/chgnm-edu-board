package egovframework._gbe.web.dto.print;

import egovframework._gbe.web.dto.report.ReportDto;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
public class PrintReportResultDto {

    private Long reprtNo;

    /**
     * 년
     */
    private int year;

    /**
     * 월
     */
    private int mt;

    /**
     * 구분
     */
    private String jobSe;


    /**
     * 기관(학교)명
     */
    private String orgNm;

    /**
     * 작성자
     */
    private String writer;

    /**
     * 작성일자
     */
    private LocalDateTime registDt;

    /**
     * 업체명
     */
    private String cmpnyNm;

    /**
     * 기타 지원사항
     */
    private String memo;

    /**
     * 내역
     */
    private List<PrintReportDetailDto> detailDtos = new ArrayList<>();

    /**
     * 담당자 정보
     */
    private List<PrintReportChrgDto> inChargeInfos = new ArrayList<>();

    private String printTitle;

    public String getPrintTitle() {
        return "정기점검보고서(" + year + "." + mt + ")";
    }

    public static PrintReportResultDto changeData(ReportDto reportDto) {
        PrintReportResultDto rreportPrintDto = new PrintReportResultDto();
        rreportPrintDto.reprtNo = reportDto.getReprtNo();
        rreportPrintDto.printTitle = "정기점검보고서(" + reportDto.getYear() + "." + reportDto.getMt() + ")";
        rreportPrintDto.writer = reportDto.getWriter();
        rreportPrintDto.registDt = reportDto.getRegistDt();
        rreportPrintDto.orgNm = reportDto.getOrgNm();
        rreportPrintDto.jobSe = reportDto.getJobSe();
        rreportPrintDto.cmpnyNm = reportDto.getCmpnyNm();
        rreportPrintDto.memo = reportDto.getMemo();
        return rreportPrintDto;
    }
}
