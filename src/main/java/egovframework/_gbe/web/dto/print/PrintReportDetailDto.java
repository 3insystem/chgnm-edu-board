package egovframework._gbe.web.dto.print;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class PrintReportDetailDto {
    
    private Long reprtDetailNo;

    private Long reprtNo;

    /**
     * 요청자
     */
    private String applcnt;

    /**
     * 접수일
     */
    private LocalDateTime rqstdt;

    /**
     * 담당자(완료자)
     */
    private String cmpter;

    /**
     * 완료일
     */
    private LocalDateTime comptDe;

    /**
     * 제목
     */
    private String subject;

    /**
     * 처리 내용
     */
    private String processCn;

}
