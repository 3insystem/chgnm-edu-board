package egovframework._gbe.web.dto.msg;

import egovframework._gbe.web.dto.msg.NoticeMessengerDto;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MessengerDao {
    int sendMessenger(NoticeMessengerDto dto);
}
