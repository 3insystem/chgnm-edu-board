package egovframework._gbe.web.dto.msg;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

@Getter
@Setter
public class SmsDto {

    /* 예약 전송 여부 I:즉시 발송, R:예약 발송 */
    private String reservedFg;

    /* 발신자 번호 */
    private String sndPhnId;

    /* 수신자 번호 */
    private String rcvPhnId;

    /* 문자 내용 */
    private String sndMsg;

    /* 예약 전송 : 전송을 원하는 시간(reserved_fg=”R”), 즉시 전송 : 현재시간(reserved_fg=”I”)
       example : "20210330174124"
    */
    private String reservedDttm;

    private String msgTitle;

    public void setReservedFg() {
        // 1: 월요일 ~ 7: 일요일
        int dateInt = LocalDate.now().getDayOfWeek().getValue();
        if (dateInt > 5) { // 주말
            int plus = dateInt == 6 ? 2 : 1;
            setReservedWeekend(plus);
        } else if (dateInt == 5) { // 금요일
            setReservedWeekday(3);
        } else { // 평일
            setReservedWeekday(1);
        }
    }

    private void setReservedWeekday(int plus) {
        LocalTime time = LocalTime
                .of(18, 0, 0); // 기준시간
        LocalTime nowTime = LocalDateTime.now().toLocalTime(); // 지금 시간

        if (nowTime.isAfter(time)) {
            this.reservedFg = "I";
            this.reservedDttm = LocalDateTime
                    .now()
                    .format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
        } else {
            setReservedWeekend(plus);
        }
    }

    private void setReservedWeekend(int plus) {
        this.reservedFg = "R";
        this.reservedDttm = LocalDateTime
                .of(
                        LocalDate.now().plusDays(plus),
                        LocalTime.of(8, 0, 0)
                )
                .format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
    }

    public void setPhnNum() {
        this.sndPhnId = this.sndPhnId.replaceAll("-", "");
        this.rcvPhnId = this.rcvPhnId.replaceAll("-", "");
    }
}
