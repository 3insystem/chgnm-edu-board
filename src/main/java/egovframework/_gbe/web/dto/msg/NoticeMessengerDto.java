package egovframework._gbe.web.dto.msg;


import lombok.Data;

@Data
public class NoticeMessengerDto {

    private int tCode;

    private int bCode;

    private String sender;

    private String sendername;

    private String receiver;

    private String title;

    private String content;

    private String opt1;

    private String linkmessage;
}
