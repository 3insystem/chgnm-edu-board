package egovframework._gbe.web.dto.msg;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.jackson.Jacksonized;

import java.beans.ConstructorProperties;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MessageRequestDto {

    private String callback;
    private List<Map<String, String>> recverList = new ArrayList<>();
    private String usid;
    private String subject;
    private String content;
    private String scheduleType;
    private String scheduleTime;

}
