package egovframework._gbe.web.dto.msg;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MessengerDto {

    private String sndId;

    private String rcvId;

    private String subject;

    private String content;

    private String url;
}
