package egovframework._gbe.web.dto.equipment.file;

import egovframework._gbe.domain.cmpds.file.CmpdsFile;
import egovframework._gbe.domain.mntmgt.file.MntmgtFile;
import lombok.Data;

@Data
public class EquipmentFileDto {
    private Long fileNo;

    private String fileStreCours;

    private String streFileNm;

    private String orignlFileNm;

    private String fileExtsn;

    private String fileCn;

    private Long fileSize;

    private String atchAt;

    public EquipmentFileDto(MntmgtFile mntmgtFile) {
        this.fileNo = mntmgtFile.getFileNo();
        this.fileStreCours = mntmgtFile.getFileStreCours();
        this.streFileNm = mntmgtFile.getStreFileNm();
        this.orignlFileNm = mntmgtFile.getOrignlFileNm();
        this.fileExtsn = mntmgtFile.getFileExtsn();
        this.fileCn = mntmgtFile.getFileCn();
        this.fileSize = mntmgtFile.getFileSize();
        this.atchAt = mntmgtFile.getAtchAt();
    }

    public EquipmentFileDto(CmpdsFile cmpdsFile) {
        this.fileNo = cmpdsFile.getFileNo();
        this.fileStreCours = cmpdsFile.getFileStreCours();
        this.streFileNm = cmpdsFile.getStreFileNm();
        this.orignlFileNm = cmpdsFile.getOrignlFileNm();
        this.fileExtsn = cmpdsFile.getFileExtsn();
        this.fileCn = cmpdsFile.getFileCn();
        this.fileSize = cmpdsFile.getFileSize();
        this.atchAt = cmpdsFile.getAtchAt();
    }
}
