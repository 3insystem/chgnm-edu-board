package egovframework._gbe.web.dto.equipment;

import egovframework._gbe.domain.cmpds.Cmpds;
import egovframework._gbe.domain.mntmgt.Mntmgt;
import egovframework._gbe.web.dto.adm.code.CodeDto;
import egovframework._gbe.web.dto.common.CmmnCodeDto;
import egovframework._gbe.web.dto.equipment.file.EquipmentFileDto;
import egovframework._gbe.web.dto.user.cmpny.CmpnyDto;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class EquipmentSave {

    private String rceptNo;

    /**
     * 기관코드
     */
    private String orgCd;
    /**
     * 업체사업자 번호
     */
    private String bizrno;

    /**
     * 제목
     */
    private String subject;

    /**
     * 내용
     */
    private String content;

    /**
     * 장애관리 구분
     */
    private String maintenanceType;

    /**
     * 장애관리 분류 상세
     */
    private String clSe;

    /**
     * 소모품 구분
     */
    private String expendablesType;
    /**
     * 소모품 2차 구분
     */
    private String secondExpendablesType;

    /**
     * 상태 구분
     */
    private String statusType;

    /**
     * 긴급여부
     */
    private char emrgncyAt;

    /**
     * 연락처
     */
    private String cttpcl;

    /**
     * 첨부파일
     */
    private List<MultipartFile> files = new ArrayList<>();

    private List<EquipmentFileDto> saveFiles = new ArrayList<>();

    private List<Long> savedFileIds;

    /**
     * 업체정보
     */
    private List<CmpnyDto> companys;

    private List<CodeDto> maintenanceDetailsTypes;


    public String getExpendablesType() {
        String temp = "";
        if(secondExpendablesType == null && expendablesType != null && expendablesType.contains("CMPD10000")) {
            temp = expendablesType;
            expendablesType = "CMPD100000";
            secondExpendablesType = temp;
        }
        return expendablesType;
    }


    public static EquipmentSave entityToDto(Mntmgt mntmgt, String cttpcl, List<EquipmentFileDto> files) {
        EquipmentSave equipmentSave = new EquipmentSave();
        equipmentSave.setRceptNo(mntmgt.getRceptNo());
        equipmentSave.setBizrno(mntmgt.getBizrno().getBizrno());
        equipmentSave.setSubject(mntmgt.getSubject());
        equipmentSave.setContent(mntmgt.getContent());
        equipmentSave.setMaintenanceType(mntmgt.getMaintenanceType());
        equipmentSave.setCttpcl(cttpcl);
        equipmentSave.setClSe(mntmgt.getClSe());
        equipmentSave.setSaveFiles(files);
        return equipmentSave;
    }

    public static EquipmentSave entityToDto(Cmpds cmpds, List<EquipmentFileDto> files) {
        EquipmentSave equipmentSave = new EquipmentSave();
        equipmentSave.setRceptNo(cmpds.getRceptNo());
        equipmentSave.setBizrno(cmpds.getBizrno().getBizrno());
        equipmentSave.setSubject(cmpds.getSubject());
        equipmentSave.setContent(cmpds.getContent());
        equipmentSave.setExpendablesType(cmpds.getExpendablesType());
        equipmentSave.setCttpcl(cmpds.getCttpcl());
        equipmentSave.setSaveFiles(files);
        return equipmentSave;
    }

}
