package egovframework._gbe.web.dto.equipment;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class EquipmentListDto {

    /**
     * 접수번호
     */
    private String rceptNo;
    /**
     * 기관(학교)코드
     */
    private String orgCd;
    /**
     * 기관(학교)이름
     */
    private String orgNm;
    /**
     * 업체사업자 번호
     */
    private String bizrno;
    /**
     * 제목
     */
    private String subject;
    /**
     * 상태 구분
     */
    private String statusType;
    /**
     * 장애관리 분류 
     */
    private String maintenanceType;

    /**
     * 장애관리 분류 상세
     */
    private String maintenanceDetailType;

    /**
     * 소모품 구분
     */
    private String expendablesType;
    /**
     * 담당자 이름
     */
    private String chargerNm;
    /**
     * 접수일
     */
    private LocalDateTime rceptDe;
    /**
     * 접수자 이름
     */
    private String rceptNm;
    /**
     * 완료일
     */
    private LocalDateTime comptDe;
    /**
     * 최종완료일
     */
    private LocalDateTime lastComptDe;
    /**
     * 업체명
     */
    private String cmpnyNm;
    private String cmpPosition;

    /**
     * 신청인
     */
    private String writer;
    /**
     * 등록일
     */
    private LocalDateTime registDt;
    /**
     * 미처리 개수
     */
    private long unprocessedCount;

    private int totalCnt;

    public String getChargerNmAndPosition() {
        return chargerNm != null ? chargerNm + " " + cmpPosition : "";
    }
    public String getExpendablesType() {
        return expendablesType.equals("토너") || expendablesType.equals("기타") || expendablesType.equals("전산용지") ?
                "인쇄용품(" + expendablesType +")" : expendablesType;
    }
}
