package egovframework._gbe.web.dto.equipment;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class EquipmentPersonInChargeInfoDto {
    private String usid;
    private String useSe;
    private String mbtlnum;
    private String chrgJob;
    private String orgCode;
    private String jobSe;

    public EquipmentPersonInChargeInfoDto(String mbtlnum) {
        this.mbtlnum = mbtlnum;
    }
}
