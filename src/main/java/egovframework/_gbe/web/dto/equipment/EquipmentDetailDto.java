package egovframework._gbe.web.dto.equipment;

import egovframework._gbe.web.dto.equipment.file.EquipmentFileDto;
import lombok.Data;
import org.apache.commons.text.StringEscapeUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
public class EquipmentDetailDto {

    /**
     * 접수번호
     */
    private String rceptNo;
    /**
     * 업체명
     */
    private String cmpnyNm;
    /**
     * 업체 대표번호
     */
    private String reprsntTelno;
    /**
     * 기관(학교명)
     */
    private String orgNm;
    /**
     * 제목
     */
    private String subject;
    /**
     * 내용
     */
    private String content;
    /**
     * 긴급여부
     */
    private char emrgncyAt;
    /**
     * 상태 구분
     */
    private String statusType;
    /**
     * 장애관리 분류
     */
    private String maintenanceType;
    /**
     * 분류 상세
     */
    private String maintenanceDetailType;
    /**
     * 소모품 구분
     */
    private String expendablesType;
    /**
     * 담당자 아이디
     */
    private String chargerId;
    /**
     * 담당자 이름
     */
    private String chargerNm;
    /**
     * 담당자 직책
     */
    private String cmpPosition;
    /**
     * 담당자 연락처
     */
    private String mbtlnum;
    /**
     * 접수일
     */
    private LocalDateTime rceptDe;
    /**
     * 접수자 이름
     */
    private String rceptNm;
    /**
     * 업체완료일
     */
    private LocalDateTime comptDe;
    /**
     * 업체완료자 이름
     */
    private String comptNm;

    /**
     * 최종완료자 이름
     */
    private String lastComptNm;

    /**
     * 최종완료일
     */
    private LocalDateTime lastComptDe;

    /**
     * 첨부파일
     */
    private List<EquipmentFileDto> files = new ArrayList<>();

    /**
     * 처리내용
     */
    private String processCn;
    /**
     * 참고파일
     */
    private String referAtchmnfl;
    /**
     * 신청인 이름
     */
    private String registNm;
    /**
     * 등록 아이디
     */
    private String registId;
    /**
     * 등록일
     */
    private LocalDateTime registDt;

    /**
     * 수정자 이름
     */
    private String updtNm;
    /**
     * 수정일
     */
    private LocalDateTime updtDt;

    /**
     * 신청인 연락처
     */
    private String cttpcl;


    private boolean isRcept = false; // 접수
    private boolean isCompt = false; // 업체완료
    private boolean isLastCompt = false; // 최종완료
    private boolean isCancel = false; // 취소
    private boolean isHold = false; // 보류

    public String getChargerNmAndPosition() {
        return chargerNm != null ? chargerNm + " " + cmpPosition : "";
    }

    public String getExpendablesType() {
        return expendablesType.equals("토너") || expendablesType.equals("기타") || expendablesType.equals("전산용지") ?
                "인쇄용품(" + expendablesType +")" : expendablesType;
    }

    /**
     * html 특수 문자 제거
     */
    public String getContent() {
        return StringEscapeUtils.unescapeHtml4(content);
    }

    public String getProcessCn() {
        return StringEscapeUtils.unescapeHtml4(processCn);
    }


}
