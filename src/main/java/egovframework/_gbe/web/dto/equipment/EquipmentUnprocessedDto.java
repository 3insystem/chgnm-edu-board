package egovframework._gbe.web.dto.equipment;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EquipmentUnprocessedDto {
    private String maintenanceType;
    private int unprocessedCount;
}
