package egovframework._gbe.web.dto.report;

import egovframework._gbe.common.parameter.SearchParam;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class ReportDto {

    private Long reprtNo;

    /**
     * 제목
     */
    private String subject;

    /**
     * 년
     */
    private int year;

    /**
     * 월
     */
    private int mt;

    /**
     * 기관(학교)명
     */
    private String orgNm;

    /**
     * 업체명
     */
    private String cmpnyNm;

    /**
     * 구분
     */
    private String jobSe;

    /**
     * 시작일자
     */
    private LocalDate bgnde;

    /**
     * 종료일자
     */
    private LocalDate endde;

    /**
     * 등록일
     */
    private LocalDateTime registDt;

    /**
     * 작성자
     */
    private String writer;

    /**
     * 담당자
     */
    private String chrgNm;

    /**
     * 담당자 연락처
     */
    private String mbtlnum;

    /**
     * 이메일
     */
    private String email;

    private String memo;

    private Integer totalCnt;

    private String institutionName;

    public void setInstitutionName(String institutionName, SearchParam searchParam) {
        this.institutionName = searchParam.getViewCd() == null ? institutionName : "전체";
    }
}
