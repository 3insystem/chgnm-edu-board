package egovframework._gbe.web.dto.report;

import lombok.Data;

@Data
public class ReportChrgDto {
    private String userNm;
    private String mbtlnum;
    private String email;
}
