package egovframework._gbe.web.dto.report;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
public class ReportDetailResultDto {

    private Long reprtNo;
    
    /**
     * 보고서 구분
     */
    private String reportClassification;

    /**
     * 구분
     */ 
    private String jobSe;

    /**
     * 기간
     */
    private String period;

    /**
     * 기관(학교)명
     */
    private String orgNm;

    /**
     * 작성자
     */
    private String writer;

    /**
     * 작성일자
     */
    private LocalDateTime registDt;

    /**
     * 업체명
     */
    private String cmpnyNm;

    /**
     * 담당자
     */
    private String chrgNm;

    /**
     * 담당자 연락처
     */
    private String mbtlnum;

    /**
     * 이메일
     */
    private String email;

    /**
     * 기타 지원사항
     */
    private String memo;

    /**
     * 내역
     */
    private List<ReportDetailDto> detailDtos = new ArrayList<>();

    /**
     * 담당자 정보
     */
    private List<ReportChrgDto> inChargeInfos = new ArrayList<>();


    public static ReportDetailResultDto changeData(ReportDto reportDto) {
        ReportDetailResultDto resultDto = new ReportDetailResultDto();
        resultDto.reprtNo = reportDto.getReprtNo();
        resultDto.reportClassification = reportDto.getYear() + "년 " + reportDto.getMt() + "월";
        resultDto.jobSe = reportDto.getJobSe();
        resultDto.period = reportDto.getBgnde() + " ~ " + reportDto.getEndde();
        resultDto.writer = reportDto.getWriter();
        resultDto.orgNm = reportDto.getOrgNm();
        resultDto.cmpnyNm = reportDto.getCmpnyNm();
        resultDto.chrgNm = reportDto.getChrgNm();
        resultDto.mbtlnum = reportDto.getMbtlnum();
        resultDto.email = reportDto.getEmail();
        resultDto.registDt = reportDto.getRegistDt();
        resultDto.memo = reportDto.getMemo();
        return resultDto;
    }
}
