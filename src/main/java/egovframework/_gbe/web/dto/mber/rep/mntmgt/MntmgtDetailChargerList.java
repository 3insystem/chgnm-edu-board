package egovframework._gbe.web.dto.mber.rep.mntmgt;

import lombok.Data;

import java.util.List;

@Data
public class MntmgtDetailChargerList {
    private String chargerId;

    private String chargerNm;
}
