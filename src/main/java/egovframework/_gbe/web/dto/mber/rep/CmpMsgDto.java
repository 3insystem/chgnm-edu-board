package egovframework._gbe.web.dto.mber.rep;

import egovframework._gbe.domain.user.User;
import lombok.Data;

@Data
public class CmpMsgDto {

    private String jobSe; /* 유지관리 소모품 구분 */

    private String rcvId; /* 수신자 정보 */

    private String statusSe; /* 진행 상태 */
    
    private User user; /* 담당자 정보 */
}
