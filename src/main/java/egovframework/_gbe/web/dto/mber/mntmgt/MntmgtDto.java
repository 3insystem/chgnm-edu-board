package egovframework._gbe.web.dto.mber.mntmgt;

import egovframework._gbe.common.entity.BaseEntity;
import egovframework._gbe.type.MaintenanceType;
import egovframework._gbe.type.StatusType;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.time.LocalDate;

@Data
public class MntmgtDto {

    /**
     * 접수번호
     */
    private String rceptNo;
    /**
     * 기관(학교)코드
     */

    private String orgCd;
    /**
     * 업체사업자 번호
     */
    private String bizrno;
    /**
     * 제목
     */
    private String subject;
    /**
     * 내용
     */
    private String content;
    /**
     * 긴급여부
     */
    private char emrgncyAt;
    /**
     * 상태 구분
     */
    private StatusType statusType;
    /**
     * 유지보수 구분
     */
    private MaintenanceType maintenanceType;
    /**
     * 담당자 아이디
     */
    private String chargerId;
    /**
     * 접수일
     */
    private LocalDate rceptDe;
    /**
     * 접수자 아이디
     */
    private String rceptId;
    /**
     * 완료일
     */
    private LocalDate comptDe;
    /**
     * 완료자 아이디
     */
    private String comptId;
    /**
     * 첨부파일
     */
    private MultipartFile files;
    /**
     * 처리내용
     */
    private String processCn;
    /**
     * 참고파일
     */
    private String referAtchmnfl;
    /**
     * 사용여부
     */
    private char delAt;

}
