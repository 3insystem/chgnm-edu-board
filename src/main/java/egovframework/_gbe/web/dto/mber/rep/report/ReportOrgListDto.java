package egovframework._gbe.web.dto.mber.rep.report;

import lombok.Data;

@Data
public class ReportOrgListDto {

    private String orgNm; /* 업체와 연결된 학교명 */
    
    private String orgCd; /* 학교 코드 */
}
