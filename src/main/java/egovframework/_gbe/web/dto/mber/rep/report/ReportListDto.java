package egovframework._gbe.web.dto.mber.rep.report;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ReportListDto {

    private String reprtNo; /* 리포트 접수번호 */

    private String orgNm; /* 학교 명 */

    private String jobSe;  /* 유지관리 소모품 구분 값 */

    private int year; /* 년도 */

    private int mt; /* 월 */
    
    private String sj; /* 제목 */

    private LocalDateTime bgnde;  /* 기간 - 시작일 */

    private LocalDateTime endde;  /* 기간 - 종료일 */

    private LocalDateTime registDt;  /* 등록일 */
    
    private Integer totalCnt; /* 전체 수 */
}
