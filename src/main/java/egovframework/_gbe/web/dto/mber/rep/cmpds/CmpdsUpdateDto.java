package egovframework._gbe.web.dto.mber.rep.cmpds;

import egovframework._gbe.domain.mntmgt.Mntmgt;
import egovframework._gbe.web.dto.equipment.file.EquipmentFileDto;
import egovframework._gbe.web.dto.mber.rep.mntmgt.MntmgtUpdateDto;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

@Data
public class CmpdsUpdateDto {
    /* 상태 구분 */
    private String statusType;

    /* 담당자 아이디 */
    private String chargerId;

    /* 처리내용 */
    private String processCn;

    /* 참고파일 */
    private List<MultipartFile> files = new ArrayList<>();

    private List<EquipmentFileDto> saveFiles = new ArrayList<>();

    private List<Long> savedFileIds;

    public static MntmgtUpdateDto check (Mntmgt mntmgt) {
        MntmgtUpdateDto mntmgtUpdateDto = new MntmgtUpdateDto();
        mntmgtUpdateDto.setStatusType(mntmgt.getStatusType());
        mntmgtUpdateDto.setChargerId(mntmgt.getChargerUser().getUsid());
        mntmgtUpdateDto.setProcessCn(mntmgt.getProcessCn());

        return mntmgtUpdateDto;
    }
}
