package egovframework._gbe.web.dto.mber.rep;

import lombok.Getter;
import lombok.Setter;

@Getter@Setter
public class SlctnDto {
    /* 선정번호 */
    private String slctnNo;
    /* 업체코드 */
    private String bizrno;
    /* 기관(학교)번호 */
    private String orgCd;
    /* 승인구분(승인대기, 승인, 중지) */
    private String confmSe;
    /* 승인자아이디 */
    private String confmerId;
    /* 승인기관(대리 승인시: XXX지원청, 교육청) */
    private String confmOrgCd;
    /* 승인일 */
    private String confmDe;
    /* 요청일 */
    private String registDt;
    /* 요청자 */
    private String registId;
    /* 비고 */
    private String rm;
    /* 회사 아이디 */
    private String companyId;
    /* 업체명 */
    private String cmpnyNm;
    /* 업체주소 */
    private String adres;
    /* 대표자명 */
    private String repNm;
    /* 수정자 아이디 */
    private String updusrId;
    /* 수정자 일시 */
    private String updtDt;
    /* 총 건수 */
    private Integer totalCnt;
    /* 미처리 건수 */
    private Integer unproCnt;
    /* 행번호 */
    private String rowNum;
    /* 행번호 */
    private String OrgNm;

    private String usid;

    private String mbtlnum;

    private String cmpsOrgNm;
}
