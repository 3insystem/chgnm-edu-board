package egovframework._gbe.web.dto.mber.rep;

import lombok.Getter;

@Getter
public class SelCmpnyAgreeListDto {
    /* 사용자명 */
    private String chrgJob;
    /* 기관코드 */
    private String orgCd;
    /* 기관이름 */
    private String orgNm;
    /* 유지관리담당 유무 */
    private String mainYn;
    /* 소모품담당 유무 */
    private String consYn;
}
