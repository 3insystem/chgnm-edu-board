package egovframework._gbe.web.dto.mber.rep.report;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.List;

@Data
public class ReportDetailDto {
    private String reprtNo; /* 접수번호 */

    private String sj; /* 제목 */
    
    private String year; /* 년도 */

    private String month; /* 월 */

    private String chk; /* 유지관리 소모품 구분 (검색조건) */
    
    private String orgNm; /* 학교명 (검색조건) */

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate; /* 시작년월 (검색조건) */

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate; /* 종료년월 (검색조건) */

    private List<ReportDetailListDto> detailList; /* 점검보고서 상세 리스트 */
    
    private String memo; /* 기타 지원사항 */
}
