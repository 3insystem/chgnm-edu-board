package egovframework._gbe.web.dto.mber.rep.cmpds;

import lombok.Data;
import org.apache.commons.text.StringEscapeUtils;

import java.time.LocalDateTime;

@Data
public class CmpdsListDto {

    /* 접수번호 */
    private String rceptNo;

    /* 학교 명 */
    private String orgNm;

    /* 소모품 구분 */
    private String cmpdsSe;

    /* 제목 */
    private String sj;

    /* 처리내용 */
    private String processCn;

    /* 신청자 이름 */
    private String registerNm;

    /* 담당자 명 */
    private String chargerNm;

    /* 담당자 아이디 */
    private String chargerId;

    /* 신청일 */
    private LocalDateTime registDt;

    /* 완료일 */
    private LocalDateTime comptDe;

    /* 상태 구분 */
    private String sttusSe;

    /* 긴급여부 */
    /*private char emrgncyAt;*/

    /* 사용여부 */
    private boolean delAt;

    /* 미처리 개수 */
    private Integer totalCnt;

    public String getProcessCn() {
        return StringEscapeUtils.unescapeHtml4(processCn);
    }
    public String getSj() {
        return StringEscapeUtils.unescapeHtml4(sj);
    }
}
