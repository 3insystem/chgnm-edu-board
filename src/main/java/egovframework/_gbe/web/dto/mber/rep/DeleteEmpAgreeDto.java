package egovframework._gbe.web.dto.mber.rep;

import egovframework._gbe.common.util.SecurityUtils;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DeleteEmpAgreeDto {

    /* 유저 ID */
    private String usid;
    /* 사업자등록번호 */
    private String bizrno;
    /* 사용구분 */
    private String useSe;
    /* 등록자 */
    private String registerId;
    /* 수정자 */
    private String updusrId;
    /* 승인코드 */
    private String confmSe;
    /* 담당자아이디 */
    private String chrgId;
    /* 담당업무 */
    private String chrgJob;
    /* 삭제여부 */
    private String delAt;

    private String userId;
    private String mberId;
    /*
     * mberId : 삭제 대상 유저
     */
    public void init(){
        /* 현재 접속유저 */
        String userId = SecurityUtils.getUserId();
        this.usid = this.usid + "_CMPNY!";
        this.bizrno = SecurityUtils.getLoginUser().getBizrno();
        this.useSe = "USESE00004";
        this.registerId = userId;
        this.updusrId = userId;
        this.chrgId = this.usid;
        this.chrgJob = null;
        this.confmSe = "CONFM00003";
        this.delAt = "Y";
        //삭제예정
        this.userId = this.usid;
        this.mberId = this.usid;
    }
    public void setUpdusrId() {
        this.updusrId = SecurityUtils.getUserId();
    }
}
