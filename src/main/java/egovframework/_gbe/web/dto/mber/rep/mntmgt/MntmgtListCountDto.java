package egovframework._gbe.web.dto.mber.rep.mntmgt;

import lombok.Data;

@Data
public class MntmgtListCountDto {

    /* 미처리 카운트 */
    private String unprocessed;

    /* 총 카운트 */
    private String alc;
}
