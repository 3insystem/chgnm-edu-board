package egovframework._gbe.web.dto.mber.rep.report;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Data
public class ReportDetailListDto {
    private String registerNm;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate registDt;

    private String chargerId;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate comptDe;

    private String sj;

    private String processCn;
}
