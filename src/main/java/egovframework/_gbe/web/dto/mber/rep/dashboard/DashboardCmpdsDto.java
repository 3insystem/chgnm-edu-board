package egovframework._gbe.web.dto.mber.rep.dashboard;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class DashboardCmpdsDto {

    /* 접수번호 */
    private String rceptNo;

    /* 유지보수 구분 */
    private String cmpdsSe;

    /* 제목 */
    private String sj;

    /* 상태 구분 */
    private String sttusSe;

    /* 기관(학교)명 */
    private String orgNm;

    /* 신청일 */
    private LocalDateTime registDt;
}
