package egovframework._gbe.web.dto.mber.rep.dashboard;

import lombok.Data;

@Data
public class DashboardCountDto {
    /* 총 카운트 */
    private String alc;

    /* 장애 카운트 */
    private String err;

    /* 설치 카운트 */
    private String install;

    /* 점검 카운트 */
    private String inspection;

    /* 기타 카운트 */
    private String etc;

    /* 전체 소모품 카운트 */
    private String suc;
}
