package egovframework._gbe.web.dto.mber.rep.dashboard;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class DashboardMntmgtDto {

    /* 접수번호 */
    private String rceptNo;

    /* 유지보수 구분 */
    private String mntmgtSe;

    /* 유지보수 분류 상세 구분 */
    private String clSe;

    /* 제목 */
    private String sj;

    /* 상태 구분 */
    private String sttusSe;

    /* 기관(학교)명 */
    private String orgNm;

    /* 신청일 */
    private LocalDateTime registDt;
}
