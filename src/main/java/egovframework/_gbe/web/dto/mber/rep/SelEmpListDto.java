package egovframework._gbe.web.dto.mber.rep;

import egovframework._gbe.type.CmpMntmgtType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SelEmpListDto {
    /* 사용자명 */
    private String userNm;
    /* 사용자아이디 */
    private String usid;
    /* 이메일 */
    private String email;
    /* 휴대전화 */
    private String mbtlnum;
    /* 직책 */
    private String rspofcSe;
    /* 가입일 */
    private String registDt;
    /* 담당업무 */
    private String chrgJob;
    /* 유지관리담당 기관(학교) */
    private String mainList;
    /* 소모품담당기관 기관(학교) */
    private String consList;
    /* 승인구분 */
    private String confmSe;

    private Integer totalCnt;

    private CmpMntmgtType cmpMntmgtType;

    public static SelEmpListDto  SelEmpListDto(SelEmpListDto dto){
        SelEmpListDto result = new SelEmpListDto();

        String[] chrgJobList;
        String chrgString = "";
        if(dto.chrgJob != null){
            chrgJobList = dto.chrgJob.split(",");
            for (String chrgJobItem: chrgJobList) {
                String status = CmpMntmgtType.valueOf(chrgJobItem).getStatus();
                if(chrgString.equals("")){
                    chrgString = status;
                } else {
                    chrgString = chrgString + "," + status;
                }
            }
        }

        result.userNm = dto.userNm;
        result.usid = dto.usid;
        result.email = dto.email;
        result.mbtlnum = dto.mbtlnum;
        result.rspofcSe = dto.rspofcSe;
        result.registDt = dto.registDt;
        result.chrgJob = chrgString;
        result.mainList = dto.mainList;
        result.consList = dto.consList;
        result.confmSe = dto.confmSe;
        result.totalCnt = dto.totalCnt.intValue();

        return result;
    }
}
