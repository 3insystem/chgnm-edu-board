package egovframework._gbe.web.dto.mber.rep.cmpds;

import egovframework._gbe.web.dto.equipment.file.EquipmentFileDto;
import egovframework._gbe.web.dto.mber.rep.mntmgt.MntmgtDetailChargerList;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
public class CmpdsDetailDto {

    /* 접수번호 */
    private String rceptNo;

    /* 기관(학교)명 */
    private String orgNm;

    /* 기관(학교) 연락처 */
    private String orgTelno;

    /* 유지보수 구분 */
    private String cmpdsSe;

    /* 제목 */
    private String sj;

    /* 내용 */
    private String cn;

    /* 첨부파일 */
    private List<EquipmentFileDto> files = new ArrayList<>();

    /* 참고파일 */
    private List<EquipmentFileDto> saveFiles = new ArrayList<>();
    private List<Long> savedFileIds;

    /* 상태 구분 */
    private String sttusSe;

    /* 처리내용 */
    private String processCn;

    /* 신청자 이름 */
    private String registerNm;

    /* 신청일 */
    private LocalDateTime registDt;

    /* 접수자 이름 */
    private String rcepterNm;

    /* 접수일 */
    private LocalDateTime rceptDe;

    /* 보류, 취소 이름 */
    private String updateNm;

    /* 보류, 취소 날짜 */
    private LocalDateTime updtDt;

    /* 완료자 이름 */
    private String compterNm;

    /* 완료일 */
    private LocalDateTime comptDe;

    /* 사용여부 */
    private boolean delAt;

    /* 사업자번호 */
    private String bizrno;

    /* 담당자 리스트 */
    private List<MntmgtDetailChargerList> chargerLists;

    /* 담당자 아이디 상세페이지 조회 용도 */
    private String chargerId;

    /* 담당자 이름 상세페이지 조회 용도 */
    private String chargerNm;

    /* 사용자 구분 */
    private String useSe;

    /* 사용자 아이디 (담당자 본인 지정 용도) */
    private String usId;
}
