package egovframework._gbe.web.dto.board;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
public class HolidaySave {
    private Long holidayId;

    private LocalDate modalStartDate;

    private LocalDate modalEndDate;

    private String title;

    private String usid;

    private String startDateChk;
}
