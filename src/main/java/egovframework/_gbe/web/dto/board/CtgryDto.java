package egovframework._gbe.web.dto.board;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter @Setter
@NoArgsConstructor
public class CtgryDto {

    private Long boardCategoryId;

    private Long boardMngId;

    private String categoryNm;

    private String delAt;

    public static List<Map<String, String>> getUpdateList(List<CtgryDto> ctgryList, List<Map<String, String>> updateList, Long boardMngId, String usid) {
        List<Map<String, String>> list = new ArrayList<>();

        for (CtgryDto c : ctgryList) {
            Map<String, String> map = new HashMap<>();
            boolean flag = false;
            String categoryNm = c.getCategoryNm();

            for (Map<String, String> m : updateList) {
                if (c.getBoardCategoryId().equals(Long.valueOf(m.get("boardCategoryId")))) {
                    flag = true;
                    categoryNm = m.get("categoryNm");
                    break;
                }
            }

            map.put("boardCategoryId", String.valueOf(c.getBoardCategoryId()));
            map.put("boardMngId", String.valueOf(boardMngId));
            map.put("categoryNm", categoryNm);
            map.put("usid", usid);

            if (flag) {
                map.put("delAt", "N");
            } else {
                map.put("delAt", "Y");
            }

            list.add(map);
        }

        for (Map<String, String> m : updateList) {
            Map<String, String> map = new HashMap<>();
            if (!StringUtils.hasLength(m.get("boardCategoryId"))) {
                map.put("boardMngId", String.valueOf(boardMngId));
                map.put("categoryNm", m.get("categoryNm"));
                map.put("delAt", "N");
                map.put("usid", usid);
                list.add(map);
            }
        }

        return list;
    }
}
