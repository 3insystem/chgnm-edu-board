package egovframework._gbe.web.dto.board;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
public class BoardListDto {

    private Long id;

    private Long boardCategoryId;

    private String usid;

    private String userNm;

    private String title;

    private String postAt;

    private String allPostAt;

    private String secretAt;

    private String fileId;

    private String status;

    private String updusrId;

    private String updtDt;

    private String newIcon;

    private Integer totalCnt;

    private String registDt;
}
