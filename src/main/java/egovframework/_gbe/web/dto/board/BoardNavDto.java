package egovframework._gbe.web.dto.board;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class BoardNavDto {

    private Long boardMngId;

    private String boardTitle;

    private String ncnm;
}
