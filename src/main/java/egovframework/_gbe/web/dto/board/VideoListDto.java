package egovframework._gbe.web.dto.board;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
public class VideoListDto {

    private Long id;

    private Long boardCategoryId;

    private String usid;

    private String userNm;

    private String title;

    private String updusrId;

    private String updtDt;

    private String newIcon;

    private String videoLink;

    private Integer totalCnt;

    private String registDt;
}
