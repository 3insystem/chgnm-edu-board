package egovframework._gbe.web.dto.board;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
public class BoardFileDto {

    private Long boardFileId;

    private Integer fileSn;

    private String fileStreCours;

    private String streFileNm;

    private String orignlFileNm;

    private String fileExtsn;

    private String fileCn;

    private Long fileSize;
}
