package egovframework._gbe.web.dto.board;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.text.StringEscapeUtils;

@Getter @Setter
@NoArgsConstructor
public class FaqListDto {

    private Long boardId;

    private Long boardCategoryId;

    private String usid;

    private String userNm;

    private String title;

    private String updusrId;

    private String updtDt;

    private String content;

    private String insttAt;

    private String cmpnyAt;

    private String newIcon;

    private Integer totalCnt;

    private String registDt;
    public String content() {
        return StringEscapeUtils.unescapeHtml4(content);
    }
}
