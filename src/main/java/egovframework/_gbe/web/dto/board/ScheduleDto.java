package egovframework._gbe.web.dto.board;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
public class ScheduleDto {

    private Long id;

    private Long boardCategoryId;

    private String usid;

    private String userNm;

    private String startDt;

    private String endDt;

    private String allday;

    private String title;

    private String updusrId;

    private String updtDt;

    private String location;

    private String content;

    private String groupId;
}
