package egovframework._gbe.web.dto.board;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

@Getter @Setter
@NoArgsConstructor
public class BoardSave {

    private Long boardId;

    private Long boardCategoryId;

    private String boardType;

    private String title;

    private String content;

    private String usid;

    private String userNm;

    private String status;

    private String fileId;

    private String password;

    private String secretAt;

    private String delAt;

    private String qnaSmsAt;

    private String nuriNum;

    private String comment;

    private String postAt;

    private String allPostAt;

    private String wrtrSmsAt;

    private String wrtrEmailAt;

    private String reserveDt;

    private String orgCd;

    private String viewCd;

    private String updtDt;

    private List<MultipartFile> files = new ArrayList<>();

    private List<BoardFileDto> savedFiles = new ArrayList<>();

    private List<Long> savedFileSns = new ArrayList<>();

    public void setSavedFiles(List<BoardFileDto> files) {
        this.savedFiles = files;
    }

}
