package egovframework._gbe.web.dto.board;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter @Setter
@NoArgsConstructor
public class ThumbnailListDto {

    private Long id;

    private Long boardCategoryId;

    private String usid;

    private String userNm;

    private String title;

    private String thumbnailId;

    private String thumbnailNm;

    private String fileId;

    private String ebookLink;

    private String newIcon;

    private String registDt;

    private String updusrId;

    private String updtDt;

    private Integer totalCnt;

}
