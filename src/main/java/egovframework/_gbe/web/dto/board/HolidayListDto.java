package egovframework._gbe.web.dto.board;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
public class HolidayListDto {
    private String holidayId;

    private String usid;

    private String title;

    private LocalDateTime startDate;

    private LocalDateTime endDate;

    private String userNm;

    private String registDt;
}
