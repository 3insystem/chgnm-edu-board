package egovframework._gbe.web.dto.board;

import egovframework._gbe.common.util.file.UploadFile;
import egovframework._gbe.domain.mntmgt.file.MntmgtFile;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

@Getter @Setter
@NoArgsConstructor
public class ThumbnailSave {

    private Long boardThumbnailId;

    private Long boardCategoryId;

    private String title;

    private String content;

    private String usid;

    private String userNm;

    private String thumbnailId;

    private String thumbnailNm;

    private String fileId;

    private String ebookLink;

    private String registDt;

    private String updtDt;

    private String delAt;

    private String nuriNum;

    private String comment;

    private String fileStreCours;

    private String orignlFileNm;

    private List<MultipartFile> files = new ArrayList<>();

    private List<BoardFileDto> savedFiles = new ArrayList<>();

    private List<Long> savedFileSns = new ArrayList<>();

    public void setSavedFiles(List<BoardFileDto> files) {
        this.savedFiles = files;
    }

    private List<MultipartFile> thumFiles = new ArrayList<>();

    private List<BoardFileDto> savedThumFiles = new ArrayList<>();

    private List<Long> savedThumFileSns = new ArrayList<>();

    public void setSavedThumFiles(List<BoardFileDto> thumFiles) {
        this.savedThumFiles = thumFiles;
    }



    public static ThumbnailSave of(UploadFile uploadFile, String type) {
        ThumbnailSave file = new ThumbnailSave();
        file.fileStreCours = uploadFile.getFileStreCours();
        //file.streFileNm = uploadFile.getStreFileNm();
        file.orignlFileNm = uploadFile.getOriginlFileNm();
        //file.fileExtsn = uploadFile.getFileExtsn();
        //file.fileCn = uploadFile.getFileCn();
        //file.fileSize = uploadFile.getFileSize();
        //file.atchAt = type;
        return file;
    }

}
