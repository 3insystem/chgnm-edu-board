package egovframework._gbe.web.dto.board;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
public class BoardMngDto {

    private Long boardMngId;

    private String boardType;

    private String boardTitle;

    private String useAt;

    private Integer pageCnt;

    private Integer newWrtDtCnt;

    private Integer noticeWrtDtCnt;

    private Integer anoticeWrtDtCnt;

    private String comntAt;

    private Integer postRetDt;

    private Integer postDtCnt;

    private String fileFldrNm;

    private Integer fileCnt;

    private String fileExt;

    private Long fileSize;

    private Long fileFullSize;

    private String editorAt;

    private String secretAt;

    private String postAt;

    private String allPostAt;

    private String adminSmsAt;

    private String adminEmailAt;

    private String wrtrSmsAt;

    private String wrtrEmailAt;

    private String wrtrPrevAt;

    private String wrtrNameAt;

    private String banWord;

    private String ctgryUseAt;

    private String essTitle;

    private String essContent;

    private String essEmail;

    private String approvalAt;

    private String postResAt;

    private int categoryCount;

    private String ncnm;

    private Integer totalCnt;
}
