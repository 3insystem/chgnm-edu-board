package egovframework._gbe.web.dto.board;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
public class VideoSave {

    private Long boardVideoId;

    private Long boardCategoryId;

    private String usid;

    private String userNm;

    private String title;

    private String videoLink;

    private String delAt;

    private String nuriNum;

    private String updtDt;
}
