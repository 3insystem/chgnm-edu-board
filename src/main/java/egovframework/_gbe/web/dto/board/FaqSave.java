package egovframework._gbe.web.dto.board;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class FaqSave {
    private Long boardId;

    private Long boardCategoryId;

    private String title;

    private String content;

    private String usid;

    private String insttAt;

    private String cmpnyAt;

    private String delAt;

    private String nuriNum;

    private String delReason;

    private String rstReason;
}
