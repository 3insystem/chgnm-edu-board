package egovframework._gbe.web.dto.dashboard;

import egovframework._gbe.web.dto.equipment.EquipmentListDto;
import egovframework._gbe.web.dto.equipment.EquipmentUnprocessedDto;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class DashBoardDto {

    private List<EquipmentUnprocessedDto> unprocessedDtos = new ArrayList<>();
    private List<EquipmentListDto> insttMntmgtDtos = new ArrayList<>();
    private List<EquipmentListDto> insttCmpdsDtos = new ArrayList<>();
    private long cmpdsUnprocessedCount;
    private int totalCount;


}
