package egovframework._gbe.web.dto.log;

import lombok.Getter;

@Getter
public class LogGuestDto {

    /* 사용자아이디 */
    private String usid;

    /* URL */
    private String url;

    /* 시스템이름 */
    private String sysNm;

    /* 시스템세부항목 */
    private String sysDetail;

    /* 방법(GET, POST, PUT, DELETE) */
    private String mth;

    /* 아이피 */
    private String ip;

    /* 등록자 아이디 */
    private String registerId;

    /* 수정자 아이디 */
    private String updusrId;

    public LogGuestDto(String usid, String url, String sysNm, String sysDetail, String mth, String ip, String registerId, String updusrId){

        this.usid = usid;
        this.url = url;
        this.sysNm = sysNm;
        this.sysDetail = sysDetail;
        this.mth = mth;
        this.ip = ip;
        this.registerId = registerId;
        this.updusrId = updusrId;

    }
}
