package egovframework._gbe.web.dto.user;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDto {

    /* 유저 ID */
    private String usid;
    /* 비밀번호 */
    private String password;
    /* 사용자명 */
    private String userNm;
    /* 사업자등록번호 */
    private String bizrno;
    /* 업체명 */
    private String cmpnyNm;
    /* 사용권한 */
    private String authSe;
    /* 휴대폰 번호 */
    private String mbtlnum;
    /* 권한명 */
    private String role;
    /* 사용구분 */
    private String useSe;
    /* 사용구분설명 */
    private String useSeNm;
    /* 사용여부 */
    private String useYn;
    /* 기관코드 */
    private String orgCd;
    /* 기관이름 */
    private String orgNm;
    /* 상위기관코드 */
    private String upperOrgCd;
    /* 파견중일 경우 파견기관코드 */
    private String dsptcOrgCd;
    /* 삭제여부 */
    private String delYn;
    /* 구데이터 여부 */
    private String changeAt;
}
