package egovframework._gbe.web.dto.user;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class InsttUpdate {

    private String userId;

    @NotBlank(message = "비밀번호를 입력해 주세요.")
    private String password;

    private String userNm;

    private String mbtlnum;

    /* ex) 포항지원청 > 두호초등학교 */
    private String org;

    @NotBlank(message = "이메일을 입력해 주세요.")
    private String email;

    private String dsptcAt;

    private String orgCd;

    /*파견기간 추가 */
    private String dsptcBgnde;

    private String dsptcEndde;

    /* 파견 신청시 체크유무 */
    private String chkDsptcAt;

}
