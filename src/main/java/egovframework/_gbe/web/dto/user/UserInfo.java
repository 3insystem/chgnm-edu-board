package egovframework._gbe.web.dto.user;

import egovframework._gbe.domain.code.CmmnCode;
import egovframework._gbe.domain.user.User;
import egovframework._gbe.domain.user.cmpny.Cmpny;
import egovframework._gbe.domain.user.dsptc.Dsptc;
import egovframework._gbe.domain.user.organization.Organizaion;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter @Setter
@NoArgsConstructor
public class UserInfo {

    private String usid;

    private Organizaion orgCd;

    private CmmnCode authSe;

    private String delAt;

    private CmmnCode useSe;

    private String password;

    private String userNm;

    private String ahrztEsntlNm;

    private String ahrztNoVal;

    private Integer loginFailCnt;

    private String stplatAgreAt;

    private String indvdlinfoAgreAt;

    private String esntlIdntfcAgreAt;

    private String mbtlnum;

    private String email;

    private Cmpny cmpny;

    private CmmnCode rspofcSe;

    private String sanctnOfcpsNm;

    private String useYn;

    private String delYn;

    private String telno;

    private Organizaion orgCode;

    private String orgNm;

    private String ofcpsNm;

    private String ofcpsCd;

    private Organizaion cmpsOrgCd;

    private String cmpsOrgNm;

    private String clsfCd;

    private String clsfNm;

    private Long companyId;

    private String changeAt;

    private List<Dsptc> dsptcList = new ArrayList<>();

    public UserInfo(User user, String userNm, String mbtlnum) {
        this.usid = user.getUsid();
        this.orgCd = user.getOrgCd();
        this.authSe = user.getAuthSe();
        this.delAt = user.getDelAt();
        this.useSe = user.getUseSe();
        this.password = user.getPassword();
        this.userNm = userNm;
        this.ahrztEsntlNm = user.getAhrztEsntlNm();
        this.ahrztNoVal = user.getAhrztNoVal();
        this.loginFailCnt = user.getLoginFailCnt();
        this.stplatAgreAt = user.getStplatAgreAt();
        this.indvdlinfoAgreAt = user.getIndvdlinfoAgreAt();
        this.esntlIdntfcAgreAt = user.getEsntlIdntfcAgreAt();
        this.mbtlnum = mbtlnum;
        this.email = user.getEmail();
        this.cmpny = user.getCmpny();
        this.rspofcSe = user.getRspofcSe();
        this.sanctnOfcpsNm = user.getSanctnOfcpsNm();
        this.useYn = user.getUseYn();
        this.delYn = user.getDelYn();
        this.telno = user.getTelno();
        this.orgCode = user.getOrgCode();
        this.orgNm = user.getOrgNm();
        this.ofcpsNm = user.getOfcpsNm();
        this.ofcpsCd = user.getOfcpsCd();
        this.cmpsOrgCd = user.getCmpsOrgCd();
        this.cmpsOrgNm = user.getCmpsOrgNm();
        this.clsfCd = user.getClsfCd();
        this.clsfNm = user.getClsfNm();
        this.companyId = user.getCompanyId();
        this.changeAt = user.getChangeAt();
        this.dsptcList = user.getDsptcList();
    }

    public String getFormattedUsid() {
        String s = this.getUsid().replaceAll("_CMPNY!", "");
        return s;
    }
}
