package egovframework._gbe.web.dto.user;

import egovframework._gbe.domain.user.User;
import lombok.Getter;
import lombok.Setter;

// 예시 dto
@Getter @Setter
public class UserExDto {

    private String userId;

    private String password;

    private String userNm;

    public UserExDto(User user) {
        this.userId = user.getUsid();
        this.password = user.getPassword();
        this.userNm = user.getUserNm();
    }
}
