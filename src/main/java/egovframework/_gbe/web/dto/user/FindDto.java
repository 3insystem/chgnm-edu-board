package egovframework._gbe.web.dto.user;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class FindDto {

    private String userId;

    private String userNm;

    private String email;

    private String mbtlnum;
}
