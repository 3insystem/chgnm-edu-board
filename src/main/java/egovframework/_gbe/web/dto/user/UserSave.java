package egovframework._gbe.web.dto.user;

import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

@Setter
@Getter
public class UserSave {

    // 개인정보
    private String userId;

    private String password;

    private String rePassword;

    private String userNm;

    private String mbtlnum;

    private String email;

    // 업체정보
    private String cmpnyNm;

    private String bizrno;

    private MultipartFile files;

    private String zip;

    private String adres;

    private String detailAdres;

    private String ctprvn;

    private String signgu;

    private String reprsntTelno;

    // 직원정보
    private String position;

    public String getBizrnoFilter() {
        return this.bizrno.replaceAll("-", "");
    }
}
