package egovframework._gbe.web.dto.user.cmpny;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CmpnyDto {

    /* 사업자번호 */
    private String bizrno;
    /* 업체명 */
    private String cmpnyNm;
    /* 우편번호 */
    private String zip;
    /* 주소 */
    private String adres;
    /* 상세주소 */
    private String detailAdres;
    /* 시도 */
    private String ctprvn;
    /* 시군구 */
    private String signgu;
    /* 대표번호 */
    private String reprsntTelno;
    /* 삭제여부 */
    private String delAt;
    /* 파일아이디 */
    private Long fileNo;
    /* 파일 원래 이름 */
    private String orignlFileNm;
    /* 대표 아이디 */
    private String usid;
}
