package egovframework._gbe.web.dto.user;

import egovframework._gbe.common.util.CommonUtils;
import egovframework._gbe.domain.user.User;
import egovframework._gbe.domain.user.cmpny.CmpnyFile;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
@NoArgsConstructor
public class UserUpdate {

    private String userId;

    private String password;

    private String userNm;

    private String mbtlnum;

    private String email;

    private String useSe;

    private String bizrno;

    private String cmpnyNm;

    private String zip;

    private String adres;

    private String detailAdres;

    private String ctprvn;

    private String signgu;

    private String reprsntTelno;

    private String position;

    private CmpnyFile cmpnyFile;

    private Long companyId;

    private Long savedFileIds;

    public UserUpdate(User user) {
        this.userId = user.getUsid().replaceAll("_CMPNY!", "");
        this.userNm = user.getUserNm();
        this.mbtlnum = user.getMbtlnum();
        this.email = user.getEmail();
        this.companyId = user.getCompanyId();

        if (user.getCmpny() != null) {
            this.bizrno = user.getCmpny().getBizrno();
            this.cmpnyNm = user.getCmpny().getCmpnyNm();
            this.zip = user.getCmpny().getZip();
            this.adres = user.getCmpny().getAdres();
            this.detailAdres = user.getCmpny().getDetailAdres();
            this.ctprvn = user.getCmpny().getCtprvn();
            this.signgu = user.getCmpny().getSigngu();
            this.reprsntTelno = user.getCmpny().getReprsntTelno();
            this.cmpnyFile = user.getCmpny().getCmpnyFile();
        }
    }

    public String getBizrnoFilter() {
        return this.bizrno.replaceAll("-", "");
    }

    public String formattedBizrno() {
        String ff = CommonUtils.stringToBizrno(this.bizrno);
        return ff;
    }
}
