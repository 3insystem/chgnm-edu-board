package egovframework._gbe.web.dto.user.cmpny;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BizrnoCheckDto {

    /* 사업자번호 */
    String bizrno;
    /* 업체명 */
    String cmpnyNm;
}
