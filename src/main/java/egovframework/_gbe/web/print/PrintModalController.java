package egovframework._gbe.web.print;

import egovframework._gbe.domain.log.SystemName;
import egovframework._gbe.service.print.PrintModalService;
import egovframework._gbe.web.dto.print.PrintReportResultDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequiredArgsConstructor
@RequestMapping("/print")
public class PrintModalController {

    private final PrintModalService printModalService;

    @GetMapping("/report/{reprtNo}")
    @SystemName(name = "점검보고서", detail = "조회")
    public String reportPrintModal(@PathVariable Long reprtNo, Model model) {
        PrintReportResultDto result = printModalService.getReportPrint(reprtNo);
        model.addAttribute("modal", result);
        return "/print/reportPrint :: #print";
    }
}
