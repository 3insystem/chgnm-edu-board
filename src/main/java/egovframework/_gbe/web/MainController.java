package egovframework._gbe.web;

import egovframework._gbe.common.util.CommonUtils;
import egovframework._gbe.common.util.SecurityUtils;
import egovframework._gbe.domain.user.SessionUser;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Objects;

@Controller
@RequestMapping("/")
public class MainController {

    @GetMapping
    public String redirect(RedirectAttributes redirectAttributes) {
        SessionUser loginUser = SecurityUtils.getLoginUser();
        if (!StringUtils.hasLength(loginUser.getUserId())) {

            String serverType = CommonUtils.getServerType();

            redirectAttributes.addAttribute("serverType", serverType);

            return "redirect:/login";
        }

        if (loginUser.getUseSe().equals("none") && loginUser.getChangeAt() != null) {
            return "redirect:/user/" + loginUser.getUserId().replaceAll("_CMPNY!", "");
        }

        if (loginUser.getUseSe().equals("USESE00001") && loginUser.getChangeAt() != null) {
            return "redirect:/user/instt/" + loginUser.getUserId().replaceAll("_CMPNY!", "");
        }

        String auth = loginUser.getAuth();
        String useSeNm = loginUser.getUseSeNm();
        redirectAttributes.addAttribute("auth", auth);
        redirectAttributes.addAttribute("useSeNm", useSeNm);

        return "redirect:/{auth}/{useSeNm}/dashboard";
    }
}
