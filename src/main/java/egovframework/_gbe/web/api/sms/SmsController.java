package egovframework._gbe.web.api.sms;

import egovframework._gbe.common.message.MessageService;
import egovframework._gbe.web.dto.msg.MessageRequestDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/{auth}/{useSeNm}/sms")
public class SmsController {

    private final MessageService messageService;

    @PostMapping("/send")
    public ResponseEntity<?> sendSms(@PathVariable("auth") String auth, @PathVariable("useSeNm") String useSeNm, @RequestBody MessageRequestDto dto) throws Exception {
        messageService.sendMsg(dto.getCallback(), dto.getRecverList(), dto.getUsid(), dto.getSubject(), dto.getContent(), dto.getScheduleType(), dto.getScheduleTime());
        return ResponseEntity.ok("문자메시지를 발송하였습니다.");
    }
}
