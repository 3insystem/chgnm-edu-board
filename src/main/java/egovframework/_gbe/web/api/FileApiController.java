package egovframework._gbe.web.api;

import egovframework._gbe.common.util.file.BoardFileUtils;
import egovframework._gbe.common.util.file.FileUtils;
import egovframework._gbe.common.util.file.UploadFile;
import egovframework._gbe.repository.board.BoardMngDao;
import egovframework._gbe.repository.equipment.cmpds.file.CmpdsFileRepository;
import egovframework._gbe.repository.equipment.mntmgt.file.MntmgtFileRepository;
import egovframework._gbe.repository.user.cmpny.CmpnyFileRepository;
import egovframework._gbe.web.dto.board.BoardFileDto;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Map;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/files")
public class FileApiController {

    private final CmpnyFileRepository cmpnyFileRepository;
    private final MntmgtFileRepository mntmgtFileRepository;
    private final CmpdsFileRepository cmpdsFileRepository;
    private final BoardMngDao boardFileRepository;

    @PostMapping("/edit/upload")
    public ResponseEntity editImgUpload(@RequestPart("file") MultipartFile multipartFile) throws IOException {
        UploadFile uploadFile = FileUtils.uploadFile(multipartFile);
        String fileUrl = "/api/v1/files/img/" + LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd")) + "/" + uploadFile.getStreFileNm();

        return ResponseEntity.ok(fileUrl);
    }

    @GetMapping("/img/{date}/{filename}")
    public Resource imgThumbnail(@PathVariable String date, @PathVariable String filename) throws MalformedURLException {
        return new UrlResource("file:" + FileUtils.getFullPath(date, filename));
    }

    //게시판 이미지
    @GetMapping("/img/{category}/{date}/{filename}")
    public Resource imgBThumbnail(@PathVariable String category, @PathVariable String date, @PathVariable String filename) throws MalformedURLException {
        return new UrlResource("file:" + BoardFileUtils.getFullPath(category, date, filename));
    }


    @GetMapping("/attach/{type}/{fileno}")
    public ResponseEntity download(@PathVariable String type, @PathVariable Long fileno) throws MalformedURLException {
        Map<String, String> fileMap;
        switch (type) {
            case "cmpny":
                fileMap = cmpnyFileRepository.findById(fileno)
                        .orElseThrow(EntityNotFoundException::new)
                        .entityToMap();
                break;
            case "cmpds":
                fileMap = cmpdsFileRepository.findById(fileno)
                        .orElseThrow(EntityNotFoundException::new)
                        .entityToMap();
                break;
            case "mntmgt":
                fileMap = mntmgtFileRepository.findById(fileno)
                        .orElseThrow(EntityNotFoundException::new)
                        .entityToMap();
                break;
            default:
                throw new MalformedURLException("파일을 찾을 수 없습니다.");
        }

        return FileUtils.downloadFile(fileMap);

    }


    @GetMapping("/attach/{type}/{fileno}/{filesn}")
    public ResponseEntity download(@PathVariable String type, @PathVariable Long fileno, @PathVariable Long filesn) throws MalformedURLException {
        BoardFileDto fileMap;
        switch (type) {
            case "board":
                fileMap = boardFileRepository.findFiles(fileno, filesn);
                break;

            default:
                throw new MalformedURLException("파일을 찾을 수 없습니다.");
        }
        return BoardFileUtils.downloadFile(fileMap);

    }

}
