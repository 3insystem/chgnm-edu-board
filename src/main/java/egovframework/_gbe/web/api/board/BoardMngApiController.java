package egovframework._gbe.web.api.board;

import egovframework._gbe.domain.log.SystemName;
import egovframework._gbe.service.board.BoardService;
import egovframework._gbe.web.dto.board.*;
import lombok.RequiredArgsConstructor;
import org.json.simple.parser.ParseException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@RestController
@RequiredArgsConstructor
@RequestMapping({"/adm/edu/boardMng", "/api/v1/board"})
public class BoardMngApiController {

    private final BoardService boardService;

    @PostMapping("/nav")
    public ResponseEntity nav() {
        List<BoardNavDto> list = boardService.findNavList();
        return ResponseEntity.ok(list);
    }

    @SystemName(name = "게시판관리", detail = "등록")
    @PostMapping("/add")
    public ResponseEntity save(@RequestBody BoardMngSave saveData) throws Exception {
        int result = boardService.save(saveData);
        return ResponseEntity.ok(result);
    }

    @SystemName(name = "게시판관리", detail = "수정")
    @PostMapping("/{boardMngId}/edit")
    public ResponseEntity update(@PathVariable Long boardMngId, @RequestBody BoardMngSave updtData) throws Exception {
        int result = boardService.update(boardMngId, updtData);
        return ResponseEntity.ok(result);
    }

    @SystemName(name = "게시판관리", detail = "수정")
    @PostMapping("/change")
    public ResponseEntity change(@RequestBody List<Long> list) {
        int result = boardService.change(list);
        return ResponseEntity.ok(result);
    }

    @SystemName(name = "게시판관리", detail = "조회")
    @PostMapping("/getCategory/{boardMngId}")
    public ResponseEntity getCategory(@PathVariable Long boardMngId) {
        List<CtgryDto> list = boardService.findCtgryByBoardMngId(boardMngId);
        return ResponseEntity.ok(list);
    }

    @SystemName(name = "게시판관리", detail = "조회")
    @PostMapping("/getCategoryCount/{boardMngId}")
    public ResponseEntity getCategoryCount(@PathVariable Long boardMngId) {
        int result = boardService.findCtgryByBoardMngId(boardMngId).size();
        return ResponseEntity.ok(result);
    }

    @SystemName(name = "게시판관리", detail = "조회")
    @PostMapping("/setup")
    public ResponseEntity setup(@RequestBody Map<String, Long> map) {
        BoardMngDto result = boardService.findById(map.get("boardMngId"));
        return ResponseEntity.ok(result);
    }

    @SystemName(name = "게시판관리", detail = "등록")
    @PostMapping("/board/add")
    public ResponseEntity save(@Valid BoardSave boardSave, HttpServletRequest request) throws Exception {
        Long boardId = boardService.save(boardSave, request);
        return ResponseEntity.ok(boardId);
    }

    @SystemName(name = "게시판관리", detail = "수정")
    @PostMapping("/board/edit")
    public ResponseEntity update(@Valid BoardSave boardSave, HttpServletRequest request) throws Exception {
        int result = boardService.update(boardSave, request);
        return ResponseEntity.ok(result);
    }

    @SystemName(name = "게시판관리", detail = "삭제")
    @PostMapping("/board/delete")
    public ResponseEntity deleteBoard(@RequestBody Map<String, Object> map) {
        int result = boardService.deleteBoard(map);
        return ResponseEntity.ok(result);
    }

    @SystemName(name = "게시판관리", detail = "삭제")
    @PostMapping("/board/deleteAll")
    public ResponseEntity deleteBoardAll(@RequestBody Map<String, Object> map) {
        int result = boardService.deleteBoardAll(map);
        return ResponseEntity.ok(result);
    }

    @SystemName(name = "게시판관리", detail = "수정")
    @PostMapping("/board/restore")
    public ResponseEntity restoreBoard(@RequestBody Map<String, Object> map) {
        int result = boardService.restoreBoard(map);
        return ResponseEntity.ok(result);
    }

    @SystemName(name = "게시판관리", detail = "수정")
    @PostMapping("/board/restoreAll")
    public ResponseEntity restoreBoardAll(@RequestBody Map<String, Object> map) {
        int result = boardService.restoreBoardAll(map);
        return ResponseEntity.ok(result);
    }

    @SystemName(name = "게시판관리", detail = "댓글")
    @PostMapping("/board/comment")
    public ResponseEntity comment(@RequestBody Map<String, Object> map) {
        int result = boardService.comment(map);
        return ResponseEntity.ok(result);
    }

    @SystemName(name = "게시판관리", detail = "조회")
    @PostMapping("/board/secret")
    public ResponseEntity secret(@RequestBody Map<String, Object> map) {
        Map<String, Object> rtnMap = boardService.getSecretAt(map);
        return ResponseEntity.ok(rtnMap);
    }

    @SystemName(name = "게시판관리", detail = "조회")
    @PostMapping("/calendar")
    public ResponseEntity schedule(@RequestBody Map<String, String> map) {
        List<ScheduleDto> list = boardService.findScheduleAll(map);
        return ResponseEntity.ok(list);
    }

    @SystemName(name = "게시판관리", detail = "등록")
    @PostMapping("/calendar/add")
    public ResponseEntity save(@RequestBody ScheduleDto saveData) {
        int result = boardService.save(saveData);
        return ResponseEntity.ok(result);
    }

    @SystemName(name = "게시판관리", detail = "수정")
    @PostMapping("/calendar/edit")
    public ResponseEntity update(@RequestBody ScheduleDto updtData) {
        int result = boardService.update(updtData);
        return ResponseEntity.ok(result);
    }

    @SystemName(name = "게시판관리", detail = "삭제")
    @PostMapping("/calendar/delete")
    public ResponseEntity deleteSchedule(@RequestBody Map<String, Object> map) {
        int result = boardService.deleteShceule(map);
        return ResponseEntity.ok(result);
    }

    @SystemName(name = "게시판관리", detail = "수정")
    @PostMapping("/calendar/restore")
    public ResponseEntity restoreSchedule(@RequestBody Map<String, Object> map) {
        int result = boardService.restoreSchedule(map);
        return ResponseEntity.ok(result);
    }

    @SystemName(name = "게시판관리", detail = "등록")
    @PostMapping("/video/add")
    public ResponseEntity save(@RequestBody VideoSave saveData) {
        Long boardId = boardService.save(saveData);
        return ResponseEntity.ok(boardId);
    }

    @SystemName(name = "게시판관리", detail = "수정")
    @PostMapping("/video/edit")
    public ResponseEntity update(@RequestBody VideoSave boardSave) throws Exception {
        int result = boardService.update(boardSave);
        return ResponseEntity.ok(result);
    }

    @SystemName(name = "게시판관리", detail = "삭제")
    @PostMapping("/video/delete")
    public ResponseEntity deleteVideo(@RequestBody Map<String, Object> map) {
        int result = boardService.deleteVideo(map);
        return ResponseEntity.ok(result);
    }

    @SystemName(name = "게시판관리", detail = "삭제")
    @PostMapping("/video/deleteAll")
    public ResponseEntity deleteVideoAll(@RequestBody Map<String, Object> map) {
        int result = boardService.deleteVideoAll(map);
        return ResponseEntity.ok(result);
    }

    @SystemName(name = "게시판관리", detail = "수정")
    @PostMapping("/video/restore")
    public ResponseEntity restoreVideo(@RequestBody Map<String, Object> map) {
        int result = boardService.restoreVideo(map);
        return ResponseEntity.ok(result);
    }

    @SystemName(name = "게시판관리", detail = "수정")
    @PostMapping("/video/restoreAll")
    public ResponseEntity restoreVideoAll(@RequestBody Map<String, Object> map) {
        int result = boardService.restoreVideoAll(map);
        return ResponseEntity.ok(result);
    }

    @SystemName(name = "게시판관리", detail = "등록")
    @PostMapping("/faq/add")
    public ResponseEntity faqSave(@Valid FaqSave faqSave, HttpServletRequest request) throws IOException, ParseException {
        Long boardId = boardService.faqSave(faqSave, request);
        return ResponseEntity.ok(boardId);
    }

    @SystemName(name = "게시판관리", detail = "삭제")
    @PostMapping("/faq/del")
    public ResponseEntity faqDel(@RequestBody List<FaqSave> delList) {
        boardService.faqDel(delList);
        return ResponseEntity.ok().build();
    }

    @SystemName(name = "게시판관리", detail = "수정")
    @PostMapping("/faq/restoreAll")
    public ResponseEntity faqRestoreAll(@RequestBody List<FaqSave> rstList) {
        boardService.faqRestoreAll(rstList);
        return ResponseEntity.ok().build();
    }

    @SystemName(name = "게시판관리", detail = "수정")
    @PostMapping("/faq/restore")
    public ResponseEntity faqRestore(@RequestBody FaqSave rstData) {
        int result = boardService.faqRestore(rstData);
        return ResponseEntity.ok(result);
    }

    @SystemName(name = "게시판관리", detail = "등록")
    @PostMapping("/holiday/add")
    public ResponseEntity hoildaySave(@RequestBody HolidaySave holidaySave) {
        boolean chk = false;
        chk = boardService.holidaySave(holidaySave);
        return ResponseEntity.ok(chk);
    }

    @SystemName(name = "게시판관리", detail = "삭제")
    @PostMapping("/holiday/del")
    public ResponseEntity hoildayDel(@RequestBody List<HolidayListDto> delList) {
        boardService.holidayDel(delList);
        return ResponseEntity.ok().build();
    }

    @SystemName(name = "게시판관리", detail = "등록")
    @PostMapping("/thumbnail/add")
    public ResponseEntity save(@Valid ThumbnailSave saveData,
                               @RequestPart(value = "thumFile", required = false) List<MultipartFile> thumFile,
                               HttpServletRequest request) throws Exception {
        Long boardId = boardService.save(saveData, thumFile, request);
        return ResponseEntity.ok(boardId);
    }

    @SystemName(name = "게시판관리", detail = "수정")
    @PostMapping("/thumbnail/edit")
    public ResponseEntity update(@Valid ThumbnailSave boardSave,
                               @RequestPart(value = "thumFile", required = false) List<MultipartFile> thumFile,
                                 HttpServletRequest request) throws Exception {
        int result = boardService.update(boardSave, thumFile, request);
        return ResponseEntity.ok(result);
    }

    @SystemName(name = "게시판관리", detail = "삭제")
    @PostMapping("/thumbnail/delete")
    public ResponseEntity deleteThumbnail(@RequestBody Map<String, Object> map) {
        int result = boardService.deleteThumbnail(map);
        return ResponseEntity.ok(result);
    }

    @SystemName(name = "게시판관리", detail = "삭제")
    @PostMapping("/thumbnail/deleteAll")
    public ResponseEntity deleteThumbnailAll(@RequestBody Map<String, Object> map) {
        int result = boardService.deleteThumbnailAll(map);
        return ResponseEntity.ok(result);
    }

    @SystemName(name = "게시판관리", detail = "수정")
    @PostMapping("/thumbnail/restore")
    public ResponseEntity restoreThumbnail(@RequestBody Map<String, Object> map) {
        int result = boardService.restoreThumbnail(map);
        return ResponseEntity.ok(result);
    }

    @SystemName(name = "게시판관리", detail = "수정")
    @PostMapping("/thumbnail/restoreAll")
    public ResponseEntity restoreThumbnailAll(@RequestBody Map<String, Object> map) {
        int result = boardService.restoreThumbnailAll(map);
        return ResponseEntity.ok(result);
    }

    @SystemName(name = "게시판관리", detail = "이동")
    @PostMapping("/{boardType}/transport")
    public ResponseEntity transport(@PathVariable String boardType, @RequestBody Map<String, Object> map) {
        int result = boardService.transport(boardType, map);
        return ResponseEntity.ok(result);
    }
}
