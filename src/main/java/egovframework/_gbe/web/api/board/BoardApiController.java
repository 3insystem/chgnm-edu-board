package egovframework._gbe.web.api.board;

import egovframework._gbe.service.board.BoardFService;
import egovframework._gbe.web.dto.board.BoardSave;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping({"/adm/edu/community", "/api/v1/community"})
@RequiredArgsConstructor
public class BoardApiController {

    private final BoardFService boardFService;
    @PostMapping("/add")
    public ResponseEntity save(@RequestBody BoardSave saveData) throws Exception {
        int result = boardFService.save(saveData);
        return ResponseEntity.ok(result);
    }
}