package egovframework._gbe.web.api.adm.auth;

import egovframework._gbe.domain.log.SystemName;
import egovframework._gbe.service.adm.auth.AuthService;
import egovframework._gbe.service.msg.NoticeMessengerService;
import egovframework._gbe.web.dto.adm.auth.AuthSaveDto;
import egovframework._gbe.web.dto.msg.NoticeMessengerDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/adm/{useSeNm}/auth")
@RequiredArgsConstructor
public class AuthApiController {

    private final NoticeMessengerService noticeMessengerService;
    private final AuthService authService;
    @SystemName(name = "권한/파견관리", detail = "수정")
    @PostMapping("/authSave")
    public ResponseEntity AuthSave(@RequestBody Map<String, List<AuthSaveDto>> saveData) throws Exception{

        Map resultMap = new HashMap<>();
        List<AuthSaveDto> list = saveData.get("data");

        resultMap = authService.updAuthSaveList(list);

        List<NoticeMessengerDto> userList = (List<NoticeMessengerDto>) resultMap.remove("userList");
        noticeMessengerService.sendList(userList);

        return ResponseEntity.ok(resultMap);
    }
}
