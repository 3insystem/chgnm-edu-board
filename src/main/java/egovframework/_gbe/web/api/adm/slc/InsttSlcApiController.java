package egovframework._gbe.web.api.adm.slc;

import egovframework._gbe.common.message.MessageDto;
import egovframework._gbe.common.message.MessageService;
import egovframework._gbe.common.util.CryptUtils;
import egovframework._gbe.common.util.SecurityUtils;
import egovframework._gbe.domain.log.SystemName;
import egovframework._gbe.domain.user.cmpny.Cmpny;
import egovframework._gbe.service.adm.slc.InsttSlcService;
import egovframework._gbe.service.user.UserService;
import egovframework._gbe.web.dto.mber.rep.SlctnDto;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/instt/slc")
@RequiredArgsConstructor
public class InsttSlcApiController {
    private final UserService userService;
    private final MessageService messageService;
    private final InsttSlcService insttSlcService;
    private final CryptUtils cryptUtils;

    @SystemName(name = "업체관리", detail = "승인")
    @PostMapping("/slcConfm")
    public Map slcConfm(@RequestBody Map map) throws Exception {
        Map resultMap = new HashMap();
        String OrgCd = SecurityUtils.getLoginUser().getOrgCd();
        String userId = SecurityUtils.getLoginUser().getUserId();
        String slctnNo = (String) map.get("slctnNo");
        String bizrno = insttSlcService.slcBizrno(slctnNo);
        resultMap = insttSlcService.slcConfm(slctnNo,userId,OrgCd,bizrno);
        String orgNm = insttSlcService.selOrgNm(slctnNo);
        //처리 후 문자 메시지 발송
        List<Map<String, String>> smsList = new ArrayList<>();
        Map<String, String> smsMap = new HashMap<>();
        List<SlctnDto> selSmsList = insttSlcService.selSms(bizrno);
        //처리 후 문자 메시지 발송
        for (SlctnDto slctnDto : selSmsList) {
            String phone = cryptUtils.decrypt(slctnDto.getMbtlnum());
            smsMap.put("name", slctnDto.getCmpsOrgNm());
            smsMap.put("phone", phone.replaceAll("-",""));
            smsList.add(smsMap);
        }
            messageService.sendMsg("", smsList, userId, "[충청남도교육청]장애관리업체 승인건", MessageDto.createSlcMessage(orgNm), "0", "");

        return resultMap;
    }

    @SystemName(name = "업체관리", detail = "중지")
    @PostMapping("/slcStop")
    public Map slcStopBtn(@RequestBody Map map){
        Map resultMap = new HashMap();
        String userId = SecurityUtils.getLoginUser().getUserId();
        String slctnNo = (String) map.get("slctnNo");
        resultMap = insttSlcService.slcStopBtn(slctnNo,userId);
        return resultMap;
    }

}
