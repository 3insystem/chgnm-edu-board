package egovframework._gbe.web.api.adm.dep;

import egovframework._gbe.domain.log.SystemName;
import egovframework._gbe.service.adm.dep.DepService;
import egovframework._gbe.web.dto.adm.auth.AuthSaveDto;
import egovframework._gbe.web.dto.adm.dep.DepUpdDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/adm/{useSeNm}/dep")
@RequiredArgsConstructor
public class DepApiController {

    private final DepService depService;
    @SystemName(name = "기관(학교)관리", detail = "수정")
    @PostMapping("/depUpdate")
    public ResponseEntity depUpdate(@RequestBody DepUpdDto updateData) throws Exception{

        Map resultMap = depService.depUpdate(updateData);

        return ResponseEntity.ok(resultMap);
    }
}
