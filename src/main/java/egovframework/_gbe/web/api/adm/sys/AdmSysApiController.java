package egovframework._gbe.web.api.adm.sys;

import egovframework._gbe.service.common.CommonService;
import egovframework._gbe.web.dto.adm.code.CodeDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/adm/sys")
@RequiredArgsConstructor
public class AdmSysApiController {

    private final CommonService commonService;

    @PostMapping("/code/delete")
    public ResponseEntity delete(@RequestBody Map<String, String> map) {
        commonService.delete(map.get("clCode"));
        return ResponseEntity.ok().build();
    }

    @PostMapping("/code/save")
    public ResponseEntity save(@RequestBody List<CodeDto> codeDtoList) {
        commonService.saveList(codeDtoList);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/code/{clCode}")
    public ResponseEntity update(@PathVariable String clCode, @RequestBody List<CodeDto> codeDtoList) {
        commonService.update(codeDtoList);
        return ResponseEntity.ok().build();
    }

}
