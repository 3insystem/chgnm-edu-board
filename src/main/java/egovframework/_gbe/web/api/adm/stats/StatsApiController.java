package egovframework._gbe.web.api.adm.stats;

import egovframework._gbe.common.excpetion.UnableChangeStatusException;
import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.domain.log.SystemName;
import egovframework._gbe.service.adm.stats.StatsService;
import egovframework._gbe.web.dto.adm.stats.StatsJsonDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/adm/{useSeNm}/stats")
@RequiredArgsConstructor
public class StatsApiController {

    private final StatsService statsService;

    @SystemName(name = "통계", detail = "조회")
    @PostMapping("/rec")
    public ResponseEntity statsRec(@RequestBody SearchParam searchParam, Model model) {

        StatsJsonDto statsJsonDto = statsService.statsRec(searchParam);
        model.addAttribute("menu", 10);  //메뉴정보

        return ResponseEntity.ok(statsJsonDto);
    }

    @SystemName(name = "통계", detail = "조회")
    @PostMapping({"/sch", "/cmp", "/emp"})
    public ResponseEntity statsCommon(@RequestBody SearchParam searchParam, Model model) throws UnableChangeStatusException {

        StatsJsonDto statsJsonDto = statsService.statsCommon(searchParam);
        model.addAttribute("menu", 10);  //메뉴정보

        return ResponseEntity.ok(statsJsonDto);
    }

    @SystemName(name = "통계", detail = "조회")
    @PostMapping({"/join"})
    public ResponseEntity statsJoin(@RequestBody SearchParam searchParam, Model model) throws UnableChangeStatusException {

        StatsJsonDto statsJsonDto = statsService.statsJoin(searchParam);
        model.addAttribute("menu", 10);  //메뉴정보

        return ResponseEntity.ok(statsJsonDto);
    }

    @SystemName(name = "통계", detail = "조회")
    @PostMapping({"/menu"})
    public ResponseEntity statsMenu(@RequestBody SearchParam searchParam, Model model) throws UnableChangeStatusException {

        StatsJsonDto statsJsonDto = statsService.statsMenu(searchParam);
        model.addAttribute("menu", 10);  //메뉴정보

        return ResponseEntity.ok(statsJsonDto);
    }
}
