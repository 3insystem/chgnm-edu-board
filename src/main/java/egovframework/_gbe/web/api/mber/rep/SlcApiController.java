package egovframework._gbe.web.api.mber.rep;

import egovframework._gbe.common.util.SecurityUtils;
import egovframework._gbe.domain.log.SystemName;
import egovframework._gbe.service.common.CommonService;
import egovframework._gbe.service.mber.rep.RepService;
import egovframework._gbe.service.mber.rep.SlcService;
import egovframework._gbe.service.msg.MessengerService;
import lombok.RequiredArgsConstructor;
import net.bytebuddy.implementation.bind.MethodDelegationBinder;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/mber/slc")
@RequiredArgsConstructor
public class SlcApiController {
    private final MessengerService messengerService;
    private final CommonService commonService;
    private final SlcService slcService;

    @SystemName(name = "기관(학교)현황", detail = "기관(학교)추가")
    @PostMapping("/insertSlc")
    public Map insertSlc(@RequestBody Map map) throws Exception {
        /**/
        Map resultMap = new HashMap();
        /* 현재 접속중인 유저(업체) */
        String userId = SecurityUtils.getLoginUser().getUserId();
        /* 현재 접속중인 유저의 사업자번호 */
        String bizrno = SecurityUtils.getLoginUser().getBizrno();
        String orgCd = (String) map.get("orgCd");
            resultMap = slcService.insertSlc(userId, bizrno, orgCd);
        return resultMap;
    }

    @SystemName(name = "기관(학교)현황", detail = "삭제")
    @PostMapping("/delete")
    public Map deleteSlc(@RequestBody Map map){
        Map resultMap = new HashMap();
        String userId = SecurityUtils.getLoginUser().getUserId();
        String bizrno = SecurityUtils.getLoginUser().getBizrno();
        String slctnNo = (String) map.get("slctnNo");
        resultMap = slcService.deleteSlc(slctnNo,userId,bizrno);

        return resultMap;
    }

    @SystemName(name = "기관(학교)현황", detail = "중지")
    @PostMapping("/slcStop")
    public Map slcStopBtn(@RequestBody Map map){
        Map resultMap = new HashMap();
        String userId = SecurityUtils.getLoginUser().getUserId();
        String slctnNo = (String) map.get("slctnNo");
        resultMap = slcService.slcStopBtn(slctnNo,userId);
        return resultMap;
    }
}
