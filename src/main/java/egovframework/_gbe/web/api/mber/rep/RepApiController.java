package egovframework._gbe.web.api.mber.rep;

import egovframework._gbe.common.message.MessageDto;
import egovframework._gbe.common.message.MessageService;
import egovframework._gbe.common.util.CryptUtils;
import egovframework._gbe.common.util.SecurityUtils;
import egovframework._gbe.domain.log.SystemName;
import egovframework._gbe.domain.user.User;
import egovframework._gbe.service.common.CommonService;
import egovframework._gbe.service.mber.rep.RepService;
import egovframework._gbe.service.user.UserService;
import egovframework._gbe.web.dto.mber.rep.DeleteEmpAgreeDto;
import egovframework._gbe.web.dto.mber.rep.SelCmpnyAgreeListDto;
import egovframework._gbe.web.dto.mber.rep.SelEmpListDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/mber/rep")
@RequiredArgsConstructor
public class RepApiController {

    private final UserService userService;
    private final MessageService messageService;
    private final CryptUtils cryptUtils;
    private final RepService repService;
    /*
        해당 유저 업무 권한 찾기
        Map : 해당 유저 Id
     */
    @SystemName(name = "담당자(직원)관리", detail = "조회")
    @PostMapping("/selEmpAgree")
    public Map selEmpAgree(@RequestBody Map<String, String> map) {

        Map resultMap = new HashMap();

        List<SelCmpnyAgreeListDto> selEmpList = repService.SelCmpnyAgreeList(map);

        if(selEmpList != null){
            resultMap.put("result", "S");
        } else {
            resultMap.put("result", "F");
        }

        resultMap.put("dto", selEmpList);

        return resultMap;
    }

    /*
        해당 유저 업무 권한 부여
        Map : TB_EMP_MANAGE, TB_CHRG_INSTT 파라미터
     */
    @SystemName(name = "담당자(직원)관리", detail = "수정")
    @PostMapping("/saveEmpAgree")
    public Map saveEmpAgree(@RequestBody Map<String, Map<String, Object>> map) throws Exception {

        Map resultMap = new HashMap();

        if(map.get("data") != null){
            resultMap = repService.saveEmpAgree(map.get("data"));
        } else {
            resultMap.put("result", "F");
        }

        List<String> empList = (List<String>) resultMap.get("empList");
        //처리 후 문자 메시지 발송
        for(String usid : empList){

            User userInfo = userService.findById(usid);

            String content = userInfo.getCmpny().getCmpnyNm() + "업체 담당자로 승인처리 되었습니다. \n 상세 담당업무 내용은 로그인 > 마이페이지 > 내정보에서 확인해주세요.";

            String decUserNm =  cryptUtils.decrypt(userInfo.getUserNm());
            String decMbtlnum = cryptUtils.decrypt(userInfo.getMbtlnum());
            List<Map<String, String>> smsList = new ArrayList<>();
            Map<String, String> smsMap = new HashMap<>();
            smsMap.put("name", decUserNm);
            smsMap.put("phone", decMbtlnum.replaceAll("-",""));
            smsList.add(smsMap);

            messageService.sendMsg("", smsList, SecurityUtils.getUserId(), "[충청남도교육청]업체담당자 승인건", content, "0", "");
        }

        return resultMap;
    }
    /*
        해당 유저 업무 권한 삭제
        Map : TB_EMP_MANAGE
     */
    @SystemName(name = "담당자(직원)관리", detail = "삭제")
    @PostMapping("/deleteEmpAgree")
    public Map deleteEmpAgree(@RequestBody DeleteEmpAgreeDto dto) {

        Map resultMap = new HashMap();

        if(dto != null){
            resultMap = repService.deleteEmpAgree(dto);
        } else {
            resultMap.put("result", "F");
        }

        return resultMap;
    }
}
