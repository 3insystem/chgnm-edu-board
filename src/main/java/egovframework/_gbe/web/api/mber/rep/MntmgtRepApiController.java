package egovframework._gbe.web.api.mber.rep;

import egovframework._gbe.common.message.MessageService;
import egovframework._gbe.common.util.SecurityUtils;
import egovframework._gbe.domain.log.SystemName;
import egovframework._gbe.domain.mntmgt.Mntmgt;
import egovframework._gbe.repository.mntmgt.MntmgtCmpnyRepository;
import egovframework._gbe.service.mber.cmpds.CmpdsCmpnyService;
import egovframework._gbe.service.mber.cmpnymsg.CmpMsgService;
import egovframework._gbe.service.mber.mntmgt.MntmgtCmpnyService;
import egovframework._gbe.service.mber.report.ReportCmpnyService;
import egovframework._gbe.web.dto.mber.rep.cmpds.CmpdsUpdateDto;
import egovframework._gbe.web.dto.mber.rep.mntmgt.MntmgtUpdateDto;
import egovframework._gbe.web.dto.mber.rep.report.ReportDetailDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityExistsException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequiredArgsConstructor
@RequestMapping("/mber/rep")
public class MntmgtRepApiController {

    private final MntmgtCmpnyService mntmgtCmpnyService;
    //    private final CmpdsCmpnyService cmpdsCmpnyService;
    private final CmpMsgService cmpMsgService;
    private final MessageService messageService;

    private final ReportCmpnyService reportCmpnyService;

    private final MntmgtCmpnyRepository mntmgtCmpnyRepository;

    @SystemName(name = "유지관리", detail = "수정")
    @PostMapping("/mntmgt/detail/{rceptNo}")
    public ResponseEntity mntmgtDetailUpdate(@PathVariable String rceptNo, @Valid MntmgtUpdateDto saveData, HttpServletRequest request) throws Exception{
        String userId = SecurityUtils.getLoginUser().getUserId();
        boolean chk = mntmgtCmpnyService.webFilter(userId, rceptNo, saveData, request); /* WebFilter */
        String statusChk = mntmgtCmpnyService.statusChkb(rceptNo); /* 담당자 배정시 문자 보내는 용도 */
        String updateChk = ""; /* webfilter 업데이트 체크 */
        Mntmgt mntmgt = mntmgtCmpnyRepository.findById(rceptNo)
                .orElseThrow(EntityExistsException::new);

        if(mntmgt.getStatusType().equals("STTUS00003") || mntmgt.getStatusType().equals("STTUS00006")) {
            return ResponseEntity.ok("false");
        }

        if (chk) {
            updateChk = "true";
            mntmgtCmpnyService.mntmgtUpdate(rceptNo, saveData);
            cmpMsgService.sendCmpSms(rceptNo, statusChk); /* SMS */
            cmpMsgService.sendCmpMessenger(rceptNo); /* 메신저 */
        }

        return ResponseEntity.ok(updateChk);
    }

//    @SystemName(name = "소모품", detail = "수정")
//    @PostMapping("/cmpds/detail/{rceptNo}")
//    public ResponseEntity cmpdsDetailUpdate(@PathVariable String rceptNo,
//                                       @Valid CmpdsUpdateDto saveData) throws Exception{
//
//        cmpdsCmpnyService.cmpdsUpdate(rceptNo, saveData);
//
//        return ResponseEntity.ok().build();
//    }

    @SystemName(name = "점검보고서", detail = "등록")
    @PostMapping("/report/register")
    public ResponseEntity reportRegister(@RequestBody ReportDetailDto reportDetailDto) throws Exception{

        String reprtNo = null;
        reportCmpnyService.reportSave(reprtNo, reportDetailDto);
        cmpMsgService.sendCmpReportMessenger(reportDetailDto);

        return ResponseEntity.ok().build();
    }

    @SystemName(name = "점검보고서", detail = "수정")
    @PostMapping("/report/register/{reprtNo}")
    public ResponseEntity reportUpdate(@PathVariable String reprtNo, @RequestBody ReportDetailDto reportDetailDto) throws Exception{

        reportCmpnyService.reportSave(reprtNo, reportDetailDto);
//        cmpMsgService.sendCmpReportMessenger(reportDetailDto);  수정시 메신저 제거

        return ResponseEntity.ok().build();
    }
}
