package egovframework._gbe.web.api.equipment.cmpds;

import egovframework._gbe.domain.log.SystemName;
import egovframework._gbe.service.equipment.EquipmentCommonService;
import egovframework._gbe.web.dto.equipment.EquipmentListDto;
import egovframework._gbe.web.dto.equipment.EquipmentSave;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping("/{auth}/{useSeNm}/cmpds")
public class CmpdsApiController {
    private final EquipmentCommonService insttCommonService;

    @Autowired
    public CmpdsApiController(@Qualifier("CmpdsService") EquipmentCommonService insttCommonService) {
        this.insttCommonService = insttCommonService;
    }

    @PostMapping("/register")
    @SystemName(name = "소모품", detail = "등록")
    public ResponseEntity<?> cmpdsRegister(@Valid EquipmentSave equipmentSave, HttpServletRequest request) throws Exception{
        insttCommonService.equipmentDataSave(equipmentSave, request);
        return ResponseEntity.ok().body("등록");
    }

    @PostMapping("/update")
    @SystemName(name = "소모품", detail = "수정")
    public ResponseEntity<?> cmpdsUpdate(@Valid EquipmentSave equipmentSave, HttpServletRequest request) throws Exception{
        insttCommonService.equipmentDataUpdate(equipmentSave, request);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @PostMapping("/cancel")
    @SystemName(name = "소모품", detail = "수정")
    public ResponseEntity<?> cmpdsCancel(@RequestBody EquipmentListDto dto) {
        insttCommonService.cancelEquipmentData(dto.getRceptNo());
        return ResponseEntity.ok().body("신청내역이 취소되었습니다.");
    }

}
