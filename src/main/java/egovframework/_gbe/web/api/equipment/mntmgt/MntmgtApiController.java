package egovframework._gbe.web.api.equipment.mntmgt;

import egovframework._gbe.common.response.RestResponse;
import egovframework._gbe.domain.log.SystemName;
import egovframework._gbe.service.equipment.EquipmentCommonService;
import egovframework._gbe.web.dto.equipment.EquipmentListDto;
import egovframework._gbe.web.dto.equipment.EquipmentSave;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping("/{auth}/{useSeNm}/mntmgt")
public class MntmgtApiController {
    private final EquipmentCommonService equipmentCommonService;

    @Autowired
    public MntmgtApiController(@Qualifier("MntmgtService") EquipmentCommonService equipmentCommonService) {
        this.equipmentCommonService = equipmentCommonService;
    }

    @PostMapping("/register")
    @SystemName(name = "유지관리", detail = "등록")
    public ResponseEntity<?> mntmgtRegister(@Valid EquipmentSave equipmentSave, HttpServletRequest request) throws Exception{
        RestResponse restResponse = equipmentCommonService.equipmentDataSave(equipmentSave, request);
        return ResponseEntity.ok().body(restResponse);
    }

    @PostMapping("/update")
    @SystemName(name = "유지관리", detail = "수정")
    public ResponseEntity<?> mntmgtUpdate(@Valid EquipmentSave equipmentSave, HttpServletRequest request) throws Exception{
        RestResponse restResponse = equipmentCommonService.equipmentDataUpdate(equipmentSave, request);
        return ResponseEntity.ok().body(restResponse);
    }

    @PostMapping("/cancel")
    @SystemName(name = "유지관리", detail = "수정")
    public ResponseEntity<?> mntmgtCancel(@RequestBody EquipmentListDto dto) {
        String result = equipmentCommonService.cancelEquipmentData(dto.getRceptNo());
        return ResponseEntity.ok().body(result);
    }


    @PostMapping("/complete")
    @SystemName(name = "유지관리", detail = "수정")
    public ResponseEntity<?> mntmgtComplete(@RequestBody EquipmentListDto dto) {
        String result = equipmentCommonService.equipmentDataLastComplete(dto.getRceptNo());
        return ResponseEntity.ok().body(result);
    }

}
