package egovframework._gbe.web.api;

import egovframework._gbe.common.excpetion.UnableChangeStatusException;
import egovframework._gbe.common.message.MessageDto;
import egovframework._gbe.common.message.MessageService;
import egovframework._gbe.common.parameter.MailParam;
import egovframework._gbe.common.util.CommonUtils;
import egovframework._gbe.common.util.MailUtils;
import egovframework._gbe.common.util.SecurityUtils;
import egovframework._gbe.domain.log.SystemName;
import egovframework._gbe.domain.user.SessionUser;
import egovframework._gbe.service.common.CommonService;
import egovframework._gbe.web.dto.msg.NoticeMessengerDto;
import javak.crypto.SecretKey;
import javak.crypto.spec.IvParameterSpec;
import ksign.jce.provider.pkcs.EnvelopedData;
import ksign.jce.provider.pkcs.SignedData;
import ksign.jce.provider.pkcs.VID;
import ksign.jce.provider.validate.ValidateCert;
import ksign.jce.util.Base64;
import ksign.jce.util.JCEUtil;
import lombok.RequiredArgsConstructor;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.security.cert.Certificate;

@RestController
@RequiredArgsConstructor
@RequestMapping("/common")
public class CommonApiController {
    private final CommonService commonService;
    private final MessageService messageService;
    @PostMapping("/selCmmnCode")
    public ResponseEntity selCmmnCode(@RequestBody Map<String, String> map) {

        String clCode = map.get("clCode");
        Map resultMap = new HashMap<>();

        if(clCode != null){
            resultMap = commonService.selCmmnCode(clCode);
        } else {
            return ResponseEntity.ok("fail");
        }

        return ResponseEntity.ok(resultMap);
    }

    @PostMapping("/mail/certify")
    public ResponseEntity mail(@RequestBody MailParam mailMap) {
        String num = MailUtils.certifyMail(mailMap);
        return ResponseEntity.ok(num);
    }

    @PostMapping("/selUpperOrgByOrgCd")
    public ResponseEntity selUpperOrgByOrgCd(@RequestBody Map paramMap){

        Map resultMap = new HashMap<>();
        List resultList = new ArrayList();
        String orgCd = (String) paramMap.get("orgCd");
        if(orgCd != null){
            resultList = commonService.selUpperOrgByOrgCd(orgCd);
            if(resultMap != null){
                resultMap.put("result", "S");
                resultMap.put("data", resultList);
            } else {
                resultMap.put("result", "F");
                resultMap.put("msg", "조회결과가 없습니다.");
            }
        } else {
            resultMap.put("result", "S");
//            resultMap.put("msg", "파라미터를 확인해주세요.");
        }

        return ResponseEntity.ok(resultMap);
    }

    @PostMapping("/selAduOrgCd")
    public ResponseEntity selAduOrgCd(@RequestBody Map<String, String> param) {
        List<Map<String, String>> list = commonService.selAduOrgCd();
        return ResponseEntity.ok(list);
    }

    @PostMapping("/selNomalOrgCd")
    public ResponseEntity selNomalOrgCd(@RequestBody Map<String, String> param) {
        List<Map<String, String>> list = commonService.selNomalOrgCd(param);
        return ResponseEntity.ok(list);
    }

    @PostMapping("/selMainOrgCd")
    public ResponseEntity selMainOrgCd(@RequestBody Map<String, String> param) {
        List<Map<String, String>> list = commonService.selMainOrgCd(param);
        return ResponseEntity.ok(list);
    }

    @PostMapping("/mbtlnum/certify")
    public ResponseEntity certify(@RequestBody Map<String, String> param) throws Exception {
        String random = CommonUtils.generateRandomNum(6);

        String msg = String.format("[충청남도 교육청 장애관리 시스템] 인증번호 [%s]를 입력해주세요.", random);

        List<Map<String, String>> smsList = new ArrayList<>();
        Map<String, String> smsMap = new HashMap<>();
        smsMap.put("name", param.get("name"));
        smsMap.put("phone", param.get("mbtlnum").replaceAll("-",""));
        smsList.add(smsMap);

        messageService.sendMsg("", smsList, "", "[충청남도교육청]인증문자", msg, "0", "");

        return ResponseEntity.ok(random);
    }

    @SystemName(name = "공통", detail = "기타")
    @PostMapping("/sessionUserChk")
    public ResponseEntity sessionUserChk() throws UnableChangeStatusException {

        boolean result = commonService.sessionUserChk();

        return ResponseEntity.ok(result);
    }

    @SystemName(name = "공통", detail = "기타")
    @PostMapping("/sessionUserChg")
    public ResponseEntity sessionUserChg() {

        commonService.sessionUserChg();

        return ResponseEntity.ok(true);
    }

    @SystemName(name = "공통", detail = "기타")
    @PostMapping("/initPKI")
    public ResponseEntity initPKI(HttpServletRequest req) throws IOException {

        Map resultMap = new HashMap<>();
        HttpSession m_Session = req.getSession();
        String sessionID = m_Session.getId();

        String OS = System.getProperty("os.name").toLowerCase();
        String temp_file_path = (OS.indexOf("win") >=0) ? "C:/GPKI/Certificate/class1/" : "/home/ksign/cert/GPKI/";
        String certPath = temp_file_path + "SVRZ1234567094_env.cer";
        System.out.println("==============================초기화시작==========");
        RandomAccessFile f = new RandomAccessFile(certPath, "r");
        byte[] b = new byte[(int)f.length()];
        f.readFully(b);
        String serverEnvCert = new String(Base64.encode(b));
        System.out.println("==============================초기화끝==========");
        resultMap.put("sessionID", sessionID);
        resultMap.put("serverEnvCert", serverEnvCert);

        return ResponseEntity.ok(resultMap);
    }

    @SystemName(name = "공통", detail = "기타")
    @PostMapping("/getPKIInfo")
    public ResponseEntity getPKIInfo(@RequestBody Map<String, String> token) throws UnableChangeStatusException {

        SessionUser loginUser = SecurityUtils.getLoginUser();

        boolean admChk = false;
        String OS = System.getProperty("os.name").toLowerCase();
        String temp_file_path = (OS.indexOf("win") >=0) ? "C:/GPKI/Certificate/class1/" : "/home/ksign/cert/GPKI/";

        final String ENV_CER = temp_file_path + "SVRZ1234567094_env.cer";
        final String ENV_KEY = temp_file_path + "SVRZ1234567094_env.key";
        final String SIG_CER = temp_file_path + "SVRZ1234567094_env.cer";
        final String SIG_KEY = temp_file_path + "SVRZ1234567094_env.cer";

        final String CERT_PW = "epkiTest!2";

        final String licensePath = (OS.indexOf("win") >= 0) ? "C:/Window/system32" : "/usr/local/KCASE/License";

        String tokenValue = token.get("token");

        Map resultMap = new HashMap<>();

        String dn = "";
        String issuerDN = "";
        String serial = "";

        try {
            JCEUtil.initProvider();
            // 서버 인증서 정보를 가져옵니다.

            X509Certificate cert = (X509Certificate)JCEUtil.readCertificate(ENV_CER);
            PrivateKey privateKey = JCEUtil.readPrivateKey(CERT_PW, ENV_KEY);

            // 채널보안 로그인 동시수행 메시지에서 로그인 요청 메시지를 읽어옵니다.
            EnvelopedData envelopedData = new EnvelopedData();
            byte[] decBytes = envelopedData.decrypt(Base64.decode(tokenValue), cert, privateKey);

            SignedData signedData = new SignedData();
            signedData.verify(Base64.decode(decBytes));

            byte[] decryptedLoginMsg = signedData.getMessage();

            // JSON Parse Start
            JSONParser parser = new JSONParser();
            JSONObject reqJsonData = (JSONObject)parser.parse(new String(decryptedLoginMsg));

            // R값과 VID 값을 추출
            String rn = (String)reqJsonData.get("RN");
            String vid = (String)reqJsonData.get("VID");

            // 인증 요청 메시지로부터 클라이언트 인증서를 획득합니다.
            X509Certificate loginCert = (X509Certificate)signedData.getSignerCert(0);
            dn = loginCert.getSubjectDN().getName();
            issuerDN = loginCert.getIssuerDN().getName();
            serial = loginCert.getSerialNumber().toString(16);

            // 요청자 VID 정보가 포함될 경우 신원확인을 합니다.
            if (vid != null) {
                if (!VID.validCertVID(loginCert, Base64.decode(rn), vid.toCharArray()))
                    resultMap.put("result", "F");
                resultMap.put("msg", "VID 검증실패");
                //result = "VID 검증실패";
            }

            // 채널보안에서 사용할 키와 IV를 가져옵니다.
            SecretKey _key = envelopedData.getSecretKey();
            IvParameterSpec _iv = envelopedData.getIvSpec();

            // 인증서를 검증하기 위해 CertValidator 객체를 생성합니다.
            X509Certificate certificate = (X509Certificate)JCEUtil.readCertificate(ENV_CER);
            ValidateCert validator = new ValidateCert();
            validator.setCertPathOption(ValidateCert.CERT_PATH_CA | ValidateCert.CERT_PATH_ROOT);
            validator.setRevokeOption(ValidateCert.CERT_REVOKE_CRL);

            Certificate[] certs = new Certificate[] {certificate};
            //validator.setLocalSaveCRL(true);
            validator.validateCertificate(certs, ValidateCert.KCE_SN_CERT);


        } catch (Exception e) {
            resultMap.put("result", "F");
            resultMap.put("msg", e.toString());
            System.out.println("==============================초기화==========");
            System.out.println(e.toString());
            System.out.println("==============================초기화==========");
            //result = e.toString();
        }

        JSONObject jsonObj = new JSONObject();
        /*
        jsonObj.put("result", result);
        jsonObj.put("dn", dn);
        jsonObj.put("issuerDN", issuerDN);
        */
        jsonObj.put("serial", serial);

        admChk = commonService.sessionUserChk();

        if(admChk){
            String dbSerial = commonService.selAhrztNoVal(loginUser.getUserId());
            if(serial.equals(dbSerial)){
                resultMap.put("result", "S");
                resultMap.put("userSe", "adm");
            } else {
                resultMap.put("result", "F");
                resultMap.put("msg", "본인 확인에 실패했습니다.");
            }
        } else {
            resultMap.put("result", "S");
            resultMap.put("userSe", "mber");
        }

        return ResponseEntity.ok(resultMap);
    }
}
