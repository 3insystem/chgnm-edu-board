package egovframework._gbe.web.api.user;

import egovframework._gbe.common.excpetion.UnableChangeStatusException;
import egovframework._gbe.common.util.CryptUtils;
import egovframework._gbe.common.util.SecurityUtils;
import egovframework._gbe.domain.code.CmmnCode;
import egovframework._gbe.domain.log.SystemName;
import egovframework._gbe.domain.user.PrivateSessionUser;
import egovframework._gbe.domain.user.SessionUser;
import egovframework._gbe.domain.user.User;
import egovframework._gbe.service.common.CommonService;
import egovframework._gbe.service.user.UserService;
import egovframework._gbe.web.dto.user.InsttUpdate;
import egovframework._gbe.web.dto.user.UserUpdate;
import egovframework._gbe.web.dto.user.cmpny.CmpnyDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserApiController {

    private final CommonService commonService;
    private final UserService userService;
    private final CryptUtils cryptUtils;

    @SystemName(name = "회원관리", detail = "기타")
    @PostMapping("/getSessionUser")
    public ResponseEntity getSessionUser() {
        SessionUser sessionUser = SecurityUtils.getLoginUser();
        PrivateSessionUser user = new PrivateSessionUser(sessionUser);
        return ResponseEntity.ok(user);
    }

    @SystemName(name = "회원관리", detail = "기타")
    @PostMapping("/findBizrno")
    public ResponseEntity findBizrno(@RequestBody Map<String, String> map) throws Exception {
        String bizrno = map.get("bizrno");
        Map resultMap = new HashMap<>();
        if(bizrno != null){
            CmpnyDto dto = userService.selectCmpny(bizrno);

            if(dto != null){
                resultMap.put("result", "S");
                resultMap.put("dto", dto);
            } else {
                resultMap.put("result", "B");
            }
        } else {
            resultMap.put("result", "F");
        }
        return ResponseEntity.ok(resultMap);
    }

    @SystemName(name = "회원관리", detail = "수정")
    @PostMapping("/{userId}")
    public ResponseEntity update(@PathVariable String userId,
                                 @RequestPart UserUpdate userUpdate,
                                 @RequestPart(required = false) MultipartFile file) throws IOException {
        User user = userService.update(userUpdate, file);

        CmmnCode rspofCmmnCode;
        if(userUpdate.getPosition() != null && !"".equals(userUpdate.getPosition())){
            rspofCmmnCode = commonService.findById(userUpdate.getPosition());
            user.setRspoFcSe(rspofCmmnCode);
        }

        SessionUser loginUser = SecurityUtils.getLoginUser();
        loginUser.setUserNm(cryptUtils.decrypt(user.getUserNm()));
        loginUser.setBizrno(user.getCmpny().getBizrno());
        loginUser.setUseSe(user.getUseSe().getCode());
        loginUser.setChangeAt();

        return ResponseEntity.ok(user.getUsid());
    }

    @SystemName(name = "회원관리", detail = "수정")
    @PostMapping("/instt/{userId}")
    public ResponseEntity update(@PathVariable String userId, @RequestBody InsttUpdate updateData) {
        User user = userService.insttUpdate(userId, updateData);

        SessionUser loginUser = SecurityUtils.getLoginUser();
        loginUser.setChangeAt();

        return ResponseEntity.ok(user.getUsid());
    }

    @SystemName(name = "회원관리", detail = "수정")
    @PostMapping({"/mypage/rep", "/mypage/emp"})
    public ResponseEntity update(@RequestPart UserUpdate userUpdate,
                                 @RequestPart(required = false) MultipartFile file) throws IOException {
        User user = userService.update(userUpdate, file);

        CmmnCode rspofCmmnCode;
        if(userUpdate.getPosition() != null && !"".equals(userUpdate.getPosition())){
            rspofCmmnCode = commonService.findById(userUpdate.getPosition());
            user.setRspoFcSe(rspofCmmnCode);
        }

        SessionUser loginUser = SecurityUtils.getLoginUser();
        loginUser.setUserNm(cryptUtils.decrypt(user.getUserNm()));
        loginUser.setBizrno(user.getCmpny().getBizrno());
        loginUser.setUseSe(user.getUseSe().getCode());
        loginUser.setChangeAt();

        return ResponseEntity.ok(user.getUsid());
    }
}
