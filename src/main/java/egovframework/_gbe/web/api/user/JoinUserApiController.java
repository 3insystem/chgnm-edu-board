package egovframework._gbe.web.api.user;

import egovframework._gbe.domain.log.SystemName;
import egovframework._gbe.domain.user.User;
import egovframework._gbe.service.user.UserService;
import egovframework._gbe.web.dto.user.UserSave;
import egovframework._gbe.web.dto.user.cmpny.BizrnoCheckDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/join")
@RequiredArgsConstructor
public class JoinUserApiController {

    private final UserService userService;

    @PostMapping("/idCheck")
    public ResponseEntity idCheck(@RequestBody Map<String, String> map) {
        if (map.isEmpty()) {
            return ResponseEntity.ok("B");
        }

        String flag = userService.idCheck(map.get("userId"));
        return ResponseEntity.ok(flag);
    }

    @PostMapping("/bizrnoCheck")
    public Map bizrnoCheck(@RequestBody Map<String, String> map) {

        String bizrno = map.get("bizrno");
        Map resultMap = new HashMap<>();
        if(bizrno != null){
            BizrnoCheckDto dto = userService.selectCmpnyBizrno(bizrno);

            if(dto != null){
                resultMap.put("result", "S"); // 성공
                resultMap.put("dto", dto);
            } else {
                resultMap.put("result", "B"); //값이 없음
            }
        } else {
            resultMap.put("result", "F"); // 실패
        }
        return resultMap;
    }

    /**
     * 업체 대표 회원가입
     * */
    @SystemName(name = "회원관리", detail = "기타")
    @PostMapping(value = "/step3/saveFile")
    public ResponseEntity saveFile(@Valid UserSave userSave) throws Exception {
        User user = userService.save(userSave, "USESE00002");
        return ResponseEntity.ok(user.getUsid());
    }

    /**
     * 업체 직원 회원가입 -> 직원은 사용자구분이 일반으로 default, 대표가 업무 승인시 직원으로 변경됨
     * */
    @SystemName(name = "회원관리", detail = "기타")
    @PostMapping("/step3/save")
    public ResponseEntity save(@RequestBody UserSave saveData) throws Exception {
        User user = userService.save(saveData, "USESE00004");
        return ResponseEntity.ok(user.getUsid());
    }
}
