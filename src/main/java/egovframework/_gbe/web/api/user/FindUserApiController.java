package egovframework._gbe.web.api.user;

import egovframework._gbe.domain.log.SystemName;
import egovframework._gbe.domain.user.User;
import egovframework._gbe.service.user.UserService;
import egovframework._gbe.web.dto.user.FindDto;
import egovframework._gbe.web.dto.user.LoginDto;
import egovframework._gbe.web.dto.user.UserInfo;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/find")
@RequiredArgsConstructor
public class FindUserApiController {

    private final UserService userService;

/*
    @SystemName(name = "회원관리", detail = "기타")
    @PostMapping("/emailCheck")
    public ResponseEntity emailCheck(@RequestBody FindDto findDto) {
        UserInfo user = userService.findByEmail(findDto);

        String type = "N";
        if (user != null) type = "Y";

        return ResponseEntity.ok(type);
    }
*/

    @SystemName(name = "회원관리", detail = "기타")
    @PostMapping("/mbtlnumCheck")
    public ResponseEntity mbtlnumCheck(@RequestBody FindDto findDto) {
        UserInfo user = userService.findByMbtlnum(findDto);

        String type = "N";
        if (user != null) type = "Y";

        return ResponseEntity.ok(type);
    }

    @SystemName(name = "회원관리", detail = "기타")
    @PostMapping("/findId")
    public ResponseEntity findId(@RequestBody FindDto findDto) {
        UserInfo user = userService.findByMbtlnum(findDto);
        String usid = user.getUsid().replaceAll("_CMPNY!", "");
        return ResponseEntity.ok(usid);
    }

    @SystemName(name = "회원관리", detail = "기타")
    @PostMapping("/reset")
    public ResponseEntity passwordReset(@RequestBody LoginDto loginDto) {
        User user = userService.passwordReset(loginDto);

        String type = "N";
        if (user != null) type = "Y";

        return ResponseEntity.ok(type);
    }
}
