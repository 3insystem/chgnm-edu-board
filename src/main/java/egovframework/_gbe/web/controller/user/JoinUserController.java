package egovframework._gbe.web.controller.user;

import egovframework._gbe.domain.code.CmmnCode;
import egovframework._gbe.domain.log.SystemName;
import egovframework._gbe.service.common.CommonService;
import egovframework._gbe.web.dto.user.UserSave;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping("/join")
@RequiredArgsConstructor
public class JoinUserController {

    private final CommonService commonService;

    @GetMapping("/step1")
    public String join1() {
        return "user/join/step1";
    }

    @GetMapping("/step2")
    public String join2(@RequestParam String type, Model model) {
        model.addAttribute("name", type.equals("cmpny")?"업체":"담당자");
        model.addAttribute("type", type);
        return "user/join/step2";
    }

    @GetMapping("/step3")
    public String join3(@RequestParam String type, Model model,
                        @ModelAttribute(name = "userSave") UserSave userSave) {
        model.addAttribute("name", type.equals("cmpny")?"업체":"담당자");
        model.addAttribute("type", type);
        return "user/join/" + type + "/step3";
    }

    @GetMapping("/step4")
    public String join4(@RequestParam String type, Model model) {
        return "user/join/" + type + "/step4";
    }

    @ModelAttribute("positions")
    public List<CmmnCode> positions() {
        return commonService.findAll("RSPOF");
    }
}
