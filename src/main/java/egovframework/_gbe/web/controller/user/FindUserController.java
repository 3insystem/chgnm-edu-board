package egovframework._gbe.web.controller.user;

import egovframework._gbe.domain.log.SystemName;
import egovframework._gbe.service.user.UserService;
import egovframework._gbe.web.dto.user.FindDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/find")
@RequiredArgsConstructor
public class FindUserController {

    private final UserService userService;

    @SystemName(name = "회원관리", detail = "기타")
    @GetMapping("/id")
    public String findId(@ModelAttribute FindDto findDto) {
        return "user/find/modal/findId :: #form";
    }

    @SystemName(name = "회원관리", detail = "기타")
    @GetMapping("/pwd")
    public String findPwd(@ModelAttribute FindDto findDto) {
        return "user/find/modal/findPwd :: #form";
    }

}
