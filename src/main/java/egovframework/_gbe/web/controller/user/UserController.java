package egovframework._gbe.web.controller.user;

import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.common.util.SecurityUtils;
import egovframework._gbe.domain.code.CmmnCode;
import egovframework._gbe.domain.log.SystemName;
import egovframework._gbe.domain.user.SessionUser;
import egovframework._gbe.domain.user.User;
import egovframework._gbe.domain.user.cmpny.Cmpny;
import egovframework._gbe.domain.user.dsptc.Dsptc;
import egovframework._gbe.service.common.CommonService;
import egovframework._gbe.service.mber.mntmgt.MntmgtCmpnyService;
import egovframework._gbe.service.mber.rep.RepService;
import egovframework._gbe.service.user.UserService;
import egovframework._gbe.web.dto.mber.rep.SelEmpListDto;
import egovframework._gbe.web.dto.mber.rep.dashboard.DashboardCountDto;
import egovframework._gbe.web.dto.user.UserInfo;
import egovframework._gbe.web.dto.user.UserUpdate;
import io.jsonwebtoken.JwtHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static egovframework._gbe.common.util.SecurityUtils.getLoginUser;

@Controller
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {

    public final UserService userService;
    public final CommonService commonService;
    public final RepService repService;
    public final MntmgtCmpnyService mntmgtCmpnyService;

    @SystemName(name = "회원관리", detail = "수정")
    @GetMapping("/mypage")
    public String update(Model model) {
        SessionUser sessionUser = SecurityUtils.getLoginUser();
        String useSe = sessionUser.getUseSe();
        if (useSe.equals("USESE00002") || useSe.equals("USESE00003") || useSe.equals("USESE00004")) {
//            User user = userService.findById(sessionUser.getUserId());
            UserInfo userInfo = userService.getUser(sessionUser.getUserId());
            model.addAttribute("authority", getLoginUser().getUseSe());
            model.addAttribute("user", userInfo);
            model.addAttribute("menu", 9);  //메뉴정보
            if (useSe.equals("USESE00002")) {
                return "user/mypage/rep";
            } else {
                SearchParam param = new SearchParam();
                param.setWord2(userInfo.getUsid());
                param.setWord3(userInfo.getCmpny().getBizrno());
                SelEmpListDto hist = repService.selEmps(param);
                DashboardCountDto count = mntmgtCmpnyService.cmpnyDashboardCount();

                model.addAttribute("hist", hist);
                model.addAttribute("mntmgtCount", count.getAlc());

                return "user/mypage/emp";
            }
        } else {
            return "redirect:/user/instt/" + sessionUser.getUserId().replaceAll("_CMPNY!", "");
        }
    }


    @SystemName(name = "회원관리", detail = "수정")
    @GetMapping("/{userId}")
    public String userUpdate(@PathVariable String userId, Model model) throws Exception {
        UserUpdate result = userService.getUserUpdate(userId + "_CMPNY!");
        if (result.getBizrnoFilter() != null) {
            Cmpny cmpny = userService.findCmpny(result.getBizrnoFilter());
            model.addAttribute("cmpny", cmpny);
        }

        model.addAttribute("result", result);
        return "user/edit/userEdit";
    }

    @SystemName(name = "회원관리", detail = "수정")
    @GetMapping("/instt/{userId}")
    public String insttUserUpdate(@PathVariable String userId, Model model) {
        UserInfo result = userService.getUser(userId);
        if (result.getOrgCode() != null) {
            Map<String, String > orgCdMap = commonService.selOrgCdSection(result.getOrgCode().getOrgCd());
            model.addAttribute("section", orgCdMap.get("pathNm"));
        }

        String dsptcAt = "N";
        String dsptcOrgCd = "";
        for (Dsptc dsptc : result.getDsptcList()) {
            if (dsptc.getDelAt().equals("N")) {
                dsptcAt = "Y";
                String bassOrgCd = dsptc.getBassOrgCd().getOrgNm();
                String orgCd = dsptc.getOrgCd().getOrgNm();
                String dsptcSe = dsptc.getDsptcSe().getCodeNm();
                String startdt = dsptc.getDsptcBgnde();
                String enddt = dsptc.getDsptcEndde();
                dsptcOrgCd = String.format("%s > %s (%s ~ %s, %s)", bassOrgCd, orgCd, startdt, enddt, dsptcSe);
                break;
            }
        }

        model.addAttribute("dsptcAt", dsptcAt);
        model.addAttribute("dsptcOrgCd", dsptcOrgCd);
        model.addAttribute("result", result);
        return "user/edit/insttEdit";
    }

    @ModelAttribute("positions")
    public List<CmmnCode> positions() {
        return commonService.findAll("RSPOF");
    }
}
