package egovframework._gbe.web.controller.mber.usr;

import egovframework._gbe.domain.log.SystemName;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/mber/usr")
@RequiredArgsConstructor
public class MberUserController {

    @SystemName(name = "대시보드", detail = "조회")
    @GetMapping("/dashboard")
    public String dashboard(Model model) {
        return "mber/user/dashboard";
    }
}
