package egovframework._gbe.web.controller.mber.rep;

import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.common.util.CommonUtils;
import egovframework._gbe.domain.log.SystemName;
import egovframework._gbe.service.mber.rep.SlcService;
import egovframework._gbe.web.dto.mber.rep.SlctnDto;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequiredArgsConstructor
@RequestMapping("/mber/rep")
public class SlcController {

    private final SlcService slcService;

    @SystemName(name = "기관(학교)현황", detail = "조회")
    @GetMapping("/slcList")
    public String selSlcList(@ModelAttribute SearchParam searchParam, Pageable pageable, Model model) {
        pageable = CommonUtils.setListCount(searchParam, pageable);
        Page<SlctnDto> selSlcList = slcService.selSlcList(searchParam, pageable);
        model.addAttribute("result", selSlcList);
        model.addAttribute("menu", 2);  //메뉴정보
        return "/mber/rep/slcList";
    }
}
