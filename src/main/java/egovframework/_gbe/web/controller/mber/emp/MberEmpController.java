package egovframework._gbe.web.controller.mber.emp;

import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.common.util.CommonUtils;
import egovframework._gbe.domain.log.SystemName;
import egovframework._gbe.service.mber.cmpds.CmpdsCmpnyService;
import egovframework._gbe.service.mber.mntmgt.MntmgtCmpnyService;
import egovframework._gbe.type.*;
import egovframework._gbe.web.dto.mber.rep.cmpds.CmpdsDetailDto;
import egovframework._gbe.web.dto.mber.rep.mntmgt.MntmgtDetailDto;
import egovframework._gbe.web.dto.mber.rep.mntmgt.MntmgtListCountDto;
import egovframework._gbe.web.dto.mber.rep.mntmgt.MntmgtListDto;
import egovframework._gbe.web.dto.mber.rep.cmpds.CmpdsListDto;
import egovframework._gbe.web.dto.mber.rep.dashboard.DashboardCmpdsDto;
import egovframework._gbe.web.dto.mber.rep.dashboard.DashboardCountDto;
import egovframework._gbe.web.dto.mber.rep.dashboard.DashboardMntmgtDto;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/mber/emp")
@RequiredArgsConstructor
public class MberEmpController {

    private final MntmgtCmpnyService mntmgtCmpnyService;
    private final CmpdsCmpnyService cmpdsCmpnyService;

    @SystemName(name = "대시보드", detail = "조회")
    @GetMapping("/dashboard")
    public String dashboard(Model model) {

        DashboardCountDto result = mntmgtCmpnyService.cmpnyDashboardCount();
        List<DashboardMntmgtDto> mntgmtlist = mntmgtCmpnyService.cmpnyDashboardMntgmt();
        //List<DashboardCmpdsDto> cmpdslist = cmpdsCmpnyService.cmpnyDashboardCmpds();

        model.addAttribute("result", result);
        model.addAttribute("mntgmtlist", mntgmtlist);
        //model.addAttribute("cmpdslist", cmpdslist);

        return "mber/cmp/dashboard";
    }

    @SystemName(name = "유지관리", detail = "목록")
    @GetMapping("/mntmgt")
    public String mngmgetList(@ModelAttribute SearchParam searchParam, Pageable pageable, Model model) {
        pageable = CommonUtils.setListCount(searchParam, pageable);

        Page<MntmgtListDto> result =  mntmgtCmpnyService.mntmgtEmpList(pageable, searchParam);
        MntmgtListCountDto count =  mntmgtCmpnyService.getEmpMntmgtListCount(searchParam);
        String useSe = mntmgtCmpnyService.cmpnyUseSe();

        model.addAttribute("result", result);
        model.addAttribute("count", count);
        model.addAttribute("useSe", useSe);

        return "mber/cmp/cmpmntmgt/list";
    }

    @SystemName(name = "유지관리", detail = "상세")
    @GetMapping("/mntmgt/detail/{rceptNo}")
    public String mntmgtDetail(@PathVariable String rceptNo, Model model) {

        MntmgtDetailDto result = mntmgtCmpnyService.mntmgtDetail(rceptNo);
        model.addAttribute("result", result);

        return "mber/cmp/cmpmntmgt/detail";
    }


//    @SystemName(name = "소모품", detail = "목록")
//    @GetMapping("/cmpds")
//    public String cmpdsList(@ModelAttribute SearchParam searchParam, Pageable pageable, Model model) {
//        pageable = CommonUtils.setListCount(searchParam, pageable);
//
//        Page<CmpdsListDto> result =  cmpdsCmpnyService.cmpdsEmpList(pageable, searchParam);
//        MntmgtListCountDto count =  cmpdsCmpnyService.getEmpCmpdsListCount(searchParam); // 수정 필요 담당자용으로
//        String useSe = mntmgtCmpnyService.cmpnyUseSe(); // 검색 창 권한별로 다르게 보여주는 용도
//
//        model.addAttribute("result", result);
//        model.addAttribute("count", count);
//        model.addAttribute("useSe", useSe);
//
//        return "mber/cmp/cmpcmpds/list";
//    }
//
//    @SystemName(name = "소모품", detail = "상세")
//    @GetMapping("/cmpds/detail/{rceptNo}")
//    public String cmpdsDetail(@PathVariable String rceptNo, Model model) {
//
//        CmpdsDetailDto result = cmpdsCmpnyService.cmpdsDetail(rceptNo);
//        model.addAttribute("result", result);
//
//        return "mber/cmp/cmpcmpds/detail";
//    }

    @ModelAttribute("selectKey2List")
    public CmpStatusType[] statusType() { return CmpStatusType.values(); }

    @ModelAttribute("selectKey3List")
    public CmpMntmgtType[] maintenanceType() { return CmpMntmgtType.values(); }

    @ModelAttribute("selectKey4List")
    public ClSeType[] clSeType() { return ClSeType.values(); }
}
