package egovframework._gbe.web.controller.mber.rep;

import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.common.util.CommonUtils;
import egovframework._gbe.domain.log.SystemName;
import egovframework._gbe.service.mber.cmpds.CmpdsCmpnyService;
import egovframework._gbe.service.mber.mntmgt.MntmgtCmpnyService;
import egovframework._gbe.service.mber.rep.RepService;
import egovframework._gbe.service.mber.report.ReportCmpnyService;
import egovframework._gbe.type.*;
import egovframework._gbe.web.dto.mber.rep.cmpds.CmpdsDetailDto;
import egovframework._gbe.web.dto.mber.rep.cmpds.CmpdsListDto;
import egovframework._gbe.web.dto.mber.rep.dashboard.DashboardCmpdsDto;
import egovframework._gbe.web.dto.mber.rep.dashboard.DashboardCountDto;
import egovframework._gbe.web.dto.mber.rep.dashboard.DashboardMntmgtDto;
import egovframework._gbe.web.dto.mber.rep.mntmgt.MntmgtDetailDto;
import egovframework._gbe.web.dto.mber.rep.mntmgt.MntmgtListCountDto;
import egovframework._gbe.web.dto.mber.rep.mntmgt.MntmgtListDto;
import egovframework._gbe.web.dto.mber.rep.SelEmpListDto;
import egovframework._gbe.web.dto.mber.rep.report.ReportDetailDto;
import egovframework._gbe.web.dto.mber.rep.report.ReportListDto;
import egovframework._gbe.web.dto.mber.rep.report.ReportOrgListDto;
import egovframework._gbe.web.dto.print.PrintReportResultDto;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/mber/rep")
@RequiredArgsConstructor
public class RepController {
    private final RepService repService;

    private final MntmgtCmpnyService mntmgtCmpnyService;
    private final CmpdsCmpnyService cmpdsCmpnyService;
    private final ReportCmpnyService reportCmpnyService;

    @SystemName(name = "대시보드", detail = "조회")
    @GetMapping("/dashboard")
    public String dashboard(Model model) {

        DashboardCountDto result = mntmgtCmpnyService.cmpnyDashboardCount();
        List<DashboardMntmgtDto> mntgmtlist = mntmgtCmpnyService.cmpnyDashboardMntgmt();
        //List<DashboardCmpdsDto> cmpdslist = cmpdsCmpnyService.cmpnyDashboardCmpds();

        model.addAttribute("result", result);
        model.addAttribute("mntgmtlist", mntgmtlist);
        //model.addAttribute("cmpdslist", cmpdslist);

        return "mber/cmp/dashboard";
    }

    @SystemName(name = "담당자(직원)관리", detail = "조회")
    @GetMapping("/empAgree")
    public String empAgree(@ModelAttribute SearchParam searchParam, Pageable pageable, Model model) {
        pageable = CommonUtils.setListCount(searchParam, pageable);
        Page<SelEmpListDto> selEmpList = repService.selEmpList(searchParam, pageable);

        model.addAttribute("result", selEmpList);
        model.addAttribute("menu", 3);  //메뉴정보
        return "/mber/rep/empAgree";
    }

    @SystemName(name = "유지관리", detail = "목록")
    @GetMapping("/mntmgt")
    public String mngmgetList(@ModelAttribute SearchParam searchParam, Pageable pageable, Model model) {
        pageable = CommonUtils.setListCount(searchParam, pageable);

        Page<MntmgtListDto> result =  mntmgtCmpnyService.mntmgtList(pageable, searchParam);
        MntmgtListCountDto count =  mntmgtCmpnyService.getRepMntmgtListCount(searchParam);
        String useSe = mntmgtCmpnyService.cmpnyUseSe();

        model.addAttribute("result", result);
        model.addAttribute("count", count);
        model.addAttribute("useSe", useSe);
        model.addAttribute("menu", 1);  //메뉴정보
        return "mber/cmp/cmpmntmgt/list";
    }

    @SystemName(name = "유지관리", detail = "상세")
    @GetMapping("/mntmgt/detail/{rceptNo}")
    public String mntmgtDetail(@PathVariable String rceptNo, Model model) {

        MntmgtDetailDto result = mntmgtCmpnyService.mntmgtDetail(rceptNo);
        model.addAttribute("result", result);
        model.addAttribute("menu", 1);  //메뉴정보
        return "mber/cmp/cmpmntmgt/detail";
    }


//    @SystemName(name = "소모품", detail = "목록")
//    @GetMapping("/cmpds")
//    public String cmpdsList(@ModelAttribute SearchParam searchParam, Pageable pageable, Model model) {
//        pageable = CommonUtils.setListCount(searchParam, pageable);
//
//        Page<CmpdsListDto> result =  cmpdsCmpnyService.cmpdsList(pageable, searchParam);
//        MntmgtListCountDto count =  cmpdsCmpnyService.getRepCmpdsListCount(searchParam);
//        String useSe = mntmgtCmpnyService.cmpnyUseSe();
//
//        model.addAttribute("result", result);
//        model.addAttribute("count", count);
//        model.addAttribute("useSe", useSe);
//
//        return "mber/cmp/cmpcmpds/list";
//    }
//
//    @SystemName(name = "소모품", detail = "상세")
//    @GetMapping("/cmpds/detail/{rceptNo}")
//    public String cmpdsDetail(@PathVariable String rceptNo, Model model) {
//
//        CmpdsDetailDto result = cmpdsCmpnyService.cmpdsDetail(rceptNo);
//        model.addAttribute("result", result);
//
//        return "mber/cmp/cmpcmpds/detail";
//    }

    @SystemName(name = "점검보고서", detail = "목록")
    @GetMapping("/report")
    public String reportList(@ModelAttribute SearchParam searchParam, Pageable pageable, Model model) {
        pageable = CommonUtils.setListCount(searchParam, pageable);

        Page<ReportListDto> result =  reportCmpnyService.reportList(pageable, searchParam);

        model.addAttribute("result", result);
        model.addAttribute("modal", new PrintReportResultDto()); /* 인쇄 모달 추가 */

        return "mber/cmp/report/report";
    }

    @SystemName(name = "점검보고서", detail = "상세")
    @GetMapping("/report/register/{reprtNo}")
    public String reportDetail(@PathVariable String reprtNo, ReportDetailDto reportDetailDto, Model model, HttpServletRequest request) {

        reportCmpnyService.detailView(reprtNo, reportDetailDto);
        List<ReportOrgListDto> orgList = reportCmpnyService.orgsaveList(reprtNo); /* 연결된 학교 목록 */
        List<String> yearList = reportCmpnyService.yearList(); /* 년도 */
        List<String> monthList = reportCmpnyService.monthList(); /* 월 */
        String search = request.getParameter("orgNm"); /* 검색 확인 */

        model.addAttribute("orgList", orgList);
        model.addAttribute("yearList", yearList);
        model.addAttribute("monthList", monthList);

        if (search != null) {
            List<MntmgtListDto> result =  reportCmpnyService.mntmgtList(reportDetailDto);
            model.addAttribute("result", result);
        } else {
            List<MntmgtListDto> result =  reportCmpnyService.getrtdetailList(reprtNo); /* 실제 저장된 점검보고서 상세 */
            model.addAttribute("result", result);
        }


        return "mber/cmp/report/reportDetail";
    }

    @SystemName(name = "점검보고서", detail = "조회")
    @GetMapping("/report/register")
    public String reportRegister(@ModelAttribute ReportDetailDto reportDetailDto, Model model) {

        reportCmpnyService.nullChk(reportDetailDto);
        List<ReportOrgListDto> orgList = reportCmpnyService.orgList(); /* 연결된 학교 목록 */
        List<String> yearList = reportCmpnyService.yearList(); /* 년도 */
        List<String> monthList = reportCmpnyService.monthList(); /* 월 */

        model.addAttribute("orgList", orgList);
        model.addAttribute("yearList", yearList);
        model.addAttribute("monthList", monthList);

        if (reportDetailDto.getOrgNm() != null && reportDetailDto.getStartDate() != null && reportDetailDto.getEndDate() != null) {
            List<MntmgtListDto> result =  reportCmpnyService.mntmgtList(reportDetailDto);
            model.addAttribute("result", result);
        }

        return "mber/cmp/report/reportDetail";
    }

    @ModelAttribute("selectKey1List")
    public ConfmSe[] confmSeList() {
        return ConfmSe.values();
    }

    @ModelAttribute("selectKey2List")
    public CmpStatusType[] statusType() { return CmpStatusType.values(); }

    @ModelAttribute("selectKey3List")
    public CmpMntmgtType[] maintenanceType() { return CmpMntmgtType.values(); }

    @ModelAttribute("selectKey4List")
    public ClSeType[] clSeType() { return ClSeType.values(); }

    @ModelAttribute("selectKey5List")
    public List<ReportOrgListDto> orgNm() { return reportCmpnyService.orgList(); } /* 점검보고서 학교명 */
}
