package egovframework._gbe.web.controller.board;

import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.common.util.SecurityUtils;
import egovframework._gbe.domain.log.SystemName;
import egovframework._gbe.domain.user.SessionUser;
import egovframework._gbe.service.board.BoardFService;
import egovframework._gbe.service.board.BoardService;
import egovframework._gbe.service.common.CommonService;
import egovframework._gbe.web.dto.board.*;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


@Controller
@RequestMapping("/{auth}/{useSeNm}/community")
@RequiredArgsConstructor

public class BoardController {
    private final BoardService boardService;
    private final BoardFService boardFService;

    @SystemName(name = "커뮤니티", detail = "커뮤니티목록")
    @GetMapping({"/{boardType}/{boardMngId}/{categoryId}", "/{boardType}/{boardMngId}/{categoryId}"})
    public String main(@ModelAttribute SearchParam searchParam, @PathVariable String boardType,
                       @PathVariable Long boardMngId,
                       @PathVariable(required = false) String categoryId,
                       @PathVariable String auth,
                       @PathVariable String useSeNm,
                       Pageable pageable, Model model) {
        List<CtgryDto> ctgryList = boardService.findCtgryByBoardMngId(boardMngId);
        String path = modelDataByType(searchParam, boardType, boardMngId, categoryId, pageable, model);

        //게시판명
        String boardTitle = boardFService.slcBoardNm(boardMngId);

        model.addAttribute("ctgryList", ctgryList);
        model.addAttribute("boardMngId", boardMngId);
        model.addAttribute("categoryId", categoryId);
        model.addAttribute("boardType", boardType);
        model.addAttribute("boardTitle", boardTitle);
        model.addAttribute("defaultPageUrl", "/" + auth + "/" + useSeNm + "/community/" + boardType + "/" + boardMngId);
        return path;
    }

    private String modelDataByType(SearchParam searchParam, String boardType, Long boardMngId, String categoryId, Pageable pageable, Model model) {
        // categoryId가 null이면 "전체" 카테고리로 인식

        switch (boardType) {
            case "gen": case "ref":
                List<BoardListDto> postList = boardService.findPostAll(searchParam, boardMngId, categoryId);
                Page<BoardListDto> result = boardService.findBoardAllByUserType(searchParam, boardMngId, categoryId, pageable);
                model.addAttribute("postList", postList);
                model.addAttribute("result", result);
                return "board/front/board/main";
            case "qna":
                List<BoardListDto> qpostList = boardService.findPostAll(searchParam, boardMngId, categoryId);
                Page<BoardListDto> qresult = boardService.findQBoardAllByUserType(searchParam, boardMngId, categoryId, pageable);
                model.addAttribute("postList", qpostList);
                model.addAttribute("result", qresult);
                return "board/front/qna/main";


            case "faq":
                Page<FaqListDto> result2 = boardService.findUserFaqAll(searchParam, boardMngId, categoryId, pageable);
                model.addAttribute("result", result2);
                return "board/front/faq/main";
            case "thu": case "new":
                Page<ThumbnailListDto> result3 = boardService.findThumbnailAll(searchParam, boardMngId, categoryId, pageable);
                model.addAttribute("result", result3);
                return "board/front/thumbnail/main";
            case "vid":
                Page<VideoListDto> result4 = boardService.findVideoAll(searchParam, boardMngId, categoryId, pageable);
                model.addAttribute("result", result4);
                return "board/front/video/main";
            case "cal":
                return "board/front/calendar/main";
            default: //일반 게시판으로 통일
                return "board/front/board/main";
        }
    }

    @SystemName(name = "커뮤니티", detail = "저장")
    @GetMapping("/{boardType}/{boardMngId}/{categoryId}/add")
    public String add(@ModelAttribute BoardSave boardSave, @PathVariable String boardType,
                      @PathVariable Long boardMngId,
                      @PathVariable String categoryId,
                      @PathVariable String auth,
                      @PathVariable String useSeNm,
                      Model model) {
        List<CtgryDto> ctgryList = boardService.findCtgryByBoardMngId(boardMngId);

        //게시판명
        String boardTitle = boardFService.slcBoardNm(boardMngId);

        model.addAttribute("ctgryList", ctgryList);
        model.addAttribute("boardMngId", boardMngId);
        model.addAttribute("categoryId", categoryId);
        model.addAttribute("boardType", boardType);
        model.addAttribute("boardTitle", boardTitle);
        model.addAttribute("defaultPageUrl", "/" + auth + "/" + useSeNm + "/community/" + boardType + "/" + boardMngId);

        return "board/front/qna/add";
    }

    @GetMapping({"/{boardType}/{boardMngId}/{categoryId}/{boardId}"})
    public String detail(@PathVariable String boardType, @PathVariable Long boardMngId,
                         @PathVariable String categoryId,
                         @PathVariable Long boardId,
                         @PathVariable String auth,
                         @PathVariable String useSeNm,
                         Model model) {
        List<CtgryDto> ctgryList = boardService.findCtgryByBoardMngId(boardMngId);

        //게시판명
        String boardTitle = boardFService.slcBoardNm(boardMngId);

        String path = detailModelByType(boardType, boardMngId, boardId, model);

        model.addAttribute("ctgryList", ctgryList);
        model.addAttribute("boardMngId", boardMngId);
        model.addAttribute("categoryId", categoryId);
        model.addAttribute("boardId", boardId);
        model.addAttribute("boardType", boardType);
        model.addAttribute("boardTitle", boardTitle);
        model.addAttribute("defaultPageUrl", "/" + auth + "/" + useSeNm + "/community/" + boardType + "/" + boardMngId);
        return path;
    }

    private String detailModelByType(String boardType, Long boardMngId, Long boardId, Model model) {
        // categoryId가 null이면 "전체" 카테고리로 인식

        //게시판명
        String boardTitle = boardFService.slcBoardNm(boardMngId);
        model.addAttribute("boardTitle", boardTitle);

        switch (boardType) {
            case "gen": case "ref":
                BoardSave result = boardService.findBoard(boardId);
                model.addAttribute("result", result);
                return "board/front/board/detail";
            case "qna":
                BoardSave qresult = boardService.findBoard(boardId);
                model.addAttribute("result", qresult);
                return "board/front/qna/detail";

//            case "faq":
//                FaqSave faqDetail = boardService.faqDetail(boardId);
//                model.addAttribute("result", faqDetail);
//                return "board/front/faq/add";

            case "thu": case "new":
                ThumbnailSave thumResult = boardService.findThumbnail(boardId);
                model.addAttribute("result", thumResult);
                return "board/front/thumbnail/detail";
//            case "new":
//                model.addAttribute("result", new BoardSave());
//                return "board/front/news/detail";
            case "vid":
                VideoSave result5 = boardService.findVideo(boardId);
                model.addAttribute("result", result5);
                return "board/front/video/detail";
            default: //일반 게시판으로 통일
                return "board/front/board/detail";
        }
    }

    @GetMapping("/calendar/{boardMngId}/{type}/{scheduleId}")
    public String detail(@PathVariable Long boardMngId, @PathVariable String type,
                         @PathVariable Long scheduleId,
                         Model model) {
        List<CtgryDto> ctgryList = boardService.findCtgryByBoardMngId(boardMngId);

        //게시판명
        String boardTitle = boardFService.slcBoardNm(boardMngId);

        ScheduleDto scheduleDto = boardService.findSchedule(type, scheduleId);

        model.addAttribute("ctgryList", ctgryList);
        model.addAttribute("scheduleDto", scheduleDto);
        model.addAttribute("boardMngId", boardMngId);
        model.addAttribute("type", type);
        model.addAttribute("boardTitle", boardTitle);

        return "board/front/calendar/edit::#form";
    }

    @GetMapping("/calendar/{boardMngId}/add/{date}")
    public String add(@ModelAttribute ScheduleDto scheduleDto, @PathVariable Long boardMngId,
                      @PathVariable String date,
                      Model model) {
        List<CtgryDto> ctgryList = boardService.findCtgryByBoardMngId(boardMngId);

        model.addAttribute("ctgryList", ctgryList);
        model.addAttribute("boardMngId", boardMngId);
        model.addAttribute("date", date);
        return "board/category/calendar/add::#form";
    }
}