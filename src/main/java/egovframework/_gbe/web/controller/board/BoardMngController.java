package egovframework._gbe.web.controller.board;

import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.common.util.CommonUtils;
import egovframework._gbe.domain.code.CmmnCode;
import egovframework._gbe.domain.log.SystemName;
import egovframework._gbe.service.board.BoardService;
import egovframework._gbe.service.common.CommonService;
import egovframework._gbe.web.dto.board.*;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/adm/edu/boardMng")
@RequiredArgsConstructor
public class BoardMngController {

    private final BoardService boardService;
    private final CommonService commonService;

    @SystemName(name = "게시판관리", detail = "목록")
    @GetMapping
    public String main(@ModelAttribute SearchParam searchParam, Pageable pageable, Model model) {
        pageable = CommonUtils.setListCount(searchParam, pageable);
        Page<BoardMngDto> result = boardService.findAll(searchParam, pageable);

        model.addAttribute("result", result);
        model.addAttribute("menu", 6);  //메뉴정보
        return "board/boardMngList";
    }

    @SystemName(name = "게시판관리", detail = "등록")
    @GetMapping("/add")
    public String add(@ModelAttribute BoardMngDto boardMngDto, Model model) {
        List<CmmnCode> codeList = commonService.findAll("BOARD");

        model.addAttribute("codeList", codeList);
        model.addAttribute("menu", 6);  //메뉴정보
        return "board/boardMngAdd";
    }

    @SystemName(name = "게시판관리", detail = "수정")
    @GetMapping("/{boardMngId}/edit")
    public String edit(@PathVariable Long boardMngId, Model model) {
        BoardMngDto result = boardService.findById(boardMngId);
        List<CtgryDto> ctgryList = boardService.findCtgryByBoardMngId(boardMngId);
        List<CmmnCode> codeList = commonService.findAll("BOARD");

        model.addAttribute("result", result);
        model.addAttribute("ctgryList", ctgryList);
        model.addAttribute("codeList", codeList);
        model.addAttribute("menu", 6);  //메뉴정보
        return "board/boardMngEdit";
    }

    @SystemName(name = "게시판관리", detail = "등록")
    @GetMapping("/{boardMngId}/copy")
    public String copy(@PathVariable Long boardMngId, Model model) {
        BoardMngDto result = boardService.findById(boardMngId);
        List<CtgryDto> ctgryList = boardService.findCtgryByBoardMngId(boardMngId);
        List<CmmnCode> codeList = commonService.findAll("BOARD");

        model.addAttribute("result", result);
        model.addAttribute("ctgryList", ctgryList);
        model.addAttribute("codeList", codeList);
        model.addAttribute("menu", 6);  //메뉴정보
        return "board/boardMngCopy";
    }

    @SystemName(name = "게시판관리", detail = "조회")
    @GetMapping("/{boardType}/{boardMngId}/{categoryId}")
    public String main(@ModelAttribute SearchParam searchParam, @PathVariable String boardType,
                       @PathVariable Long boardMngId,
                       @PathVariable String categoryId,
                       Pageable pageable, Model model) {
        List<CtgryDto> ctgryList = boardService.findCtgryByBoardMngId(boardMngId);
        String path = mainModelByType(searchParam, boardType, boardMngId, categoryId, pageable, model);

        model.addAttribute("ctgryList", ctgryList);
        model.addAttribute("boardMngId", boardMngId);
        model.addAttribute("categoryId", categoryId);
        model.addAttribute("boardType", boardType);
        model.addAttribute("menu", 6);  //메뉴정보
        model.addAttribute("defaultPageUrl", "/adm/edu/boardMng/"+boardType+"/"+boardMngId);
        return path;
    }

    private String mainModelByType(SearchParam searchParam, String boardType, Long boardMngId, String categoryId, Pageable pageable, Model model) {
        // categoryId가 null이면 "전체" 카테고리로 인식
        switch (boardType) {
            case "gen": case "qna": case "ref":
                List<BoardListDto> postList = boardService.findAPostAll(searchParam, boardMngId, categoryId);
                Page<BoardListDto> result = boardService.findBoardAll(searchParam, boardMngId, categoryId, pageable);
                model.addAttribute("postList", postList);
                model.addAttribute("result", result);
                return "board/category/board/main";
            case "faq":
                List<BoardListDto> postList2 = boardService.findAPostAll(searchParam, boardMngId, categoryId);
                Page<FaqListDto> result2 = boardService.findFaqAll(searchParam, boardMngId, categoryId, pageable);
                model.addAttribute("postList", postList2);
                model.addAttribute("result", result2);
                return "board/category/faq/main";
            case "thu": case "new":
                Page<ThumbnailListDto> result3 = boardService.findThumbnailAll(searchParam, boardMngId, categoryId, pageable);
                model.addAttribute("result", result3);
                return "board/category/thumbnail/main";
            //case "new":
            //    Page<ThumbnailListDto> result5 = boardService.findThumbnailAll(searchParam, boardMngId, categoryId, pageable);
            //    model.addAttribute("result", result5);
            //    return "board/category/news/main";
            case "vid":
                Page<VideoListDto> result4 = boardService.findVideoAll(searchParam, boardMngId, categoryId, pageable);
                model.addAttribute("result", result4);
                return "board/category/video/main";
            case "cal":
                return "board/category/calendar/main";
            default: //일반 게시판으로 통일
                return "board/category/board/main";
        }
    }

    @SystemName(name = "게시판관리", detail = "등록")
    @GetMapping("/{boardType}/{boardMngId}/{categoryId}/add")
    public String add(@PathVariable String boardType, @PathVariable Long boardMngId,
                      @PathVariable String categoryId,
                      Model model) {
        List<CtgryDto> ctgryList = boardService.findCtgryByBoardMngId(boardMngId);
        String path = addModelByTYpe(boardType, model);

        model.addAttribute("ctgryList", ctgryList);
        model.addAttribute("boardMngId", boardMngId);
        model.addAttribute("categoryId", categoryId);
        model.addAttribute("boardType", boardType);
        model.addAttribute("defaultPageUrl", "/adm/edu/boardMng/"+boardType+"/"+boardMngId);
        return path;
    }

    private String addModelByTYpe(String boardType, Model model) {
        // categoryId가 null이면 "전체" 카테고리로 인식

        switch (boardType) {
            case "gen": case "qna": case "ref":
                model.addAttribute("result", new BoardSave());
                return "board/category/board/add";
            case "faq":
                model.addAttribute("result", new FaqSave());
                return "board/category/faq/add";
            case "thu": case "new":
                model.addAttribute("result", new ThumbnailSave());
                return "board/category/thumbnail/add";
            //case "new":
            //    model.addAttribute("result", new BoardSave());
            //    return "board/category/news/add";
            case "vid":
                model.addAttribute("result", new VideoSave());
                return "board/category/video/add";
            default: //일반 게시판으로 통일
                return "board/category/board/add";
        }
    }

    @SystemName(name = "게시판관리", detail = "조회")
    @GetMapping("/{boardType}/{boardMngId}/{categoryId}/{boardId}")
    public String detail(@PathVariable String boardType, @PathVariable Long boardMngId,
                         @PathVariable String categoryId,
                         @PathVariable Long boardId,
                         Model model) {
        List<CtgryDto> ctgryList = boardService.findCtgryByBoardMngId(boardMngId);
        String path = detailModelByType(boardType, boardId, model);

        model.addAttribute("ctgryList", ctgryList);
        model.addAttribute("boardMngId", boardMngId);
        model.addAttribute("categoryId", categoryId);
        model.addAttribute("boardId", boardId);
        model.addAttribute("boardType", boardType);
        model.addAttribute("defaultPageUrl", "/adm/edu/boardMng/"+boardType+"/"+boardMngId);
        return path;
    }

    private String detailModelByType(String boardType, Long boardId, Model model) {
        // categoryId가 null이면 "전체" 카테고리로 인식

        switch (boardType) {
            case "gen": case "qna": case "ref":
                BoardSave result = boardService.findBoard(boardId);
                model.addAttribute("result", result);
                return "board/category/board/detail";
            case "faq":
                FaqSave faqDetail = boardService.faqDetail(boardId);
                model.addAttribute("result", faqDetail);
                return "board/category/faq/add";
            case "thu": case "new":
                ThumbnailSave thumResult = boardService.findThumbnail(boardId);
                model.addAttribute("result", thumResult);
                return "board/category/thumbnail/detail";
          //  case "new":
          //    model.addAttribute("result", new BoardSave());
          //    return "board/category/news/detail";
            case "vid":
                VideoSave result5 = boardService.findVideo(boardId);
                model.addAttribute("result", result5);
                return "board/category/video/detail";
            default: //일반 게시판으로 통일
                return "board/category/board/detail";
        }
    }

    @SystemName(name = "게시판관리", detail = "수정")
    @GetMapping("/{boardType}/{boardMngId}/{categoryId}/{boardId}/edit")
    public String edit(@PathVariable String boardType, @PathVariable Long boardMngId,
                       @PathVariable String categoryId,
                       @PathVariable Long boardId,
                       Model model) {
        List<CtgryDto> ctgryList = boardService.findCtgryByBoardMngId(boardMngId);
        String path = editModelByType(boardType, boardId, model);

        model.addAttribute("ctgryList", ctgryList);
        model.addAttribute("boardMngId", boardMngId);
        model.addAttribute("categoryId", categoryId);
        model.addAttribute("boardId", boardId);
        model.addAttribute("boardType", boardType);
        model.addAttribute("defaultPageUrl", "/adm/edu/boardMng/"+boardType+"/"+boardMngId);
        return path;
    }

    private String editModelByType(String boardType, Long boardId, Model model) {
        // categoryId가 null이면 "전체" 카테고리로 인식

        switch (boardType) {
            case "gen": case "qna": case "ref":
                BoardSave result = boardService.findBoard(boardId);
                model.addAttribute("result", result);
                return "board/category/board/edit";
            case "faq":
                model.addAttribute("result", new BoardSave());
                return "board/category/faq/edit";
            case "thu": case "new":
                ThumbnailSave result3 = boardService.findThumbnail(boardId);
                model.addAttribute("result", result3);
                return "board/category/thumbnail/edit";
            //case "new":
            //    model.addAttribute("result", new BoardSave());
            //    return "board/category/news/edit";
            case "vid":
                VideoSave result5 = boardService.findVideo(boardId);
                model.addAttribute("result", result5);
                return "board/category/video/edit";
            default: //일반 게시판으로 통일
                return "board/category/board/edit";
        }
    }

    @SystemName(name = "게시판관리", detail = "수정")
    @GetMapping("/{boardType}/{boardMngId}/transport")
    public String transport(@PathVariable String boardType, @PathVariable Long boardMngId, Model model) {
        List<Map<String, String>> list = boardService.findBoardMngByBoardType(boardType);
        model.addAttribute("result", list);

        if (boardType.equals("cal")) {
            return "board/category/calendar/transport::#form";
        }

        return "board/modal/transport::#form";
    }

    @SystemName(name = "게시판관리", detail = "조회")
    @GetMapping("/calendar/{boardMngId}/{type}/{scheduleId}")
    public String scheduleDetail(@RequestParam(name = "mngType", required = false) String mngType, @PathVariable Long boardMngId,
                         @PathVariable String type, @PathVariable Long scheduleId,
                         Model model) {
        List<CtgryDto> ctgryList = boardService.findCtgryByBoardMngId(boardMngId);
        ScheduleDto scheduleDto;

        if(mngType == null){
            scheduleDto = boardService.findSchedule(type, scheduleId);
        } else {
            Map searchMap = new HashMap();
            searchMap.put("mngType", mngType);
            searchMap.put("scheduleId", scheduleId);
            scheduleDto = boardService.findDelSchedule(type, searchMap);
        }

        model.addAttribute("ctgryList", ctgryList);
        model.addAttribute("scheduleDto", scheduleDto);
        model.addAttribute("boardMngId", boardMngId);
        model.addAttribute("mngType", mngType);
        model.addAttribute("type", type);
        return "board/category/calendar/edit::#form";
    }

    @SystemName(name = "게시판관리", detail = "등록")
    @GetMapping("/calendar/{boardMngId}/add/{date}")
    public String add(@ModelAttribute ScheduleDto scheduleDto, @PathVariable Long boardMngId,
                      @PathVariable String date,
                      Model model) {
        List<CtgryDto> ctgryList = boardService.findCtgryByBoardMngId(boardMngId);

        model.addAttribute("ctgryList", ctgryList);
        model.addAttribute("boardMngId", boardMngId);
        model.addAttribute("date", date);
        return "board/category/calendar/add::#form";
    }

    @SystemName(name = "게시판관리", detail = "조회")
    @GetMapping("/holiday")
    public String holiday(SearchParam searchParam ,Model model) {
        List<HolidayListDto> result = boardService.holidayList(searchParam);

        model.addAttribute("result",result);
        return "board/category/holiday/main";
    }

    @SystemName(name = "게시판관리", detail = "등록")
    @GetMapping("/holiday/add")
    public String holidayAdd(Model model) {
        HolidaySave result = new HolidaySave();
        result.setStartDateChk("false");

        model.addAttribute("result", result);
        return "board/category/holiday/add::#form";
    }

    @SystemName(name = "게시판관리", detail = "수정")
    @GetMapping("/holiday/add/{boardId}")
    public String holidayUpdate(Model model, @PathVariable Long boardId) {
        HolidaySave result = boardService.holidayDetail(boardId);

        model.addAttribute("result", result);
        return "board/category/holiday/add::#form";
    }

    @SystemName(name = "게시판관리", detail = "조회")
    @GetMapping("/del/{boardType}/{boardMngId}/{categoryId}")
    public String delList(@ModelAttribute SearchParam searchParam, @PathVariable String boardType,
                          @PathVariable Long boardMngId,
                          @PathVariable String categoryId,
                          Pageable pageable, Model model) {

        searchParam.setSelectKey1("del");
        List<CtgryDto> ctgryList = boardService.findCtgryByBoardMngId(boardMngId);
        String path = mainModelByType(searchParam, boardType, boardMngId, categoryId, pageable, model);

        model.addAttribute("mngType", "del");
        model.addAttribute("ctgryList", ctgryList);
        model.addAttribute("boardMngId", boardMngId);
        model.addAttribute("categoryId", categoryId);
        model.addAttribute("boardType", boardType);
        model.addAttribute("defaultPageUrl", "/adm/edu/boardMng/del/"+boardType+"/"+boardMngId);
        return path;
    }

    @SystemName(name = "게시판관리", detail = "조회")
    @GetMapping("del/{boardType}/{boardMngId}/{categoryId}/{boardId}")
    public String delDetail(@PathVariable String boardType, @PathVariable Long boardMngId,
                         @PathVariable String categoryId,
                         @PathVariable Long boardId,
                         Model model) {
        List<CtgryDto> ctgryList = boardService.findCtgryByBoardMngId(boardMngId);
        String path = detailModelByType(boardType, boardId, model);

        model.addAttribute("mngType", "del");
        model.addAttribute("ctgryList", ctgryList);
        model.addAttribute("boardMngId", boardMngId);
        model.addAttribute("categoryId", categoryId);
        model.addAttribute("boardId", boardId);
        model.addAttribute("boardType", boardType);
        model.addAttribute("defaultPageUrl", "/adm/edu/boardMng/del/"+boardType+"/"+boardMngId);
        return path;
    }
}
