package egovframework._gbe.web.controller.equipment.cmpds;

import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.domain.log.SystemName;
import egovframework._gbe.service.common.CommonService;
import egovframework._gbe.service.equipment.EquipmentCommonService;
import egovframework._gbe.web.dto.common.CmmnCodeDto;
import egovframework._gbe.web.dto.equipment.EquipmentDetailDto;
import egovframework._gbe.web.dto.equipment.EquipmentListDto;
import egovframework._gbe.web.dto.equipment.EquipmentSave;
import egovframework._gbe.web.dto.user.cmpny.CmpnyDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.stream.Collectors;

import static egovframework._gbe.common.util.CommonUtils.setListCount;
import static egovframework._gbe.common.util.SecurityUtils.getLoginUser;

@Controller
@RequestMapping("/{auth}/{useSeNm}/cmpds")
public class CmpdsController {
    private final EquipmentCommonService equipmentCommonService;
    private final CommonService commonService;

    @Autowired
    public CmpdsController(@Qualifier("CmpdsService") EquipmentCommonService equipmentCommonService, CommonService commonService) {
        this.equipmentCommonService = equipmentCommonService;
        this.commonService = commonService;
    }

    @ModelAttribute("expendablesType")
    public List<CmmnCodeDto> expendablesType() {
        return commonService.findAll("CMPDS").stream()
                .map(CmmnCodeDto::entityToDto)
                .collect(Collectors.toList());
    }

    @ModelAttribute("statusType")
    public List<CmmnCodeDto> statusType() {
        return commonService.findAll("STTUS").stream()
                .map(CmmnCodeDto::entityToDto)
                .collect(Collectors.toList());
    }


    @GetMapping
    @SystemName(name = "소모품", detail = "목록")
    public String cmpdsList(Pageable pageable, @ModelAttribute SearchParam searchParam, Model model) {
        Page<EquipmentListDto> result = equipmentCommonService.getEquipmentDatas(setListCount(searchParam, pageable), searchParam);
        model.addAttribute("result", result);
        model.addAttribute("searchParam", searchParam);
        model.addAttribute("authority", getLoginUser().getAuth());
        return "/equipment/cmpds/list";
    }


    @GetMapping("/detail/{rceptNo}")
    @SystemName(name = "소모품", detail = "상세")
    public String detail(@PathVariable String rceptNo, Model model) {
        EquipmentDetailDto result = equipmentCommonService.getEquipmentDataDetail(rceptNo);
        model.addAttribute("result", result);
        model.addAttribute("userId", getLoginUser().getUserId());
        model.addAttribute("authority", getLoginUser().getAuth());
        return "/equipment/cmpds/detail";
    }


    @GetMapping("/register")
    @SystemName(name = "소모품", detail = "조회")
    public String register(Model model) {
        List<CmpnyDto> compnys = equipmentCommonService.getCompanyInstitutionsMatching(null);
        model.addAttribute("cmpds", new EquipmentSave());
        model.addAttribute("compnys", compnys);
        return "/equipment/cmpds/save";
    }

    @GetMapping("/update/{rceptNo}")
    @SystemName(name = "소모품", detail = "조회")
    public String update(@PathVariable String rceptNo, Model model) {
        EquipmentSave equipmentSave = equipmentCommonService.getEquipmentData(rceptNo);
        List<CmpnyDto> compnys = equipmentCommonService.getCompanyInstitutionsMatching(rceptNo);
        model.addAttribute("cmpds", equipmentSave);
        model.addAttribute("compnys", compnys);
        return "/equipment/cmpds/save";
    }

}
