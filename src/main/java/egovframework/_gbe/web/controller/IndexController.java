package egovframework._gbe.web.controller;

import egovframework._gbe.common.message.MessageService;
import egovframework._gbe.common.util.CryptUtils;
import egovframework._gbe.domain.log.SystemName;
import egovframework._gbe.service.common.CommonService;
import egovframework._gbe.service.DashboardService;
import egovframework._gbe.web.dto.common.CmmnCodeDto;
import egovframework._gbe.web.dto.dashboard.DashBoardDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
@RequiredArgsConstructor
@RequestMapping("/{auth}/{useSeNm}/dashboard")
public class IndexController {

    private final DashboardService dashboardService;
    private final CommonService commonService;
    private final MessageService messageService;
    private final CryptUtils cryptUtils;

    @ModelAttribute("maintenanceType")
    public List<CmmnCodeDto> maintenanceType() {
        return commonService.findAll("MNTMG").stream()
                .map(CmmnCodeDto::entityToDto)
                .collect(Collectors.toList());
    }

    @ModelAttribute("expendablesType")
    public List<CmmnCodeDto> expendablesType() {
        return commonService.findAll("CMPDS").stream()
                .map(CmmnCodeDto::entityToDto)
                .collect(Collectors.toList());
    }

    @GetMapping
    @SystemName(name = "대시보드", detail = "조회")
    public String dashboard(@PathVariable String auth, @PathVariable String useSeNm, Model model) throws Exception {
        DashBoardDto result = dashboardService.getDashBoard();
        model.addAttribute("result", result);

//        String a = "이현승";
//        String enc = cryptUtils.encrypt(a);
//        System.out.println("enc = " + enc);
//        String dec = cryptUtils.decrypt(enc);
//        System.out.println("dec = " + dec);

        /*
        List<Map<String, String>> list = new ArrayList<>();
        Map<String, String> map = new HashMap<>();
        map.put("name", "이름");
        map.put("phone", "01055555555");
        list.add(map);

        messageService.sendMsg(list, "edumin1", "제목", "내용", "0", "");
        */
        return "/dashboard";
    }
}
