package egovframework._gbe.web.controller.adm.stats;

import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.domain.log.SystemName;
import egovframework._gbe.service.adm.stats.StatsService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.time.LocalDateTime;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;

@Controller
@RequiredArgsConstructor
public class StatsController {

    public final StatsService statsService;

    @SystemName(name = "통계", detail = "조회")
    @GetMapping("/adm/{useSeNm}/stats")
    public String stats(@ModelAttribute SearchParam searchParam, Model model) throws Exception{
        //데이터가 들어있는 년도 + 현재 년도
        ArrayList<String> yearList = statsService.selYear();
        //1월 ~ 현재 월 까지
        ArrayList<String> monthList = new ArrayList<>();
        int nowMonth = LocalDateTime.now().getMonthValue();
        for (int i = 0; i < nowMonth; i ++){
            monthList.add(String.format("%02d", (i+1)));
        }

        String year = String.valueOf(LocalDateTime.now().getYear());
        String month = String.valueOf(nowMonth);

        searchParam.setSel0(year);
        searchParam.setSel1(month);
        searchParam.setSelectList(yearList);
        model.addAttribute("monthList", monthList);
        model.addAttribute("menu", 10);  //메뉴정보
        searchParam.setStartDate(LocalDateTime.now().with(TemporalAdjusters.firstDayOfMonth()).toLocalDate());
        searchParam.setEndDate(LocalDateTime.now().toLocalDate());

        return "adm/stats/detail";

    }
}
