package egovframework._gbe.web.controller.adm.dep;

import egovframework._gbe.common.excpetion.UnableChangeStatusException;
import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.common.util.CommonUtils;
import egovframework._gbe.domain.log.SystemName;
import egovframework._gbe.service.adm.dep.DepService;
import egovframework._gbe.service.common.CommonService;
import egovframework._gbe.web.dto.adm.dep.DepListDto;
import egovframework._gbe.web.dto.adm.dep.DepMatchSelDto;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Map;

@Controller
@RequiredArgsConstructor
public class DepController {

    private final CommonService commonService;
    private final DepService depService;

    @SystemName(name = "기관(학교)관리", detail = "조회")
    @GetMapping("/adm/{useSeNm}/dep")
    public String depList(@PathVariable String useSeNm, @ModelAttribute SearchParam searchParam, Pageable pageable, Model model) throws Exception{

        if(!("edu".equals(useSeNm) || "sup".equals(useSeNm))){
            throw new UnableChangeStatusException("잘못된 접근입니다.");
        }

        pageable = CommonUtils.setListCount(searchParam, pageable);

        Page<DepListDto> result = depService.depList(searchParam, pageable);

        model.addAttribute("menu", 2);  //메뉴정보
        model.addAttribute("result", result);

        return "adm/dep/list";
    }

    @SystemName(name = "기관(학교)관리", detail = "기타")
    @GetMapping("/adm/{useSeNm}/dep/match")
    public String match(@ModelAttribute DepMatchSelDto depMatchSelDto) {

        List<Map<String, String>> supList = commonService.selAduOrgCd();

        depMatchSelDto.setSupList(supList);

        return "adm/dep/modal/match :: #form";
    }
}
