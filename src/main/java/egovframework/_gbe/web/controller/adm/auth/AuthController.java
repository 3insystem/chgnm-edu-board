package egovframework._gbe.web.controller.adm.auth;

import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.common.util.CommonUtils;
import egovframework._gbe.domain.log.SystemName;
import egovframework._gbe.service.adm.auth.AuthService;
import egovframework._gbe.type.AuthType;
import egovframework._gbe.type.DsptcType;
import egovframework._gbe.web.dto.adm.auth.AuthListDto;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

@Controller
@RequiredArgsConstructor
public class AuthController {

    private final AuthService authService;

    @SystemName(name = "권한/파견 관리", detail = "조회")
    @GetMapping("/adm/{useSeNm}/auth")
    public String authList(@ModelAttribute SearchParam searchParam, Pageable pageable, Model model) {
        pageable = CommonUtils.setListCount(searchParam, pageable);

        Page<AuthListDto> result = authService.EduUserList(searchParam, pageable);

        model.addAttribute("selectKey1List", AuthType.values());
        model.addAttribute("selectKey2List", DsptcType.values());
        model.addAttribute("menu", 4);  //메뉴정보
        model.addAttribute("result", result);

        return "adm/auth/list";
    }
}
