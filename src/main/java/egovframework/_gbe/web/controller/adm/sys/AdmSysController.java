package egovframework._gbe.web.controller.adm.sys;

import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.domain.log.SystemName;
import egovframework._gbe.service.DashboardService;
import egovframework._gbe.service.common.CommonService;
import egovframework._gbe.type.CategoryType;
import egovframework._gbe.web.dto.adm.code.ClCodeDto;
import egovframework._gbe.web.dto.adm.code.CodeDto;
import egovframework._gbe.web.dto.common.CmmnCodeDto;
import egovframework._gbe.web.dto.dashboard.DashBoardDto;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping({"/adm/sys", "/adm/edu"})
@RequiredArgsConstructor
public class AdmSysController {

    private final CommonService commonService;
    private final DashboardService dashboardService;

    @ModelAttribute("maintenanceType")
    public List<CmmnCodeDto> maintenanceType() {
        return commonService.findAll("MNTMG").stream()
                .map(CmmnCodeDto::entityToDto)
                .collect(Collectors.toList());
    }

    @ModelAttribute("expendablesType")
    public List<CmmnCodeDto> expendablesType() {
        return commonService.findAll("CMPDS").stream()
                .map(CmmnCodeDto::entityToDto)
                .collect(Collectors.toList());
    }

    @SystemName(name = "대시보드", detail = "조회")
    @GetMapping("/dashboard")
    public String dashboard(Model model) {
        DashBoardDto result = dashboardService.getDashBoard();
        model.addAttribute("result", result);
        return "/dashboard";
    }

    @SystemName(name = "코드관리", detail = "조회")
    @GetMapping("/code")
    public String main(@ModelAttribute SearchParam searchParam, Pageable pageable, Model model) {
        Page<ClCodeDto> clCodeList = commonService.findClCodeList(searchParam, pageable);

        model.addAttribute("menu", 11);  //메뉴정보
        model.addAttribute("result", clCodeList);
        return "adm/sys/codeMain";
    }

    @SystemName(name = "코드관리", detail = "등록")
    @GetMapping("/code/add")
    public String add(@ModelAttribute(name = "result") CodeDto result, Model model) {
        model.addAttribute("menu", 11);  //메뉴정보

        return "adm/sys/codeAdd";
    }

    @SystemName(name = "코드관리", detail = "상세")
    @GetMapping("/code/{clcode}")
    public String detail(@PathVariable String clcode, Model model) {
        List<CodeDto> result = commonService.findAll(clcode)
                .stream()
                .map(CodeDto::new)
                .collect(Collectors.toList());

        model.addAttribute("menu", 11);  //메뉴정보
        model.addAttribute("result", result);
        return "adm/sys/codeDetail";
    }

    @SystemName(name = "IP등록설정관리", detail = "등록")
    @GetMapping("ipchk")
    public String ipchk(Model model) {

        model.addAttribute("menu", 12);  //메뉴정보

        return "adm/ip";
    }

    @ModelAttribute("selectKey1List")
    public CategoryType[] selectKey1List() {
        return CategoryType.values();
    }
}
