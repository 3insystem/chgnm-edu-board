package egovframework._gbe.web.controller.adm.slc;

import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.common.util.CommonUtils;
import egovframework._gbe.domain.log.SystemName;
import egovframework._gbe.service.adm.slc.InsttSlcService;
import egovframework._gbe.type.ConfmSe;
import egovframework._gbe.web.dto.mber.rep.SlctnDto;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/adm/{useSeNm}")
@RequiredArgsConstructor
public class InsttSlcController {

    private final InsttSlcService insttSlcService;

    @SystemName(name = "업체관리", detail = "조회[교육청관리자]")
    @GetMapping("/eduSlcList")
    public String insttEduSlcList(@ModelAttribute SearchParam searchParam, Model model, Pageable pageable) {
        Page<SlctnDto> insttSupSlcList = insttSlcService.insttselSlcList(searchParam, pageable);
        model.addAttribute("menu", 3);  //메뉴정보
        model.addAttribute("result", insttSupSlcList);
        return "/adm/eduSlcList";
    }

    @SystemName(name = "업체관리", detail = "조회[지원청관리자]")
    @GetMapping("/supSlcList")
    public String insttSupSlcList(@ModelAttribute SearchParam searchParam, Model model, Pageable pageable) {
        pageable = CommonUtils.setListCount(searchParam, pageable);
        Page<SlctnDto> insttSupSlcList = insttSlcService.insttSupSelSlcList(searchParam, pageable);
        model.addAttribute("menu", 3);  //메뉴정보
        model.addAttribute("result", insttSupSlcList);
        return "/adm/supSlcList";
    }

    @SystemName(name = "업체관리", detail = "조회[기관(학교)관리자]")
    @GetMapping("/schSlcList")
    public String insttselSlcList(@ModelAttribute SearchParam searchParam, Model model, Pageable pageable) {
        pageable = CommonUtils.setListCount(searchParam, pageable);
        Page<SlctnDto> insttSelSlcList = insttSlcService.insttSchSelSlcList(searchParam, pageable);
        model.addAttribute("menu", 3);  //메뉴정보
        model.addAttribute("result", insttSelSlcList);
        return "/adm/schSlcList";
    }

    @ModelAttribute("selectKey1List")
    public ConfmSe[] confmSeList() {
        return ConfmSe.values();
    }
}
