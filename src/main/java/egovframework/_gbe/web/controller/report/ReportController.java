package egovframework._gbe.web.controller.report;

import egovframework._gbe.common.parameter.SearchParam;
import egovframework._gbe.domain.log.SystemName;
import egovframework._gbe.service.common.CommonService;
import egovframework._gbe.service.equipment.EquipmentCommonService;
import egovframework._gbe.service.report.ReportService;
import egovframework._gbe.web.dto.print.PrintReportResultDto;
import egovframework._gbe.web.dto.report.ReportDetailResultDto;
import egovframework._gbe.web.dto.report.ReportDto;
import egovframework._gbe.web.dto.user.cmpny.CmpnyDto;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

import static egovframework._gbe.common.util.CommonUtils.setListCount;
import static egovframework._gbe.common.util.SecurityUtils.getLoginUser;

@Controller
@RequestMapping("/{auth}/{useSeNm}/report")
public class ReportController {

    private final EquipmentCommonService equipmentCommonService;
    private final ReportService reportService;
    private final CommonService commonService;

    public ReportController(@Qualifier("MntmgtService") EquipmentCommonService equipmentCommonService, ReportService reportService, CommonService commonService) {
        this.reportService = reportService;
        this.equipmentCommonService = equipmentCommonService;
        this.commonService = commonService;
    }

    @ModelAttribute("cmpnys")
    public List<CmpnyDto> maintenanceType() {
        return equipmentCommonService.getCompanyInstitutionsMatching(null);
    }

//    @ModelAttribute("jobType")
//    public List<CmmnCodeDto> statusType() {
//        return commonService.findAll("JOBSE").stream()
//                .map(CmmnCodeDto::entityToDto)
//                .collect(Collectors.toList());
//    }

    @GetMapping
    @SystemName(name = "점검보고서", detail = "목록")
    public String reportList(Pageable pageable, @ModelAttribute SearchParam searchParam, Model model) {
        Page<ReportDto> result = reportService.getReportList(setListCount(searchParam, pageable), searchParam);
        model.addAttribute("menu", 9);  //메뉴정보
        model.addAttribute("result", result);
        model.addAttribute("institutionName", reportService.getInstitutionsName());
        model.addAttribute("authority", getLoginUser().getUseSe());
        model.addAttribute("modal", new PrintReportResultDto());
        return "/report/report";
    }

    @GetMapping("/detail/{reprtNo}")
    @SystemName(name = "점검보고서", detail = "상세")
    public String reportDetail(@PathVariable Long reprtNo, Model model) {
        ReportDetailResultDto result = reportService.getReportDetail(reprtNo);
        model.addAttribute("menu", 9);  //메뉴정보
        model.addAttribute("result", result);
        return "/report/detail";
    }


}
