package egovframework._gbe.web;

import egovframework._gbe.common.util.CommonUtils;
import egovframework._gbe.domain.log.SystemName;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequiredArgsConstructor
public class LoginController {

    @SystemName(name = "회원관리", detail = "로그인")
    @GetMapping("/login")
    public String loginPage(Model model) {

        String serverType = CommonUtils.getServerType();

        model.addAttribute("serverType", serverType);

        return "login";
    }
}