package egovframework._gbe.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class SubController {

    @GetMapping("/sub1")
    public String sub1Page() {
        return "login";
    }

    @GetMapping("/sub2")
    public String sub2Page() {
        return "sub2";
    }

    @GetMapping("/sub3")
    public String sub3Page() {
        return "join/cmpny/join2";
    }

    @GetMapping("/sub4")
    public String sub4Page() {
        return "sub4";
    }

    @GetMapping("/sub5")
    public String sub5Page() {
        return "sub5";
    }

    @PostMapping("/sub5")
    public String sub5PageP() {
        return "sub5";
    }

    @GetMapping("/sub6")
    public String sub6Page() {
        return "sub6";
    }

    @GetMapping("/sub7")
    public String sub7Page() {
        return "sub7";
    }

    @GetMapping("/sub8")
    public String sub8Page() {
        return "sub8";
    }

    @GetMapping("/sub9")
    public String sub9Page() {
        return "sub9";
    }

    @GetMapping("/sub10")
    public String sub10Page() {
        return "sub10";
    }

    @GetMapping("/sub11")
    public String sub11Page() {
        return "sub11";
    }

    @GetMapping("/sub12")
    public String sub12Page() { return "sub12"; }

    @GetMapping("/sub13")
    public String sub13Page() {
        return "sub13";
    }

    @GetMapping("/sub14")
    public String sub14Page() {
        return "sub14";
    }

    @GetMapping("/sub15")
    public String sub15Page() {
        return "sub15";
    }

    @GetMapping("/sub16")
    public String sub16Page() {
        return "sub16";
    }

    @GetMapping("/sub17")
    public String sub17Page() {
        return "sub17";
    }

    @GetMapping("/sub18")
    public String sub18Page() {
        return "sub18";
    }

    @GetMapping("/sub19")
    public String sub19Page() {
        return "sub19";
    }

    @GetMapping("/sub20")
    public String sub20Page() {
        return "sub20";
    }

    @GetMapping("/sub21")
    public String sub21Page() {
        return "sub21";
    }

//    @GetMapping("/sub22")
//    public String sub22Page() {
//        return "sub22";
//    }
//
//    @GetMapping("/sub23")
//    public String sub23Page() {
//        return "sub23";
//    }
//
//    @GetMapping("/sub24")
//    public String sub24Page() { return "sub24"; }

    @GetMapping("/sub25")
    public String sub25Page() { return "sub25"; }

    @GetMapping("/sub26")
    public String sub26Page() { return "sub26"; }

    @GetMapping("/sub27")
    public String sub27Page() { return "sub27"; }

    @GetMapping("/sub28")
    public String sub28Page() { return "sub28"; }

    @GetMapping("/sub29")
    public String sub29Page() { return "sub29"; }

    @GetMapping("/sub30")
    public String sub30Page() { return "sub30"; }

    @GetMapping("/sub31")
    public String sub31Page() { return "sub31"; }

    @GetMapping("/sub32")
    public String sub32Page() { return "sub32"; }

    @GetMapping("/sub33")
    public String sub33Page() { return "sub33"; }

    @GetMapping("/sub34")
    public String sub34Page() { return "sub34"; }

    @GetMapping("/sub35")
    public String sub35Page() { return "sub35"; }

    @GetMapping("/sub36")
    public String sub36Page() { return "sub36"; }

    @GetMapping("/sub37")
    public String sub37Page() { return "sub37"; }

    @GetMapping("/sub38")
    public String sub38Page() { return "sub38"; }

    @GetMapping("/sub39")
    public String sub39Page() { return "sub39"; }

    @GetMapping("/sub40")
    public String sub40Page() { return "sub40"; }

    @GetMapping("/sub41")
    public String sub41Page() { return "sub41"; }

    @GetMapping("/sub42")
    public String sub42Page() { return "sub42"; }

    @GetMapping("/sub43")
    public String sub43Page() { return "sub43"; }

    @GetMapping("/sub44")
    public String sub44Page() { return "sub44"; }

    @GetMapping("/sub45")
    public String sub45Page() { return "sub45"; }

    @GetMapping("/sub46")
    public String sub46Page() { return "sub46"; }

    @GetMapping("/sub47")
    public String sub47Page() { return "sub47"; }

    @GetMapping("/sub48")
    public String sub48Page() { return "sub48"; }

    @GetMapping("/sub49")
    public String sub49Page() { return "sub49"; }

    @GetMapping("/sub50")
    public String sub50Page() { return "sub50"; }

    @GetMapping("/sub51")
    public String sub51Page() { return "sub51"; }

    @GetMapping("/sub52")
    public String sub52Page() { return "sub52"; }

    @GetMapping("/sub53")
    public String sub53Page() { return "sub53"; }

    @GetMapping("/sub54")
    public String sub54Page() { return "sub54"; }
}