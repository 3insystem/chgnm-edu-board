package egovframework._gbe.type;

public class BoardEnum {

    public enum BoardType {

        GENERAL("1", "일반게시판"),
        FAQ("2", "QNA게시판"),
        QNA("3", "FAQ게시판");

        private final String value;
        private final String description;

        BoardType(String value, String description) {
            this.value = value;
            this.description = description;
        }

        public String getValue() {
            return value;
        }

        public String getDescription() {
            return description;
        }
    }


    public enum BoardType2 {
        TITLE ("게시판명");

        public final String description;

        BoardType2(String description) {
            this.description = description;
        }

    }



    public enum BoardType3 {

        TITLE("1", "제목"),
        NAME("2", "이름"),
        CONTENT("3", "내용");

        private final String cvalue;
        private final String cdescription;

        BoardType3(String cvalue, String cdescription) {
            this.cvalue = cvalue;
            this.cdescription = cdescription;
        }

        public String getValue() {
            return cvalue;
        }

        public String getDescription() {
            return cdescription;
        }
    }

}
