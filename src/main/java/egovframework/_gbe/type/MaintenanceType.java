package egovframework._gbe.type;

/**
 * 유지관리 구분
 */
public enum MaintenanceType {
    FLAW("장애"),
    INSTALL("설치"),
    INSPECTION("점검"),
    ETC("기타");

    private String status;

    MaintenanceType(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
