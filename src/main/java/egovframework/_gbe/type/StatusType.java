package egovframework._gbe.type;

/**
 * 유지관리, 소모품 관리 상태 구분
 */
public enum StatusType {
    APPLY("신청"),
    RECEIPT("접수"),
    CANCEL("취소"),
    HOLD("보류"),
    COMPLETE("완료");

    private String status;

    StatusType(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
