package egovframework._gbe.type;

import lombok.Getter;

@Getter
public enum DsptcType {

    DSPTC00001("파견대기"),
    DSPTC00002("파견"),
    DSPTC00003("파견종료");

    private final String description;

    DsptcType(String description) {
        this.description = description;
    }
}
