package egovframework._gbe.type;

import egovframework._gbe.common.util.EnumNameParser;
import lombok.Getter;

@Getter
public enum CategoryType implements EnumNameParser {

    CATEGORY("분류"),
    NAME("분류명");

    private final String description;

    CategoryType(String description) {
        this.description = description;
    }
}
