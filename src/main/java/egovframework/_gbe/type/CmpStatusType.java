package egovframework._gbe.type;

public enum CmpStatusType {

    STTUS00001("신청"),
    STTUS00002("접수"),
    STTUS00003("취소"),
    STTUS00004("보류"),
    STTUS00005("완료");

    private String status;

    CmpStatusType(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
