package egovframework._gbe.type;

/* 소모품 구분 */
public enum CmpdsType {

    CMPD100000("인쇄용품"),
    CMPD100001("인쇄용품(토너)"),
    CMPD100002("인쇄용품(전산용지)"),
    CMPD100003("인쇄용품(기타)"),
    CMPD200000("컴퓨터용품");

    private String status;

    CmpdsType(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
