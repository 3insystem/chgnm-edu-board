package egovframework._gbe.type;

public enum ClSeType {

    CLSE000001("H/W"),
    CLSE000002("S/W"),
    CLSE000003("복합기"),
    CLSE000004("기타");

    private String status;

    ClSeType(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
