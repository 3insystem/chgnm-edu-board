package egovframework._gbe.type;

public enum OrgLevel {
    /**
     * 도교육청 수준
     */
    LEVEL_PROVINCIAL,
    /**
     * 지역 교육지원청 수준
     */
    LEVEL_LOCAL,
    /**
     * 학교 수준
     */
    LEVEL_SCHOOL;


    public static OrgLevel[] getOrgLevels() {
        return new OrgLevel[]{LEVEL_PROVINCIAL, LEVEL_LOCAL};
    }

    public static OrgLevel[] getAllOrgLevel() {
        return new OrgLevel[]{LEVEL_PROVINCIAL, LEVEL_LOCAL, LEVEL_SCHOOL};
    }
}
