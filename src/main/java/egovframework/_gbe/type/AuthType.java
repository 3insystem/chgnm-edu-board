package egovframework._gbe.type;

import lombok.Getter;

@Getter
public enum AuthType {

    adm("관리자"),
    mber("일반");

    private final String description;

    AuthType(String description) {
        this.description = description;
    }
}
