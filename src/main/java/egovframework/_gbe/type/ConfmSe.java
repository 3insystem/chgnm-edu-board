package egovframework._gbe.type;

import lombok.Getter;

@Getter
public enum ConfmSe {

    CONFM00001("승인대기"),
    CONFM00002("승인"),
    CONFM00003("중지");

    private final String description;

    ConfmSe(String description) {
        this.description = description;
    }
}
