package egovframework._gbe.type;

public enum CmpMntmgtType {

    MNTMG00001("장애"),
    MNTMG00002("설치"),
    MNTMG00003("점검"),
    MNTMG00004("기타");

    private String status;

    CmpMntmgtType(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
