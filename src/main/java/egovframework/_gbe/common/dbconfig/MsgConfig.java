package egovframework._gbe.common.dbconfig;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;

@Configuration
@MapperScan(value="egovframework._gbe.common",annotationClass = MsgConnMapper.class,sqlSessionFactoryRef="MsgSqlSessionFactory")//멀티DB사용시 mapper클래스파일 스켄용 basePackages를 DB별로 따로설정, 지금은 따로 어노테이션 만드므로 상관없음
public class MsgConfig {

    private static final Logger log = LoggerFactory.getLogger(MsgConfig.class);

    @Bean(name = "MsgDataSource")
    @ConfigurationProperties(prefix="spring.datasource.msn-datasource")
    public DataSource SecondDataSource() {

        log.info("MsgDataSource yml 설정");

        return DataSourceBuilder.create().build();
    }

    @Bean(name = "MsgSqlSessionFactory")
    public SqlSessionFactory msgSqlSessionFactory(@Qualifier("MsgDataSource") DataSource msgDataSource) throws Exception {
        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        sqlSessionFactoryBean.setDataSource(msgDataSource);
        sqlSessionFactoryBean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("/egovframework/mapper/msg/*_maria.xml"));
        sqlSessionFactoryBean.setConfigLocation(new PathMatchingResourcePatternResolver().getResource("/egovframework/mapper/config/mapper-msg-config.xml")); //mybatis 설정 xml 파일매핑
        sqlSessionFactoryBean.setTypeAliasesPackage("egovframework._gbe.common.message");
        return sqlSessionFactoryBean.getObject();
    }

    @Bean(name = "MsgSessionTemplate")
    public SqlSessionTemplate msgSqlSessionTemplate(@Qualifier("MsgSqlSessionFactory") SqlSessionFactory msgSqlSessionTemplate) {
        return new SqlSessionTemplate(msgSqlSessionTemplate);
    }


    @Bean(name = "MsgTransactionManager")
    public DataSourceTransactionManager PrimaryTransactionManager(@Qualifier("MsgDataSource") DataSource msgDataSource) {
        return new DataSourceTransactionManager(msgDataSource);
    }

}
