package egovframework._gbe.common.dbconfig;


import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

@Configuration
@MapperScan(value="egovframework._gbe",annotationClass = PrimaryConnMapper.class,sqlSessionFactoryRef="PrimarySqlSessionFactory")//멀티DB사용시 mapper클래스파일 스켄용 basePackages를 DB별로 따로설정, 지금은 따로 어노테이션 만드므로 상관없음
public class PrimaryConfig {
    private static final Logger log = LoggerFactory.getLogger(PrimaryConfig.class);

    @Primary//동일한 유형의 Bean이 여러 개 있을 때 해당 Bean에 더 높은 우선권을 부여
    @Bean(name = "PrimaryDataSource")
    @ConfigurationProperties(prefix = "spring.datasource.primary") //application.yaml에서 어떤 properties를 읽을 지 지정
    public DataSource primaryDataSource() {

        log.info("yml 설정으로 PrimaryDataSource set" );
        return DataSourceBuilder
                .create()
                .build(); //type(HikariDataSource.class).
    }

    @Primary
    @Bean(name = "PrimarySessionFactory")
    public SqlSessionFactory primarySessionFactory(@Qualifier("PrimaryDataSource") DataSource primaryDataSource, ApplicationContext applicationContex) throws Exception {

        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        sqlSessionFactoryBean.setDataSource(primaryDataSource);

        sqlSessionFactoryBean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("/egovframework/mapper/**/*_maria.xml"));
        sqlSessionFactoryBean.setConfigLocation(new PathMatchingResourcePatternResolver().getResource("/egovframework/mapper/config/mapper-config.xml")); //mybatis 설정 xml 파일매핑
        //sqlSessionFactoryBean.setMapperLocations(applicationContex.getResources("classpath:mapper/mysql/*.xml"));
        sqlSessionFactoryBean.setTypeAliasesPackage("egovframework._gbe.domain"); //benas pakage에 dao나 vo 모아둘 때 구분하기 위해 쓰는 것도 좋음
        //log.info("여기" + new PathMatchingResourcePatternResolver().getResources("mapper/mysql/*.xml").toString());

        //sqlSessionFactoryBean.setTypeAliasesPackage(null); //Mapper 에서 사용하고자하는 VO 및 Entity 에 대해서
        return sqlSessionFactoryBean.getObject();
    }

    @Primary
    @Bean(name = "PrimarySessionTemplate")
    public SqlSessionTemplate primarySessionTemplate(@Qualifier("PrimarySessionFactory") SqlSessionFactory primarySessionFactory) {
        return new SqlSessionTemplate(primarySessionFactory);
    }


    @Bean(name = "PrimaryTransactionManager")
    @Primary
    public DataSourceTransactionManager PrimaryTransactionManager(@Qualifier("PrimaryDataSource") DataSource primaryDataSource) {
        return new DataSourceTransactionManager(primaryDataSource);
    }

}
