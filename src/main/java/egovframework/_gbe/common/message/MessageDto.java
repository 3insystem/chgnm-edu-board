package egovframework._gbe.common.message;

import egovframework._gbe.domain.mntmgt.Mntmgt;
import egovframework._gbe.domain.user.organization.Organizaion;
import egovframework._gbe.web.dto.user.UserDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@NoArgsConstructor
public class MessageDto {

    /*
    * 발송구분 : SMS / LMS
    * */
    private String sendType;

    /*
    * 사용자 ID(알리미시스템관리자에 발급요청 또는 연계사용자 ID 입력)
    * */
    private String userid;

    /*
    * 글제목
    * */
    private String subject;

    /*
    * 받는번호(최대 3000byte)
    * 예 : 이름^01012341224|이름^01012341234^이름|01012341232^이름|
    * */
    private String address;

    /*
    * 본문내용
    * */
    private String content;

    /*
    * 발송번호
    * */
    private String callback;
    /*
    * 0:즉시발송, 1:예약발송
    * */
    private String scheduleType;

    /*
    * 예약발송 시각 : YYYYMMDDHHmm
    * */
    private String scheduleTime;

    /*
    * 연동프로그램명
    * */
    private String worknm;

    public MessageDto(String callback, String usid, String subject, String address, String content, String scheduleType, String scheduleTime) throws Exception {
        //if (!StringUtils.hasLength(usid)) throw new Exception("사용자 아이디가 없습니다.");
        if (!StringUtils.hasLength(address)) throw new Exception("받는 사람이 없습니다.");
        if (!StringUtils.hasLength(content)) throw new Exception("본문 내용이 없습니다.");
        if (!StringUtils.hasLength(scheduleType)) throw new Exception("발송구분이 없습니다.");

        if (!StringUtils.hasLength(callback)) {
            this.callback = "0416408245";
        } else {
            this.callback = callback;
        }

        this.sendType = "SMS";
        this.userid = usid;
        this.address = address;
        this.content = content;
        this.scheduleType = scheduleType;
        if (scheduleType.equals("1")) {
            if (!StringUtils.hasLength(scheduleTime)) throw new Exception("예약발송시간이 없습니다.");
            this.scheduleTime = scheduleTime;
        }

        if (this.content.getBytes().length > 70) {
            if (!StringUtils.hasLength(subject)) throw new Exception("글 제목이 없습니다.");
            this.subject = subject;
            this.sendType = "LMS";
        }
    }

    public static List<Map<String, String>> createSmsInfo (UserDto userDto) {
        List<Map<String, String>> list = new ArrayList<>();
        Map<String, String> map = new HashMap<>();
        map.put("name", userDto.getUserNm());
        map.put("phone", userDto.getMbtlnum());
        list.add(map);
        return list;
    }

    public static String createSmsMessage(Mntmgt mntmgt, Organizaion organizaion) {
        String message = "장애관리 " + mntmgt.getRceptNo() + " 신청\nrc.cne.go.kr \n" + organizaion.getOrgNm();
        return getByteSubString(message);
    }

    public static String createSlcMessage(String schNm) {
        final String message = schNm + "기관(학교) 장애관리 업체로 승인 처리 되었습니다. \n 로그인 > 기관(학교)현황 페이지에서 확인해주세요.";
        return message;
    }

    private static String getByteSubString(String message) {
        final int maxByte = 80;
        if(message.getBytes().length > maxByte) {
            message = new String(message.getBytes(), 0, maxByte);
        }
        return message;
    }
}
