package egovframework._gbe.common.message;

import egovframework._gbe.common.dbconfig.MsgConnMapper;


@MsgConnMapper
public interface MessageDao {

    void sendMsg(MessageDto dto);
}
