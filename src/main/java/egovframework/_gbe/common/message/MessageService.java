package egovframework._gbe.common.message;

import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.MapUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class MessageService {

    private final MessageDao messageDao;

    public void sendMsg(String callback, List<Map<String, String>> recverList, String usid, String subject, String content, String scheduleType, String scheduleTime) throws Exception {
        if (recverList.size() == 0) throw new Exception("받는 사람이 없습니다.");

        String address = "";
        List<String> adress = new ArrayList<>();
        for (Map<String, String> map : recverList) {
            if (MapUtils.isEmpty(map)) throw new Exception("발신정보가 없습니다.");
            if (!map.containsKey("name") || map.get("name") == null || map.get("name").equals("")) {
                throw new Exception("발신자가 없습니다.");
            }

            if (!map.containsKey("phone") || map.get("phone") == null || map.get("phone").equals("")) {
                throw new Exception("발신자 전화번호가 없습니다.");
            }

            String recver = map.get("name");
            String phone = map.get("phone");
            String nm = String.format("%s^%s", recver, phone);
            adress.add(nm);
        }

        address = String.join("|", adress) + "|";

        if (StringUtils.hasLength(address)) {
            MessageDto dto = new MessageDto(callback, usid, subject, address, content, scheduleType, scheduleTime);
            messageDao.sendMsg(dto);
        }

    }
}
