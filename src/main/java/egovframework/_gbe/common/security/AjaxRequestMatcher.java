package egovframework._gbe.common.security;

import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.http.HttpServletRequest;

/**
 * Ajax 요청 매처
 * <p>
 * 해더의 MIME 타입을 통해 Ajax 요청인지 판별한다
 */
public class AjaxRequestMatcher implements RequestMatcher {
    private final String expectedHeaderName;

    private final String expectedHeaderValue;

    public AjaxRequestMatcher() {
        this.expectedHeaderName = "Accept";
        this.expectedHeaderValue = "application/json";
    }

    @Override
    public boolean matches(HttpServletRequest request) {
        String actualHeaderValue = request.getHeader(this.expectedHeaderName);
        if (this.expectedHeaderValue == null) {
            return actualHeaderValue != null;
        }
        return actualHeaderValue.contains(this.expectedHeaderValue);
    }

    @Override
    public String toString() {
        return "AjaxRequestMatcher [expectedHeaderName=" + this.expectedHeaderName + ", expectedHeaderValue="
                + this.expectedHeaderValue + "]";
    }
}
