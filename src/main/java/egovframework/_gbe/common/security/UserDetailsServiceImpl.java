package egovframework._gbe.common.security;

import egovframework._gbe.common.util.CryptUtils;
import egovframework._gbe.domain.user.SessionUser;
import egovframework._gbe.repository.common.CommonDao;
import egovframework._gbe.repository.user.UserDao;
import egovframework._gbe.web.dto.user.UserDto;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.Map;

@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {
    private final UserDao userDao;
    private final CommonDao commonDao;
    private final CryptUtils cryptUtils;

    @Override
    public SessionUser loadUserByUsername(String userId) throws UsernameNotFoundException {
        ServletRequestAttributes sessionAttr = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes());

        if (!StringUtils.hasLength(userId)) {
            throw new UsernameNotFoundException("아이디를 입력해 주세요.");
        }

        UserDto userDto = userDao.selectUserDetail(userId);

        if (userDto == null) {
            throw new UsernameNotFoundException("아이디가 존재하지 않습니다.");
        }

        SessionUser sessionUser = SessionUser.builder()
                .userId(userDto.getUsid())
                .password(userDto.getPassword())
                .userNm(userDto.getUserNm())
                .bizrno(userDto.getBizrno())
                .cmpnyNm(userDto.getCmpnyNm())
                .auth("mber")
                .role("ROLE_AUTH000001")
                .useSe(userDto.getUseSe())
                .useSeNm(userDto.getUseSeNm())
                .orgCd(userDto.getOrgCd())
                .orgNm(userDto.getOrgNm())
                .changeAt(userDto.getChangeAt())
                .enabled(userDto.getUseYn().equals("Y"))
                .build();

        if (StringUtils.hasLength(userDto.getOrgCd())) {
            if (StringUtils.hasLength(userDto.getDsptcOrgCd())) {
                userDto.setOrgCd(userDto.getDsptcOrgCd());
                sessionUser.setOrgCd(userDto.getDsptcOrgCd());
            }

            Map<String, String> map = commonDao.selOrgCdToUseSe(userDto.getUpperOrgCd());
            String type = map.get("type");

            sessionUser.setOrgType(type);
            switch (type) {
                case "edu": case "sup":
                    sessionUser.setUpperOrgCd(userDto.getUpperOrgCd());
                    break;
                case "sch":
                    sessionUser.setUpperOrgCd(userDto.getOrgCd());
                    break;
                default:
                    throw new UsernameNotFoundException("기관이 존재하지 않습니다.");
            }
        }

        return sessionUser;
    }
}
