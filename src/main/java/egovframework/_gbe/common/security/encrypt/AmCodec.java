package egovframework._gbe.common.security.encrypt;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;

public class AmCodec {
    public AmCodec() {
    }

    public String EncryptSEED(byte[] plainData, int offset, int len) {
        StringBuffer encryptString = new StringBuffer();
        byte[] pbData = new byte[16];
        byte[] outData = new byte[16];
        byte[] pbUserKey = {0x75, 0x01, 0x65, 0x41, 0x56, 0x73, (byte)/*-86*/0xAA, (byte)/*-16*/0xF0, 0x02, 0x78, (byte)/*-124*/0x84, 0x24, (byte)/*-128*/0x80, 0x01, 0x13, 0x01};
        int[] pdwRoundKey = new int[32];

        try {
            int length = len;
            // Key Schedule Algorithm
            SEED.SeedRoundKey(pdwRoundKey, pbUserKey);

            //CBase64 base64;
            int nCopyDataLength = 0;
            for (int nIndex = offset; nIndex < length; nIndex += 16) {
                Arrays.fill(pbData, (byte) 0);
                Arrays.fill(outData, (byte) 0);

                nCopyDataLength = (length - nIndex >= 16 ? 16 : length - nIndex);

                for (int i = 0; i < nCopyDataLength; i++) {
                    pbData[i] = plainData[i + nIndex];
                }

                // Encryption Algorithm
                SEED.SeedEncrypt(pbData, pdwRoundKey, outData);

                // BASE64 ���ڵ��� �����Ͽ� ��ȣ���� �����Ѵ�.
                encryptString.append(Base64.encodeBytes(outData));
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return encryptString.toString();
    }

    public String EncryptSEED(String plainText) {
        String encryptString = "";
        byte[] pbData = new byte[16];
        byte[] outData = new byte[16];
        byte[] pbUserKey = {0x75, 0x01, 0x65, 0x41, 0x56, 0x73, (byte)/*-86*/0xAA, (byte)/*-16*/0xF0, 0x02, 0x78, (byte)/*-124*/0x84, 0x24, (byte)/*-128*/0x80, 0x01, 0x13, 0x01};
        int[] pdwRoundKey = new int[32];

        try {
            byte[] plainData = plainText.getBytes("EUC-KR");
            int length = plainData.length;
            // Key Schedule Algorithm
            SEED.SeedRoundKey(pdwRoundKey, pbUserKey);

            //CBase64 base64;
            int nCopyDataLength = 0;
            for (int nIndex = 0; nIndex < length; nIndex += 16) {
                Arrays.fill(pbData, (byte) 0);
                Arrays.fill(outData, (byte) 0);

                nCopyDataLength = (length - nIndex >= 16 ? 16 : length - nIndex);

                for (int i = 0; i < nCopyDataLength; i++) {
                    pbData[i] = plainData[i + nIndex];
                }

                // Encryption Algorithm
                SEED.SeedEncrypt(pbData, pdwRoundKey, outData);

                // BASE64 ���ڵ��� �����Ͽ� ��ȣ���� �����Ѵ�.
                encryptString += Base64.encodeBytes(outData);
            }
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return encryptString;
    }

    public String DecryptSEED(String cipherText) {
        byte[] outData = new byte[16];
        byte[] pbUserKey = {0x75, 0x01, 0x65, 0x41, 0x56, 0x73, (byte)/*-86*/0xAA, (byte)/*-16*/0xF0, 0x02, 0x78, (byte)/*-124*/0x84, 0x24, (byte)/*-128*/0x80, 0x01, 0x13, 0x01};
        int[] pdwRoundKey = new int[32];

        String token = "==";
        StringBuffer DecryptData = new StringBuffer();
        String[] cipherArray = cipherText.split(token);

        // Key Schedule Algorithm
        SEED.SeedRoundKey(pdwRoundKey, pbUserKey);

        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            for (int i = 0; i < cipherArray.length; i++) {
                String aString = cipherArray[i];
                if (aString.length() == 0)
                    break;

                String tempString = aString + token;
                try {
                    byte[] bytes = tempString.getBytes("UTF-8");
                    byte[] bytes2 = Base64.decode(bytes);
                    SEED.SeedDecrypt(bytes2, pdwRoundKey, outData);
                    baos.write(outData);
                } catch (Exception e) {
                    return null;
                }
            }
            byte[] oo = baos.toByteArray();
            String str = new String(oo, "EUC-KR");

            DecryptData.append(str);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //DecryptData
        String retrunStr = DecryptData.toString();
        return retrunStr.trim();
    }

    public byte[] DecryptSEEDToByte(String cipherText) {
        byte[] outData = new byte[16];
        byte[] pbUserKey = {0x75, 0x01, 0x65, 0x41, 0x56, 0x73, (byte)/*-86*/0xAA, (byte)/*-16*/0xF0, 0x02, 0x78, (byte)/*-124*/0x84, 0x24, (byte)/*-128*/0x80, 0x01, 0x13, 0x01};
        int[] pdwRoundKey = new int[32];
        byte[] oo = null;

        String token = "==";
        StringBuffer DecryptData = new StringBuffer();
        String[] cipherArray = cipherText.split(token);

        // Key Schedule Algorithm
        SEED.SeedRoundKey(pdwRoundKey, pbUserKey);

        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            for (int i = 0; i < cipherArray.length; i++) {
                String aString = cipherArray[i];
                if (aString.length() == 0)
                    break;

                String tempString = aString + token;
                try {
                    byte[] bytes = tempString.getBytes("UTF-8");
                    byte[] bytes2 = Base64.decode(bytes);
                    SEED.SeedDecrypt(bytes2, pdwRoundKey, outData);
                    baos.write(outData);
                } catch (Exception e) {
                    return null;
                }
            }
            oo = baos.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return oo;
    }

    /**
     * SEED 알고리즘 이외, 암호화에 필요한 연산 방법 문의
     * <p>
     * - 저희 시스템 내에서 사용중인 암호화 모듈 전달 드립니다.
     * 암호화 방식은 SEED+Base64 암호화 방식이고 메신저로부터 전달 받은 암호화 값을 Base64 Decoding 후 SEED Decoding 진행하시면 될 것 같습니다.
     * AmCodec.java 소스 내 SEED 암/복호화 함수가 있습니다. 해당 함수를 사용하여 복호화 처리 하시면 됩니다.
     * <p>
     * ■ 사용 소스 :  AmCodec.java
     * <p>
     * ■ 사용 함수
     * <p>
     * - 암호화 : EncryptSEED(str)
     * <p>
     * - 복호화 : DecryptSEED(str)
     * <p>
     * 3. 샘플 데이터
     * <p>
     * ■ 테스트  : SEED + Base64
     * <p>
     * - AS-IS: ultari3
     * <p>
     * - TO-BE: Z1NTSXBlNVhaa0ZVY1JjaldHbDB2Zz09
     *
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {

        AmCodec ac = new AmCodec();

        String chpherText = "Z1NTSXBlNVhaa0ZVY1JjaldHbDB2Zz09";
        byte[] bytes = Base64.decode(chpherText);
        String s = new String(bytes);
        String c = ac.DecryptSEED(s);
        System.out.println(c); // equals "ultari3"

        String plainText = "ultari3";
        String e = ac.EncryptSEED(plainText);
        String s1 = Base64.encodeBytes(e.getBytes());
        System.out.println(s1); // equals "Z1NTSXBlNVhaa0ZVY1JjaldHbDB2Zz09"
    }
}