package egovframework._gbe.common.security;

import egovframework._gbe.common.security.token.SSOTokenAuthenticationProvider;
import egovframework._gbe.common.security.token.TokenAuthenticationFilter;
import egovframework._gbe.common.security.token.TokenParameterRequestMatcher;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationDetailsSource;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig {

    private final UserDetailsServiceImpl userDetailsServiceImpl;
    private final AuthenticationDetailsSource authenticationDetailsSource;

    public static String TOKEN_KEY = "udata";

    private final String[] permitAllList = {
            "/admin/init", // 삭제예정
            "/api/v1/**", // 공통 api
            "/join/**", // 회원가입
            "/find/**", // id 및 pwd 찾기
            "/common/**", // 공통 api
            "/user/**" // (구) 데이터 전용 - 권한없는 유저 회원수정하기
    };

    @Bean
    protected SecurityFilterChain filterChain(HttpSecurity http,
                                              TokenAuthenticationFilter tokenAuthenticationFilter) throws Exception {
        http
                .csrf().disable();

        http
                .authorizeRequests()
                .antMatchers(permitAllList).permitAll()
                .requestMatchers(
                        new AntPathRequestMatcher("/"),
                        new AntPathRequestMatcher("/css/**"),
                        new AntPathRequestMatcher("/images/**"),
                        new AntPathRequestMatcher("/js/**"),
                        new AntPathRequestMatcher("/lib/**"),
                        new AntPathRequestMatcher("/report/**")
                ).permitAll()
                .antMatchers("/mber/**").hasAnyRole("AUTH000001")
                .antMatchers("/adm/**").hasAnyRole("AUTH000002")
                .anyRequest().authenticated()
                .and()

                .formLogin()
                .loginPage("/login")
                .defaultSuccessUrl("/", true)
                .successHandler(new LoginSuccessHandler())
                .permitAll();

        http
                .logout()
                .permitAll();

        http
                .exceptionHandling()
                .authenticationEntryPoint(new AjaxAuthenticationEntryPoint("/login"));

        http
                .authenticationProvider(new AuthenticationProviderImpl(userDetailsServiceImpl, passwordEncoder())) // 업체 로그인(아이디 비밀번호 로그인)
                .addFilterAfter(tokenAuthenticationFilter, UsernamePasswordAuthenticationFilter.class); // 기관 로그인(토큰로그인)

        return http.build();

    }

    // 토큰 로그인
    @Bean
    public TokenAuthenticationFilter tokenAuthenticationFilter(SSOTokenAuthenticationProvider authenticationProvider) {
        return new TokenAuthenticationFilter(TOKEN_KEY, new TokenParameterRequestMatcher(TOKEN_KEY), new ProviderManager(authenticationProvider));
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }
}
