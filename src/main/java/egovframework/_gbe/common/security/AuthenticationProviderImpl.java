package egovframework._gbe.common.security;

import egovframework._gbe.common.util.CommonUtils;
import egovframework._gbe.domain.user.SessionUser;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequiredArgsConstructor
public class AuthenticationProviderImpl implements AuthenticationProvider {

    private final UserDetailsServiceImpl userDetailsService;
    private final PasswordEncoder passwordEncoder;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String userId = authentication.getName();
        String server = CommonUtils.getServerType();

        switch (server){
            case "instt":
                break;
            case "cmpny":
                userId = userId + "_CMPNY!";
                break;
        }

        String password = (String) authentication.getCredentials();

        SessionUser sessionUser = userDetailsService.loadUserByUsername(userId);

        if (!passwordEncoder.matches(password, sessionUser.getPassword())) {
            throw new BadCredentialsException("비밀번호가 일치하지 않습니다.");
        }

        Collection<? extends GrantedAuthority> authorities = sessionUser.getAuthorities();
        return new UsernamePasswordAuthenticationToken(sessionUser, null, authorities);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
    }
}
