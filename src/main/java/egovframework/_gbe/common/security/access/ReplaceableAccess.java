package egovframework._gbe.common.security.access;

import egovframework._gbe.type.OrgLevel;

/**
 * 대체 가능한 접근권한(기관코드, 기관수준)
 * <p>
 * 소속기관이 도교육청, 지역교육지원청인 사용자는 접근권한을 변경(대체)할 수 있다.
 * <p>
 * 기관코드와 기관수준을 얻는 getCode(), getLevel() 등 메소드는
 * 대체 접근권한이 설정된 경우 본래 접근권한의 값 대신 대체된 값을 반환한다.
 * <p>
 * 반면 getOriginalCode(), getOriginalLevel() 등 메소드는 항상 본래 접근권한의 값을 반환한다.
 */
public class ReplaceableAccess implements Access {
    /**
     * 원본 기관코드
     */
    private String code;

    /**
     * 원본 기관이름
     */
    private String name;

    /**
     * 원본 기관수준
     */
    private OrgLevel level;

    /**
     * 사용자 ID
     */
    private String usid;

    /**
     * 대체 여부
     */
    private boolean replaced = false;

    /**
     * 대체된 기관코드
     */
    private String replaceCode;

    /**
     * 대체된 기관이름
     */
    private String replaceName;

    /**
     * 대체된 기관수준
     */
    private OrgLevel replaceLevel;


    /**
     * 기관코드 조회
     * <p>
     * 대체된 기관코드가 있을때는 이를 대신 한다.
     *
     * @return
     */
    public String getCode() {
        if (replaced)
            return this.replaceCode;
        else
            return this.code;
    }

    /**
     * 기관 이름 조회
     * <p>
     * 대체된 기관이름이 있을때는 이를 대신한다.
     *
     * @return
     */
    @Override
    public String getName() {
        if (replaced)
            return this.replaceName;
        else
            return this.name;
    }

    public String getNameIfChanged() {
        if (this.name.equals(this.replaceName)) {
            return "";
        } else {
            return getName();
        }
    }

    /**
     * 기관수준 조회
     * <p>
     * 대체된 기관수준이 있을때는 이를 대신 한다.
     *
     * @return
     */
    public OrgLevel getLevel() {
        if (replaced)
            return this.replaceLevel;
        else
            return this.level;
    }

    /**
     * 원본 기관코드 조회
     *
     * @return
     */
    public String getOriginalCode() {
        return this.code;
    }

    /**
     * 원본 기관이름 조회
     *
     * @return
     */
    public String getOriginalName() {
        return this.name;
    }

    /**
     * 원본 기관수준 조회
     *
     * @return
     */
    public OrgLevel getOriginalLevel() {
        return this.level;
    }

    /**
     * 대체 여부 조회
     *
     * @return
     */
    public boolean isReplaced() {
        return replaced;
    }

    /**
     * 로그인 사용자 아이디
     *
     * @return
     */
    @Override
    public String getUsid() {
        return this.usid;
    }

    /**
     * 접근권한(기관코드, 기관수준) 대체
     *
     * @param replaceCode
     * @param replaceLevel
     */
    public void replace(String replaceCode, String replaceName, OrgLevel replaceLevel) {
        this.replaceCode = replaceCode;
        this.replaceName = replaceName;
        this.replaceLevel = replaceLevel;
        this.replaced = true;
    }


    /**
     * 대체된 접근권한 취소
     */
    public void unreplace() {
        this.replaceCode = null;
        this.replaceName = null;
        this.replaceLevel = null;
        this.replaced = false;
    }

    public ReplaceableAccess(String code, String name, OrgLevel level, String replaceCode, String replaceName, OrgLevel replaceLevel) {
        this.code = code;
        this.name = name;
        this.level = level;
        replace(replaceCode, replaceName, replaceLevel);
    }

    public ReplaceableAccess(String code, String name, OrgLevel level) {
        this.code = code;
        this.name = name;
        this.level = level;
    }

    public ReplaceableAccess(String code, String name, OrgLevel level, String usid) {
        this.code = code;
        this.name = name;
        this.level = level;
        this.usid = usid;
    }

    public static ReplaceableAccess of(SimpleAccess originalAccess) {
        return new ReplaceableAccess(originalAccess.getCode(), originalAccess.getName(), originalAccess.getLevel());
    }

    public static ReplaceableAccess of(String originalCode, String originalName, OrgLevel originalLevel) {
        return new ReplaceableAccess(originalCode, originalName, originalLevel);
    }

    public static ReplaceableAccess of(String originalCode, String originalName, OrgLevel originalLevel, String usid) {
        return new ReplaceableAccess(originalCode, originalName, originalLevel, usid);
    }
}
