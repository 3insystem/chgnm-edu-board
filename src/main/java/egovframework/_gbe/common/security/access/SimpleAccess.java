package egovframework._gbe.common.security.access;

import egovframework._gbe.type.OrgLevel;

/**
 * 간단한 접근권한
 */
public class SimpleAccess implements Access {

    /**
     * 기관코드
     */
    private String code;

    /**
     * 기관 이름
     */
    private String name;

    /**
     * 기관수준
     */
    private OrgLevel level;

    /**
     * 사용자 ID
     */
    private String usid;

    /**
     * 기관코드 조회
     *
     * @return
     */
    public String getCode() {
        return this.code;
    }

    /**
     * 기관 이름
     *
     * @return
     */
    @Override
    public String getName() {
        return this.name;
    }

    /**
     * 기관수준 조회
     *
     * @return
     */
    public OrgLevel getLevel() {
        return this.level;
    }

    /**
     * 로그인 사용자 아이디
     *
     * @return
     */
    @Override
    public String getUsid() {
        return this.usid;
    }

    private SimpleAccess(String code, String name, OrgLevel level) {
        this.code = code;
        this.name = name;
        this.level = level;
    }

    public static SimpleAccess of(String code, String name, OrgLevel level) {
        return new SimpleAccess(code, name, level);
    }
}
