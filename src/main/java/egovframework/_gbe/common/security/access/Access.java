package egovframework._gbe.common.security.access;

import egovframework._gbe.type.OrgLevel;

/**
 * 접근권한(기관코드, 기관수준)
 */
public interface Access {

    /**
     * 기관코드
     * <p>
     * 각종 등록/조회/업데이트/삭제 등의 작업의 주체가 되는 기관을 의미
     *
     * @return
     */
    String getCode();

    /**
     * 기관 이름
     * <p>
     * 각종 등록/조회/업데이트/삭제 등의 작업의 주체가 되는 기관을 의미
     *
     * @return
     */
    String getName();

    /**
     * 기관수준
     * <p>
     * 기관수준은 도교육청/지역교육지원청/학교(또는 학교와 동등)등을 의미하며
     * 비즈니스 로직상 기관 수준에 따라 접근할 기능이 다르다.
     *
     * @return
     */
    OrgLevel getLevel();

    /**
     * 사용자 아이디
     * <P>
     * 교원노트북 관련 공차대장, 반출입대장에서 개인 아이디 필요.
     * @return
     */
    String getUsid();
}
