package egovframework._gbe.common.security;

import org.springframework.security.web.authentication.WebAuthenticationDetails;

import javax.servlet.http.HttpServletRequest;

public class FormWebAuthenticationDetails extends WebAuthenticationDetails {

    private final String loginType;

    // 사용자가 전달하는 추가적인 파라미터들을 저장하는 클래스
    public FormWebAuthenticationDetails(HttpServletRequest request) {
        super(request);
        loginType =  request.getParameter("loginType");
    }

    public String getSecretKey() {
        return loginType;
    }
}
