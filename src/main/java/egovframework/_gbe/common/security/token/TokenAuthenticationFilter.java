package egovframework._gbe.common.security.token;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * [ 메신저 퀵메뉴 호출 정보 ]
 * <p>
 * ■ url : "http://[정보화장비url]?udata=<ENC_PARAM>"
 * <p>
 * - <ENC_PARAM> 정보
 * <p>
 * Parameter SEED + Base64
 * <p>
 * ■ Parameter : "USER_ID|login|MILLISECOND"
 * <p>
 * MILLISECOND을 보고 현재 시간과 비교하여  10분 이상 차이가 있을 시 로그인 실패처리 (10분은 샘플 수치로 적절히 조정하시면됨)
 * <p>
 * (클라이언트의 시간으로 비교를 하기 때문에  10분정도의 시간차는 감안이 필요하기 때문)
 * <p>
 * ■ method : get
 * <p>
 * <p>
 * [ SEED + Base64 암호화 test 정보 ]
 * <p>
 * ■ AS-IS: "ultari3"
 * <p>
 * ■ TO-BE: "Z1NTSXBlNVhaa0ZVY1JjaldHbDB2Zz09"
 */
public class TokenAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    private final String TOKEN_KEY;

    public TokenAuthenticationFilter(String tokenKey, RequestMatcher requiresAuthenticationRequestMatcher, AuthenticationManager authenticationManager) {
        super(requiresAuthenticationRequestMatcher, authenticationManager);
        this.TOKEN_KEY = tokenKey;
        setAuthenticationSuccessHandler(new UrlThroughAuthenticationSuccessHandler());
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException, ServletException {
        String token = obtainSsoToken(request);
        SSOAuthenticationToken authenticationToken = new SSOAuthenticationToken(token);
        setDetails(request, authenticationToken);
        return this.getAuthenticationManager().authenticate(authenticationToken);
    }

    private String obtainSsoToken(HttpServletRequest request) {
        return request.getParameter(TOKEN_KEY);
    }

    protected void setDetails(HttpServletRequest request, AbstractAuthenticationToken authRequest) {
        authRequest.setDetails(this.authenticationDetailsSource.buildDetails(request));
    }
}
