package egovframework._gbe.common.security.token;

import java.time.LocalDateTime;

public class UrlTokenHelper {

    /**
     * Token URL 생성
     * <p>
     * 서버 경로를 제외한 나머지 URI와 URI 파라미터를 제공하면,
     * 토큰 인증을 포함하는 URL을 생성한다.
     *
     * @param usid   접속 아이디(토큰 생성에 활용)
     * @param action 접속 형태 (토큰 생성에 활용)
     * @return 서버 도메인 경로, URI, 토큰이 결합된 URL
     */
    public static String makeTokenUrl(String baseUrl, String usid, String action) {
        if (usid.contains("_CMPNY!")) {
            usid = usid.replaceAll("_CMPNY!", "");
        }

        TokenMessage tokenMessage = new TokenMessage(usid, action, LocalDateTime.now());
        String token = TokenCypher.encryption(tokenMessage);
        String urlWithToken = baseUrl + "?udata=" + token;
        return urlWithToken;
    }
}
