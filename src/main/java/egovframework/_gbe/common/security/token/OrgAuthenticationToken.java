package egovframework._gbe.common.security.token;

import org.apache.commons.collections.list.UnmodifiableList;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;

import java.util.ArrayList;
import java.util.Collection;

/**
 * 인증 토큰
 * <p>
 * 인증 사용자(principal)와 관련한 정보들을 포함
 */
public class OrgAuthenticationToken extends UsernamePasswordAuthenticationToken {

    /**
     * 보조 권한 목록
     * <p>
     * 최초 인증 이후, 기관 관리자 인증시 추가 되는 권한 목록을 의미하며
     * 언제든지 추가되거나 삭제될 수 있다.
     */
    private Collection<GrantedAuthority> extraAuthorities = new ArrayList<>();

    public OrgAuthenticationToken(Object principal, Object credentials) {
        super(principal, credentials);
    }

    public OrgAuthenticationToken(Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities) {
        super(principal, credentials, authorities);
    }

    public OrgAuthenticationToken(Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities, Collection<? extends GrantedAuthority> extraAuthorities) {
        super(principal, credentials, authorities);
        setExtraAuthorities(extraAuthorities);
    }

    /**
     * 보조 권한 설정
     * <p>
     * 최초 인증 이후, 기관 관리자 인증시 추가 되는 권한 목록
     *
     * @param extraAuthorities
     */
    public void setExtraAuthorities(Collection<? extends GrantedAuthority> extraAuthorities) {

        if (extraAuthorities == null) {
            this.extraAuthorities = AuthorityUtils.NO_AUTHORITIES;
        } else {
            this.extraAuthorities = new ArrayList<>(extraAuthorities);
        }
    }

    /**
     * 보조 권한 추가
     *
     * @param extraAuthority
     */
    public void addExtraAuthorities(GrantedAuthority extraAuthority) {
        this.extraAuthorities.add(extraAuthority);
    }

    /**
     * 모든 보조 권한 삭제
     */
    public void clearExtraAuthorities() {
        this.extraAuthorities = AuthorityUtils.NO_AUTHORITIES;
    }

    /**
     * 기존 권한과 보조 권한을 모두 반환
     *
     * @return
     */
    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        ArrayList<GrantedAuthority> mergedAuthorities = new ArrayList<>();
        mergedAuthorities.addAll(super.getAuthorities());
        mergedAuthorities.addAll(this.extraAuthorities);
        return UnmodifiableList.decorate(mergedAuthorities);
    }

    /**
     * 보조 권한 조회
     *
     * @return
     */
    public Collection<GrantedAuthority> getExtraAuthorities() {
        return UnmodifiableList.decorate(new ArrayList(this.extraAuthorities));
    }
}
