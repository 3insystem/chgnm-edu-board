package egovframework._gbe.common.security.token;

import egovframework._gbe.domain.user.SessionUser;
import egovframework._gbe.repository.common.CommonDao;
import egovframework._gbe.repository.user.UserRepository;
import egovframework._gbe.repository.user.organization.OrganizationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
@RequiredArgsConstructor
public class AuthenticationProviderService {

    private final UserDetailsService userDetailsService;
    private final UserRepository userRepository;
    private final OrganizationRepository organizationRepository;
    private final CommonDao commonDao;

    public Authentication createAuthentication(String principal) {
        SessionUser sessionUser = (SessionUser) userDetailsService.loadUserByUsername(principal);

        Collection<? extends GrantedAuthority> authorities = sessionUser.getAuthorities();
        return new UsernamePasswordAuthenticationToken(sessionUser, null, authorities);
    }
}
