package egovframework._gbe.common.security.token;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

/**
 * 토큰
 * <p>
 * 토큰은 다음의 정보를 포함하고 있다.
 *
 * <ul>
 *     <li>사용자 식별자(아이디)</li>
 *     <li>액션(로그인 등)</li>
 *     <li>토큰 만료 시간</li>
 * </ul>
 * <p>
 * 토큰은 문자열로 직렬화, 문자열로 부터 역직렬화 할 수 있으며, <br>
 * 원격지에서 수신한 토큰이 신뢰할 수 있는지를 확인하려면, <br>
 * 암호화, 전자서명 등의 장치가 별도로 필요하다.
 */
public class TokenMessage {
    /**
     * 토큰내 포함될 정보(문자열) 분리자
     */
    final static String SPLITER = "|";

    /**
     * 사용자 식벼랒(아이디
     */
    private String principal;

    /**
     * 액션(로그인 등)
     */
    private String action;

    /**
     * 토큰 생성 시간
     */
    private LocalDateTime creationAt;

    public String getPrincipal() {
        return principal;
    }

    public String getAction() {
        return action;
    }

    public LocalDateTime getCreationAt() {
        return creationAt;
    }

    public TokenMessage(String principal, String action, LocalDateTime creationAt) {
        this.principal = principal;
        this.action = action;
        this.creationAt = creationAt;
    }

    /**
     * 직렬화
     * <p>
     * 예) "USER_ID|login|MILLISECOND"
     *
     * @return 직렬화된 문자열
     */
    public String serialize() {
        ZonedDateTime zdt = ZonedDateTime.of(this.creationAt, ZoneId.systemDefault());
        long millisecond = zdt.toInstant().toEpochMilli();
        String str = this.principal + SPLITER + this.action + SPLITER + millisecond;
        return str;
    }

    /**
     * 역직렬화
     *
     * @param str 직렬화된 문자열
     * @return 역직렬화된 토큰
     */
    public static TokenMessage deserialize(String str) {
        String[] split = str.split("\\" + SPLITER);
        String principal = split[0];
        String action = split[1];
        LocalDateTime expireAt = Instant.ofEpochMilli(Long.valueOf(split[2]))
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
        return new TokenMessage(principal, action, expireAt);
    }
}
