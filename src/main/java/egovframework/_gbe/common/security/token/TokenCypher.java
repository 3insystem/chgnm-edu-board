package egovframework._gbe.common.security.token;

import egovframework._gbe.common.security.encrypt.AmCodec;
import egovframework._gbe.common.security.encrypt.Base64;

import java.io.IOException;

/**
 * 토큰 암복호화기
 */
public class TokenCypher {

    /**
     * 토큰 암호화
     *
     * @param message 토큰
     * @return 암호화된 직렬화된 토큰 문자열
     */
    public static String encryption(TokenMessage message) {
        String serialize = message.serialize();
        return doEncryption(serialize);
    }

    /**
     * 토큰 복호화
     *
     * @param token 암호화된 직렬화된 토큰 문자열
     * @return 복호화된 토큰
     */
    public static TokenMessage decryption(String token) {
        String plainText = doDecryption(token);
        TokenMessage tokenMessage = TokenMessage.deserialize(plainText);
        return tokenMessage;
    }

    private static String doEncryption(String plainText) {
        AmCodec ac = new AmCodec();
        String chpherText = ac.EncryptSEED(plainText);
        String encodedText = Base64.encodeBytes(chpherText.getBytes());
        return encodedText;
    }

    private static String doDecryption(String encodedText) {
        AmCodec ac = new AmCodec();
        try {
            byte[] bytes = Base64.decode(encodedText);
            String chpherText = new String(bytes);
            String plainText = ac.DecryptSEED(chpherText);
            return plainText;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
