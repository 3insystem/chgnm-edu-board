package egovframework._gbe.common.security.token;

import org.springframework.security.authentication.AbstractAuthenticationToken;

public class SSOAuthenticationToken extends AbstractAuthenticationToken {

    private final Object ssoToken;

    public SSOAuthenticationToken(String ssoToken) {
        super(null);
        this.ssoToken = ssoToken;
        setAuthenticated(false);
    }

    @Override
    public Object getPrincipal() {
        return ssoToken;
    }

    @Override
    public Object getCredentials() {
        return null;
    }

}
