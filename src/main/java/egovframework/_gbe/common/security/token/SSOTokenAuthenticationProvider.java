package egovframework._gbe.common.security.token;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * 토큰 인증 처리 제공자
 */
@Component
public class SSOTokenAuthenticationProvider implements AuthenticationProvider {

    private TokenCypher tokenCypher;
    private final AuthenticationProviderService authenticationProviderService;

    public SSOTokenAuthenticationProvider(AuthenticationProviderService authenticationProviderService) {
        this.authenticationProviderService = authenticationProviderService;
        this.tokenCypher = new TokenCypher();
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String encryptedToken = (String) authentication.getPrincipal();
        TokenMessage tokenMessage;

        try {
            tokenMessage = tokenCypher.decryption(encryptedToken);
        } catch (Exception e) {
            throw new BadCredentialsException("올바르지 않은 토큰입니다.");
        }

        LocalDateTime expiredAt = getExpiredAt(tokenMessage);
        if (LocalDateTime.now().isAfter(expiredAt))
            throw new BadCredentialsException("접속 허용 기간이 만료된 url 입니다.");

        String usid = tokenMessage.getPrincipal();
        Authentication newAuthentication = authenticationProviderService.createAuthentication(usid);
        return newAuthentication;
    }

    private static LocalDateTime getExpiredAt(TokenMessage tokenMessage) {
        LocalDateTime expiredAt;
        switch (tokenMessage.getAction()) {
            case "login":
                expiredAt = tokenMessage.getCreationAt().plusMinutes(10);
                break;
            case "invitation":
                expiredAt = tokenMessage.getCreationAt().plusDays(3);
                break;
            default:
                throw new BadCredentialsException("올바르지 않은 토큰입니다.");
        }
        return expiredAt;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return (SSOAuthenticationToken.class.isAssignableFrom(authentication));
    }
}
