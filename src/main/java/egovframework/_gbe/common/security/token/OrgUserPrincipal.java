package egovframework._gbe.common.security.token;

import egovframework._gbe.common.security.access.Access;
import egovframework._gbe.common.security.access.ReplaceableAccess;
import egovframework._gbe.domain.user.User;
import egovframework._gbe.domain.user.organization.Organizaion;
import egovframework._gbe.type.OrgLevel;
import lombok.Getter;

import java.security.Principal;

/**
 * 인증주체(접속 사용자) 정보
 */

public class OrgUserPrincipal implements Principal {

    /**
     * 사용자 로그인 ID
     */
    @Getter
    private String usid;

    /**
     * 기관 코드
     */
    @Getter
    private String orgCd;

    /**
     * 사용자명
     */
    @Getter
    private String userNm;

    /**
     * 연락처
     */
    @Getter
    private String mbtlnum;

    /**
     * 재직 상태 코드
     */
    @Getter
    private String hffcStsSeCd;

    @Getter
    private Organizaion organizaion;

    /**
     * 접근권한(기관코드, 기관수준)
     */
    @Getter
    private Access access;

    protected OrgUserPrincipal(String usid, String orgCd, String userNm, String mbtlnum, String hffcStsSeCd, Organizaion organizaion, OrgLevel orgLevel) {
        this.usid = usid;
        this.orgCd = orgCd;
        this.userNm = userNm;
        this.mbtlnum = mbtlnum;
        this.hffcStsSeCd = hffcStsSeCd;
        this.organizaion = organizaion;
        // 접근권한은 사용자의 기관(부서)가 아닌
        // 소속기관(대외기관)에 대한 접근권한을 가지도록 한다.
//        this.access = ReplaceableAccess.of(this.orgMaching.getAflaOrgCd(), this.orgMaching.getAflaOrgNm(), orgLevel);
        // FIXME: 로그인 아이디를 다른 곳에서 받아 올 수 있는지 확인해 볼것.
        this.access = ReplaceableAccess.of(this.organizaion.getOrgCd(), this.organizaion.getOrgNm(), orgLevel, usid);
    }

    public static OrgUserPrincipal ofUser(User user, Organizaion organizaion, OrgLevel orgLevel) {
        return new OrgUserPrincipal(user.getUsid(), user.getOrgCd().getOrgCd(), user.getUserNm(), user.getTelno(), user.getDelYn(), organizaion, orgLevel);
    }

    public static OrgUserPrincipal of(String usid, String orgCd, String userNm, String telno, String hffcStsSeCd, Organizaion organizaion, OrgLevel orgLevel) {
        return new OrgUserPrincipal(usid, orgCd, userNm, telno, hffcStsSeCd, organizaion, orgLevel);
    }

    @Override
    public String getName() {
        return this.getUsid();
    }
}
