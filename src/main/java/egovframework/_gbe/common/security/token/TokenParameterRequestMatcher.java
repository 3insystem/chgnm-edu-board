package egovframework._gbe.common.security.token;

import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public class TokenParameterRequestMatcher implements RequestMatcher {

    private final String TOKEN_KEY;

    public TokenParameterRequestMatcher(String TOKEN_KEY) {
        this.TOKEN_KEY = TOKEN_KEY;
    }

    @Override
    public boolean matches(HttpServletRequest request) {
        Map<String, String[]> parameterMap = request.getParameterMap();
        String method = request.getMethod();
        boolean result = parameterMap.containsKey(this.TOKEN_KEY) && "GET".equals(method);
        return result;
    }
}
