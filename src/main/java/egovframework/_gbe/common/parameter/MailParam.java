package egovframework._gbe.common.parameter;

import lombok.Getter;

@Getter
public class MailParam {

    private String email;

    private String num;
}
