package egovframework._gbe.common.parameter;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.time.YearMonth;
import java.util.ArrayList;

@Getter @Setter
public class SearchParam {

    // 목록 조회 parameter

    private String word1;

    private String word2;

    private String word3;

    private String word4;

    private String word5;

    private String word6;

    private String selectKey1;

    private String selectKey2;

    private String selectKey3;

    private String selectKey4;

    private String selectKey5;

    private String selectKey6;

    private ArrayList<String> selectList;

    private Long id1;

    private String id2;

    private String id3;

    private String usid;
    private String bizrno;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;

    private Integer listCount;

    // 기관 코드 parameter

    private String sel0;

    private String sel1;

    private String sel2;

    private String sel3;

    private String viewCd;

    private String viewCdNm;

    private String allYn;

    // Pagenation : mybatis 전용
    private Integer _page;
    private Integer _pageSize;
    
    // 기관코드
    private String orgCd;

    private boolean isSearch;

    public void settingPage(Pageable pageable) {
        this._page = pageable.getPageSize() * pageable.getPageNumber();
        this._pageSize = pageable.getPageSize();
    }

    public Integer get_page() {
        return _page;
    }

    public Integer get_pageSize() {
        return _pageSize;
    }

    public boolean isSearch() {
        return (word1 != null && !word1.isEmpty())
                || (word2 != null && !word2.isEmpty())
//                || (word3 != null && !word3.isEmpty()) //  공통검색 word3
                || (word4 != null && !word4.isEmpty())
                || (word5 != null && !word5.isEmpty())
                || (selectKey1 != null && !selectKey1.isEmpty())
                || (selectKey2 != null && !selectKey2.isEmpty())
                || (selectKey3 != null && !selectKey3.isEmpty())
                || (selectKey4 != null && !selectKey4.isEmpty())
                || (selectKey5 != null && !selectKey5.isEmpty())
                || startDate != null
                || endDate != null
                || (listCount != null && listCount > 15);
    }
}
