package egovframework._gbe.common.schuduler;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


@Component
@RequiredArgsConstructor
public class Scheduler {

    private static final Logger log = LoggerFactory.getLogger(Scheduler.class);
    private final ScheduleService scheduleService;

//    @Scheduled(cron = "0 0 2 1/1 * ?")
//    public void procOrgnzt() {
//        log.info("프로시저 PROC_ORGNZT() 실행");
//        scheduleService.procOrgnzt();
//        log.info("프로시저 PROC_ORGNZT() 종료");
//    }
//
//    @Scheduled(cron = "0 30 2 1/1 * ?")
//    public void procUserInstt() {
//        log.info("프로시저 PROC_USER_INSTT() 실행");
//        scheduleService.procUserInstt();
//        log.info("프로시저 PROC_USER_INSTT() 종료");
//    }

    @Scheduled(cron = "0 0 3 1/1 * ?")
    public void procUserEnc() {
        scheduleService.procUserEnc();
    }

    @Scheduled(cron = "0 0 0 * * *")
    public void updBoardRet() {
        scheduleService.updBoardRet();
    }

    @Scheduled(cron = "0 0 1 * * *")
    public void updBoardDt() {
        scheduleService.updBoardDt();
    }
}
