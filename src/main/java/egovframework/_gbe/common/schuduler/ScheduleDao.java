package egovframework._gbe.common.schuduler;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ScheduleDao {

    void procOrgnzt();
    void procUserInstt();

    void procUserEnc();

    void updBoardRet();

    void updBoardDt();
}
