package egovframework._gbe.common.schuduler;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class ScheduleService {

    private final ScheduleDao scheduleDao;

    @Transactional
    public void procOrgnzt() {
        scheduleDao.procOrgnzt();
    }

    @Transactional
    public void procUserInstt() {
        scheduleDao.procUserInstt();
    }

    @Transactional
    public void procUserEnc() {
        scheduleDao.procUserEnc();
    }

    @Transactional
    public void updBoardRet() {
        scheduleDao.updBoardRet();
    }

    @Transactional
    public void updBoardDt() {
        scheduleDao.updBoardDt();
    }
}
