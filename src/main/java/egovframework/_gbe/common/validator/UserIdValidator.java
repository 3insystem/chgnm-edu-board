package egovframework._gbe.common.validator;

import egovframework._gbe.domain.user.User;
import egovframework._gbe.repository.user.UserRepository;
import egovframework._gbe.web.dto.user.UserSave;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class UserIdValidator implements Validator {

    private final UserRepository userRepository;

    @Override
    public boolean supports(Class<?> clazz) {
        return UserSave.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        UserSave userSave = (UserSave) target;

        Optional<User> user = userRepository.findById(userSave.getUserId());

        if (!user.isPresent()) {
            errors.rejectValue("userId", "Exist.userId");
        }
    }
}
