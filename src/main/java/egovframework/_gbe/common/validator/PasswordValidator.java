package egovframework._gbe.common.validator;

import egovframework._gbe.web.dto.user.PasswordDto;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class PasswordValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return PasswordDto.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        PasswordDto passwordDto = (PasswordDto) target;

        if (passwordDto.getPassword().isEmpty() || passwordDto.getConfirm().isEmpty()) {
            return;
        }

        if (!passwordDto.getPassword().equals(passwordDto.getConfirm())) {
            errors.reject("password.mismatch");
        }
    }
}
