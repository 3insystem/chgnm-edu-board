package egovframework._gbe.common.util.file;

import egovframework._gbe.web.dto.board.BoardFileDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriUtils;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.UUID;

@Component
public class BoardFileUtils {

    private static String fileDir;

    @Value("${file.boardDir}")
    public void setFileDir(String value) {
        fileDir = value;
    }

    public static UploadFile uploadFile(MultipartFile multipartFile, String chkUpload) throws IOException {
        if (multipartFile == null) {
            return null;
        }

        String originalFileName = multipartFile.getOriginalFilename();
        String storeFileName = createStoreFileName(originalFileName);
        String pathChk = String.valueOf(getPath(chkUpload));

        String path = getFullPath(chkUpload, storeFileName);

        File filePath = new File(path);
        if(!filePath.exists()) {
            filePath.mkdirs();
        }

        multipartFile.transferTo(filePath);

        UploadFile uploadFile = new UploadFile();
        uploadFile.setFileStreCours(path);
        uploadFile.setOriginlFileNm(originalFileName);
        uploadFile.setStreFileNm(storeFileName);
        uploadFile.setFileExtsn(extractExt(originalFileName));
        uploadFile.setFileSize(multipartFile.getSize());

        return uploadFile;
    }

    public static ResponseEntity downloadFile(BoardFileDto fileMap) throws MalformedURLException {

//        UrlResource resource = new UrlResource("file:" + getFullPath(fileMap.get("streFileNm")));
        UrlResource resource = new UrlResource("file:" + fileMap.getFileStreCours());

        String encodedUploadFileName = UriUtils.encode(fileMap.getOrignlFileNm(), StandardCharsets.UTF_8);
        String contentDisposition = "attachment; filename=\"" + encodedUploadFileName + "\"";

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.CONTENT_DISPOSITION, contentDisposition);
//        httpHeaders.add(HttpHeaders.CONTENT_TYPE, contentType);
        httpHeaders.add("filename", encodedUploadFileName);

        return ResponseEntity.ok()
                .headers(httpHeaders)
                .body(resource);
    }

    public static void deleteFile(String streFileNm, String chkUpload) {
        if (!StringUtils.hasLength(streFileNm)) return;

        File file = new File(getFullPath(chkUpload, streFileNm));
        if (file.exists()) file.delete();
    }


    public static File getPath(String chkUpload) {

        File file = new File(fileDir + "/" + chkUpload + "/" + LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd")));
        // 디렉토리 생성
        boolean directoryCreated = file.mkdirs();

        return file ;
    }

    public static String getFullPath(String chkUpload, String filename) {
        return fileDir + "/" + chkUpload + "/" + LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd")) +"/" + filename;
    }

    public static String getFullPath(String chkUpload, String date, String filename) {
        return fileDir + "/" + chkUpload + "/" + date +"/" + filename;
    }

    private static String createStoreFileName(String originalFileName) {
        String ext = extractExt(originalFileName);
        String uuid = UUID.randomUUID().toString();
        return uuid + "." + ext;
    }

    private static String extractExt(String originalFileName) {
        int pos = originalFileName.lastIndexOf(".");
        return originalFileName.substring(pos + 1);
    }
}
