package egovframework._gbe.common.util.file;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriUtils;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.UUID;

@Component
public class FileUtils {

    private static String fileDir;

    @Value("${file.dir}")
    public void setFileDir(String value) {
        fileDir = value;
    }

    public static UploadFile uploadFile(MultipartFile multipartFile) throws IOException {
        if (multipartFile == null) {
            return null;
        }

        String originalFileName = multipartFile.getOriginalFilename();
        String storeFileName = createStoreFileName(originalFileName);
        String pathChk = String.valueOf(getPath());
        String path = getFullPath(storeFileName);

        File filePath = new File(path);
        if(!filePath.exists()) {
            filePath.mkdir();
        }

        multipartFile.transferTo(filePath);

        UploadFile uploadFile = new UploadFile();
        uploadFile.setFileStreCours(path);
        uploadFile.setOriginlFileNm(originalFileName);
        uploadFile.setStreFileNm(storeFileName);
        uploadFile.setFileExtsn(extractExt(originalFileName));
        uploadFile.setFileSize(multipartFile.getSize());

        return uploadFile;
    }

    public static ResponseEntity downloadFile(Map<String, String> fileMap) throws MalformedURLException {
//        UrlResource resource = new UrlResource("file:" + getFullPath(fileMap.get("streFileNm")));
        UrlResource resource = new UrlResource("file:" + fileMap.get("fileStreCours"));

        String encodedUploadFileName = UriUtils.encode(fileMap.get("orignlFileNm"), StandardCharsets.UTF_8);
        String contentDisposition = "attachment; filename=\"" + encodedUploadFileName + "\"";

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.CONTENT_DISPOSITION, contentDisposition);
//        httpHeaders.add(HttpHeaders.CONTENT_TYPE, contentType);
        httpHeaders.add("filename", encodedUploadFileName);

        return ResponseEntity.ok()
                .headers(httpHeaders)
                .body(resource);
    }

    public static void deleteFile(String streFileNm) {
        if (!StringUtils.hasLength(streFileNm)) return;

        File file = new File(getFullPath(streFileNm));
        if (file.exists()) file.delete();
    }

    public static void deleteFilePath(String path) throws IOException {
        File file = new File(path);
        Files.deleteIfExists(file.toPath());
    }

    public static File getPath() {

        File file = new File(fileDir + "/" + LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd")));
        // 디렉토리 생성
        boolean directoryCreated = file.mkdirs();

        return file;
    }

    public static String getFullPath(String filename) {
        return fileDir + LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd")) +"/" + filename;
    }

    public static String getFullPath(String date, String filename) {
        return fileDir + "/" + date +"/" + filename;
    }

    private static String createStoreFileName(String originalFileName) {
        String ext = extractExt(originalFileName);
        String uuid = UUID.randomUUID().toString();
        return uuid + "." + ext;
    }

    private static String extractExt(String originalFileName) {
        int pos = originalFileName.lastIndexOf(".");
        return originalFileName.substring(pos + 1);
    }
}
