package egovframework._gbe.common.util.file;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class UploadFile {

    private String fileStreCours;

    private String streFileNm;

    private String originlFileNm;

    private String fileExtsn;

    private String fileCn;

    private Long fileSize;
}
