package egovframework._gbe.common.util;

import egovframework._gbe.domain.user.SessionUser;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityUtils {

    public static SessionUser getLoginUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object principal = authentication.getPrincipal();
        if (principal.equals("anonymousUser")) {
            return new SessionUser();
        }

        return (SessionUser) authentication.getPrincipal();
    }

    public static String getUserId() {
        SessionUser user = getLoginUser();

        switch (user.getUseSe()) {
            case "USESE00002": case "USESE00003": case "USESE00004":
                if (user.getUserId().contains("_CMPNY!")) {
                    return user.getUserId();
                } else {
                    return user.getUserId() + "_CMPNY!";
                }
            default:
                return user.getUserId();
        }
    }
}
