package egovframework._gbe.common.util;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
@RequiredArgsConstructor
public class CryptUtils {

    private final CryptDao cryptDao;

    public String encrypt(String enc) {
        if (!StringUtils.hasLength(enc)) {
            return "";
        }

        return cryptDao.encrypt(enc);
    }

    public String decrypt(String dec) {
        if (!StringUtils.hasLength(dec)) {
            return "";
        }

        if (dec.length() < 20) {
            return dec;
        }

        return cryptDao.decrypt(dec);
    }

}
