package egovframework._gbe.common.util;

public interface EnumNameParser {
    String getDescription();
    String name();

    default String nameToUrl() {
        return name().toLowerCase()
                .replace("_", "-");
    }
}
