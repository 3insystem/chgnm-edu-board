package egovframework._gbe.common.util;

import egovframework._gbe.common.util.file.FileUtils;
import net.sf.classifier4J.util.WFMPPost;
import org.json.simple.parser.ParseException;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class WebFilterUtils {

    public static String getWfsend(String actionUrl, String listUrl, String writer, String subject, String contents, String file_path, String file_name, String userIp) throws IOException, ParseException {

        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        String url = String.valueOf(request.getRequestURL());
        String chkPort = String.valueOf(request.getServerPort());

      //  System.out.println("url="+url);
      //  System.out.println("chkPort="+chkPort);

        String chkServer = "", fiterServer = "";
        if (url.contains("as.cne.go.kr")) { //운영(내부)
            chkServer = "as.cne.go.kr";
            fiterServer = "103.30.161.149";
        } else if (url.contains("rc.cne.go.kr")) { //운영(외부)
            chkServer = "rc.cne.go.kr";
            fiterServer = "103.30.161.149";
        } else if (url.contains("localhost") || url.contains("127.0.0.1")) { //로컬
            chkServer = "127.0.0.1";
            fiterServer = "183.107.177.64";
        } else if (url.contains("180.210.81.161:8082") || url.contains("180.210.81.161:8083")) { //개발
            chkServer = "180.210.81.161";
            fiterServer = "183.107.177.64";
        }

        String webServer = chkServer;                            // 웹서버 도메인 //180.210.81.161, 127.0.0.1, as.cne.go.kr(내부), rc.cne.go.kr(외부)
        String webServerPort = chkPort;                          // 웹서버 포트 //8282, 80
        String webFilterServer = fiterServer;                    // 웹필터 도메인 - wf.cne.go.kr (103.30.161.149) , 테스트서버용 => 183.107.177.64 (외부 테스트 가능)
        int webFilterServerPort = 80;                            // 웹필터 포트(0~65535)
        int timeout = 300;                                        // 웹필터타임아웃(초 1~3600)
        String charSet = "utf8";                                // 웹서버캐릭터셋(utf8/euc-kr)
        String mode = "JSON";                                    // 응답형태(JSON/URL)
        String contextRoot = "";                                // 컨텍스트루트
        String debug = "FALSE";                                    // 디버그 출력 사용여부(FALSE/TRUE)

        WFMPPost wfsend = new WFMPPost(webServer, webServerPort, webFilterServer, webFilterServerPort, timeout, charSet, mode, contextRoot, debug);

        String protocol = "http";								// 전송프로토콜

        String wfResponse = wfsend.sendWebFilter(protocol, actionUrl, listUrl, writer, subject, contents, file_path, file_name, userIp);

        String chkMsg = "";
        if (wfsend.wfGetStatus(wfResponse).equals("Y")) {
            System.out.println("▶ 결과 : " + wfsend.getResultMsg());
           // System.out.println("");

            if (mode.equals("JSON")) {

                FileUtils.deleteFilePath(file_path.replace("|", ""));

                //System.out.println("!!!+1");
                //------------------------------------------------------------------------------------------------------------------------------------------------------
                // JSON 응답
                //------------------------------------------------------------------------------------------------------------------------------------------------------
                //System.out.println("▶ 웹필터 JSON BASE64 DECODE 응답");
                //System.out.println(wfsend.wfGetJsonMsgDecode(wfResponse)); // JSON 응답 base64 디코드 데이타
                //System.out.println("");
                //System.out.println("▶ 관리자 지정 검출 문구");
                //System.out.println(wfsend.wfGetMessage(wfResponse)); // 차단메세지 (관리자페이지에서 설정한 검출메세지)
                chkMsg = "개인정보보호법 제24조 및 제29조에 따라 홈페이지를 통한 고유식별번호 등록을 차단하고 있습니다. 귀하가 작성하신 글은 고유식별번호를 포함하므로 등록을 제한합니다.";
                //System.out.println("");
                //System.out.println("▶ 본문/파일 개인정보 검출내역");
                //System.out.println(wfsend.wfGetDetectResult(wfResponse)); // 차단내역 (본문,파일 검출내역- 분류, 건수, 내역)
                //System.out.println("");
            } else {
                //------------------------------------------------------------------------------------------------------------------------------------------------------
                // 팝업 차단 URL 응답 = 이 부분은 사용안함
                //------------------------------------------------------------------------------------------------------------------------------------------------------
                //System.out.println("▶ 웹필터 차단 URL");
                //System.out.println(wfsend.getDenyURL()); // 차단 팝업창 URL (새창으로 사용자에게 띄워주어야 함)
                //System.out.println("");
            }


        } else if (wfsend.wfGetStatus(wfResponse).equals("N")) { //개인정보 미검출 (참일 경우)
            //System.out.println("!!!+2");
            //System.out.println("▶ 결과 : " + wfsend.getResultMsg());
            //System.out.println("");
            chkMsg = "";
        } else if (wfsend.wfGetStatus(wfResponse).equals("F")) {
           // System.out.println("!!!+3");
            //System.out.println("▶ 결과 : " + wfsend.getResultMsg());
            //System.out.println("");
            chkMsg = "개인정보보호법 제24조 및 제29조에 따라 홈페이지를 통한 고유식별번호 등록을 차단하고 있습니다. 귀하가 작성하신 글은 고유식별번호를 포함하므로 등록을 제한합니다.";
        } else if (wfsend.wfGetStatus(wfResponse).equals("B")) {
           // System.out.println("!!!+4");
            //System.out.println("▶ 결과 : " + wfsend.getResultMsg());
            //System.out.println("");
            chkMsg = "개인정보보호법 제24조 및 제29조에 따라 홈페이지를 통한 고유식별번호 등록을 차단하고 있습니다. 귀하가 작성하신 글은 고유식별번호를 포함하므로 등록을 제한합니다.";
        }

        if (chkMsg == "") {
            if (wfsend.getResultMsg().equals("개인정보 검출 확인")) {
                chkMsg = "개인정보보호법 제24조 및 제29조에 따라 홈페이지를 통한 고유식별번호 등록을 차단하고 있습니다. 귀하가 작성하신 글은 고유식별번호를 포함하므로 등록을 제한합니다.";
            }
        }

        return chkMsg;
    }
}
