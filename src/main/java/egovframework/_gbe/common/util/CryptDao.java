package egovframework._gbe.common.util;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CryptDao {

    public String encrypt(String enc);

    public String decrypt(String dec);
}
