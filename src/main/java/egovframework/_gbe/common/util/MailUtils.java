package egovframework._gbe.common.util;

import egovframework._gbe.common.parameter.MailParam;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.internet.MimeMessage;

@Component
public class MailUtils {
    private static JavaMailSender mailSender = null;

    MailUtils(JavaMailSender mailSender){
        this.mailSender = mailSender;
    }

    public static String certifyMail(MailParam mailParam) {
        String num = CommonUtils.generateRandomNum(6);
        boolean flag = false;

        try {
            MimeMessage message = mailSender.createMimeMessage();
            MimeMessageHelper messageHelper = new MimeMessageHelper(message, false, "UTF-8");

            messageHelper.setSubject("[충청남도 교육청 장애관리 시스템] 인증번호를 입력해 주세요.");
            messageHelper.setText("인증번호 [" + num + "]를 화면의 인증번호란에 입력해 주세요.", true);
            messageHelper.setFrom("hdo03072@gmail.com");

            messageHelper.setTo(mailParam.getEmail());

            mailSender.send(message); // 메일발송
            flag = true;
        }
        catch(Exception e){
            e.printStackTrace();
            flag = false;
        }

        if (flag) {
            return num;
        } else {
            return null;
        }
    }
}
