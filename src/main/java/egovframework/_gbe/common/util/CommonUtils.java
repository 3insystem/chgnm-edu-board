package egovframework._gbe.common.util;

import egovframework._gbe.common.parameter.SearchParam;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.UnknownHostException;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class CommonUtils {

    // 접수번호 공통 추출
    public String getRceptNo() {
        String prefix = "A";
        String now = LocalDate.parse(LocalDate.now().toString(), DateTimeFormatter.ofPattern("yyyyMMdd")).toString();
        String no = String.join("-", new String[]{prefix, now});

        return "";
    }

    public static String userId(String userId) {
        return userId + "_CMPNY!";
    }

    public static String generateRandomNum(int num) {
        return RandomStringUtils.randomNumeric(num);
    }

    public static String stringToBizrno(String bizrno) {
        if (bizrno == null) {
            return "";
        }
        if (bizrno.length() == 8) {
            return bizrno.replaceFirst("^([0-9]{4})([0-9]{4})$", "$1-$2");
        } else if (bizrno.length() == 12) {
            return bizrno.replaceFirst("(^[0-9]{4})([0-9]{4})([0-9]{4})$", "$1-$2-$3");
        }
        return bizrno.replaceFirst("(^02|[0-9]{3})([0-9]{2})([0-9]{5})$", "$1-$2-$3");
    }

    public static Pageable setListCount(SearchParam searchParam, Pageable pageable) {
        if (searchParam.getListCount() != null) {
            pageable = PageRequest.of(pageable.getPageNumber(), searchParam.getListCount());
        }

        return pageable;
    }

    //ip
    public static String getClientIp(HttpServletRequest request) throws UnknownHostException {
        String ip = request.getHeader("X-Forwarded-For");

        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("X-Real-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("X-RealIP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("REMOTE_ADDR");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }

        if (ip.equals("0:0:0:0:0:0:0:1") || ip.equals("127.0.0.1")) {
            InetAddress address = InetAddress.getLocalHost();
            ip = address.getHostAddress();
//            ip = address.getHostName() + "/" + address.getHostAddress();
        }

        return ip;
    }

    public static String getServerType() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        String serverType = "local";
        String url = String.valueOf(request.getRequestURL());

        if(url.contains("180.210.81.161:8082")|| url.contains("180.210.81.161:8282") || url.contains("localhost") || url.contains("rc.cne.go.kr")) { //rc
            serverType = "cmpny";

        }else if(url.contains("180.210.81.161:8081")|| url.contains("180.210.81.161:8283") || url.contains("127.0.0.1") || url.contains("as.cne.go.kr") ) { //as
            serverType = "instt";

        }
        return serverType;
    }
}
