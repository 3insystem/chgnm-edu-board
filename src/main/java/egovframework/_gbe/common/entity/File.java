package egovframework._gbe.common.entity;

import lombok.Getter;

import javax.persistence.*;

@Getter
@MappedSuperclass
public class File extends BaseEntity {

    @Column(name = "FILE_STRE_COURS")
    protected String fileStreCours;

    @Column(name = "STRE_FILE_NM")
    protected String streFileNm;

    @Column(name = "ORIGNL_FILE_NM")
    protected String orignlFileNm;

    @Column(name = "FILE_EXTSN")
    protected String fileExtsn;

    @Column(name = "FILE_CN")
    protected String fileCn;

    @Column(name = "FILE_SIZE")
    protected Long fileSize;
}
