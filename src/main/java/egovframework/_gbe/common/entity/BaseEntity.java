package egovframework._gbe.common.entity;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class BaseEntity {

    @CreatedBy
    @Column(name = "REGISTER_ID")
    private String registerId;

    @CreatedDate
    @Column(name = "REGIST_DT")
    private LocalDateTime registDt;

    @LastModifiedBy
    @Column(name = "UPDUSR_ID")
    private String updusrId;

    @LastModifiedDate
    @Column(name = "UPDT_DT")
    private LocalDateTime updtDt;
}
