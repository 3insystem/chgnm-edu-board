package egovframework._gbe.common.interceptor;

import egovframework._gbe.common.util.SecurityUtils;
import egovframework._gbe.domain.log.Log;
import egovframework._gbe.domain.log.SystemName;
import egovframework._gbe.domain.user.User;
import egovframework._gbe.repository.log.LogGuestDao;
import egovframework._gbe.repository.log.LogRepository;
import egovframework._gbe.repository.user.UserRepository;
import egovframework._gbe.web.dto.log.LogGuestDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.InetAddress;
import java.net.UnknownHostException;

@Slf4j
@Configuration
@Component
@RequiredArgsConstructor
public class LogInterceptor implements HandlerInterceptor {

    private final LogRepository logRepository;
    private final UserRepository userRepository;

    private final LogGuestDao logGuestDao;

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        String method = request.getMethod();
        String systemNm = getSystemNm((HandlerMethod) handler);
        String systemDetail = getSystemDetailName((HandlerMethod) handler);

        String userInfo = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
        if (StringUtils.hasLength(systemNm)) {
            if (!userInfo.equals("anonymousUser")) {
                Log log = Log.builder()
                        .user(getLoginUser())
                        .url(request.getRequestURI())
                        .sysNm(systemNm)
                        .sysDetail(systemDetail)
                        .mth(method)
                        .ip(getClientIp(request))
                        .build();

                logRepository.save(log);
            } else {
                LogGuestDto log = new LogGuestDto(userInfo
                        ,request.getRequestURI()
                        ,systemNm
                        ,systemDetail
                        ,method
                        ,getClientIp(request)
                        ,userInfo
                        ,userInfo);

                logGuestDao.insSysLog(log);
            }
        }
    }

    private String getSystemNm(HandlerMethod handlerMethod) {
        SystemName systemNm = handlerMethod.getMethodAnnotation(SystemName.class);

        if (systemNm != null) {
            return systemNm.name();
        } else {
            return "";
        }
    }

    private String getSystemDetailName(HandlerMethod handlerMethod) {
        SystemName systemNm = handlerMethod.getMethodAnnotation(SystemName.class);

        if (systemNm != null) {
            return systemNm.detail();
        } else {
            return "";
        }
    }

    private static String getClientIp(HttpServletRequest request) throws UnknownHostException {
        String ip = request.getHeader("X-Forwarded-For");

        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("X-Real-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("X-RealIP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("REMOTE_ADDR");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }

        if (ip.equals("0:0:0:0:0:0:0:1") || ip.equals("127.0.0.1")) {
            InetAddress address = InetAddress.getLocalHost();
            ip = address.getHostAddress();
//            ip = address.getHostName() + "/" + address.getHostAddress();
        }

        return ip;
    }

    private User getLoginUser() {
        return userRepository.getOne(SecurityUtils.getLoginUser().getUserId());
    }
}
