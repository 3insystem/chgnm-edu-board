package egovframework._gbe.common.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WebResponse {

    private int status;

    private String message;
    private String msg;

    private String redirect;

    private Object object;

    public WebResponse() {

    }
    public WebResponse(int status, String message, String redirect) {
        this.status = status;
        this.message = message;
        this.redirect = redirect;
    }

    public WebResponse(int status, String message) {
        this.status = status;
        this.message = message;
    }
}
