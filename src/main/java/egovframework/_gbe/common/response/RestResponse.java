package egovframework._gbe.common.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RestResponse<T> {

    /**
     * 성공여부
     */
    private boolean success;
    /**
     * 메세지
     */
    private String message;
    /**
     * 데이터
     */
    private T data;

    public RestResponse(String message, T data) {
        this.success = true;
        this.message = message;
        this.data = data;
    }



}
