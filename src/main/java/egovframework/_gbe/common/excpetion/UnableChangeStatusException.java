package egovframework._gbe.common.excpetion;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class UnableChangeStatusException extends Exception {

    public UnableChangeStatusException(String message) {
        super(message);
    }
}
