package egovframework._gbe.domain.code;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QCmmnCode is a Querydsl query type for CmmnCode
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QCmmnCode extends EntityPathBase<CmmnCode> {

    private static final long serialVersionUID = 2010673337L;

    public static final QCmmnCode cmmnCode = new QCmmnCode("cmmnCode");

    public final egovframework._gbe.common.entity.QBaseEntity _super = new egovframework._gbe.common.entity.QBaseEntity(this);

    public final StringPath clCode = createString("clCode");

    public final StringPath clNm = createString("clNm");

    public final StringPath code = createString("code");

    public final StringPath codeDc = createString("codeDc");

    public final StringPath codeDp = createString("codeDp");

    public final StringPath codeNm = createString("codeNm");

    public final StringPath codeSn = createString("codeSn");

    public final StringPath delAt = createString("delAt");

    public final StringPath ncnm = createString("ncnm");

    public final StringPath parntsCode = createString("parntsCode");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> registDt = _super.registDt;

    //inherited
    public final StringPath registerId = _super.registerId;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> updtDt = _super.updtDt;

    //inherited
    public final StringPath updusrId = _super.updusrId;

    public final StringPath useBeginDe = createString("useBeginDe");

    public final StringPath useEndDe = createString("useEndDe");

    public QCmmnCode(String variable) {
        super(CmmnCode.class, forVariable(variable));
    }

    public QCmmnCode(Path<? extends CmmnCode> path) {
        super(path.getType(), path.getMetadata());
    }

    public QCmmnCode(PathMetadata metadata) {
        super(CmmnCode.class, metadata);
    }

}

