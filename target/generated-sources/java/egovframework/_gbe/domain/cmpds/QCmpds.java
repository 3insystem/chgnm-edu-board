package egovframework._gbe.domain.cmpds;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QCmpds is a Querydsl query type for Cmpds
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QCmpds extends EntityPathBase<Cmpds> {

    private static final long serialVersionUID = 188586748L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QCmpds cmpds = new QCmpds("cmpds");

    public final egovframework._gbe.common.entity.QBaseEntity _super = new egovframework._gbe.common.entity.QBaseEntity(this);

    public final egovframework._gbe.domain.user.cmpny.QCmpny bizrno;

    public final egovframework._gbe.domain.user.QUser chargerUser;

    public final ListPath<egovframework._gbe.domain.cmpds.file.CmpdsFile, egovframework._gbe.domain.cmpds.file.QCmpdsFile> cmpdsFiles = this.<egovframework._gbe.domain.cmpds.file.CmpdsFile, egovframework._gbe.domain.cmpds.file.QCmpdsFile>createList("cmpdsFiles", egovframework._gbe.domain.cmpds.file.CmpdsFile.class, egovframework._gbe.domain.cmpds.file.QCmpdsFile.class, PathInits.DIRECT2);

    public final DateTimePath<java.time.LocalDateTime> comptDe = createDateTime("comptDe", java.time.LocalDateTime.class);

    public final egovframework._gbe.domain.user.QUser comptUser;

    public final StringPath content = createString("content");

    public final StringPath cttpcl = createString("cttpcl");

    public final BooleanPath delAt = createBoolean("delAt");

    public final StringPath expendablesType = createString("expendablesType");

    public final egovframework._gbe.domain.user.organization.QOrganizaion orgCd;

    public final StringPath processCn = createString("processCn");

    public final DateTimePath<java.time.LocalDateTime> rceptDe = createDateTime("rceptDe", java.time.LocalDateTime.class);

    public final StringPath rceptNo = createString("rceptNo");

    public final egovframework._gbe.domain.user.QUser rceptUser;

    public final ListPath<egovframework._gbe.domain.cmpds.file.CmpdsFile, egovframework._gbe.domain.cmpds.file.QCmpdsFile> referAtchmnfl = this.<egovframework._gbe.domain.cmpds.file.CmpdsFile, egovframework._gbe.domain.cmpds.file.QCmpdsFile>createList("referAtchmnfl", egovframework._gbe.domain.cmpds.file.CmpdsFile.class, egovframework._gbe.domain.cmpds.file.QCmpdsFile.class, PathInits.DIRECT2);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> registDt = _super.registDt;

    //inherited
    public final StringPath registerId = _super.registerId;

    public final StringPath statusType = createString("statusType");

    public final StringPath subject = createString("subject");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> updtDt = _super.updtDt;

    //inherited
    public final StringPath updusrId = _super.updusrId;

    public QCmpds(String variable) {
        this(Cmpds.class, forVariable(variable), INITS);
    }

    public QCmpds(Path<? extends Cmpds> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QCmpds(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QCmpds(PathMetadata metadata, PathInits inits) {
        this(Cmpds.class, metadata, inits);
    }

    public QCmpds(Class<? extends Cmpds> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.bizrno = inits.isInitialized("bizrno") ? new egovframework._gbe.domain.user.cmpny.QCmpny(forProperty("bizrno"), inits.get("bizrno")) : null;
        this.chargerUser = inits.isInitialized("chargerUser") ? new egovframework._gbe.domain.user.QUser(forProperty("chargerUser"), inits.get("chargerUser")) : null;
        this.comptUser = inits.isInitialized("comptUser") ? new egovframework._gbe.domain.user.QUser(forProperty("comptUser"), inits.get("comptUser")) : null;
        this.orgCd = inits.isInitialized("orgCd") ? new egovframework._gbe.domain.user.organization.QOrganizaion(forProperty("orgCd")) : null;
        this.rceptUser = inits.isInitialized("rceptUser") ? new egovframework._gbe.domain.user.QUser(forProperty("rceptUser"), inits.get("rceptUser")) : null;
    }

}

