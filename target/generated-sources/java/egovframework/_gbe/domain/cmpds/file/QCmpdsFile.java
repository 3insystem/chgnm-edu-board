package egovframework._gbe.domain.cmpds.file;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QCmpdsFile is a Querydsl query type for CmpdsFile
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QCmpdsFile extends EntityPathBase<CmpdsFile> {

    private static final long serialVersionUID = -1812872584L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QCmpdsFile cmpdsFile = new QCmpdsFile("cmpdsFile");

    public final egovframework._gbe.common.entity.QFile _super = new egovframework._gbe.common.entity.QFile(this);

    public final StringPath atchAt = createString("atchAt");

    public final egovframework._gbe.domain.cmpds.QCmpds cmpds;

    //inherited
    public final StringPath fileCn = _super.fileCn;

    //inherited
    public final StringPath fileExtsn = _super.fileExtsn;

    public final NumberPath<Long> fileNo = createNumber("fileNo", Long.class);

    //inherited
    public final NumberPath<Long> fileSize = _super.fileSize;

    //inherited
    public final StringPath fileStreCours = _super.fileStreCours;

    //inherited
    public final StringPath orignlFileNm = _super.orignlFileNm;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> registDt = _super.registDt;

    //inherited
    public final StringPath registerId = _super.registerId;

    //inherited
    public final StringPath streFileNm = _super.streFileNm;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> updtDt = _super.updtDt;

    //inherited
    public final StringPath updusrId = _super.updusrId;

    public QCmpdsFile(String variable) {
        this(CmpdsFile.class, forVariable(variable), INITS);
    }

    public QCmpdsFile(Path<? extends CmpdsFile> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QCmpdsFile(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QCmpdsFile(PathMetadata metadata, PathInits inits) {
        this(CmpdsFile.class, metadata, inits);
    }

    public QCmpdsFile(Class<? extends CmpdsFile> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.cmpds = inits.isInitialized("cmpds") ? new egovframework._gbe.domain.cmpds.QCmpds(forProperty("cmpds"), inits.get("cmpds")) : null;
    }

}

