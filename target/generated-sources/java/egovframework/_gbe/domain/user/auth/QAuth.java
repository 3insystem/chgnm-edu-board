package egovframework._gbe.domain.user.auth;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QAuth is a Querydsl query type for Auth
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QAuth extends EntityPathBase<Auth> {

    private static final long serialVersionUID = -1140259253L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QAuth auth = new QAuth("auth");

    public final egovframework._gbe.domain.code.QCmmnCode authSe;

    public final StringPath delAt = createString("delAt");

    public final egovframework._gbe.domain.user.organization.QOrganizaion orgCd;

    public final DatePath<java.time.LocalDate> registDt = createDate("registDt", java.time.LocalDate.class);

    public final StringPath registerId = createString("registerId");

    public final DatePath<java.time.LocalDate> updtDt = createDate("updtDt", java.time.LocalDate.class);

    public final StringPath updusrId = createString("updusrId");

    public final StringPath usid = createString("usid");

    public QAuth(String variable) {
        this(Auth.class, forVariable(variable), INITS);
    }

    public QAuth(Path<? extends Auth> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QAuth(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QAuth(PathMetadata metadata, PathInits inits) {
        this(Auth.class, metadata, inits);
    }

    public QAuth(Class<? extends Auth> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.authSe = inits.isInitialized("authSe") ? new egovframework._gbe.domain.code.QCmmnCode(forProperty("authSe")) : null;
        this.orgCd = inits.isInitialized("orgCd") ? new egovframework._gbe.domain.user.organization.QOrganizaion(forProperty("orgCd")) : null;
    }

}

