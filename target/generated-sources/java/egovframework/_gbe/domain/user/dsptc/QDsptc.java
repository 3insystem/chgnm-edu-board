package egovframework._gbe.domain.user.dsptc;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QDsptc is a Querydsl query type for Dsptc
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QDsptc extends EntityPathBase<Dsptc> {

    private static final long serialVersionUID = 302226703L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QDsptc dsptc = new QDsptc("dsptc");

    public final egovframework._gbe.common.entity.QBaseEntity _super = new egovframework._gbe.common.entity.QBaseEntity(this);

    public final StringPath agreAt = createString("agreAt");

    public final egovframework._gbe.domain.user.organization.QOrganizaion bassOrgCd;

    public final egovframework._gbe.domain.user.QUser confirmer;

    public final DatePath<java.time.LocalDate> confmDe = createDate("confmDe", java.time.LocalDate.class);

    public final StringPath delAt = createString("delAt");

    public final StringPath dsptcBgnde = createString("dsptcBgnde");

    public final StringPath dsptcEndde = createString("dsptcEndde");

    public final StringPath dsptcNo = createString("dsptcNo");

    public final egovframework._gbe.domain.user.organization.QOrganizaion dsptcOrgCd;

    public final egovframework._gbe.domain.code.QCmmnCode dsptcSe;

    public final egovframework._gbe.domain.user.organization.QOrganizaion orgCd;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> registDt = _super.registDt;

    //inherited
    public final StringPath registerId = _super.registerId;

    public final StringPath rm = createString("rm");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> updtDt = _super.updtDt;

    //inherited
    public final StringPath updusrId = _super.updusrId;

    public final egovframework._gbe.domain.user.QUser user;

    public QDsptc(String variable) {
        this(Dsptc.class, forVariable(variable), INITS);
    }

    public QDsptc(Path<? extends Dsptc> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QDsptc(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QDsptc(PathMetadata metadata, PathInits inits) {
        this(Dsptc.class, metadata, inits);
    }

    public QDsptc(Class<? extends Dsptc> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.bassOrgCd = inits.isInitialized("bassOrgCd") ? new egovframework._gbe.domain.user.organization.QOrganizaion(forProperty("bassOrgCd")) : null;
        this.confirmer = inits.isInitialized("confirmer") ? new egovframework._gbe.domain.user.QUser(forProperty("confirmer"), inits.get("confirmer")) : null;
        this.dsptcOrgCd = inits.isInitialized("dsptcOrgCd") ? new egovframework._gbe.domain.user.organization.QOrganizaion(forProperty("dsptcOrgCd")) : null;
        this.dsptcSe = inits.isInitialized("dsptcSe") ? new egovframework._gbe.domain.code.QCmmnCode(forProperty("dsptcSe")) : null;
        this.orgCd = inits.isInitialized("orgCd") ? new egovframework._gbe.domain.user.organization.QOrganizaion(forProperty("orgCd")) : null;
        this.user = inits.isInitialized("user") ? new egovframework._gbe.domain.user.QUser(forProperty("user"), inits.get("user")) : null;
    }

}

