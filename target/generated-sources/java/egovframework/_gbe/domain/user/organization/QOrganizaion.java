package egovframework._gbe.domain.user.organization;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QOrganizaion is a Querydsl query type for Organizaion
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QOrganizaion extends EntityPathBase<Organizaion> {

    private static final long serialVersionUID = 586820625L;

    public static final QOrganizaion organizaion = new QOrganizaion("organizaion");

    public final egovframework._gbe.common.entity.QBaseEntity _super = new egovframework._gbe.common.entity.QBaseEntity(this);

    public final StringPath delYn = createString("delYn");

    public final StringPath orgCd = createString("orgCd");

    public final StringPath orgCdwptrCd = createString("orgCdwptrCd");

    public final StringPath orgNm = createString("orgNm");

    public final StringPath orgSeCd = createString("orgSeCd");

    public final StringPath orgUseYn = createString("orgUseYn");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> registDt = _super.registDt;

    //inherited
    public final StringPath registerId = _super.registerId;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> updtDt = _super.updtDt;

    //inherited
    public final StringPath updusrId = _super.updusrId;

    public final StringPath upperOrgCd = createString("upperOrgCd");

    public final StringPath upperOrgCd2 = createString("upperOrgCd2");

    public QOrganizaion(String variable) {
        super(Organizaion.class, forVariable(variable));
    }

    public QOrganizaion(Path<? extends Organizaion> path) {
        super(path.getType(), path.getMetadata());
    }

    public QOrganizaion(PathMetadata metadata) {
        super(Organizaion.class, metadata);
    }

}

