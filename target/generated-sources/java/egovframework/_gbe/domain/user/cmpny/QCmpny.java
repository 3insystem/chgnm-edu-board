package egovframework._gbe.domain.user.cmpny;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QCmpny is a Querydsl query type for Cmpny
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QCmpny extends EntityPathBase<Cmpny> {

    private static final long serialVersionUID = -539526255L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QCmpny cmpny = new QCmpny("cmpny");

    public final egovframework._gbe.common.entity.QBaseEntity _super = new egovframework._gbe.common.entity.QBaseEntity(this);

    public final StringPath adres = createString("adres");

    public final StringPath bizrno = createString("bizrno");

    public final StringPath bizrnoAtchmnfl = createString("bizrnoAtchmnfl");

    public final QCmpnyFile cmpnyFile;

    public final StringPath cmpnyNm = createString("cmpnyNm");

    public final StringPath companyId = createString("companyId");

    public final StringPath ctprvn = createString("ctprvn");

    public final StringPath delAt = createString("delAt");

    public final StringPath detailAdres = createString("detailAdres");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> registDt = _super.registDt;

    //inherited
    public final StringPath registerId = _super.registerId;

    public final StringPath reprsntTelno = createString("reprsntTelno");

    public final StringPath signgu = createString("signgu");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> updtDt = _super.updtDt;

    //inherited
    public final StringPath updusrId = _super.updusrId;

    public final StringPath zip = createString("zip");

    public QCmpny(String variable) {
        this(Cmpny.class, forVariable(variable), INITS);
    }

    public QCmpny(Path<? extends Cmpny> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QCmpny(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QCmpny(PathMetadata metadata, PathInits inits) {
        this(Cmpny.class, metadata, inits);
    }

    public QCmpny(Class<? extends Cmpny> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.cmpnyFile = inits.isInitialized("cmpnyFile") ? new QCmpnyFile(forProperty("cmpnyFile"), inits.get("cmpnyFile")) : null;
    }

}

