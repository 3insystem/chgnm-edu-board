package egovframework._gbe.domain.user.cmpny;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QCmpnyFile is a Querydsl query type for CmpnyFile
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QCmpnyFile extends EntityPathBase<CmpnyFile> {

    private static final long serialVersionUID = -373377875L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QCmpnyFile cmpnyFile = new QCmpnyFile("cmpnyFile");

    public final egovframework._gbe.common.entity.QFile _super = new egovframework._gbe.common.entity.QFile(this);

    public final QCmpny cmpny;

    //inherited
    public final StringPath fileCn = _super.fileCn;

    //inherited
    public final StringPath fileExtsn = _super.fileExtsn;

    public final NumberPath<Long> fileNo = createNumber("fileNo", Long.class);

    //inherited
    public final NumberPath<Long> fileSize = _super.fileSize;

    //inherited
    public final StringPath fileStreCours = _super.fileStreCours;

    //inherited
    public final StringPath orignlFileNm = _super.orignlFileNm;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> registDt = _super.registDt;

    //inherited
    public final StringPath registerId = _super.registerId;

    //inherited
    public final StringPath streFileNm = _super.streFileNm;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> updtDt = _super.updtDt;

    //inherited
    public final StringPath updusrId = _super.updusrId;

    public QCmpnyFile(String variable) {
        this(CmpnyFile.class, forVariable(variable), INITS);
    }

    public QCmpnyFile(Path<? extends CmpnyFile> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QCmpnyFile(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QCmpnyFile(PathMetadata metadata, PathInits inits) {
        this(CmpnyFile.class, metadata, inits);
    }

    public QCmpnyFile(Class<? extends CmpnyFile> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.cmpny = inits.isInitialized("cmpny") ? new QCmpny(forProperty("cmpny"), inits.get("cmpny")) : null;
    }

}

