package egovframework._gbe.domain.user;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QUser is a Querydsl query type for User
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QUser extends EntityPathBase<User> {

    private static final long serialVersionUID = -493582898L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QUser user = new QUser("user");

    public final egovframework._gbe.domain.user.auth.QAuth _super;

    public final StringPath ahrztEsntlNm = createString("ahrztEsntlNm");

    public final StringPath ahrztNoVal = createString("ahrztNoVal");

    // inherited
    public final egovframework._gbe.domain.code.QCmmnCode authSe;

    public final StringPath changeAt = createString("changeAt");

    public final StringPath clsfCd = createString("clsfCd");

    public final StringPath clsfNm = createString("clsfNm");

    public final egovframework._gbe.domain.user.cmpny.QCmpny cmpny;

    public final egovframework._gbe.domain.user.organization.QOrganizaion cmpsOrgCd;

    public final StringPath cmpsOrgNm = createString("cmpsOrgNm");

    public final NumberPath<Long> companyId = createNumber("companyId", Long.class);

    //inherited
    public final StringPath delAt;

    public final StringPath delYn = createString("delYn");

    public final ListPath<egovframework._gbe.domain.user.dsptc.Dsptc, egovframework._gbe.domain.user.dsptc.QDsptc> dsptcList = this.<egovframework._gbe.domain.user.dsptc.Dsptc, egovframework._gbe.domain.user.dsptc.QDsptc>createList("dsptcList", egovframework._gbe.domain.user.dsptc.Dsptc.class, egovframework._gbe.domain.user.dsptc.QDsptc.class, PathInits.DIRECT2);

    public final StringPath email = createString("email");

    public final StringPath esntlIdntfcAgreAt = createString("esntlIdntfcAgreAt");

    public final StringPath indvdlinfoAgreAt = createString("indvdlinfoAgreAt");

    public final NumberPath<Integer> loginFailCnt = createNumber("loginFailCnt", Integer.class);

    public final StringPath mbtlnum = createString("mbtlnum");

    public final StringPath ofcpsCd = createString("ofcpsCd");

    public final StringPath ofcpsNm = createString("ofcpsNm");

    // inherited
    public final egovframework._gbe.domain.user.organization.QOrganizaion orgCd;

    public final egovframework._gbe.domain.user.organization.QOrganizaion orgCode;

    public final StringPath orgNm = createString("orgNm");

    public final StringPath password = createString("password");

    //inherited
    public final DatePath<java.time.LocalDate> registDt;

    //inherited
    public final StringPath registerId;

    public final DateTimePath<java.time.LocalDateTime> rgstDt = createDateTime("rgstDt", java.time.LocalDateTime.class);

    public final StringPath rgstId = createString("rgstId");

    public final egovframework._gbe.domain.code.QCmmnCode rspofcSe;

    public final StringPath sanctnOfcpsNm = createString("sanctnOfcpsNm");

    public final StringPath stplatAgreAt = createString("stplatAgreAt");

    public final StringPath telno = createString("telno");

    //inherited
    public final DatePath<java.time.LocalDate> updtDt;

    public final StringPath updtId = createString("updtId");

    public final DateTimePath<java.time.LocalDateTime> updusrDt = createDateTime("updusrDt", java.time.LocalDateTime.class);

    //inherited
    public final StringPath updusrId;

    public final StringPath userNm = createString("userNm");

    public final egovframework._gbe.domain.code.QCmmnCode useSe;

    public final StringPath useYn = createString("useYn");

    //inherited
    public final StringPath usid;

    public QUser(String variable) {
        this(User.class, forVariable(variable), INITS);
    }

    public QUser(Path<? extends User> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QUser(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QUser(PathMetadata metadata, PathInits inits) {
        this(User.class, metadata, inits);
    }

    public QUser(Class<? extends User> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this._super = new egovframework._gbe.domain.user.auth.QAuth(type, metadata, inits);
        this.authSe = _super.authSe;
        this.cmpny = inits.isInitialized("cmpny") ? new egovframework._gbe.domain.user.cmpny.QCmpny(forProperty("cmpny"), inits.get("cmpny")) : null;
        this.cmpsOrgCd = inits.isInitialized("cmpsOrgCd") ? new egovframework._gbe.domain.user.organization.QOrganizaion(forProperty("cmpsOrgCd")) : null;
        this.delAt = _super.delAt;
        this.orgCd = _super.orgCd;
        this.orgCode = inits.isInitialized("orgCode") ? new egovframework._gbe.domain.user.organization.QOrganizaion(forProperty("orgCode")) : null;
        this.registDt = _super.registDt;
        this.registerId = _super.registerId;
        this.rspofcSe = inits.isInitialized("rspofcSe") ? new egovframework._gbe.domain.code.QCmmnCode(forProperty("rspofcSe")) : null;
        this.updtDt = _super.updtDt;
        this.updusrId = _super.updusrId;
        this.useSe = inits.isInitialized("useSe") ? new egovframework._gbe.domain.code.QCmmnCode(forProperty("useSe")) : null;
        this.usid = _super.usid;
    }

}

