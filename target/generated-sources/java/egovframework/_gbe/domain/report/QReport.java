package egovframework._gbe.domain.report;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QReport is a Querydsl query type for Report
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QReport extends EntityPathBase<Report> {

    private static final long serialVersionUID = -1152552722L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QReport report = new QReport("report");

    public final egovframework._gbe.common.entity.QBaseEntity _super = new egovframework._gbe.common.entity.QBaseEntity(this);

    public final DatePath<java.time.LocalDate> bgnde = createDate("bgnde", java.time.LocalDate.class);

    public final egovframework._gbe.domain.user.cmpny.QCmpny bizrno;

    public final StringPath chrdId = createString("chrdId");

    public final BooleanPath delAt = createBoolean("delAt");

    public final DateTimePath<java.time.LocalDateTime> delDe = createDateTime("delDe", java.time.LocalDateTime.class);

    public final egovframework._gbe.domain.user.QUser dltrId;

    public final DatePath<java.time.LocalDate> endde = createDate("endde", java.time.LocalDate.class);

    public final StringPath memo = createString("memo");

    public final NumberPath<Integer> month = createNumber("month", Integer.class);

    public final egovframework._gbe.domain.user.organization.QOrganizaion orgCd;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> registDt = _super.registDt;

    //inherited
    public final StringPath registerId = _super.registerId;

    public final ListPath<ReportDetail, QReportDetail> reportDetails = this.<ReportDetail, QReportDetail>createList("reportDetails", ReportDetail.class, QReportDetail.class, PathInits.DIRECT2);

    public final NumberPath<Long> reprtNo = createNumber("reprtNo", Long.class);

    public final StringPath sj = createString("sj");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> updtDt = _super.updtDt;

    //inherited
    public final StringPath updusrId = _super.updusrId;

    public final NumberPath<Integer> year = createNumber("year", Integer.class);

    public QReport(String variable) {
        this(Report.class, forVariable(variable), INITS);
    }

    public QReport(Path<? extends Report> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QReport(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QReport(PathMetadata metadata, PathInits inits) {
        this(Report.class, metadata, inits);
    }

    public QReport(Class<? extends Report> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.bizrno = inits.isInitialized("bizrno") ? new egovframework._gbe.domain.user.cmpny.QCmpny(forProperty("bizrno"), inits.get("bizrno")) : null;
        this.dltrId = inits.isInitialized("dltrId") ? new egovframework._gbe.domain.user.QUser(forProperty("dltrId"), inits.get("dltrId")) : null;
        this.orgCd = inits.isInitialized("orgCd") ? new egovframework._gbe.domain.user.organization.QOrganizaion(forProperty("orgCd")) : null;
    }

}

