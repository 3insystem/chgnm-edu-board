package egovframework._gbe.domain.report;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QReportDetail is a Querydsl query type for ReportDetail
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QReportDetail extends EntityPathBase<ReportDetail> {

    private static final long serialVersionUID = -1353479457L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QReportDetail reportDetail = new QReportDetail("reportDetail");

    public final egovframework._gbe.common.entity.QBaseEntity _super = new egovframework._gbe.common.entity.QBaseEntity(this);

    public final StringPath applcnt = createString("applcnt");

    public final StringPath cmpter = createString("cmpter");

    public final DatePath<java.time.LocalDate> comptDe = createDate("comptDe", java.time.LocalDate.class);

    public final DateTimePath<java.time.LocalDateTime> delDe = createDateTime("delDe", java.time.LocalDateTime.class);

    public final BooleanPath delYn = createBoolean("delYn");

    public final egovframework._gbe.domain.user.QUser dltrId;

    public final StringPath processCn = createString("processCn");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> registDt = _super.registDt;

    //inherited
    public final StringPath registerId = _super.registerId;

    public final QReport report;

    public final NumberPath<Long> reportDetailNo = createNumber("reportDetailNo", Long.class);

    public final DatePath<java.time.LocalDate> rqstdt = createDate("rqstdt", java.time.LocalDate.class);

    public final NumberPath<Integer> sort = createNumber("sort", Integer.class);

    public final StringPath subject = createString("subject");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> updtDt = _super.updtDt;

    //inherited
    public final StringPath updusrId = _super.updusrId;

    public QReportDetail(String variable) {
        this(ReportDetail.class, forVariable(variable), INITS);
    }

    public QReportDetail(Path<? extends ReportDetail> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QReportDetail(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QReportDetail(PathMetadata metadata, PathInits inits) {
        this(ReportDetail.class, metadata, inits);
    }

    public QReportDetail(Class<? extends ReportDetail> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.dltrId = inits.isInitialized("dltrId") ? new egovframework._gbe.domain.user.QUser(forProperty("dltrId"), inits.get("dltrId")) : null;
        this.report = inits.isInitialized("report") ? new QReport(forProperty("report"), inits.get("report")) : null;
    }

}

