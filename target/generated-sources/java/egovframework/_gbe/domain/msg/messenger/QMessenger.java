package egovframework._gbe.domain.msg.messenger;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QMessenger is a Querydsl query type for Messenger
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QMessenger extends EntityPathBase<Messenger> {

    private static final long serialVersionUID = 1792977291L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QMessenger messenger = new QMessenger("messenger");

    public final StringPath content = createString("content");

    public final DateTimePath<java.time.LocalDateTime> dateTime = createDateTime("dateTime", java.time.LocalDateTime.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath sended = createString("sended");

    public final StringPath sndName = createString("sndName");

    public final StringPath subject = createString("subject");

    public final StringPath sysCode = createString("sysCode");

    public final StringPath sysName = createString("sysName");

    public final StringPath url = createString("url");

    public final egovframework._gbe.domain.user.QUser user;

    public QMessenger(String variable) {
        this(Messenger.class, forVariable(variable), INITS);
    }

    public QMessenger(Path<? extends Messenger> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QMessenger(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QMessenger(PathMetadata metadata, PathInits inits) {
        this(Messenger.class, metadata, inits);
    }

    public QMessenger(Class<? extends Messenger> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.user = inits.isInitialized("user") ? new egovframework._gbe.domain.user.QUser(forProperty("user"), inits.get("user")) : null;
    }

}

