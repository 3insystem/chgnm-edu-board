package egovframework._gbe.domain.msg.sms;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QSms is a Querydsl query type for Sms
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QSms extends EntityPathBase<Sms> {

    private static final long serialVersionUID = -469334185L;

    public static final QSms sms = new QSms("sms");

    public final StringPath assignCd = createString("assignCd");

    public final StringPath callbackUrl = createString("callbackUrl");

    public final StringPath cmpMsgGroupId = createString("cmpMsgGroupId");

    public final StringPath cmpRcvDttm = createString("cmpRcvDttm");

    public final StringPath cmpSndDttm = createString("cmpSndDttm");

    public final NumberPath<Integer> contentCnt = createNumber("contentCnt", Integer.class);

    public final StringPath contentMimeType = createString("contentMimeType");

    public final StringPath contentPath = createString("contentPath");

    public final StringPath etcChar1 = createString("etcChar1");

    public final StringPath etcChar2 = createString("etcChar2");

    public final StringPath etcChar3 = createString("etcChar3");

    public final StringPath etcChar4 = createString("etcChar4");

    public final NumberPath<Integer> etcInt5 = createNumber("etcInt5", Integer.class);

    public final NumberPath<Integer> etcInt6 = createNumber("etcInt6", Integer.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath machineId = createString("machineId");

    public final StringPath msgTitle = createString("msgTitle");

    public final StringPath natCd = createString("natCd");

    public final StringPath rcvPhnId = createString("rcvPhnId");

    public final StringPath regRcvDttm = createString("regRcvDttm");

    public final StringPath regSndDttm = createString("regSndDttm");

    public final StringPath reservedDttm = createString("reservedDttm");

    public final StringPath reservedFg = createString("reservedFg");

    public final StringPath rsltVal = createString("rsltVal");

    public final StringPath savedFg = createString("savedFg");

    public final StringPath smsGb = createString("smsGb");

    public final StringPath smsStatus = createString("smsStatus");

    public final StringPath sndMsg = createString("sndMsg");

    public final StringPath sndPhnId = createString("sndPhnId");

    public final StringPath telcoId = createString("telcoId");

    public final StringPath usedCd = createString("usedCd");

    public final StringPath usrId = createString("usrId");

    public QSms(String variable) {
        super(Sms.class, forVariable(variable));
    }

    public QSms(Path<? extends Sms> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSms(PathMetadata metadata) {
        super(Sms.class, metadata);
    }

}

