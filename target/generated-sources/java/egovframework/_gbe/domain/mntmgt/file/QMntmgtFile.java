package egovframework._gbe.domain.mntmgt.file;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QMntmgtFile is a Querydsl query type for MntmgtFile
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QMntmgtFile extends EntityPathBase<MntmgtFile> {

    private static final long serialVersionUID = 272055534L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QMntmgtFile mntmgtFile = new QMntmgtFile("mntmgtFile");

    public final egovframework._gbe.common.entity.QFile _super = new egovframework._gbe.common.entity.QFile(this);

    public final StringPath atchAt = createString("atchAt");

    //inherited
    public final StringPath fileCn = _super.fileCn;

    //inherited
    public final StringPath fileExtsn = _super.fileExtsn;

    public final NumberPath<Long> fileNo = createNumber("fileNo", Long.class);

    //inherited
    public final NumberPath<Long> fileSize = _super.fileSize;

    //inherited
    public final StringPath fileStreCours = _super.fileStreCours;

    public final egovframework._gbe.domain.mntmgt.QMntmgt mntmgt;

    //inherited
    public final StringPath orignlFileNm = _super.orignlFileNm;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> registDt = _super.registDt;

    //inherited
    public final StringPath registerId = _super.registerId;

    //inherited
    public final StringPath streFileNm = _super.streFileNm;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> updtDt = _super.updtDt;

    //inherited
    public final StringPath updusrId = _super.updusrId;

    public QMntmgtFile(String variable) {
        this(MntmgtFile.class, forVariable(variable), INITS);
    }

    public QMntmgtFile(Path<? extends MntmgtFile> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QMntmgtFile(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QMntmgtFile(PathMetadata metadata, PathInits inits) {
        this(MntmgtFile.class, metadata, inits);
    }

    public QMntmgtFile(Class<? extends MntmgtFile> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.mntmgt = inits.isInitialized("mntmgt") ? new egovframework._gbe.domain.mntmgt.QMntmgt(forProperty("mntmgt"), inits.get("mntmgt")) : null;
    }

}

