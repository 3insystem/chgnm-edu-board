package egovframework._gbe.domain.mntmgt;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QMntmgt is a Querydsl query type for Mntmgt
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QMntmgt extends EntityPathBase<Mntmgt> {

    private static final long serialVersionUID = 1543808398L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QMntmgt mntmgt = new QMntmgt("mntmgt");

    public final egovframework._gbe.common.entity.QBaseEntity _super = new egovframework._gbe.common.entity.QBaseEntity(this);

    public final egovframework._gbe.domain.user.cmpny.QCmpny bizrno;

    public final egovframework._gbe.domain.user.QUser chargerUser;

    public final StringPath clSe = createString("clSe");

    public final DateTimePath<java.time.LocalDateTime> comptDe = createDateTime("comptDe", java.time.LocalDateTime.class);

    public final egovframework._gbe.domain.user.QUser comptUser;

    public final StringPath content = createString("content");

    public final StringPath cttpcl = createString("cttpcl");

    public final BooleanPath delAt = createBoolean("delAt");

    public final ComparablePath<Character> emrgncyAt = createComparable("emrgncyAt", Character.class);

    public final DateTimePath<java.time.LocalDateTime> lastComptDe = createDateTime("lastComptDe", java.time.LocalDateTime.class);

    public final egovframework._gbe.domain.user.QUser lastComptUser;

    public final StringPath maintenanceType = createString("maintenanceType");

    public final ListPath<egovframework._gbe.domain.mntmgt.file.MntmgtFile, egovframework._gbe.domain.mntmgt.file.QMntmgtFile> mntmgtFile = this.<egovframework._gbe.domain.mntmgt.file.MntmgtFile, egovframework._gbe.domain.mntmgt.file.QMntmgtFile>createList("mntmgtFile", egovframework._gbe.domain.mntmgt.file.MntmgtFile.class, egovframework._gbe.domain.mntmgt.file.QMntmgtFile.class, PathInits.DIRECT2);

    public final egovframework._gbe.domain.user.organization.QOrganizaion orgCd;

    public final StringPath processCn = createString("processCn");

    public final DateTimePath<java.time.LocalDateTime> rceptDe = createDateTime("rceptDe", java.time.LocalDateTime.class);

    public final StringPath rceptNo = createString("rceptNo");

    public final egovframework._gbe.domain.user.QUser rceptUser;

    public final ListPath<egovframework._gbe.domain.mntmgt.file.MntmgtFile, egovframework._gbe.domain.mntmgt.file.QMntmgtFile> referAtchmnfl = this.<egovframework._gbe.domain.mntmgt.file.MntmgtFile, egovframework._gbe.domain.mntmgt.file.QMntmgtFile>createList("referAtchmnfl", egovframework._gbe.domain.mntmgt.file.MntmgtFile.class, egovframework._gbe.domain.mntmgt.file.QMntmgtFile.class, PathInits.DIRECT2);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> registDt = _super.registDt;

    //inherited
    public final StringPath registerId = _super.registerId;

    public final StringPath statusType = createString("statusType");

    public final StringPath subject = createString("subject");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> updtDt = _super.updtDt;

    //inherited
    public final StringPath updusrId = _super.updusrId;

    public QMntmgt(String variable) {
        this(Mntmgt.class, forVariable(variable), INITS);
    }

    public QMntmgt(Path<? extends Mntmgt> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QMntmgt(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QMntmgt(PathMetadata metadata, PathInits inits) {
        this(Mntmgt.class, metadata, inits);
    }

    public QMntmgt(Class<? extends Mntmgt> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.bizrno = inits.isInitialized("bizrno") ? new egovframework._gbe.domain.user.cmpny.QCmpny(forProperty("bizrno"), inits.get("bizrno")) : null;
        this.chargerUser = inits.isInitialized("chargerUser") ? new egovframework._gbe.domain.user.QUser(forProperty("chargerUser"), inits.get("chargerUser")) : null;
        this.comptUser = inits.isInitialized("comptUser") ? new egovframework._gbe.domain.user.QUser(forProperty("comptUser"), inits.get("comptUser")) : null;
        this.lastComptUser = inits.isInitialized("lastComptUser") ? new egovframework._gbe.domain.user.QUser(forProperty("lastComptUser"), inits.get("lastComptUser")) : null;
        this.orgCd = inits.isInitialized("orgCd") ? new egovframework._gbe.domain.user.organization.QOrganizaion(forProperty("orgCd")) : null;
        this.rceptUser = inits.isInitialized("rceptUser") ? new egovframework._gbe.domain.user.QUser(forProperty("rceptUser"), inits.get("rceptUser")) : null;
    }

}

