package egovframework._gbe.common.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QFile is a Querydsl query type for File
 */
@Generated("com.querydsl.codegen.DefaultSupertypeSerializer")
public class QFile extends EntityPathBase<File> {

    private static final long serialVersionUID = -228994802L;

    public static final QFile file = new QFile("file");

    public final QBaseEntity _super = new QBaseEntity(this);

    public final StringPath fileCn = createString("fileCn");

    public final StringPath fileExtsn = createString("fileExtsn");

    public final NumberPath<Long> fileSize = createNumber("fileSize", Long.class);

    public final StringPath fileStreCours = createString("fileStreCours");

    public final StringPath orignlFileNm = createString("orignlFileNm");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> registDt = _super.registDt;

    //inherited
    public final StringPath registerId = _super.registerId;

    public final StringPath streFileNm = createString("streFileNm");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> updtDt = _super.updtDt;

    //inherited
    public final StringPath updusrId = _super.updusrId;

    public QFile(String variable) {
        super(File.class, forVariable(variable));
    }

    public QFile(Path<? extends File> path) {
        super(path.getType(), path.getMetadata());
    }

    public QFile(PathMetadata metadata) {
        super(File.class, metadata);
    }

}

