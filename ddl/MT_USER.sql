--------------------------------------------------------
--  DDL for Table USER_USER
--------------------------------------------------------

CREATE TABLE "ITE"."MT_USER"
(
    "USER_ID"             NUMBER,
    "LOGIN_NAME"          VARCHAR2(512 BYTE),
    "PW"                  VARCHAR2(500 BYTE),
    "NAME"                VARCHAR2(128 BYTE),
    "DISPLAY_NAME"        VARCHAR2(128 BYTE),
    "BIRTHDAY"            DATE,
    "PIC_FILE_PATH"       VARCHAR2(2000 BYTE),
    "INTRO"               VARCHAR2(4000 BYTE),
    "LOGIN_FAIL_CNT"      NUMBER,
    "REAL_NAME_CERT_KIND" VARCHAR2(80 BYTE),
    "REAL_NAME_CERT_KEY"  VARCHAR2(4000 BYTE),
    "REAL_NAME_CERT_DTM"  DATE,
    "STS_KIND"            VARCHAR2(80 BYTE),
    "STS_DTM"             DATE,
    "INS_ID"              VARCHAR2(80 BYTE),
    "INS_DTM"             DATE,
    "UPD_ID"              VARCHAR2(80 BYTE),
    "UPD_DTM"             DATE,
    "COMPANY_ID"          NUMBER,
    "POSITION_ID"         NUMBER,
    "DEL_YN"              CHAR(1 BYTE)     DEFAULT 'N',
    "PHONE"               VARCHAR2(80 BYTE),
    "ORG_CD"              VARCHAR2(80 BYTE),
    "ORG_NM"              VARCHAR2(1000 BYTE),
    "OFCPS_NM"            VARCHAR2(100 BYTE),
    "OFCPS_CD"            VARCHAR2(10 BYTE),
    "USE_YN"              VARCHAR2(1 BYTE) DEFAULT 'Y',
    "CMPS_ORG_CD"         VARCHAR2(10 BYTE),
    "CMPS_ORG_NM"         VARCHAR2(10 BYTE),
    "CLSF_CD"             VARCHAR2(10 BYTE),
    "CLSF_NM"             VARCHAR2(100 BYTE)
) SEGMENT CREATION IMMEDIATE
    PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
    STORAGE
(
    INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
    PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT
)
    TABLESPACE "ITE";

COMMENT ON COLUMN "ITE"."MT_USER"."USER_ID" IS '사용자ID';
COMMENT ON COLUMN "ITE"."MT_USER"."LOGIN_NAME" IS '로그인ID';
COMMENT ON COLUMN "ITE"."MT_USER"."PW" IS '비밀번호';
COMMENT ON COLUMN "ITE"."MT_USER"."NAME" IS '성명';
COMMENT ON COLUMN "ITE"."MT_USER"."DISPLAY_NAME" IS '별명';
COMMENT ON COLUMN "ITE"."MT_USER"."BIRTHDAY" IS '생년월일';
COMMENT ON COLUMN "ITE"."MT_USER"."PIC_FILE_PATH" IS '사진파일경로';
COMMENT ON COLUMN "ITE"."MT_USER"."INTRO" IS '자기소개';
COMMENT ON COLUMN "ITE"."MT_USER"."LOGIN_FAIL_CNT" IS '로그인실패횟수';
COMMENT ON COLUMN "ITE"."MT_USER"."REAL_NAME_CERT_KIND" IS '본인인증구분';
COMMENT ON COLUMN "ITE"."MT_USER"."REAL_NAME_CERT_KEY" IS '본인인증키';
COMMENT ON COLUMN "ITE"."MT_USER"."REAL_NAME_CERT_DTM" IS '본인인증일시';
COMMENT ON COLUMN "ITE"."MT_USER"."STS_KIND" IS '상태구분';
COMMENT ON COLUMN "ITE"."MT_USER"."STS_DTM" IS '상태변경일시';
COMMENT ON COLUMN "ITE"."MT_USER"."INS_ID" IS '등록자';
COMMENT ON COLUMN "ITE"."MT_USER"."INS_DTM" IS '등록일시';
COMMENT ON COLUMN "ITE"."MT_USER"."UPD_ID" IS '수정자';
COMMENT ON COLUMN "ITE"."MT_USER"."UPD_DTM" IS '수정일시';
COMMENT ON COLUMN "ITE"."MT_USER"."ORG_CD" IS '기관코드';
COMMENT ON COLUMN "ITE"."MT_USER"."ORG_NM" IS '조직명';
COMMENT ON COLUMN "ITE"."MT_USER"."OFCPS_NM" IS '직위명';
COMMENT ON COLUMN "ITE"."MT_USER"."OFCPS_CD" IS '직위코드';
COMMENT ON COLUMN "ITE"."MT_USER"."USE_YN" IS '사용여부';
COMMENT ON COLUMN "ITE"."MT_USER"."CMPS_ORG_CD" IS '직급코드';
COMMENT ON COLUMN "ITE"."MT_USER"."CMPS_ORG_NM" IS '직급명';
COMMENT ON COLUMN "ITE"."MT_USER"."CLSF_CD" IS '직급코드';
COMMENT ON COLUMN "ITE"."MT_USER"."CLSF_NM" IS '직급명';
COMMENT ON TABLE "ITE"."MT_USER" IS '사용자';
--------------------------------------------------------
--  DDL for Index SYS_C0015898
--------------------------------------------------------

CREATE UNIQUE INDEX "ITE"."SYS_C0015898" ON "ITE"."MT_USER" ("USER_ID")
    PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS
    STORAGE (INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
    PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
    TABLESPACE "ITE";

--------------------------------------------------------
--  DDL for Trigger TR_IU_USER_USER
--------------------------------------------------------

-- CREATE OR REPLACE TRIGGER "ITE"."TR_IU_USER_USER"
--     BEFORE INSERT OR UPDATE
--     ON USER_USER
--     REFERENCING NEW AS New OLD AS Old
--     FOR EACH ROWDECLARE
--     DB_USER_ID varchar2 (80);
-- BEGIN
--     SELECT USER
--     INTO DB_USER_ID
--     FROM DUAL;
--
--     IF INSERTING THEN
--         :NEW.INS_ID := DB_USER_ID;
--         :NEW.INS_DTM := SYSDATE;
--     ELSIF UPDATING THEN
--         :NEW.UPD_ID := DB_USER_ID;
--         :NEW.UPD_DTM := SYSDATE;
--     END IF;
-- END;
-- /
-- ALTER TRIGGER "ITE"."TR_IU_USER_USER" ENABLE;

--------------------------------------------------------
--  Constraints for Table USER_USER
--------------------------------------------------------

ALTER TABLE "ITE"."MT_USER"
    ADD CONSTRAINT "PK_USER_USER" PRIMARY KEY ("USER_ID")
        USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS
            STORAGE (INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
            PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
            TABLESPACE "ITE" ENABLE;
