create
    tablespace ITE
    datafile 'ite.dbf'
    size 300 m reuse
    autoextend on next 1024 k
    maxsize unlimited;